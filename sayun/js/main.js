import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, BrowserRouter, Route, Link, Switch } from 'react-router-dom';
import App from '../jsx/App.jsx';

ReactDOM.render(
	<HashRouter>
	    <App />
	</HashRouter>
	, document.getElementById('root')); 