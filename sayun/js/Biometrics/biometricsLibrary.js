/**
* @param csvFilepath from file input dialog
**/
function parseCsv (csvFilePath) {
	var data = Papa.parse(new File(csvFilePath), {
	BiometricRecord bmr;
	var acNumber, name, date;
	var am = [], pm = [];
	var format = 'hh:mm:ss';
	var containFlag = [];
	var records = [];
	
	//todo
	var morningOutbound = '12:00:00';
	var afternoonOutbound = '5:00:00';
	
		complete: function(results) {
			for (var outIterator = 0; outIterator < results.length; i++) {
				for(var inIterator = 1; inIterator < results[outIterator].length; i++) {
					switch(inIterator) { 
						case 1: acNumber = results[outIterator][inIterator]; break;
						
						case 2: name = results[outIterator][inIterator]; break;
						
						case 3: date = results[outIterator][inIterator]; break;
						
						case 5: 
							if(results[outIterator][inIterator] == null) {
								results[outIterator][inIterator] = null;
							} else {
								var time = moment(results[outIterator][inIterator], format),
								var afterMorningTime = moment(morningOutbound, format);
								var afterAfternoonTime = moment(afternoonOutbound, format);
								if(time.isBefore(afterMorningTime)) {
									am.push(results[outIterator][inIterator]);
								} else if(time.isBefore(afterAfternoonTime)) {
									pm.push(results[outIterator][inIterator]);
								}
							}
								break;

						case 6: 
								if(results[outIterator][inIterator] == null) {
									results[outIterator][inIterator] = null;
								} else {
									var time = moment(results[outIterator][inIterator], format),
									var afterAfternoonTime = moment(afternoonOutbound, format);
									var afterMorningTime = moment(morningOutbound, format);
									var afterAfternoonTime = moment(afternoonOutbound, format);
									if(time.isBefore(afterMorningTime)) {
										am.push(results[outIterator][inIterator]);
									} else if(time.isBefore(afterAfternoonTime)) {
										pm.push(results[outIterator][inIterator]);
									}
								}
								break;
						}
				if(!containFlag.includes(acNumber)) {
					bmr = new BiometricRecord(acNumber, name, null, date, am, pm);
					containFlag.push(acNumber);
					records.push(bmr);
				} else {
					bmr = records[containFlag.findIndex(acNumber)];
					if(am[0] != null) { bmr.am.push(am[0]); }
					if(pm[0] != null) { bmr.pm.push(pm[0]); }
				}
			}
		}
		return records;
	});
};