class BiometricRecord{
	constructor(acNumber, name,errorCode ,date ,am[] ,pm[]) {
		this.acNumber = acNumber;
		this.name = name;
		this.errorCode = errorCode;
		this.date = date;
		this.am[] = am[];
		this.pm[] = pm[];
	}
}

class BiometricModel{
	constructor(records[]) {
		this.records = records;
	}
}
