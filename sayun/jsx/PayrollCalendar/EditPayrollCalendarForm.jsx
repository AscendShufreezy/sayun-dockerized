import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import Input from '../Utils/Input.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class EditPayrollCalendarForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            name: props.calendar.name,
            status: props.calendar.status,
            activeChecked: "",
            inactiveChecked: ""
        };

        this.handleStatus = this.handleStatus.bind(this);
        this.handleName = this.handleName.bind(this);
    }

    handleName(e) {
        this.setState({
            name: e.target.value
        });
    }

    handleStatus(e) {
        var status = e.target.value;
        var activeChecked = "";
        var inactiveChecked = "";

        if(status == "Active"){
            this.setState({activeChecked: "checked"});
            this.setState({inactiveChecked:""});
            this.setState({status:"Active"});

        } else{
            this.setState({activeChecked: ""});
            this.setState({inactiveChecked: "checked"});
            this.setState({status: "Inactive"});
        }
    }

    componentWillMount(){
        var status = this.state.status;
        var activeChecked = "";
        var inactiveChecked = "";

        if(status == "Active"){
            this.setState({activeChecked: "checked"});
            this.setState({inactiveChecked:""});
            this.setState({status:"Active"});

        } else{
            this.setState({activeChecked: ""});
            this.setState({inactiveChecked: "checked"});
            this.setState({status: "Inactive"});
        }
    }

    render(){
        var calendar = this.props.calendar;

        return (
            <div>
                <div className="row" id="page-wrapper-inner3">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="row">
                                <div className="col-md-2">
                                    Name*
                                </div>
                                <div className="col-md-6">
                                     <input type="text" className="form-control" id="name" value={this.state.name} onChange={this.handleName} />
                                </div>
                            </div>
                        </div>   
                        <div className="col-md-6">
                            <div className="row">
                                <div className="col-md-2">
                                    Status
                                </div>
                                <div className="col-md-6">
                                    <input type="radio" name="status" value="Active" id="btnActive" checked={this.state.activeChecked} onChange={this.handleStatus} /><a className="radio-text">Active</a> &nbsp;&nbsp;
                                    <input type="radio" name="status" value="Inactive" id="btnInactive" checked={this.state.inactiveChecked} onChange={this.handleStatus} /><a className="radio-text">Inactive</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
            </div>
        );
    }
}   

export default EditPayrollCalendarForm;