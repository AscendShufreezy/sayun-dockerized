import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import EditPayrollCalendarForm from './EditPayrollCalendarForm.jsx';
import PayrollListPage from '../Payroll/PayrollListPage.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';;

class PayrollCalendarDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      response: [],
      transfer: false,
      isComplete: false,
      emptyField: "",
      calendar: {},
      logout: false
   };
  }

  cancelSave() {
		this.setState({transfer: true});
  }

  checkInputs() {
  	if(document.getElementById("name").value == ""){
      this.setState({emptyField: "Name"}, () => { 
          alert(this.state.emptyField + " should not be empty.");
          return "";
      });
    } else{
  		this.setState({isComplete: true}, () => { 
  			this.updatePayrollCalendar();
			});
  	}
  }  

  updatePayrollCalendar() {
      var url = window.location.href;
      var id = url.split('=').pop();
      var calendar = this.state.calendar;

      calendar.id = id;
      calendar.name = document.getElementById('name').value;

      if (document.getElementById('btnActive').checked) {
        calendar.status = document.getElementById("btnActive").value;
      } else{
        calendar.status = document.getElementById("btnInactive").value;
      }

      var data = {"id": calendar.id, "name": calendar.name,"status": calendar.status, "payroll_calendar": calendar.description}
      var self = this;

  		$.ajax({
        url: Utils.basePath + "/payroll-calendars/" + id + "/",
        type: "PUT",
        contentType: "application/json",
        data: JSON.stringify(data),
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        statusCode: {
           400: function (response) {
              alert(JSON.parse(response.responseText));
           },
           401: function (response) {
              alert("User is not authorized. Please login.");
              self.setState({logout: true});
           },
           500: function (response) {
              alert("HTTP error 500, please check your connection.");
           }
        },
        success: function (data) {
            alert("Payroll Calendar updated!");
            self.setState({transfer: true});
        },
        error: function (response) {
            console.log(response);
        }
      });
  }

  componentWillMount(){
    var group = sessionStorage.getItem('group');
    var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

    if (group != "admin-group" && group != 'hr-group' && sessionPermission.indexOf(88) == -1) {
        alert("You are not authorized to access this page. Please login as admin user.");
        this.setState({logout: true});
    }

    var url = window.location.href;
    var id = url.split('=').pop();
    var self = this;
    var calendar = {};

    $.ajax({
        url: Utils.basePath + "/payroll-calendars/" + id + "/",
        type:"GET",
        contentType: "application/json",
        async : false,
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
        },

        success: function (data) {
            calendar.id = data.id;
            calendar.name = data.name;
            calendar.status = data.status;
            calendar.description = data.payroll_calendar;
        }
    });

    this.setState({calendar: calendar})
  }

	render(){
    if (this.state.logout) {
             return <Redirect push to="/" />;
    }

    var calendar = this.state.calendar;
		if (this.state.transfer) {
		    return <Redirect push to="/payroll" />;
		}

    return (
    	<div id="page-wrapper">
      	<Menu active="Payroll"/>
      	<div className="row" id="page-wrapper-inner">
      		<Breadcrumb page="edit-calendar"/>
      		<PageTitle pageTitle="Payroll Calendar"/>
      		<div className="row" id="page-wrapper-inner3">
      			<form>
              <br />
	      			<EditPayrollCalendarForm calendar={calendar}/>
	      			<div className="row">
			            <div className="col-md-3 pull-right">
			              <div className="pull-right">
			                <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
			                <button type="button" className= "btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
			              </div>
			            </div>
			        </div>
              <hr />
              <PayrollListPage calendar={calendar.id}/>
		        </form>
      			<br />
      		</div>
      	</div>	
        <br />  
      </div>
    );
  }
}

export default PayrollCalendarDetails;

