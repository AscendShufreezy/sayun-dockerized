import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import PayrollCalendarListTable from './PayrollCalendarListTable.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class PayrollCalendarListPage extends React.Component {
	constructor(){
	    super();
	    this.state = {
	          logout: false,
	    }
	}

	deleteCalendar(){
	  	var numberOfCalendars = document.getElementsByName("checkbox");
	  	var calendarsToDelete = [];

	  	for(var i=0; i<numberOfCalendars.length;i++){
	  		if(numberOfCalendars[i].checked){
	  			calendarsToDelete.push(numberOfCalendars[i].id);
	  		}
	  	}

	  	if(calendarsToDelete.length > 0){
	  		var deleteMessage = confirm("Are you sure you want to delete the selected payroll calendar/s?");
	  		if (deleteMessage == true) {
	  			var successCounter = 0;
	  			var failCounter = 0;
	  			for(var i=0; i<calendarsToDelete.length;i++){
	  				$.ajax({
			            url: Utils.basePath + "/payroll-calendars/" + calendarsToDelete[i] + "/",
			            type:"DELETE",
			            async: false,
			            contentType: "application/json",
			            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
			            success: function (data) {
			            	successCounter++;
			            },
			            error: function (response) {
			            	failCounter++;
			            }
			        });

			        if(successCounter == calendarsToDelete.length){
		  				alert("Deletion of payroll calendar/s is successful!");
		  				location.reload();
		  			} else if(failCounter == calendarsToDelete.length){
	                	alert("Payroll Calendars with child record cannot be deleted!");
	                } else if((successCounter + failCounter) == calendarsToDelete.length){
	                	alert("Payroll Calendars with child record cannot be deleted. Only payroll calendars with no child records are deleted.");
	                	location.reload();
	                }
	  			}
		    }
	  	} else{
	  		alert('There are no payroll calendar/s selected.');
	  	}
    }

    componentWillMount(){
    	var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

    	if (sessionPermission.indexOf(88) == -1) {
    		alert("You are not authorized to access this page. Please login as admin user.");
    		this.setState({logout: true});
    	}
    }

    render(){
    	if (this.state.logout) {
             return <Redirect push to="/" />;
 		}

 		var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");
        var hide1 = "";
        var hide2 = "";
        if(sessionPermission.indexOf(85) == -1){
        	hide1 = "hidden";
        } if(sessionPermission.indexOf(87) == -1){
        	hide2 = "hidden";
        }

      	return (
	      	<div id="page-wrapper">
		      	<Menu active="Payroll"/>
		      	<div className="row" id="page-wrapper-inner">
		      		<PageTitle pageTitle="Payroll Calendar List"/>
		      		<div className="row" id="page-wrapper-inner3">
		      			<form>
			      			<PayrollCalendarListTable />
			      			<ButtonGroup buttons="addDelete" link1="/payroll/add-payroll-calendar" click2={() => this.deleteCalendar()} type1="button" hide1={hide1} hide2={hide2}/>
		      			</form>
		      			<br />
		      		</div>
		      	</div>	
		      	<br />
		    </div>
     	);
   }
}

export default PayrollCalendarListPage;

