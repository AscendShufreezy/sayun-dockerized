import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import AddPayrollCalendarForm from './AddPayrollCalendarForm.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class AddPayrollCalendar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          response: [],
          transfer: false,
          isComplete: false,
          emptyField: "",
          calendar: {},
          logout: false,
          calendarId: 0
       };
    }

    cancelSave() {
        this.setState({transfer: true});
    }

    checkInputs() {
        if(document.getElementById("name").value == ""){
            this.setState({emptyField: "Name"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else{
            this.setState({isComplete: true}, () => { 
                var calendar = {};
                calendar.name = document.getElementById("name").value;
                this.setState({calendar: calendar}, () => this.saveCalendar()); //for async calls
            });
        }
    }

    getFormattedDate(input){
      var pattern=/(.*?)\/(.*?)\/(.*?)$/;
      var result = input.replace(pattern,function(match,p1,p2,p3){
          var months=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
          return months[(p1-1)]+"-"+(p2)+"-"+p3;
      });
      return result;
    }

    formatDate(selectedDay){
      var today = new Date(selectedDay);
      var month = today.getMonth() + 1;
      var date = today.getDate();

      if(month < 10){
         month = "0" + month;
      }if(date < 10){
         date = "0" + date;
      }

      return month + "/" + date + "/" + today.getFullYear();
    }

    formatDescription(date){
      var startDate = date;
      var lastDate = new Date(startDate.getTime() + 1000*60*60*24*364);

      return this.getFormattedDate(this.formatDate(startDate)) + " - " + this.getFormattedDate(this.formatDate(lastDate));
    }

    saveCalendar() {
        var self = this;
        var calendar = this.state.calendar;
        var description = this.formatDescription(new Date());
        var data = {"name": calendar.name,"status": "Active","payroll_calendar": ""}
                    
        $.ajax({
            url: Utils.basePath + "/payroll-calendars/",
            type:"POST",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
                alert("Payroll Calendar saved!");
                self.setState({calendarId: data.id});
                self.setState({transfer: true});
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    componentWillMount(){
      var group = sessionStorage.getItem('group');
      var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

      if (group != "admin-group" && group != 'hr-group' && sessionPermission.indexOf(85) == -1) {
          alert("You are not authorized to access this page. Please login as admin user.");
          this.setState({logout: true});
      } 
    }

    render(){
      var calendarId = this.state.calendarId;
      var link = "/payroll/calendar/id=" + calendarId;

      if (this.state.logout) {
        return <Redirect push to="/" />;
      }

      if (this.state.transfer) {
        return <Redirect push to={link} />;
      }

      return (
        <div id="page-wrapper">
          <Menu active="Payroll"/>
          <div className="row" id="page-wrapper-inner">
            <Breadcrumb page="add-calendar"/>
            <PageTitle pageTitle="Add Payroll Calendar"/>
            <div className="row" id="page-wrapper-inner3">
              <form>
                <br />
                <AddPayrollCalendarForm />
                <div className="row">
                  <div className="col-md-3 pull-right">
                    <div className="pull-right">
                      <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
                      <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
                    </div>
                  </div>
                </div>
              </form>
              <br />
            </div>
          </div>
          <br />  
        </div>
    );
  }
}

export default AddPayrollCalendar;