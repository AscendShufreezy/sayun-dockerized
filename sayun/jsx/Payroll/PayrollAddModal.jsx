import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class PayrollAddModal extends React.Component {
  constructor(props){
      super(props);

      this.state = {
         payroll: {},
         weeks: 0,
         date: moment(),
         test: [{}],
         logout: false,
         invalidField: "",
         emptyField: ""
      };

      this.handleWeeks = this.handleWeeks.bind(this);
      this.handleChange = this.handleChange.bind(this);
  }

  handleChange(date) {
    this.setState({
        date: date
    });
  }

  handleWeeks(e) {
    this.setState({
        weeks: e.target.value
    });
  }

  checkInputs(calendarId) {
        if(document.getElementById("date").value == ""){
            this.setState({emptyField: "Date"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(!moment(document.getElementById("date").value,"MM/DD/YYYY",true).isValid()){
            this.setState({invalidField: "Date"}, () => { 
                alert(this.state.invalidField + " should be a valid date.");
                return "";
            });
        } else if(document.getElementById("weeks").value == ""){
            this.setState({emptyField: "Weeks"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("weeks").value < 0){
            alert("Weeks after value should not be less than 0.");
        } else{
            this.setState({isComplete: true}, () => { 
                var payroll = {};
                var calendarId = window.location.href.split('=').pop();
                payroll.calendar = calendarId;
                payroll.date = document.getElementById("date").value;
                payroll.status = "Pending";
                this.setState({payroll: payroll}, () => this.savePayroll(calendarId)); //for async calls
            });
        }
    }

    formatDate(selectedDay){
      var today = new Date(selectedDay);
      var month = today.getMonth() + 1;
      var date = today.getDate();

      if(month < 10){
         month = "0" + month;
      }if(date < 10){
         date = "0" + date;
      }

      return month + "/" + date + "/" + today.getFullYear();
    }

    getFormattedDate(input){
      var pattern=/(.*?)\/(.*?)\/(.*?)$/;
      var result = input.replace(pattern,function(match,p1,p2,p3){
          var months=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
          return (p2)+"-"+months[(p1-1)]+"-"+p3;
      });
      return result;
    }

    test(calendarId, date, coverage){
      var test = this.state.test;
      test = {'calendar': parseInt(calendarId) ,'date': date, 'coverage': coverage, 'status': 'Pending'};
      return test;
    }

    savePayroll(calendarId){
      var formattedDate = this.formatDate(this.state.date);
      var rawDate = new Date(this.state.date);
      var pushToPayrollList = [];
      formattedDate = this.getFormattedDate(formattedDate);
      var dataList = [];

      if(this.state.weeks >= 0){  
        for(var i=0; i<= this.state.weeks; i++){
          var payroll = {};
      
          //getting first and last day of the week
          /*var firstDay = rawDate.getDate() - rawDate.getDay();
          firstDay = new Date(rawDate.setDate(firstDay));
          var lastDay = new Date(firstDay.getTime() + 1000*60*60*24*6);*/
          var lastDay = rawDate;
          var firstDay = new Date(lastDay.getTime() - 1000*60*60*24*6);

          //payroll data
          payroll.date = formattedDate;
          payroll.coverage = this.getFormattedDate(this.formatDate(firstDay)) + " to " + this.getFormattedDate(this.formatDate(lastDay));
          pushToPayrollList.push(payroll);

          var sample = this.test(calendarId, formattedDate, payroll.coverage);
          dataList.push(sample);

          //date formatting
          var mmDate = new Date(new Date(formattedDate).getTime() + 1000*60*60*24*7);
          rawDate = mmDate;
          formattedDate = this.getFormattedDate(this.formatDate(mmDate));          
        }
      }

      $.ajax({
        url: Utils.basePath + "/payroll/",
        type:"POST",
        async: false,
        contentType: "application/json",
        data: JSON.stringify(dataList),
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        statusCode: {
           400: function (response) {
              alert("Please check dates selected. Dates should be unique.");
           },
           401: function (response) {
              alert("User is not authorized. Please login.");
              self.setState({logout: true});
           },
           500: function (response) {
              alert("HTTP error 500, please check your connection.");
           }
        },
        success: function(data) {
            alert("Payroll saved!");
            location.reload();
        },
        error: function (response) {
            console.log(response);
        }
      });
   }

   render(){
      if (this.state.logout) {
               return <Redirect push to="/" />;
      }

      var calendarId = this.props.calendarId;
      return(
        <div className="modal fade modal-position" id="addPayrollModal" role="dialog">
            <div className="modal-dialog modal-md">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal">&times;</button>
                  <h4 className="modal-title">Add Payroll</h4>
                </div>
                <div className="modal-body">
                  <div className="row">
                    <div className="col-md-1 form-row-padding-top">
                      Date*
                    </div>
                    <div className="col-md-5">
                      <DatePicker selected={this.state.date} onChange={this.handleChange} id="date" className="form-control"/>
                      <i className="fa fa-calendar form-row-padding-top"></i>
                    </div>
                    <div className="col-md-2 form-row-padding-top">
                      also add
                    </div>
                    <div className="col-md-2 move-modal">
                      <input id="weeks" type="number" className="form-control" value={this.state.weeks} onChange={this.handleWeeks}/>
                    </div>
                    <div className="col-md-3 move-modal2 form-row-padding-top">
                      weeks after
                    </div>
                  </div>
                  <br />
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs(calendarId)} data-dismiss="modal">Add</button>
                  <button type="button" className="btn btn-default btn-warning" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
        </div>
      );
    }
}

export default PayrollAddModal;

