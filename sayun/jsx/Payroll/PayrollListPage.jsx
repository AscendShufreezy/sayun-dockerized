import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import PayrollListTable from './PayrollListTable.jsx';
import PayrollAddModal from './PayrollAddModal.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class PayrollListPage extends React.Component {
	constructor(){
	    super();
	    this.state = {
	          logout: false,
	    }
	}

	deletePayroll(){
	  	var numberOfPayrolls = document.getElementsByName("checkbox");
	  	var payrollsToDelete = [];

	  	for(var i=0; i<numberOfPayrolls.length;i++){
	  		if(numberOfPayrolls[i].checked){
	  			payrollsToDelete.push(numberOfPayrolls[i].id);
	  		}
	  	}

	  	if(payrollsToDelete.length > 0){
	  		var deleteMessage = confirm("Are you sure you want to delete the selected payroll/s?");
	  		if (deleteMessage == true) {
	  			var successCounter = 0;
	  			var failCounter = 0;
	  			for(var i=0; i<payrollsToDelete.length;i++){
	  				$.ajax({
			            url: Utils.basePath + "/payroll/" + payrollsToDelete[i] + "/",
			            type:"DELETE",
			            async: false,
			            contentType: "application/json",
			            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
			            success: function (data) {
			            	successCounter++;
			            },
			            error: function (response) {
			            	failCounter++;			            	
			            }
			        });

			        if(successCounter == payrollsToDelete.length){
		  				alert("Deletion of payroll/s is successful!");
		  				location.reload();
		  			} else if(failCounter == payrollsToDelete.length){
	                	alert("Processed payrolls cannot be deleted!");
	                } else if((successCounter + failCounter) == payrollsToDelete.length){
	                	alert("Processed payrolls cannot be deleted. Only selected pending payrolls are deleted.");
	                	location.reload();
	                }
	  			}
		    }
	  	} else{
	  		alert('There are no payroll/s selected.');
	  	}
    }

    render(){
    	if (this.state.logout) {
             return <Redirect push to="/" />;
		}

    	var calendar = this.props.calendar;
      	return (
	      	<div>
	      		<PageTitle pageTitle="Payroll List"/>
	      		<div className="row" id="page-wrapper-inner3">
	      			<Pagination />
	      			<div>
		      			<PayrollListTable calendar={calendar}/>
		      			<div className="row">
		                    <div className="col-md-3 pull-right">
		                        <div className="pull-right">
		                           <button type="button" className="btn btn-default btn-primary" data-toggle="modal" data-target="#addPayrollModal">Add</button>
		                           <button type="button" className="btn btn-default btn-warning" onClick={() => this.deletePayroll()}>Delete</button>
		                           <PayrollAddModal calendarId={calendar.id}/>
		                        </div>
		                    </div>
		                </div>
	                </div>
	      			<br />
	      		</div>
		      	<br />
		    </div>
     	);
   }
}

export default PayrollListPage;

