import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class BreadcrumbItem extends React.Component {
  render() {
    if(this.props.linked == "linked"){
      return (
        <li className="breadcrumb-item"><Link to={this.props.url}>{this.props.title}</Link></li>
      );    
    } else{
      return (
        <li className="breadcrumb-item active">{this.props.title}</li>
      );
    }
      
  }
}

export default BreadcrumbItem;