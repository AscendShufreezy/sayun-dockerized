import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Link, Switch, Redirect } from 'react-router-dom';
import Button from './Button.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class ButtonGroup extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          response: []
       };
    }
    
    render(){
      var hide1 = this.props.hide1;
      var hide2 = this.props.hide2;

      var btnAdd = <Link to={this.props.link1}><Button type={this.props.type1} classNames="btn btn-default btn-primary" buttonName="Add" /></Link> 
      var btnDelete = <button type="hidden" className="btn btn-default btn-warning" onClick={this.props.click2}>Delete</button>
      if(hide1 == "hidden"){
        btnAdd = "";
      } if(hide2 == "hidden"){
        btnDelete = "";
      }

      if(this.props.buttons == "addDelete"){
        return (
        	<div className="row">
            <div className="col-md-3 pull-right">
              <div className="pull-right">
                {btnAdd}
                {btnDelete}
              </div>
            </div>
          </div>
        );
      } else if(this.props.buttons == "saveCancel"){
        return (
          <div className="row">
            <div className="col-md-3 pull-right">
              <div className="pull-right">
                <button type="button" className="btn btn-default btn-primary" onClick={this.props.click1}>Save</button>
                <Button type={this.props.type2} classNames="btn btn-default btn-warning" buttonName="Cancel" onclick='window.location="add-employee.html"'/> 
              </div>
            </div>
          </div>
        );
      } else if(this.props.buttons == "import"){
        return (
          <div className="row pull-right" id="page-wrapper-inner3">
            <Button type={this.props.type1} classNames="btn btn-default btn-primary" buttonName="Import Biometrics" onClick=""/>
            <br />
          </div>
        );
       }else if(this.props.buttons == "add"){
        return (
          <div className="row">
            <div className="col-md-3 pull-right">
              <div className="pull-right">
                <Link to={this.props.link1}><Button type={this.props.type1} classNames="btn btn-default btn-primary" buttonName="Add" /></Link> 
              </div>
            </div>
          </div>
        );
      } 
   }
}

export default ButtonGroup;
