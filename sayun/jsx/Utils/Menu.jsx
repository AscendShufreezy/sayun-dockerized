import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';

class Menu extends React.Component {
   render() {
    var permission = sessionStorage.getItem('group');
    var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");
    
   	if(this.props.active == "Employee"){
      var first = <li><Link to="/attendance">Attendance</Link></li>
      var second = <li><Link to="/payroll">Payroll</Link></li>

      if (sessionPermission.indexOf(32) == -1) {
        first = "";
      } if(sessionPermission.indexOf(88) == -1) {
        second = "";
      }

   		return (
      	<div>
            <br />
            <div className="row">
                <ul className="nav nav-tabs">
                  <li className="active"><a>Employee</a></li>
                  {first}
                  {second}
                </ul>
            </div>
        </div>
      );
   	} else if(this.props.active == "Attendance"){
      var first = <li><Link to="/employee">Employee</Link></li>
      var second = <li><Link to="/payroll">Payroll</Link></li>

      if (sessionPermission.indexOf(52) == -1) {
        first = "";
      } if(sessionPermission.indexOf(88) == -1) {
        second = "";
      }

   		return (
        <div>
            <br />
            <div className="row">
              <ul className="nav nav-tabs">
                {first}
                <li className="active"><a>Attendance</a></li>
                {second}
              </ul>
            </div>
        </div>
      );
   	} else if(this.props.active == "Payroll"){
      var first = <li><Link to="/employee">Employee</Link></li>
      var second = <li><Link to="/attendance">Attendance</Link></li>

      if (sessionPermission.indexOf(52) == -1) {
        first = "";
      } if(sessionPermission.indexOf(32) == -1) {
        second = "";
      }

      return (
        <div>
            <br />
            <div className="row">
                <ul className="nav nav-tabs">
                  {first}
                  {second}
                  <li className="active"><a>Payroll</a></li>
                </ul>
            </div>
        </div>
      );
    } else if(this.props.active == "Reports"){
      return (
        <div>
            <br />
            <div className="row">
                <ul className="nav nav-tabs">
                  <li><a href="../employee/employee-list.html">Employee</a></li>
                  <li><a href="../attendance/attendance-list-regular.html">Attendance</a></li>
                  <li><a href="../payroll/payroll.html">Payroll</a></li>
                  <li className="active"><a></a></li>
                </ul>
            </div>
        </div>
      );
    } else if(this.props.active == "Regular"){
      return (
        <div>
            <br />
            <div className="row" id="page-wrapper-inner3">
              <ul className="nav nav-tabs">
                <li className="active"><a>Regular</a></li>
                <li><Link to="/attendance/overtime">Overtime</Link></li>
              </ul>
            </div>
        </div>
      );
    } else if(this.props.active == "Overtime"){
      return (
        <div>
            <br />
            <div className="row" id="page-wrapper-inner3">
              <ul className="nav nav-tabs">
                <li><Link to="/attendance">Regular</Link></li>
                <li className="active"><a>Overtime</a></li>
              </ul>
            </div>
        </div>
      );
    } else if(this.props.active == "RegularEmployee"){
      var empId = window.location.href.split('=').pop();    
      var link = "/attendances/overtime/id=" + empId;
      return (
        <div>
            <br />
            <div className="row" id="page-wrapper-inner3">
              <ul className="nav nav-tabs">
                <li className="active"><a>Regular</a></li>
                <li><Link to={link}>Overtime</Link></li>
              </ul>
            </div>
        </div>
      );
    }  else if(this.props.active == "OvertimeEmployee"){
      var empId = window.location.href.split('=').pop();    
      var link = "/attendance/id=" + empId;
      return (
        <div>
            <div className="row" id="page-wrapper-inner3">
              <ul className="nav nav-tabs">
                <li><Link to={link}>Regular</Link></li>
                <li className="active"><a>Overtime</a></li>
              </ul>
            </div>
        </div>
      );
    } else if(this.props.active == "User"){
      var first = <li><Link to="/group">Groups</Link></li>
      if (sessionPermission.indexOf(12) == -1) {
        first = "";
      }

      return (
        <div>
            <br />
            <div className="row">
                <ul className="nav nav-tabs">
                  <li className="active"><a>Users</a></li>
                  {first}
                </ul>
            </div>
        </div>
      );
    } else if(this.props.active == "Group"){
      var first = <li><Link to="/user">Users</Link></li>
      if (sessionPermission.indexOf(16) == -1) {
        first = "";
      }
      return (
        <div>
            <br />
            <div className="row">
                <ul className="nav nav-tabs">
                  {first}
                  <li className="active"><a>Groups</a></li>
                </ul>
            </div>
        </div>
      );
    } else if(this.props.active == "Category"){
        return (
          <div>
              <br />
              <div className="row">
                  <ul className="nav nav-tabs">
                    <li className="active"><a>Category</a></li>
                  </ul>
              </div>
          </div>
        );
    } else if(this.props.active == "Warehouse"){
      var first = <li><Link to="/customer/type=Truck">Customers</Link></li>
      var second = <li><Link to="/item">Items</Link></li>
      var third = <li><Link to="/request">Item Out</Link></li>
      var fourth = <li><Link to="/receipt">Item In</Link></li>
      var fifth = <li><Link to="/reports/iis/inventory">Reports</Link></li>

      if (sessionPermission.indexOf(48) == -1) {
        first = "";
      } if(sessionPermission.indexOf(64) == -1) {
        second = "";
      } if(sessionPermission.indexOf(104) == -1) {
        third = "";
      } if(sessionPermission.indexOf(100) == -1) {
        fourth = "";
      } if(sessionPermission.indexOf(125) == -1) {
        fifth = "";
      }

        return (
          <div>
            <br />
            <div className="row">
                <ul className="nav nav-tabs">
                  <li className="active"><a>Warehouse</a></li>
                  {first}
                  {second}
                  {third}
                  {fourth}
                  {fifth}
                </ul>
            </div>
          </div>
        );
    } else if(this.props.active == "Customer"){
      var first = <li><Link to="/warehouse">Warehouse</Link></li>
      var second = <li><Link to="/item">Items</Link></li>
      var third = <li><Link to="/request">Item Out</Link></li>
      var fourth = <li><Link to="/receipt">Item In</Link></li>
      var fifth = <li><Link to="/reports/iis/inventory">Reports</Link></li>

      if (sessionPermission.indexOf(124) == -1) {
        first = "";
      } if(sessionPermission.indexOf(64) == -1) {
        second = "";
      } if(sessionPermission.indexOf(104) == -1) {
        third = "";
      } if(sessionPermission.indexOf(100) == -1) {
        fourth = "";
      } if(sessionPermission.indexOf(125) == -1) {
        fifth = "";
      }

        return (
          <div>
            <br />
            <div className="row">
                <ul className="nav nav-tabs">
                  {first}
                  <li className="active"><a>Customers</a></li>
                  {second}
                  {third}
                  {fourth}
                  {fifth}
                </ul>
            </div>
          </div>
        );
    } else if(this.props.active == "Item"){
      var first = <li><Link to="/warehouse">Warehouse</Link></li>
      var second = <li><Link to="/customer/type=Truck">Customers</Link></li>
      var third = <li><Link to="/request">Item Out</Link></li>
      var fourth = <li><Link to="/receipt">Item In</Link></li>
      var fifth = <li><Link to="/reports/iis/inventory">Reports</Link></li>

      if (sessionPermission.indexOf(124) == -1) {
        first = "";
      } if(sessionPermission.indexOf(48) == -1) {
        second = "";
      } if(sessionPermission.indexOf(104) == -1) {
        third = "";
      } if(sessionPermission.indexOf(100) == -1) {
        fourth = "";
      } if(sessionPermission.indexOf(125) == -1) {
        fifth = "";
      }

        return (
          <div>
            <br />
            <div className="row">
                <ul className="nav nav-tabs">
                  {first}
                  {second}
                  <li className="active"><a>Items</a></li>
                  {third}
                  {fourth}
                  {fifth}
                </ul>
            </div>
          </div>
        );
    } else if(this.props.active == "Receipt"){
        var warehouse = <li><Link to="/warehouse">Warehouse</Link></li>;
        var customer = <li><Link to="/customer/type=Truck">Customers</Link></li>;
        var item = <li><Link to="/item">Items</Link></li>;
        var request = <li><Link to="/request">Item Out</Link></li>
        var reports = <li><Link to="/reports/iis/inventory">Reports</Link></li>;

        if(permission == "pos-group"){
          warehouse = "";
          customer = "";
          item = "";
          reports = "";

          if (sessionPermission.indexOf(124) != -1) {
            warehouse = <li><Link to="/warehouse">Warehouse</Link></li>
          } if(sessionPermission.indexOf(48) != -1) {
            customer = <li><Link to="/customer/type=Truck">Customers</Link></li>
          } if(sessionPermission.indexOf(64) != -1) {
            item = <li><Link to="/item">Items</Link></li>
          } if(sessionPermission.indexOf(125) != -1) {
            reports = <li><Link to="/reports/iis/inventory">Reports</Link></li>
          }
        } 

        if (sessionPermission.indexOf(124) == -1) {
          warehouse = "";
        } if(sessionPermission.indexOf(48) == -1) {
          customer = "";
        } if(sessionPermission.indexOf(64) == -1) {
          item = "";
        } if(sessionPermission.indexOf(104) == -1) {
          request = "";
        } if(sessionPermission.indexOf(125) == -1) {
          reports = "";
        }

        return (
          <div>
            <br />
            <div className="row">
                <ul className="nav nav-tabs">
                  {warehouse}
                  {customer}
                  {item}
                  {request}
                  <li className="active"><a>Item In</a></li>
                  {reports}
                </ul>
            </div>
          </div>
        );
    } else if(this.props.active == "Request"){
        console.log('here2');
        var warehouse = <li><Link to="/warehouse">Warehouse</Link></li>;
        var customer = <li><Link to="/customer/type=Truck">Customers</Link></li>;
        var item = <li><Link to="/item">Items</Link></li>;
        var receipt = <li><Link to="/receipt">Item In</Link></li>;
        var reports = <li><Link to="/reports/iis/inventory">Reports</Link></li>;

        if(permission == "warehouseapprover-group" || permission == "cashier-group"){
          warehouse = "";
          customer = "";
          item = "";
          receipt = "";
          reports = "";

          if (sessionPermission.indexOf(124) != -1) {
            warehouse = <li><Link to="/warehouse">Warehouse</Link></li>
          } if(sessionPermission.indexOf(48) != -1) {
            customer = <li><Link to="/customer/type=Truck">Customers</Link></li>
          } if(sessionPermission.indexOf(64) != -1) {
            item = <li><Link to="/item">Items</Link></li>
          } if(sessionPermission.indexOf(100) != -1) {
            receipt = <li><Link to="/receipt">Item In</Link></li>
          } if(sessionPermission.indexOf(125) != -1) {
            reports = <li><Link to="/reports/iis/inventory">Reports</Link></li>
          }
        } else if(permission == "pos-group"){
          warehouse = "";
          customer = "";
          item = "";
          reports = "";

          if (sessionPermission.indexOf(124) != -1) {
            warehouse = <li><Link to="/warehouse">Warehouse</Link></li>
          } if(sessionPermission.indexOf(48) != -1) {
            customer = <li><Link to="/customer/type=Truck">Customers</Link></li>
          } if(sessionPermission.indexOf(64) != -1) {
            item = <li><Link to="/item">Items</Link></li>
          } if(sessionPermission.indexOf(125) != -1) {
            reports = <li><Link to="/reports/iis/inventory">Reports</Link></li>
          }
        }

        if (sessionPermission.indexOf(124) == -1) {
          warehouse = "";
        } if(sessionPermission.indexOf(48) == -1) {
          customer = "";
        } if(sessionPermission.indexOf(64) == -1) {
          item = "";
        } if(sessionPermission.indexOf(100) == -1) {
          receipt = "";
        } if(sessionPermission.indexOf(125) == -1) {
          reports = "";
        }

        return (
          <div>
            <br />
            <div className="row">
                <ul className="nav nav-tabs">
                  {warehouse}
                  {customer}
                  {item}
                  <li className="active"><a>Item Out</a></li>
                  {receipt}
                  {reports}
                </ul>
            </div>
          </div>
        );
    } else if(this.props.active == "ReportsIis"){
      var warehouse = <li><Link to="/warehouse">Warehouse</Link></li>;
      var customer = <li><Link to="/customer/type=Truck">Customers</Link></li>;
      var item = <li><Link to="/item">Items</Link></li>;
      var receipt = <li><Link to="/receipt">Item In</Link></li>;
      var request = <li><Link to="/request">Item Out</Link></li>

      if (sessionPermission.indexOf(124) == -1) {
        warehouse = "";
      } if(sessionPermission.indexOf(48) == -1) {
        customer = "";
      } if(sessionPermission.indexOf(64) == -1) {
        item = "";
      } if(sessionPermission.indexOf(100) == -1) {
        receipt = "";
      } if(sessionPermission.indexOf(104) == -1) {
        request = "";
      }

      return (
        <div>
          <br />
          <div className="row">
              <ul className="nav nav-tabs">
                {warehouse}
                {customer}
                {item}
                {request}
                {receipt}
                <li className="active"><a>Reports</a></li>
              </ul>
          </div>
        </div>
      );
    } else if(this.props.active == "Deductions"){
        var empId = window.location.href.split('=').pop();    
        var link = "/employee/details/additions/id=" + empId;
        return (
          <div>
            <br />
            <div className="row">
                <ul className="nav nav-tabs">
                  <li className="active"><a>Deductions</a></li>
                  <li><Link to={link}>Additions</Link></li>
                </ul>
            </div>
          </div>
        );
    } else if(this.props.active == "Additions"){
        var empId = window.location.href.split('=').pop();    
        var link = "/employee/details/id=" + empId;
        return (
          <div>
            <br />
            <div className="row">
                <ul className="nav nav-tabs">
                  <li><Link to={link}>Deductions</Link></li>                
                  <li className="active"><a>Additions</a></li>
                </ul>
            </div>
          </div>
        );
    } else if(this.props.active == "Inventory"){
        return (
          <div>
            <br />
            <div className="row">
                <ul className="nav nav-tabs">
                  <li className="active"><a>Inventory Balance</a></li>
                  <li><Link to="/reports/iis/summary">Item In Summary</Link></li>                
                  <li><Link to="/reports/iis/requests">Item Out Summary</Link></li>                
                </ul>
            </div>
          </div>
        );
    } else if(this.props.active == "Summary"){
        return (
          <div>
            <br />
            <div className="row">
                <ul className="nav nav-tabs">
                  <li><Link to="/reports/iis/inventory">Inventory Balance</Link></li>                
                  <li className="active"><a>Item In Summary</a></li>
                  <li><Link to="/reports/iis/requests">Item Out Summary</Link></li>                
                </ul>
            </div>
          </div>
        );
    } else if(this.props.active == "CustRequest"){
        return (
          <div>
            <br />
            <div className="row">
                <ul className="nav nav-tabs">
                  <li><Link to="/reports/iis/inventory">Inventory Balance</Link></li>  
                  <li><Link to="/reports/iis/summary">Item In Summary</Link></li>                
                  <li className="active"><a>Item Out Summary</a></li>
                </ul>
            </div>
          </div>
        );
    }
     
  }
}

export default Menu;

