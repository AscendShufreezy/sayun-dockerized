import React from 'react';
import ReactDOM from 'react-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class Input extends React.Component {
   render() {
   		var disable = this.props.disable;
   		if(disable != "Active"){
   			return (
		       <input type={this.props.type} className="form-control" id={this.props.id} value={this.props.defaultValue} onChange={this.props.onChange} placeholder={this.props.placeholder}/>
		    );
   		} else{
   			return (
		       <input type={this.props.type} className="form-control" id={this.props.id} value={this.props.defaultValue} placeholder={this.props.placeholder} disabled />
		    );
   		}
	    
   }
}

export default Input;

