import React from 'react';
import ReactDOM from 'react-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class Button extends React.Component {
   render() {
      return (
        <button type={this.props.type1} className={this.props.classNames} onClick={() => {this.props.click}}>{this.props.buttonName}</button>
      );
   }
}



export default Button;