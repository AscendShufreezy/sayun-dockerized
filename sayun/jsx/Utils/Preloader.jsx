import React from 'react';
import ReactDOM from 'react-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class Preloader extends React.Component {
   render() {
      return (
      	<div className="preloader-bg">
	      	<div className="preloader">
		       	<div className="loading-img">
			        <i className="fa fa-spinner fa-spin"></i>
			    </div>
			    <div className="loading-msg">
			        <div className="processing"><b>PROCESSING.</b> Please wait...</div>
			    </div>
		    </div>
		</div>
      );
   }
}
export default Preloader;