import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';
import moment from 'moment';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class PrintReceipt extends React.Component {
  render(){
    return (
      loadFile("invoiceTemplate.docx", function(err, content) {
        var zip = new JSZip(content);
        var doc = new Docxtemplater();
        doc.loadZip(zip);

        var url = window.location.href;
        var test = url.split('=').pop();
        test = JSON.parse(test);

        doc.setData({
          restaurantName: test.restaurantName,
          restaurantLocation: test.restaurantLocation,
          cashierName: test.cashierName,
          invoiceNumber: test.invoiceNumber,
          invoiceDate: test.invoiceDate,
          dueDate: test.dueDate,
          employeeId: test.employeeId,
          "itemsBought": JSON.parse(test.itemsBought),
          totalPrice: (test.totalPrice).toFixed(2),
          totalTax: "0.00",
          totalGST: (test.totalGST).toFixed(2),
          totalQuantity: test.totalQuantity
        })
        doc.render();
        var output = doc.getZip().generate({type:"blob"});
        saveAs(output, "output.docx");
      })
    );
  }
}

export default PrintReceipt;