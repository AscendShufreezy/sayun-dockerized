import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';;

class DateWeekRange extends React.Component {
	constructor(){
        super();

        this.state = {
            date: moment(),
            firstday: moment(),
            lastday: moment(),
            concatFirstStart: ""
        };
    }

    arrowLeft(date){
        alert(this.state.concatFirstStart);

    }

    arrowRight(date){
        alert(this.state.concatFirstStart);
    }

    formatDate(first, last){
        var firstFormat = first.getMonth() + "/" + first.getDate() + "/" + first.getFullYear();
        var lastFormat = last.getMonth() + "/" + last.getDate() + "/" + last.getFullYear();
      
        var concat = "       " + firstFormat + " - " + lastFormat;
        this.setState({concatFirstStart: concat});  
    }

    componentWillMount(){
        var today = new Date();
        var first = today.getDate() - today.getDay();
        var last = first + 6;

        first = new Date(today.setDate(first));  
        last = new Date(today.setDate(last));  

        this.formatDate(first, last);
    }

    render() {
        return (
            <div>
                <br />
                <div className="row">
                    <div className="col-md-12">
                        <div className="pull-right">
                            <div className="col-md-1">
                                <i className="fa fa-arrow-left weekrange-arrow-left" onClick={() => this.arrowLeft()}></i>
                            </div>
                            <div className="col-md-9">
                                <input type="text" className="form-control" value={this.state.concatFirstStart} disabled/>
                            </div>
                            <div className="col-md-1">
                                <i className="fa fa-arrow-right weekrange-arrow-right" onClick={() => this.arrowRight()}></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
   }
}

export default DateWeekRange;