import React from 'react';
import ReactDOM from 'react-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class Footer extends React.Component {
   render() {
      return (
      	<div className="Footer">
	        <footer className="footer" style={{height: 50}}>
	        	<div className="container">
	          		<p className="text-muted" />
	        	</div>
	        </footer>
	    </div>
      );
   }
}

export default Footer;