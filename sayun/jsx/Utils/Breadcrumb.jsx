import React from 'react';
import ReactDOM from 'react-dom';

import BreadcrumbItem from './BreadcrumbItem.jsx'

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class Breadcrumb extends React.Component {
   	render() {
	   	if(this.props.page == "add-employee"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Employee List&nbsp;" url="/employee" />
		            	<BreadcrumbItem title="Add Employee" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "edit-employee"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Employee List&nbsp;" url="/employee" />
		            	<BreadcrumbItem title="Employee" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "attendance-employee"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Attendance by Date&nbsp;" url="/attendance" />
		            	<BreadcrumbItem title="Attendance by Employee" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "add-user"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="User List&nbsp;" url="/user" />
		            	<BreadcrumbItem title="Add User" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "edit-user"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="User List&nbsp;" url="/user" />
		            	<BreadcrumbItem title="User" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "add-group"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Group List&nbsp;" url="/group" />
		            	<BreadcrumbItem title="Add Group" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "edit-group"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Group List&nbsp;" url="/group" />
		            	<BreadcrumbItem title="Group" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "add-category"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Category List&nbsp;" url="/category" />
		            	<BreadcrumbItem title="Add Category" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "edit-category"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Category List&nbsp;" url="/category" />
		            	<BreadcrumbItem title="Category" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "add-warehouse"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Warehouse List&nbsp;" url="/warehouse" />
		            	<BreadcrumbItem title="Add Warehouse" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "edit-warehouse"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Warehouse List&nbsp;" url="/warehouse" />
		            	<BreadcrumbItem title="Warehouse" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "add-customer"){
	   		var type = this.props.type;
	   		var link = "/customer/type=" + type;
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Customers List&nbsp;" url={link} />
		            	<BreadcrumbItem title="Add Customer" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "edit-customer"){
	   		var type = this.props.type;
	   		var link = "/customer/type=" + type;
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Customers List&nbsp;" url={link} />
		            	<BreadcrumbItem title="Customer" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "add-item"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Items List&nbsp;" url="/item" />
		            	<BreadcrumbItem title="Add Item" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "edit-item"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Items List&nbsp;" url="/item" />
		            	<BreadcrumbItem title="Item" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "add-receipt"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Item In List&nbsp;" url="/receipt" />
		            	<BreadcrumbItem title="Add Item In" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "edit-receipt"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Item In List&nbsp;" url="/receipt" />
		            	<BreadcrumbItem title="Item In" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "add-request"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Item Out List&nbsp;" url="/request" />
		            	<BreadcrumbItem title="Add Item Out" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "edit-request"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Item Out List&nbsp;" url="/request" />
		            	<BreadcrumbItem title="Item Out" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "add-tempdeduction"){
	   		var link = "/employee/details/id=" + this.props.id;
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Employee List&nbsp;" url="/employee" />
		            	<BreadcrumbItem linked="linked" title="Employee&nbsp;" url={link} />
		            	<BreadcrumbItem title="Add Temporary Deduction" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "edit-tempdeduction"){
	   		var link = "/employee/details/id=" + this.props.id;
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Employee List&nbsp;" url="/employee" />
		            	<BreadcrumbItem linked="linked" title="Employee&nbsp;" url={link} />
		            	<BreadcrumbItem title="Temporary Deduction" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "add-fixeddeduction"){
	   		var link = "/employee/details/id=" + this.props.id;
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Employee List&nbsp;" url="/employee" />
		            	<BreadcrumbItem linked="linked" title="Employee&nbsp;" url={link} />
		            	<BreadcrumbItem title="Add Fixed Deduction" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "edit-fixeddeduction"){
	   		var link = "/employee/details/id=" + this.props.id;
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Employee List&nbsp;" url="/employee" />
		            	<BreadcrumbItem linked="linked" title="Employee&nbsp;" url={link} />
		            	<BreadcrumbItem title="Fixed Deduction" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "add-tempaddition"){
	   		var link = "/employee/details/additions/id=" + this.props.id;
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Employee List&nbsp;" url="/employee" />
		            	<BreadcrumbItem linked="linked" title="Employee&nbsp;" url={link} />
		            	<BreadcrumbItem title="Add Temporary Addition" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "edit-tempaddition"){
	   		var link = "/employee/details/additions/id=" + this.props.id;
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Employee List&nbsp;" url="/employee" />
		            	<BreadcrumbItem linked="linked" title="Employee&nbsp;" url={link} />
		            	<BreadcrumbItem title="Temporary Addition" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "add-fixedaddition"){
	   		var link = "/employee/details/additions/id=" + this.props.id;
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Employee List&nbsp;" url="/employee" />
		            	<BreadcrumbItem linked="linked" title="Employee&nbsp;" url={link} />
		            	<BreadcrumbItem title="Add Fixed Addition" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "edit-fixedaddition"){
	   		var link = "/employee/details/additions/id=" + this.props.id;
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Employee List&nbsp;" url="/employee" />
		            	<BreadcrumbItem linked="linked" title="Employee&nbsp;" url={link} />
		            	<BreadcrumbItem title="Fixed Addition" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "add-calendar"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Payroll Calendar List&nbsp;" url="/payroll" />
		            	<BreadcrumbItem title="Add Payroll Calendar" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "edit-calendar"){
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Payroll Calendar List&nbsp;" url="/payroll" />
		            	<BreadcrumbItem title="Payroll Calendar" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "edit-payrollemployee"){
	   		var link = "/payroll/calendar/id=" + this.props.id;
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Payroll Calendar List&nbsp;" url="/payroll" />
		      			<BreadcrumbItem linked="linked" title="Payroll Calendar &nbsp;" url={link} />
		            	<BreadcrumbItem title="Payroll of Employees" />
		            </ol>
		        </div>
		    );
	   	} else if(this.props.page == "payrollcalculation"){
	   		var calendarLink = "/payroll/calendar/id=" + this.props.calendarId;
	   		var payrollEmpLink = "/payroll/id=" + this.props.payrollId;
	   		return (
		        <div>
		            <ol className="breadcrumb">
		            	<BreadcrumbItem linked="linked" title="Payroll Calendar List&nbsp;" url="/payroll" />
		      			<BreadcrumbItem linked="linked" title="Payroll Calendar &nbsp;" url={calendarLink} />
		      			<BreadcrumbItem linked="linked" title="Payroll of Employees &nbsp;" url={payrollEmpLink} />
		            	<BreadcrumbItem title="Employee Calculation" />
		            </ol>
		        </div>
		    );
	   	}
   }
}


export default Breadcrumb;