import React from 'react';
import ReactDOM from 'react-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class Pagination extends React.Component {
   render() {
      return (
      	<ul className="pagination pagination-sm pagination-spacing pull-right">
            <li>Page</li>
            <li><a href="#"> 1</a>,</li>
            <li><a href="#"> 2</a>,</li>
            <li><a href="#"> 3</a>,</li>
            <li><a href="#"> 4</a></li>
        </ul>
      );
   }
}

export default Pagination;

