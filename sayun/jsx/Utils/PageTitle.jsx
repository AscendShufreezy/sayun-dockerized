import React from 'react';
import ReactDOM from 'react-dom';

class PageTitle extends React.Component {
   render() {
      return (
      	<div>
            <h3>{this.props.pageTitle}</h3>
        </div>
      );
   }
}

export default PageTitle;

