import React from 'react';
import ReactDOM from 'react-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class Alert extends React.Component {
    render() {
	   	var icon = "";

	   	if(this.props.icon == "success"){
	   		icon = <i className="fa fa-check"></i>
	   	} else(this.props.icon == "error"){
	   		icon = <i className="fa fa-warning"></i>
	   	}

	     return (
	      	<div className="preloader-bg">
		      	<div className="preloader">
			       	<div className="loading-img">
				        {icon}
				    </div>
				    <div className="loading-msg">
				        <div className="processing"><b>{this.props.header}</b> {this.props.msg}</div>
				    </div>
			    </div>
			</div>
	    );
    }
}
export default Alert;