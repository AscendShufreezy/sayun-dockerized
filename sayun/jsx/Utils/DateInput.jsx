import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class DateInput extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
         date: props.value
      };
   }

    arrowLeft(){
      var date = this.props.value;
      date = new Date(date).getTime();
      date = date - 1000*60*60*24;
      date = new Date(date);
      alert(date);
    }

    arrowRight(){
      var date = this.props.value;
      date = new Date(date).getTime();
      date = date + 1000*60*60*24;
      date = new Date(date);
      alert(date);
    }

    render() {
      return (
        <div className="row" id="page-wrapper-inner4">
            <div className="col-md-1 form-row-padding-top">
                Date
            </div>
             <div className="col-md-3">
                <i className="fa fa-arrow-left datepicker-arrow-left" onClick={this.props.arrowLeft}/*onClick={() => this.arrowLeft()}*/></i>
                <DatePicker selected={this.props.selected} value={this.state.date} onChange={this.props.onChange} id={this.props.id} className="form-control"/>
                <i className="fa fa-calendar"></i>
                <i className="fa fa-arrow-right datepicker-arrow-right" onClick={this.props.arrowRight}></i>
            </div>
        </div>
      );
   }
}

export default DateInput;