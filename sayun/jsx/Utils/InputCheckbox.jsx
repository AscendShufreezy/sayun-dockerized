import React from 'react';
import ReactDOM from 'react-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class InputCheckbox extends React.Component {
    constructor() {
        super();

        this.state = {
          checked: false
       };
    }

    render(){
        return (
            <input name={this.props.name} checked={this.props.checked} type='checkbox' id={this.props.id}
                   {...this.props}/>
        );
    }
}

export default InputCheckbox;

