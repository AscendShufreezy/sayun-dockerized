import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Switch, Route, Link, Redirect} from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class Header extends React.Component {
	constructor(props) {
        super(props);

        this.state = {
        	response: [],
        	transfer: false,
        	changePassword: false
        };
    }

    changePassword(){
    	this.setState({changePassword: true});
    }

	logout(){
		sessionStorage.removeItem('token');
		sessionStorage.removeItem('username');
		sessionStorage.removeItem('name');
    	sessionStorage.removeItem('group');
    	sessionStorage.removeItem('id');
    	sessionStorage.removeItem('permissions');
    	this.setState({transfer: true});
	}

    render() {
	   	var permission = sessionStorage.getItem('group');
	   	var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");
   		var accountLink = "";
    	var categoryLink = "";
    	var divider = "";
    	var changePassLink = <li><a className="nav-left-item-padding nav-right-item-padding color-font-white color-white-hover" onClick={() => this.changePassword()}> Change Password</a></li>;

    	if(permission == "admin-group"){
    		divider = "|";
    		accountLink = <Link to='/user'> Account Management</Link>;
    		categoryLink = <Link to='/category'> System Management</Link>;
    		changePassLink = <li><a className="nav-left-item-padding nav-right-item-padding color-font-white color-white-hover" onClick={() => this.changePassword()}> Change Password</a></li>;
    	}

    	if (this.state.transfer) {
		    return <Redirect push to="/" />;
		}

		if (this.state.changePassword) {
		    return <Redirect push to="/change-password" />;
		}

	   	if(this.props.active == "Login"){
		    return (
		        <nav className="navbar navbar-default navbar-static-top " role="navigation" style={{marginBottom: 0}}>
				    <div className="row color-blue">
				        <div className="navbar-header">
				            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					            <span className="sr-only">Toggle navigation</span>
					            <span className="icon-bar" />
					            <span className="icon-bar" />
					            <span className="icon-bar" />
				            </button>
				        	<a className="navbar-brand header-move" style={{color: 'white'}}>Sayun Solutions HRIS / IIS</a>
				    	</div> 
					</div>
				</nav>
	      	);
	    } else if(this.props.active == "HRIS"){
	    	var hrisLink = "Human Resource Information System ";
    		var divider2 = "| ";

	    	if(permission == "warehouse-group" || permission == "warehouseapprover-group" || permission == "pos-group" || permission == "cashier-group"){
	    		hrisLink = "";
	    		divider2 = "";
	    	}

	    	var divider1 = "";
	    	var divider3 = "";
	    	var iis = "";
	    	var account = "";
	    	var system = "";

	    	if(sessionPermission.indexOf(124) != -1){
	    	 	divider1 = "| ";
	    	 	iis = <Link to='/warehouse'> Inventory Information System</Link>
	    	 } else if(sessionPermission.indexOf(48) != -1){
	    	 	divider1 = "| ";
	    	 	iis = <Link to='/customer/type=Truck'> Inventory Information System</Link>
	    	 } else if(sessionPermission.indexOf(64) != -1){
	    	 	divider1 = "| ";
	    	 	iis = <Link to='/item'> Inventory Information System</Link>
	    	 } else if(sessionPermission.indexOf(104) != -1){
	    	 	divider1 = "| ";
	    	 	iis = <Link to='/request'> Inventory Information System</Link>
	    	 } else if(sessionPermission.indexOf(100) != -1){
	    	 	divider1 = "| ";
	    	 	iis = <Link to='/receipt'> Inventory Information System</Link>
	    	 } else if(sessionPermission.indexOf(125) != -1){
	    	 	divider1 = "| ";
	    	 	iis = <Link to='/reports/iis/inventory'> Inventory Information System</Link>
	    	 }

	    	 if(sessionPermission.indexOf(16) != -1){
	    	 	account = <Link to='/user'> Account Management</Link>
	    	 } else if(sessionPermission.indexOf(12) != -1){
	    	 	account = <Link to='/group'> Account Management</Link>
	    	 }

	    	 if(sessionPermission.indexOf(44) != -1){
	    	 	divider3 = "| ";
	    	 	system = <Link to='/category'> System Management</Link>
	    	 }

		    return (
		        <nav className="navbar navbar-default navbar-static-top " role="navigation" style={{marginBottom: 0}} id="page-wrapper-top">
				    <div className="row color-blue test">
				        <div className="navbar-header">
				            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				              <span className="sr-only">Toggle navigation</span>
				              <span className="icon-bar" />
				              <span className="icon-bar" />
				              <span className="icon-bar" />
				            </button>
				            <a className="navbar-brand header-move color-font-white" style={{color: 'white'}}>Sayun Solutions HRIS / IIS</a>
				        </div>
				        <ul className="nav navbar-top-links navbar-right color-font-white">
				            <li>Welcome {sessionStorage.getItem('name')}!</li> | 
				            {changePassLink} {divider} 
				            <li><a className="nav-right-item-padding nav-left-item-padding color-font-white color-white-hover" onClick={() => this.logout()}> Logout</a></li>
				        </ul>   
				    </div>
				    <div className="row header-move">
				        {hrisLink} {divider2}
				        {iis} {divider1}
				        {account} {divider3}
				        {system}
				    </div> 
				 </nav>
		    );
	    } else if(this.props.active == "IIS"){
	    	var hrisLink = <Link to='/employee'> Human Resource Information System</Link>;
    		var divider2 = "| ";

	    	if(permission == "warehouse-group" || permission == "warehouseapprover-group" || permission == "pos-group" || permission == "cashier-group"){
	    		hrisLink = "";
	    		divider2 = "";
	    	}

    		var divider = "| ";
    		var divider1 = "";
	    	var divider3 = "";
	    	var account = "";
	    	var system = "";

	   		 if(sessionPermission.indexOf(52) != -1){
	    	 	divider2 = "| ";
	    	 	hrisLink = <Link to='/employee'> Human Resource Information System</Link>
	    	 } else if(sessionPermission.indexOf(32) != -1){
	    	 	divider2 = "| ";
	    	 	hrisLink = <Link to='/attendance'> Human Resource Information System</Link>
	    	 } else if(sessionPermission.indexOf(88) != -1){
	    	 	divider2 = "| ";
	    	 	hrisLink = <Link to='/payroll'> Human Resource Information System</Link>
	    	 } 

	    	 if(sessionPermission.indexOf(16) != -1){
	    	 	account = <Link to='/user'> Account Management</Link>
	    	 } else if(sessionPermission.indexOf(12) != -1){
	    	 	account = <Link to='/group'> Account Management</Link>
	    	 }

	    	 if(sessionPermission.indexOf(44) != -1){
	    	 	divider3 = "| ";
	    	 	system = <Link to='/category'> System Management</Link>
	    	 }
	    
		    return (
		        <nav className="navbar navbar-default navbar-static-top " role="navigation" style={{marginBottom: 0}} id="page-wrapper-top">
				    <div className="row color-blue test">
				        <div className="navbar-header">
				            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				              <span className="sr-only">Toggle navigation</span>
				              <span className="icon-bar" />
				              <span className="icon-bar" />
				              <span className="icon-bar" />
				            </button>
				            <a className="navbar-brand header-move color-font-white" style={{color: 'white'}}>Sayun Solutions HRIS / IIS</a>
				        </div>
				        <ul className="nav navbar-top-links navbar-right color-font-white">
				            <li>Welcome {sessionStorage.getItem('name')}!</li> | 
				            {changePassLink} {divider} 
				            <li><a className="nav-right-item-padding nav-left-item-padding color-font-white color-white-hover" onClick={() => this.logout()}> Logout</a></li>
				        </ul>   
				    </div>
				    <div className="row header-move">
				        {hrisLink} {divider2}
				        Inventory Information System {divider}
				        {account} {divider3}
				        {system}
				    </div> 
				 </nav>
		    );
	    } else if(this.props.active == "Account"){
	    	var divider1 = "";
	    	var divider2 = "";
	    	var divider3 = "";
	    	var hris = "";
	    	var iis = "";
	    	var system = "";
	    	 if(sessionPermission.indexOf(52) != -1){
	    	 	divider1 = "| ";
	    	 	hris = <Link to='/employee'> Human Resource Information System</Link>
	    	 } else if(sessionPermission.indexOf(32) != -1){
	    	 	divider1 = "| ";
	    	 	hris = <Link to='/attendance'> Human Resource Information System</Link>
	    	 } else if(sessionPermission.indexOf(88) != -1){
	    	 	divider1 = "| ";
	    	 	hris = <Link to='/payroll'> Human Resource Information System</Link>
	    	 } 

	    	 if(sessionPermission.indexOf(124) != -1){
	    	 	divider2 = "| ";
	    	 	iis = <Link to='/warehouse'> Inventory Information System</Link>
	    	 } else if(sessionPermission.indexOf(48) != -1){
	    	 	divider2 = "| ";
	    	 	iis = <Link to='/customer/type=Truck'> Inventory Information System</Link>
	    	 } else if(sessionPermission.indexOf(64) != -1){
	    	 	divider2 = "| ";
	    	 	iis = <Link to='/item'> Inventory Information System</Link>
	    	 } else if(sessionPermission.indexOf(104) != -1){
	    	 	divider2 = "| ";
	    	 	iis = <Link to='/request'> Inventory Information System</Link>
	    	 } else if(sessionPermission.indexOf(100) != -1){
	    	 	divider2 = "| ";
	    	 	iis = <Link to='/receipt'> Inventory Information System</Link>
	    	 } else if(sessionPermission.indexOf(125) != -1){
	    	 	divider2 = "| ";
	    	 	iis = <Link to='/reports/iis/inventory'> Inventory Information System</Link>
	    	 }

			if(sessionPermission.indexOf(44) != -1){
	    	 	divider3 = "| ";
	    	 	system = <Link to='/category'> System Management</Link>
	    	 }
	    	
		    return (
		        <nav className="navbar navbar-default navbar-static-top " role="navigation" style={{marginBottom: 0}} id="page-wrapper-top">
				    <div className="row color-blue test">
				        <div className="navbar-header">
				            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				              <span className="sr-only">Toggle navigation</span>
				              <span className="icon-bar" />
				              <span className="icon-bar" />
				              <span className="icon-bar" />
				            </button>
				            <a className="navbar-brand header-move color-font-white" style={{color: 'white'}}>Sayun Solutions HRIS / IIS</a>
				        </div>
				        <ul className="nav navbar-top-links navbar-right color-font-white">
				            <li>Welcome {sessionStorage.getItem('name')}!</li> | 
				            <li><a className="nav-left-item-padding nav-right-item-padding color-font-white color-white-hover" onClick={() => this.changePassword()}> Change Password</a></li> | 
				            <li><a className="nav-right-item-padding nav-left-item-padding color-font-white color-white-hover" onClick={() => this.logout()}> Logout</a></li>
				        </ul>   
				    </div>
				    <div className="row header-move">
				        {hris} {divider1}
				        {iis} {divider2}
				        Account Management {divider3}
				        {system}
				    </div> 
				 </nav>
		    );
	    } else if(this.props.active == "System"){
	    	var divider1 = "";
	    	var divider2 = "";
	    	var divider3 = "| ";
	    	var hris = "";
	    	var iis = "";
	    	var account = "";
	    	 if(sessionPermission.indexOf(52) != -1){
	    	 	divider1 = "| ";
	    	 	hris = <Link to='/employee'> Human Resource Information System</Link>
	    	 } else if(sessionPermission.indexOf(32) != -1){
	    	 	divider1 = "| ";
	    	 	hris = <Link to='/attendance'> Human Resource Information System</Link>
	    	 } else if(sessionPermission.indexOf(88) != -1){
	    	 	divider1 = "| ";
	    	 	hris = <Link to='/payroll'> Human Resource Information System</Link>
	    	 } 

	    	 if(sessionPermission.indexOf(124) != -1){
	    	 	divider2 = "| ";
	    	 	iis = <Link to='/warehouse'> Inventory Information System</Link>
	    	 } else if(sessionPermission.indexOf(48) != -1){
	    	 	divider2 = "| ";
	    	 	iis = <Link to='/customer/type=Truck'> Inventory Information System</Link>
	    	 } else if(sessionPermission.indexOf(64) != -1){
	    	 	divider2 = "| ";
	    	 	iis = <Link to='/item'> Inventory Information System</Link>
	    	 } else if(sessionPermission.indexOf(104) != -1){
	    	 	divider2 = "| ";
	    	 	iis = <Link to='/request'> Inventory Information System</Link>
	    	 } else if(sessionPermission.indexOf(100) != -1){
	    	 	divider2 = "| ";
	    	 	iis = <Link to='/receipt'> Inventory Information System</Link>
	    	 } else if(sessionPermission.indexOf(125) != -1){
	    	 	divider2 = "| ";
	    	 	iis = <Link to='/reports/iis/inventory'> Inventory Information System</Link>
	    	 }

	    	 if(sessionPermission.indexOf(16) != -1){
	    	 	account = <Link to='/user'> Account Management</Link>
	    	 } else if(sessionPermission.indexOf(12) != -1){
	    	 	account = <Link to='/group'> Account Management</Link>
	    	 }

		    return (
		        <nav className="navbar navbar-default navbar-static-top " role="navigation" style={{marginBottom: 0}} id="page-wrapper-top">
				    <div className="row color-blue test">
				        <div className="navbar-header">
				            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				              <span className="sr-only">Toggle navigation</span>
				              <span className="icon-bar" />
				              <span className="icon-bar" />
				              <span className="icon-bar" />
				            </button>
				            <a className="navbar-brand header-move color-font-white" style={{color: 'white'}}>Sayun Solutions HRIS / IIS</a>
				        </div>
				        <ul className="nav navbar-top-links navbar-right color-font-white">
				            <li>Welcome {sessionStorage.getItem('name')}!</li> | 
				            <li><a className="nav-left-item-padding nav-right-item-padding color-font-white color-white-hover" onClick={() => this.changePassword()}> Change Password</a></li> | 
				            <li><a className="nav-right-item-padding nav-left-item-padding color-font-white color-white-hover" onClick={() => this.logout()}> Logout</a></li>
				        </ul>   
				    </div>
				    <div className="row header-move">
				        {hris} {divider1}
				        {iis} {divider2}
				        {account} {divider3}
				        System Management
				    </div> 
				 </nav>
		    );
	    } else if(this.props.active == "Password"){
		    return (
		        <nav className="navbar navbar-default navbar-static-top " role="navigation" style={{marginBottom: 0}} id="page-wrapper-top">
				    <div className="row color-blue test">
				        <div className="navbar-header">
				            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				              <span className="sr-only">Toggle navigation</span>
				              <span className="icon-bar" />
				              <span className="icon-bar" />
				              <span className="icon-bar" />
				            </button>
				            <a className="navbar-brand header-move color-font-white" style={{color: 'white'}}>Sayun Solutions HRIS / IIS</a>
				        </div>
				        <ul className="nav navbar-top-links navbar-right color-font-white">
				            <li>Welcome {sessionStorage.getItem('name')}!</li> | 
				            <li><a className="nav-left-item-padding nav-right-item-padding color-font-white color-white-hover" onClick={() => this.changePassword()}> Change Password</a></li> | 
				            <li><a className="nav-right-item-padding nav-left-item-padding color-font-white color-white-hover" onClick={() => this.logout()}> Logout</a></li>
				        </ul>  
				    </div>
				    <div className="row header-move">
				        <Link to='/employee'> Human Resource Information System</Link> |
				        <Link to='/warehouse'> Inventory Information System</Link> |
				        <Link to='/user'> Account Management</Link> |
				        <Link to='/category'> System Management</Link>
				    </div> 
				 </nav>
		    );
	    } else{
	    	return (<h1>Page not found. </h1>);
	    }
    }
}

export default Header;

