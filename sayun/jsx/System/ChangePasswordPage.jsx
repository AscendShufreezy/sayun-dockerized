import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Header from '../Utils/Header.jsx';
import ChangePasswordForm from '../System/ChangePasswordForm.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class ChangePasswordPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          response: [],
          transfer: false,
          cancel: false,
          password: {},
          logout: false
       };
    }

    cancelSave() {
        this.setState({cancel: true});
    }

    checkInputs() {
        if(document.getElementById("oldpassword").value == ""){
            this.setState({emptyField: "Old Password"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("password").value == ""){
            this.setState({emptyField: "New Password"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("repassword").value == ""){
            this.setState({emptyField: "Retype Password"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("repassword").value != document.getElementById("password").value){
                alert("Passwords enetered do not match. Please try again.");
                document.getElementById("repassword").value = "";
                document.getElementById("password").value = "";
        } else{
            this.setState({isComplete: true}, () => { 
                var passwordObj = {};
                passwordObj.username = sessionStorage.getItem('username'); 
                passwordObj.oldpassword = document.getElementById("oldpassword").value;
                passwordObj.password = document.getElementById("password").value;
                passwordObj.repassword = document.getElementById("repassword").value;

                this.setState({password: passwordObj}, () => this.savePassword()); //for async calls
            });
        }
    }

    savePassword() {
        var self = this;
        var password = this.state.password;
        var data = {"username": password.username,"password": password.oldpassword,
                    "new_password": password.password, "new_password2": password.repassword}

        $.ajax({
            url:  Utils.basePath + "/change-password/",
            type:"POST",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).non_field_errors);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
                sessionStorage.removeItem('token');
                sessionStorage.removeItem('username');
                sessionStorage.removeItem('name');
                sessionStorage.removeItem('group');
                sessionStorage.removeItem('id');
                alert("Password changed!");           
                self.setState({transfer: true});
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    render(){
        if (this.state.logout) {
           return <Redirect push to="/" />;
        }

        if (this.state.transfer) {
            return <Redirect push to="/" />;
        }

        if (this.state.cancel) {
            return <Redirect push to="/employee" />;
        }

        return (
            <div>
                <Header active="Password"/>
                <div id="page-wrapper">
                    <br />
                    <div className="row nav nav-tabs">
                    </div>

                    <div>
                        <div className="row " id="page-wrapper-inner">
                            <PageTitle pageTitle="Change Password"/>
                            <div className="row" id="page-wrapper-inner3">
                                <form>
                                    <ChangePasswordForm />
                                    <div className="row">
                                        <div className="col-md-3 pull-right">
                                          <div className="pull-right">
                                            <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
                                            <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
                                          </div>
                                        </div>
                                    </div>
                                </form>
                                <br />
                            </div>
                        </div>
                    </div>
                    <br />  
                </div>
            </div>
        );
    }
}

export default ChangePasswordPage;