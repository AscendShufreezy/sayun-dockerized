import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';
import moment from 'moment';

import Input from '../Utils/Input.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class ChangePasswordForm extends React.Component {
    render(){
      return (
        <div>
      	    <div className="row">
                <div className="col-md-6">
                    <div className="row">
                        <div className="col-md-5">
                            Old Password*
                        </div>
                        <div className="col-md-7">
                            <Input id="oldpassword" type="password"/> 
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-5">
                            New Password*
                        </div>
                        <div className="col-md-7">
                           <Input id="password" type="password"/> 
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-5">
                            Retype New Password*
                        </div>
                        <div className="col-md-7">
                            <Input id="repassword" type="password"/> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}

export default ChangePasswordForm;

