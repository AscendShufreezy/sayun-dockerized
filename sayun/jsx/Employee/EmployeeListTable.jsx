import React from 'react';
import ReactDOM from 'react-dom';
import EmptyTable from '../Utils/EmptyTable.jsx';
import moment from 'moment';
import Preloader from '../Utils/Preloader.jsx';
import Utils from '../Utils/Utils.jsx';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class EmployeeListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [], 
         date: moment(),
         logout: false, 
         preloader: "",
         empty: "",
         logout: false,
         filter1: "",
         filter2: "",
         filter3: "",
         filter4: "",
         filter5: "",
         filter6: "",
         filter7: "",
         filter8: "",
         filter9: "",
         filter10: "",
         filter11: ""
      };
   }

   checkAllCheckbox(name) {
      var array = document.getElementsByName(name);
      for(var i=0; i<array.length; i++){
         var cb = document.getElementById(array[i].id);
         cb.checked = document.getElementById('checkAll').checked;
      }
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#employeeTable').html());
      location.href=url
      return false
   }

   componentDidMount(){
      var self = this;
      this.setState({preloader: <Preloader/>}); 
      $.ajax({
         url: Utils.basePath + "/employees/",
         type:"GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
            400: function (response) {
               alert(JSON.parse(response.responseText).id);
            },
            401: function (response) {
               alert("User is not authorized. Please login.");
               self.setState({logout: true});
            },
            500: function (response) {
               alert("HTTP error 500, please check your connection.");
            }
         },
         success: function(response) {
            const employeeList = [];

            if(sessionStorage.getItem('group') == "hr-group"){
               for(var i=0; i<response.length; i++){
                  if(response[i].type == "Per Trip" && response[i].status == "Active"){
                     var employee = {};
                     employee.checkboxId = response[i].id;
                     employee.id = response[i].id;
                     employee.status = response[i].status;
                     employee.fName = response[i].first_name;
                     employee.mName = response[i].middle_name;
                     employee.lName = response[i].last_name;
                     employee.birthday = response[i].birthdate;
                     employee.gender = response[i].gender;
                     employee.type = response[i].type;
                     employee.title = response[i].title;
                     employee.dateHired = response[i].date_hired;
                     employee.years = response[i].years;
                     employeeList.push(employee);
                  }
               }

            } else {
               for(var i=0; i<response.length; i++){
                  if(response[i].status == "Active"){
                     var employee = {};
                     employee.checkboxId = response[i].id;
                     employee.id = response[i].id;
                     employee.status = response[i].status;
                     employee.fName = response[i].first_name;
                     employee.mName = response[i].middle_name;
                     employee.lName = response[i].last_name;
                     employee.birthday = response[i].birthdate;
                     employee.gender = response[i].gender;
                     employee.type = response[i].type;
                     employee.title = response[i].title;
                     employee.dateHired = response[i].date_hired;
                     employee.years = response[i].years;
                     employeeList.push(employee);
                  }
               }
            }

            if(employeeList.length == 0){
               this.setState({empty: <EmptyTable text="Employees" span="12"/>});
            }

            this.setState({response: employeeList}); 
            this.setState({preloader: ""}); 
         }.bind(this),
         error: function(xhr, status, err) {
            /* Do nothing*/
         }.bind(this)
     });
   }

   idFormatter(cell, row){
      var link = "/employee/details/id=" + cell;
      return (
         <Link to={link}><u>{cell}</u></Link>
      );
   }

   checkboxFormatter(cell, row){
      return (
         <input type="checkbox" name="checkbox" id={cell} />
      );
   }

   filterShow1(){
      if(this.state.filter1 == ""){
         this.setState({filter1: 'RegexFilter'});
      } else{
         this.setState({filter1: ''});
      }
   }

   filterShow2(){
      if(this.state.filter2 == ""){
         this.setState({filter2: 'RegexFilter'});
      } else{
         this.setState({filter2: ''});
      }
   }

   filterShow3(){
      if(this.state.filter3 == ""){
         this.setState({filter3: 'RegexFilter'});
      } else{
         this.setState({filter3: ''});
      }
   }

   filterShow4(){
      if(this.state.filter4 == ""){
         this.setState({filter4: 'RegexFilter'});
      } else{
         this.setState({filter4: ''});
      }
   }

   filterShow5(){
      if(this.state.filter5 == ""){
         this.setState({filter5: 'RegexFilter'});
      } else{
         this.setState({filter5: ''});
      }
   }

   filterShow6(){
      if(this.state.filter6 == ""){
         this.setState({filter6: 'RegexFilter'});
      } else{
         this.setState({filter6: ''});
      }
   }

   filterShow7(){
      if(this.state.filter7 == ""){
         this.setState({filter7: 'RegexFilter'});
      } else{
         this.setState({filter7: ''});
      }
   }

   filterShow8(){
      if(this.state.filter8 == ""){
         this.setState({filter8: 'RegexFilter'});
      } else{
         this.setState({filter8: ''});
      }
   }

   filterShow9(){
      if(this.state.filter9 == ""){
         this.setState({filter9: 'RegexFilter'});
      } else{
         this.setState({filter9: ''});
      }
   }

   filterShow10(){
      if(this.state.filter10 == ""){
         this.setState({filter10: 'RegexFilter'});
      } else{
         this.setState({filter10: ''});
      }
   }

   filterShow11(){
      if(this.state.filter11 == ""){
         this.setState({filter11: 'RegexFilter'});
      } else{
         this.setState({filter11: ''});
      }
   }

   render(){
      const preloader = this.state.preloader;
      if (this.state.logout) {
         return <Redirect push to="/" />;
      }

      var filter1 = this.state.filter1;
      var filter2 = this.state.filter2;
      var filter3 = this.state.filter3;
      var filter4 = this.state.filter4;
      var filter5 = this.state.filter5;
      var filter6 = this.state.filter6;
      var filter7 = this.state.filter7;
      var filter8 = this.state.filter8;
      var filter9 = this.state.filter9;
      var filter10 = this.state.filter10;
      var filter11 = this.state.filter11;
      
      return(
         <div>
            <div className="row" id="page-wrapper-inner3">
               <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
            </div>
            <div className="row" id="employeeTable">
               <div className="col-md-12">
                  {preloader}
                  <BootstrapTable data={this.state.response} striped hover pagination>
                     <TableHeaderColumn width="40px" dataField='checkboxId' dataFormat={ this.checkboxFormatter }>
                        <input type="checkbox" id="checkAll" onChange={(e) => this.checkAllCheckbox('checkbox')} />
                     </TableHeaderColumn>
                     <TableHeaderColumn dataField='id' isKey filter={ { type: filter1 } } dataFormat={ this.idFormatter } dataSort={ true }>Employee ID <i className="fa fa-search search-size" onMouseOver={() => this.filterShow1()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='status' width="90px" filter={ { type: filter2 } }  dataSort={ true }>Status <i className="fa fa-search search-size" onMouseOver={() => this.filterShow2()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='lName' filter={ { type: filter3 } }  dataSort={ true }>Last Name <i className="fa fa-search search-size" onMouseOver={() => this.filterShow3()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='fName' filter={ { type: filter4 } }  dataSort={ true }>First Name <i className="fa fa-search search-size" onMouseOver={() => this.filterShow4()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='mName' filter={ { type: filter5 } }  dataSort={ true }>Middle Name <i className="fa fa-search search-size" onMouseOver={() => this.filterShow5()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='birthday' filter={ { type: filter6 } }  dataSort={ true }>Birthday <i className="fa fa-search search-size" onMouseOver={() => this.filterShow6()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='gender' width="90px" filter={ { filter7 } }  dataSort={ true }>Gender <i className="fa fa-search search-size" onMouseOver={() => this.filterShow7()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='type' width="80px" filter={ { type: filter8 } }  dataSort={ true }>Type <i className="fa fa-search search-size" onMouseOver={() => this.filterShow8()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='title' filter={ { type: filter9 } }  dataSort={ true }>Title <i className="fa fa-search search-size" onMouseOver={() => this.filterShow9()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='dateHired' filter={ { type: filter10 } }  dataSort={ true }>Date Hired <i className="fa fa-search search-size" onMouseOver={() => this.filterShow10()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='years' width="140px "filter={ { type: filter11 } }  dataSort={ true }>Years in Service <i className="fa fa-search search-size" onMouseOver={() => this.filterShow11()}></i></TableHeaderColumn>
                  </BootstrapTable>
               </div>
            </div>
         </div>
      );
    }
}

export default EmployeeListTable;

