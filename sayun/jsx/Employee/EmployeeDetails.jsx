import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';
import moment from 'moment';

import Breadcrumb from '../Utils/Breadcrumb.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import EditEmployeeForm from './EditEmployeeForm.jsx';
import SalaryInformation from './SalaryInformation.jsx';
import DeductionsListPage from '../Deduction/DeductionsListPage.jsx';
import FixedDeductionsListPage from '../Deduction/FixedDeductionsListPage.jsx';
import Preloader from '../Utils/Preloader.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class EmployeeDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            response: [],
            transfer: false,
            isComplete: false,
            emptyField: "",
            employee: "" , 
            preloader: "",
            logout: false,
            invalidField: ""
       };
    }

    cancelSave() {
		this.setState({transfer: true});
    }

    checkInputs() {
        if(document.getElementById("id").value == ""){
    		this.setState({emptyField: "Employee ID"}, () => { 
			    alert(this.state.emptyField + " should not be empty.");
			    return "";
			});
    	} else if(document.getElementById("fName").value == ""){
    		this.setState({emptyField: "Family Name"}, () => { 
			    alert(this.state.emptyField + " should not be empty.");
			    return "";
			});
    	} else if(document.getElementById("mName").value == ""){
    		this.setState({emptyField: "Middle Name"}, () => { 
			    alert(this.state.emptyField + " should not be empty.");
			    return "";
			});
    	} else if(document.getElementById("lName").value == ""){
    		this.setState({emptyField: "Last Name"}, () => { 
			    alert(this.state.emptyField + " should not be empty.");
			    return "";
			});
    	} else if(document.getElementById("birthday").value == ""){
    		this.setState({emptyField: "Birth date"}, () => { 
			    alert(this.state.emptyField + " should not be empty.");
			    return "";
			});
    	} else if(!moment(document.getElementById("birthday").value,'MM/DD/YYYY',true).isValid()){
            this.setState({invalidField: "Birth date"}, () => { 
                alert(this.state.invalidField + " should be a valid date.");
                return "";
            });
        } else if(document.getElementById("gender").value == ""){
    		this.setState({emptyField: "Gender"}, () => { 
			    alert(this.state.emptyField + " should not be empty.");
			    return "";
			});
    	} else if(document.getElementById("type").value == ""){
    		this.setState({emptyField: "Type"}, () => { 
			    alert(this.state.emptyField + " should not be empty.");
			    return "";
			});
    	} else if(document.getElementById("title").value == ""){
    		this.setState({emptyField: "Position"}, () => { 
			    alert(this.state.emptyField + " should not be empty.");
			    return "";
			});
    	} else if(document.getElementById("dateHired").value == ""){
    		this.setState({emptyField: "Date Hired"}, () => { 
			    alert(this.state.emptyField + " should not be empty.");
			    return "";
			});
    	} else if(!moment(document.getElementById("dateHired").value,'MM/DD/YYYY',true).isValid()){
            this.setState({invalidField: "Date Hired"}, () => { 
                alert(this.state.invalidField + " should be a valid date.");
                return "";
            });
        } else if(document.getElementById("years").value == ""){
    		this.setState({emptyField: "Years of Service"}, () => { 
			    alert(this.state.emptyField + " should not be empty.");
			    return "";
			});
    	} else if(isNaN(document.getElementById("years").value)){
            this.setState({invalidField: "Years of Service"}, () => { 
                alert(this.state.invalidField + " should be a valid number.");
                return "";
            });
        } else{
    		this.setState({isComplete: true}, () => { 
    			this.updateEmployee();
			});
    	}
    }

    updateEmployee() {
        var url = window.location.href;
        var empId = url.split('=').pop();
        var self = this;
        var employee = {};
        var remarks;

        if(document.getElementById("remarks") == null){
            remarks = "";
        } else{
            remarks = document.getElementById("remarks").value;
        }

        employee.id = document.getElementById('id').value;
        employee.fName = document.getElementById('fName').value;
        employee.mName = document.getElementById('mName').value;
        employee.lName = document.getElementById('lName').value;
        employee.birthday = document.getElementById('birthday').value;
        employee.gender = document.getElementById('gender').value;
        employee.title = document.getElementById('title').value;
        employee.type = document.getElementById('type').value;
        employee.title = document.getElementById('title').value;
        employee.dateHired = document.getElementById('dateHired').value;
        employee.status = document.getElementById('status').value;
        employee.img = document.getElementById('image').files;
        employee.remarks =remarks;
        employee.exitDate = null;

        if(document.getElementById("exitDate").value != ""){
            employee.exitDate = document.getElementById('exitDate').value;
            var data = {"id": employee.id,"first_name": employee.fName,
                    "last_name": employee.lName,"middle_name": employee.mName,
                    "birthdate": employee.birthday,"gender": employee.gender,
                    "title": employee.title, "type": employee.type,
                    "date_hired": employee.dateHired, "remarks": employee.remarks,
                    "status": employee.status, "exit_date": employee.exitDate, "make_customer": true}
        } else{
            var data = {"id": employee.id,"first_name": employee.fName,
                    "last_name": employee.lName,"middle_name": employee.mName,
                    "birthdate": employee.birthday,"gender": employee.gender,
                    "title": employee.title, "type": employee.type,
                    "date_hired": employee.dateHired, "remarks": employee.remarks,
                    "status": employee.status, "exit_date": null, "make_customer": true}
        }

        this.setState({preloader: <Preloader/>}); 
		$.ajax({
            url: Utils.basePath + "/employees/" + empId + "/",
            type: "PUT",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  if(response == "employee with this Employee ID already exists."){
                    alert("Employee ID already exists.");
                  } else alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function (data) {
                var formData = new FormData();
                formData.append('image', employee.img[0]);

                $.ajax({
                        url: Utils.basePath + "/employees/" + data.id + "/upload_image/",
                        type: 'POST',
                        contentType: "multipart/form-data",
                        data: formData,
                        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
                        async: false,
                        success: function (data) {
                            alert("Employee updated!");
                            self.setState({transfer: true});
                            self.setState({preloader: ""});
                        },
                        error: function (data) {
                            console.log(response);
                            self.setState({preloader: ""}); 
                        },
                        cache: false,
                        contentType: false,
                        processData: false}
                        );                
            },
            error: function (response) {
                console.log(response);
                self.setState({preloader: ""}); 
            }
        });
        
    }

    componentWillMount(){
        var group = sessionStorage.getItem('group');
        var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

        if (group != "admin-group" && group != 'hr-group' && sessionPermission.indexOf(52) == -1) {
            alert("You are not authorized to access this page. Please login as admin user.");
            this.setState({logout: true});
        }

        var self = this;
        var employee = {};
        var url = window.location.href;
        var empId = url.split('=').pop();
        localStorage.setItem('empId', empId);
        this.setState({preloader: <Preloader/>}); 

        $.ajax({
            url: Utils.basePath + "/employees/" + empId + "/",
            type:"GET",
            contentType: "application/json",
            async : false,
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  if(response == "employee with this Employee ID already exists."){
                    alert("Employee ID already exists.");
                  } else alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function (data) {
                employee.id = data.id;
                employee.status = data.status;
                employee.fName = data.first_name;
                employee.mName = data.middle_name;
                employee.lName = data.last_name;
                employee.birthday = data.birthdate;
                employee.gender = data.gender;
                employee.type = data.type;
                employee.title = data.title;
                employee.dateHired = data.date_hired;
                employee.years = data.years;
                employee.exitDate = data.exit_date;
                employee.remarks = data.remarks;
                employee.img = data.image;

            }
        });

        if(employee.type != "Per Trip" && group != "admin-group"){
            alert("You are not authorized to access this employee. Redirecting to Employee List.");
            this.setState({logout: true});
        } else{
            this.setState({employee: employee});
            this.setState({preloader: ""});     
        }
    }

	render(){
        const preloader = this.state.preloader;
        if (this.state.logout) {
             return <Redirect push to="/employee" />;
        }

		if (this.state.transfer) {
		    return <Redirect push to="/employee" />;
		}

        var employee = this.state.employee;
        var salaryInfo = "";

        if(employee.type != "Per Trip"){
            salaryInfo = <SalaryInformation empId={employee.id}/>
        }

      	return (
	      	<div id="page-wrapper">
		      	<Menu active="Employee"/>
		      	<div className="row" id="page-wrapper-inner">
		      		<Breadcrumb page="edit-employee"/>
		      		<PageTitle pageTitle="Employee"/>
                    {preloader}
		      		<div className="row" id="page-wrapper-inner3">
		      			<form>
			      			<EditEmployeeForm employee={employee}/>
			      			<div className="row">
					            <div className="col-md-3 pull-right">
					              <div className="pull-right">
					                <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
					                <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
					              </div>
					            </div>
					        </div>
				        </form>
                        {salaryInfo}    
                        <div id="page-wrapper-inner3">
                            <Menu active="Deductions"/>
                            <div className="row" id="page-wrapper-inner">
                                <div className="row" id="page-wrapper-inner3">
                                    <DeductionsListPage />                 
            		      			<br />
                                    <FixedDeductionsListPage /> 
                                </div>
                            </div>
                        </div>
		      		</div>
		      	</div>	
                <br />  
		    </div>
        );
   }
}

export default EmployeeDetails;

