import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Breadcrumb from '../Utils/Breadcrumb.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import EmployeeListTable from './EmployeeListTable.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class EmployeeListPage extends React.Component {
	constructor(){
		super();
		this.state = {
			logout: false
		}
	}
	deleteEmployee(){
	  	var numberOfEmployees = document.getElementsByName("checkbox");
	  	var employeesToDelete = [];

	  	for(var i=0; i<numberOfEmployees.length;i++){
	  		if(numberOfEmployees[i].checked){
	  			employeesToDelete.push(numberOfEmployees[i].id);
	  		}
	  	}

	  	if(employeesToDelete.length > 0){
	  		var deleteMessage = confirm("Are you sure you want to delete the selected employee/s?");
	  		if (deleteMessage == true) {
	  			var successCounter = 0;
	  			var failCounter = 0;	  			
	  			for(var i=0; i<employeesToDelete.length;i++){
	  				$.ajax({
			            url: Utils.basePath + "/employees/" + employeesToDelete[i] + "/",
			            type:"DELETE",
			            async: false,
			            contentType: "application/json",
			            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
			            success: function (data, xhr) {
			            	successCounter++;
			            },
			            error: function (response, xhr) {
			            	failCounter++;
			            }
			        });

					if(successCounter == employeesToDelete.length){
		  				alert("Deletion of employee/s is successful!");
		  				location.reload();
		  			} else if(failCounter == employeesToDelete.length){
	                	alert(" Employees with child record cannot be deleted!");
	                } else if((successCounter + failCounter) == employeesToDelete.length){
	                	alert("Employees with child record cannot be deleted. Only selected warehouses with no child records are deleted.");
	                	location.reload();
	                }
	  			}

	  			
		    }
	  	} else{
	  		alert('There are no employee/s selected.');
	  	}
    }

    componentWillMount(){
    	var group = sessionStorage.getItem('group');
    	var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

    	if (group != "admin-group" && group != 'hr-group' && sessionPermission.indexOf(52) == -1) {
    		alert("You are not authorized to access this page. Please login as admin user.");
    		this.setState({logout: true});
    	}
    }

    render(){
    	if (this.state.logout) {
             return <Redirect push to="/" />;
        }

        var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");
        var hide1 = "";
        var hide2 = "";
        if(sessionPermission.indexOf(49) == -1){
        	hide1 = "hidden";
        } if(sessionPermission.indexOf(51) == -1){
        	hide2 = "hidden";
        }

      	return (
	      	<div id="page-wrapper">
		      	<Menu active="Employee"/>
		      	<div className="row" id="page-wrapper-inner">
		      		<PageTitle pageTitle="Employee List"/>
		      		<div className="row" id="page-wrapper-inner3">
		      			<form>
			      			<EmployeeListTable />
			      			<ButtonGroup buttons="addDelete" link1="/employee/add-employee" click2={() => this.deleteEmployee()} type1="button" hide1={hide1} hide2={hide2}/>
		      			</form>
		      			<br />
		      		</div>
		      	</div>	
		      	<br />
		    </div>
     	);
   }
}

export default EmployeeListPage;

