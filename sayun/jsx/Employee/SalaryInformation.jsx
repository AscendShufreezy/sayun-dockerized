import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';
import PageTitle from '../Utils/PageTitle.jsx';
import Input from '../Utils/Input.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class SalaryInformation extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      transfer: false,
      isComplete: false,
      salaryInfo: "",
      account: "",
      rate: "",
      infoId: 0,
      logout: false,
      emptyField: "",
      invalidField: ""
    };

    this.handleAccount = this.handleAccount.bind(this);
    this.handleRate = this.handleRate.bind(this);
  }

  handleAccount(e) {
    this.setState({
      account: e.target.value
    });
  }

  handleRate(e) {
    this.setState({
      rate: e.target.value
    });
  }

  cancelSave() {
    this.setState({transfer: true});
  }

  checkInputs(empId, id) {
    if(document.getElementById("rate").value == ""){
      this.setState({emptyField: "Hourly Rate"}, () => { 
        alert(this.state.emptyField + " should not be empty.");
        return "";
      });
    } else if(isNaN(document.getElementById("rate").value)){
      this.setState({invalidField: "Hourly Rate"}, () => { 
        alert(this.state.invalidField + " should be a valid number.");
        return "";
      });
    } else{
      this.setState({isComplete: true}, () => { 
        this.updateSalaryInfo(empId, id);
      });
    }
  }

  getSalaryInformation(empId){
    var salaryInfo = {};
    $.ajax({
      url: Utils.basePath + "/employees/" + empId + "/salary_infos/",
      type:"GET",
      contentType: "application/json",
      async : false,
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
         400: function (response) {
            if(response == "employee with this Employee ID already exists."){
              alert("Employee ID already exists.");
            } else alert(JSON.parse(response.responseText).id);
         },
         401: function (response) {
            alert("User is not authorized. Please login.");
            self.setState({logout: true});
         },
         500: function (response) {
            alert("HTTP error 500, please check your connection.");
         }
      },
      success: function (data) {
        if(data.length != 0){
          salaryInfo.account = data[0].bank_account; 
          if(data[0].bank_account == "000-000-000-000"){
            salaryInfo.account = "";
          }
          salaryInfo.rate = data[0].hourly_rate;
          salaryInfo.id = data[0].id;
        } else{
          salaryInfo.account = "";
          salaryInfo.rate = 0;

          var data = {"employee": empId, "bank_account": "000-000-000-000", "hourly_rate": salaryInfo.rate}
          $.ajax({
            url: Utils.basePath + "/salary-infos/",
            type:"POST",
            contentType: "application/json",
            data: JSON.stringify(data),
            async : false,
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  if(response == "employee with this Employee ID already exists."){
                    alert("Employee ID already exists.");
                  } else alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function (data) {
              salaryInfo.id = data.id;
            }
          });
        }
      }
    });

    return salaryInfo;
  }

  updateSalaryInfo(empId, id){
    var salaryInfo = {};
    var account;
    if(document.getElementById("account") == null || document.getElementById("account").value == ""){
      account = " ";
    } else{
      account = document.getElementById("account").value;
    }

    salaryInfo.account = account;
    salaryInfo.rate = document.getElementById('rate').value;

    var data = {"employee": empId, "bank_account": salaryInfo.account, "hourly_rate": salaryInfo.rate}

    var self = this;
    $.ajax({
      url: Utils.basePath + "/salary-infos/" + id + "/",
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(data),
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
         400: function (response) {
            if(response == "employee with this Employee ID already exists."){
              alert("Employee ID already exists.");
            } else alert(JSON.parse(response.responseText).id);
         },
         401: function (response) {
            alert("User is not authorized. Please login.");
            self.setState({logout: true});
         },
         500: function (response) {
            alert("HTTP error 500, please check your connection.");
         }
      },
      success: function (data) {
        alert("Salary Information updated!");
        self.setState({transfer: true});
      },
      error: function (response) {
        console.log(response);
      }
    });
  }

  componentWillMount(){
    var empId = this.props.empId;
    var salaryInfo = this.getSalaryInformation(empId);
    this.setState({salaryInfo: salaryInfo, account: salaryInfo.account, rate: salaryInfo.rate, infoId: salaryInfo.id});
  }
    
	render(){
    if (this.state.logout) {
             return <Redirect push to="/" />;
    }

    if (this.state.transfer) {
        return <Redirect push to="/employee" />;
    }

		var empId = this.props.empId;
    var salaryInfo = this.state.salaryInfo;
    var infoId = this.state.infoId;

    return (
        <div className="row" id="page-wrapper-inner3">
          <hr />
          <PageTitle pageTitle="Salary Information"/>
          <div className="row" id="page-wrapper-inner3">
            <div>
              <div className="row">
                <div className="col-md-2">
                  Bank Account No.
                </div>
                <div className="col-md-3">
                  <Input id="account" type="text" defaultValue={this.state.account} onChange={this.handleAccount}/>  
                </div>
              </div>
              <div className="row form-row-padding-top">
                <div className="col-md-2">
                  Hourly Rate*
                </div>
                <div className="col-md-3">
                  <Input id="rate" type="text" defaultValue={this.state.rate} onChange={this.handleRate}/>  
                </div>
                <div className="col-md-5">
                  (Note: this value will be used to compute the overtime pay)
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-3 pull-right">
                <div className="pull-right">
                  <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs(empId, infoId)}>Save</button>
                  <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
                </div>
              </div>
            </div>
          </div>
          <hr />
        </div>
    );
  }
}

export default SalaryInformation;

