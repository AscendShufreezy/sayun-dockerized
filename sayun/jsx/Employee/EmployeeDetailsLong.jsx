import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Breadcrumb from '../Utils/Breadcrumb.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Preloader from '../Utils/Preloader.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class EmployeeDetailsLong extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      response: [],
      transfer: false,
      employee: {}, 
      preloader: "",
      logout: false
    };
  }

  componentWillMount(){
    this.setState({preloader: <Preloader/>}); 
    var empId = this.props.empId;
    var employee = {};
    $.ajax({
        url: Utils.basePath + "/employees/" + empId + "/",
        type: "GET",
        contentType: "application/json",
        async: false,
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        statusCode: {
           400: function (response) {
              if(response == "employee with this Employee ID already exists."){
                alert("Employee ID already exists.");
              } else alert(JSON.parse(response.responseText).id);
           },
           401: function (response) {
              alert("User is not authorized. Please login.");
              self.setState({logout: true});
           },
           500: function (response) {
              alert("HTTP error 500, please check your connection.");
           }
        },
        success: function (data) {
          employee.id = data.id;
          employee.fName = data.first_name;
          employee.mName = data.middle_name;
          employee.lName = data.last_name;
          employee.title = data.title;
          employee.dateHired = data.date_hired;
        },
        error: function (response) {
          console.log(response);
        }
    });
    this.setState({employee: employee});
    this.setState({preloader: ""});
  }
    
	render(){
    if (this.state.logout) {
             return <Redirect push to="/" />;
    }

    var employee = this.state.employee;
    const preloader = this.state.preloader;

    return (
      <div>
        {preloader}
        <div className="row" id="page-wrapper-inner3">
          <div className="row">
            <div className="col-md-2">
              Employee ID
            </div>
            <div className="col-md-3 form-control-height">
              {employee.id}
            </div>
            <div className="col-md-1 col-md-offset-2">
              Title
            </div>
            <div className="col-md-3">
              {employee.title}
            </div>
          </div>
          <div className="row form-row-padding-top">
            <div className="col-md-2">
              Name
            </div>
            <div className="col-md-3">
              {employee.lName}, {employee.fName} {employee.mName}
            </div>
            <div className="col-md-1 col-md-offset-2">
              Date Hired
            </div>
            <div className="col-md-3">
              {employee.dateHired}
            </div>
          </div>
        </div>
      </div>
    );
   }
}

export default EmployeeDetailsLong;

