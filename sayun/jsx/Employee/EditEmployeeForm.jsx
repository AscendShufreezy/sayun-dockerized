import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';
import moment from 'moment';

import Utils from '../Utils/Utils.jsx';
import Input from '../Utils/Input.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class EditEmployeeForm extends React.Component {
    constructor(props){
        super(props);

        var imgUrl = props.employee.img == '' ? "../../img/image-placeholder.png" : props.employee.img

        this.state = {
            date: moment(props.employee.dateHired, "MM/DD/YYYY"),
            birthday: moment(props.employee.birthday, "MM/DD/YYYY"),
            exitDate: moment(),
            status: "",
            fName: props.employee.fName,
            mName: props.employee.mName,
            lName: props.employee.lName,
            title: props.employee.title,
            gender: props.employee.gender,
            type: props.employee.type,
            status: props.employee.status,
            remarks: props.employee.remarks,
            img: imgUrl

        };

        this.handleChangeFName = this.handleChangeFName.bind(this);
        this.handleChangeMName = this.handleChangeMName.bind(this);
        this.handleChangeLName = this.handleChangeLName.bind(this);
        this.handleChangeTitle = this.handleChangeTitle.bind(this);
        this.handleGender = this.handleGender.bind(this);
        this.handleType = this.handleType.bind(this);
        this.handleStatus = this.handleStatus.bind(this);
        this.handleRemarks = this.handleRemarks.bind(this);

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeBirthday = this.handleChangeBirthday.bind(this);
        this.handleExit = this.handleExit.bind(this);
    }

    handleChangeFName(e) {
        this.setState({
            fName: e.target.value
        });
    }

    handleChangeMName(e) {
        this.setState({
            mName: e.target.value
        });
    }

    handleChangeLName(e) {
        this.setState({
            lName: e.target.value
        });
    }

    handleChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    handleGender(e) {
        this.setState({
            gender: e.target.value
        });
    }

    handleType(e) {
        this.setState({
            type: e.target.value
        });
    }

    handleStatus(e) {
        this.setState({
            status: e.target.value
        });
    }

    handleRemarks(e) {
        this.setState({
            remarks: e.target.value
        });
    }

    handleChange(date) {
        this.setState({
            date: date
        });
    }

    handleExit(exitDate) {
        this.setState({
            exitDate: exitDate
        });
    }

    handleChangeBirthday(birthday) {
        this.setState({
            birthday: birthday
        });
    }

    render(){
        var employee = this.props.employee;
        var gender = document.getElementById('gender');
        gender = "Male";
        var exitDateField;

        if(this.state.status == 'Active'){
            exitDateField = <DatePicker id="exitDate" className="form-control" disabled/>

        } else{
            exitDateField = <DatePicker id="exitDate" className="form-control" selected={this.state.exitDate} onChange={this.handleExit}/>
        }

        return (
            <div>
            <div className="row">
                <div className="col-md-6">
                    <div className="row">
                        <div className="col-md-4">
                            Employee ID*
                        </div>
                        <div className="col-md-6">
                            <input id="id" type="text" value={employee.id} className="form-control" disabled/>
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-4">
                           First Name*
                        </div>
                        <div className="col-md-6">
                            <Input id="fName" type="text" defaultValue={this.state.fName} onChange={this.handleChangeFName}/>  
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-4">
                             Middle Name*
                        </div>
                        <div className="col-md-6">
                            <Input id="mName" type="text" defaultValue={this.state.mName} onChange={this.handleChangeMName}/> 
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-4">
                             Last Name*
                        </div>
                        <div className="col-md-6">
                            <Input id="lName" type="text" defaultValue={this.state.lName} onChange={this.handleChangeLName}/> 
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-4">
                              Birth date*
                        </div>
                        <div className="col-md-6">
                            <DatePicker selected={this.state.birthday} onChange={this.handleChangeBirthday}
                             id="birthday" className="form-control" />
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-4">
                             Gender*
                        </div>
                        <div className="col-md-4">
                            <select className="form-control" id="gender" value={this.state.gender} onChange={this.handleGender}>
                                <option value="Female" selected>Female</option>
                                <option value="Male">Male</option>
                            </select>
                        </div>
                    </div>            
                    <div className="row form-row-padding-top">
                        <div className="col-md-4">
                             Type*
                        </div>
                        <div className="col-md-4">
                            <select className="form-control" id="type" value={this.state.type} onChange={this.handleType}>
                                <option value="Per Trip" selected>Per Trip</option>
                                <option value="Weekly">Weekly</option>
                                <option value="Office">Office</option>
                            </select>
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-4">
                              Position*
                        </div>
                        <div className="col-md-6">
                            <Input id="title" type="text" defaultValue={this.state.title} onChange={this.handleChangeTitle}/> 
                        </div>
                    </div>
                </div>
                <div className="col-md-6">
                    <div className="row">
                       <img src={this.state.img}  className="employee-img"/>
                    </div>
                    <div className="row form-row-padding-top">
                        <input type="file" id="image" src={this.state.img} />
                    </div>
                </div>          
            </div>
            <div className="row" id="page-wrapper-inner3">
                <div className="row form-row-padding-top">
                    <div className="col-md-2">
                        Date hired*
                    </div>
                    <div className="col-md-2">
                        <DatePicker selected={this.state.date} onChange={this.handleChange}
                             id="dateHired" className="form-control" />
                    </div>
                    <div className="col-conf-3">
                        Years in Service
                    </div>
                    <div className="col-md-2">
                        <Input id="years" type="text" defaultValue={employee.years} disable="Active" /> 
                    </div>
                </div>
                <div className="row form-row-padding-top">
                    <div className="col-md-2">
                        Status*
                    </div>
                    <div className="col-md-2">
                        <select className="form-control" id="status" value={this.state.status} onChange={this.handleStatus}>
                            <option value="Active">Active</option>
                            <option value="AWOL">AWOL</option>
                            <option value="Resigned">Resigned</option>
                            <option value="Terminated">Terminated</option>
                        </select>
                    </div>
                    <div className="col-conf-3">
                        Exit Date
                    </div>
                    <div className="col-md-2">
                        {exitDateField}
                    </div>
                </div> 
                <div className=" row form-row-padding-top">
                    <div className="col-md-2">
                        Remarks
                    </div>
                    <div className="col-md-6">
                        <textarea className="form-control" rows="5" id="remarks" maxLength="3000" defaultValue={this.state.remarks} onChange={this.handleRemarks}></textarea>
                    </div>
                </div>
            </div> 
            </div>
        );
    }
}   

export default EditEmployeeForm;

