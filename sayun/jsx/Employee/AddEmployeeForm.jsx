import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';
import moment from 'moment';
import Input from '../Utils/Input.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class AddEmployeeForm extends React.Component {
    constructor(){
        super();

        this.state = {
            date: moment(),
            birthday: ""
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeBirthday = this.handleChangeBirthday.bind(this);

    }

    handleChange(date) {
        this.setState({
            date: date
        });
    }

    handleChangeBirthday(birthday) {
        this.setState({
            birthday: birthday
        });
    }

    readImage(url){
        if (url.files && url.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#empImg').attr('src', e.target.result);
            };
            reader.readAsDataURL(url.files[0]);
        }
    }

    render(){
      return (
        <div>
      	<div className="row">
            <div className="col-md-6">
                <div className="row">
                    <div className="col-md-4">
                    	Employee ID*
                    </div>
                    <div className="col-md-6">
                        <Input id="id" type="text"/>            
                    </div>
                </div>
                <div className="row form-row-padding-top">
                    <div className="col-md-4">
                       First Name*
                    </div>
                    <div className="col-md-6">
                    	<Input id="fName" type="text" />  
                    </div>
                </div>
                <div className="row form-row-padding-top">
                    <div className="col-md-4">
                         Middle Name*
                    </div>
                	<div className="col-md-6">
                        <Input id="mName" type="text" /> 
                    </div>
                </div>
                <div className="row form-row-padding-top">
                    <div className="col-md-4">
                         Last Name*
                    </div>
                    <div className="col-md-6">
                        <Input id="lName" type="text" /> 
                    </div>
                </div>
                <div className="row form-row-padding-top">
                    <div className="col-md-4">
                          Birth date*
                    </div>
                    <div className="col-md-6">
                        <DatePicker selected={this.state.birthday} onChange={this.handleChangeBirthday} id="birthday" className="form-control"/>
                    </div>
                </div>
                <div className="row form-row-padding-top">
                    <div className="col-md-4">
                         Gender*
                    </div>
                    <div className="col-md-4">
                        <select className="form-control" id="gender">
                            <option selected disabled> </option>
                            <option value="Female">Female</option>
                            <option value="Male">Male</option>
                        </select>
                    </div>
                </div>            
                <div className="row form-row-padding-top">
                    <div className="col-md-4">
                         Type*
                    </div>
                    <div className="col-md-4">
                        <select className="form-control" id="type">
                            <option selected disabled> </option>
                            <option value="Per Trip">Per Trip</option>
                            <option value="Weekly">Weekly</option>
                            <option value="Office">Office</option>
                        </select>
                    </div>
                </div>
                <div className="row form-row-padding-top">
                    <div className="col-md-4">
                          Position*
                    </div>
                    <div className="col-md-6">
                        <Input id="title" type="text" /> 
                    </div>
                </div>
                <div className="row form-row-padding-top">
                    <div className="col-md-4">
                          Date Hired*
                    </div>
                    <div className="col-md-6">
                        <DatePicker selected={this.state.date} onChange={this.handleChange} id="dateHired" className="form-control"/>
                    </div>
                </div>  
            </div>
            <div className="col-md-6">
                <div className="row">
                    <img src="/sayun/img/image-placeholder.png" id="empImg" className="employee-img"/>
                </div>
                <div className="row form-row-padding-top">
                    <input type="file" id="image" className="btn btn-default"/>
                </div>
            </div>          
        </div>
        <div className="row" id="page-wrapper-inner3">
            <div className=" row form-row-padding-top">
                <div className="col-md-2">
                    Remarks
                </div>
                <div className="col-md-6">
                    <textarea className="form-control" rows="5" id="remarks" maxLength="3000"></textarea>
                </div>
            </div>
        </div> 
        </div>
    );
  }
}

export default AddEmployeeForm;

