import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Link, Redirect } from 'react-router-dom';
import EmployeeListPage from './Employee/EmployeeListPage.jsx';
import Header from './Utils/Header.jsx';
import Preloader from './Utils/Preloader.jsx';
import Utils from './Utils/Utils.jsx';

import '../vendor/bootstrap/css/bootstrap.min.css';
import '../vendor/metisMenu/metisMenu.min.css';
import '../dist/css/sb-admin-2.css';
import '../dist/css/index.css';
import '../vendor/morrisjs/morris.css';
import '../vendor/font-awesome/css/font-awesome.min.css';
import '../img/font-awesome/css/font-awesome.min.css';

class LoginForm extends React.Component {
	constructor(props) {
        super(props);

        this.state = {
        	response: [],
        	transfer: false, 
         	preloader: ""
        };

    }

    authenticateLogin(){
    	var username = document.getElementById('username').value;
    	var password = document.getElementById('password').value;

    	if(username === ''){
    		alert('Please input a username.');
    	} else if(password === ''){
    		alert('Please input a password.');
    	} else {
    		var state = this.state.transfer;
    		this.checkData(username, password, state);
    	}
    }

    alertInvalid(){
    	alert('Invalid username / password !');
    }

    checkData(username, password, state){
    	var deferred = $.Deferred();
    	var data = {"username": username,"password": password};
    	var self = this;

    	this.setState({preloader: <Preloader/>}); 

    	$.ajax({
		    url: Utils.basePath + "/login/",
		    type: "POST",
		    contentType: "application/json",
		    data: JSON.stringify(data),
		    success: function (data) {
		    	sessionStorage.setItem('token', data.token);
		    	$.ajax({
				    url: Utils.basePath + "/users/" + username + "/",
				    type: "GET",
				    contentType: "application/json",
				    async: false,
				    headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
				    success: function (data) {
				    	var warehouse = ""
				    	if(data.warehouses.length > 0){
				    		warehouse = data.warehouses[0].id;
				    	} 

				    	var permissionList = [];
				    	for(var i=0; i<data.groups[0].permissions.length; i++){
				    		permissionList.push(data.groups[0].permissions[i].id);
				    	}

				    	sessionStorage.setItem('id', data.id)
				        sessionStorage.setItem('username', data.username);
				        sessionStorage.setItem('name', data.name);
		    			sessionStorage.setItem('group', data.groups[0].name);
		    			sessionStorage.setItem('warehouse', warehouse);
		    			sessionStorage.setItem('permissions', permissionList);

		    			if(sessionStorage.getItem('token') != null){
		    				deferred.resolve(sessionStorage.getItem('token'));
		    			}
		    		}
				});
		    },
		    error: function (response) {
		    	self.setState({preloader: ""}); 
		    	self.alertInvalid();
		    }
		});

		$.when(deferred.promise()).done(function () {
            if(sessionStorage.getItem('token') != null){
                self.setState({transfer: true});
                self.setState({preloader: ""}); 
			}
			deferred.promise();
        });
    }

    componentWillMount(){
    	sessionStorage.removeItem('group');
    	sessionStorage.removeItem('id');
    	sessionStorage.removeItem('name');
    	sessionStorage.removeItem('token');
    	sessionStorage.removeItem('username');
    	sessionStorage.removeItem('warehouse');
    	sessionStorage.removeItem('permissions');

		document.addEventListener("keypress", function(event) {
		    if (event.keyCode == 13) {
		        document.getElementById('btnlogin').click();
		    }
		})
    }

    render() {
    	const preloader = this.state.preloader;
    	var permission = sessionStorage.getItem('group');

		if (this.state.transfer) {
			if(permission == "admin-group" || permission == "hr-group"){
		    	return <Redirect push to="/employee" />;
			} else if(permission == "warehouse-group"){
		    	return <Redirect push to="/warehouse" />;
			} else if(permission == "warehouseapprover-group" || permission == "cashier-group" || permission == "pos-group"){
		    	return <Redirect push to="/request" />;
			} else{
				var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");
				if(sessionPermission.indexOf(52) > -1){
					return <Redirect push to="/employee" />;
				} else if(sessionPermission.indexOf(32) > -1){
					return <Redirect push to="/attendance" />;
				} else if(sessionPermission.indexOf(88) > -1){
					return <Redirect push to="/payroll" />;
				} else if(sessionPermission.indexOf(123) > -1){
					return <Redirect push to="/warehouse" />;
				} else if(sessionPermission.indexOf(48) > -1){
					return <Redirect push to="/customer/type=Truck" />;
				} else if(sessionPermission.indexOf(64) > -1){
					return <Redirect push to="/item" />;
				} else if(sessionPermission.indexOf(100) > -1){
					return <Redirect push to="/receipt" />;
				} else if(sessionPermission.indexOf(104) > -1){
					return <Redirect push to="/request" />;
				} else if(sessionPermission.indexOf(16) > -1){
					return <Redirect push to="/user" />;
				} else if(sessionPermission.indexOf(12) > -1){
					return <Redirect push to="/group" />;
				} else if(sessionPermission.indexOf(44) > -1){
					return <Redirect push to="/category" />;
				} else{
					alert('No pages available.');
					return <Redirect push to="/" />
				}
			}
		}

        return (
        	<div>
        		{preloader}
	        	<Header active="Login" />
		        <div className="LoginForm">
		        	<div className="row">
		            	<div className="login-centered">
		            		<div>
			                    <h3>Login</h3>
			                    <hr/>
			                </div>
			                <form>
				                <div className="row">
							        <div className="col-md-12">
							          	<div className="row">
							            	<div className="col-md-5">
							              		<label>Username</label>
							            	</div>
							            	<div className="col-md-7">
							              		<input type="text" className="form-control" id="username" required />
							            	</div>
							          	</div>
							          	<div className="row form-row-padding-top">
							            	<div className="col-md-5">
							              		<label>Password</label>
							            	</div>
							            	<div className="col-md-7">
							              		<input type="password" className="form-control" id="password" required />
							            	</div>
							          	</div>
							        </div>
							    </div>
					            <br />
				                <div className="row form-row-padding-top">
				                    <div className="col-md-12">
				                        <div className="pull-right">
				                            <button type="button" className="btn btn-default" id="btnlogin" onClick={() => this.authenticateLogin()}>Login</button>
				                            <br />
				                        </div>
				                    </div>
				                </div>
				            </form>
			            </div>
		        	</div>
			    </div>
			</div>
      	);
   	}
}

export default LoginForm;

