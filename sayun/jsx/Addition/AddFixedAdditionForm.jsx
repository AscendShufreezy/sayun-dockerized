import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import Input from '../Utils/Input.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class AddFixedAdditionForm extends React.Component {
    constructor(){
        super();

        this.state = {
          type: "Monthly",
          optionResult: ["First Week", "Second Week", "Third Week", "Fourth Week"],
          logout: false,
          additions: []
        };

        this.handleType = this.handleType.bind(this);
    }

    handleType(e) {
        var value = e.target.value;
        this.setState({type: value});

        if(value == "Monthly"){
            this.setState({optionResult: ["First Week", "Second Week", "Third Week", "Fourth Week"]});
        } else{
            this.setState({optionResult: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]});
        }
    }

    componentWillMount(){
        var self = this;
        $.ajax({
            url: Utils.basePath + "/categories/",
            type:"GET",
            contentType: "application/json",
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
                const additions = [];
                for(var i=0;i<data.length;i++){
                    if(data[i].type == "Fixed Addition"){
                        additions.push(data[i]);
                    }
                }
                self.setState({additions: additions});
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    render(){
        if (this.state.logout) {
            return <Redirect push to="/" />;
        }
        const optionResult = this.state.optionResult.map((option) =>
            <option key={option} value={option}>{option}</option>
        );

        const additionsResult = this.state.additions.map((additions) =>
            <option value={additions.id} key={additions.id}>{additions.description}</option>
        );

        return (
            <div>
                <div>
                    <PageTitle pageTitle="Add Fixed Addition"/>
                </div>
                <div>
                    <div className="row" id="page-wrapper-inner3">
                        <div className="row">
                            <div className="col-md-2">
                                Category*
                            </div>
                            <div className="col-md-3">
                                <select className="form-control" id="category">
                                    <option selected disabled></option>
                                    {additionsResult}
                                </select>
                            </div>
                            <div className="col-md-1 col-md-offset-2">
                                Status
                            </div>
                            <div className="col-md-3">
                                Active
                            </div>
                        </div>
                        <div className="row form-row-padding-top">
                            <div className="col-md-2">
                                Description*
                            </div>
                            <div className="col-md-4">
                                <input type="text" className="form-control" id="description"/>
                            </div>
                        </div>
                        <div className="row form-row-padding-top">
                            <div className="col-md-2">
                                Frequency*
                            </div>
                            <div className="col-conf-3">
                                <select className="form-control" id="type" value={this.state.type} onChange={this.handleType}>
                                    <option value="Monthly">Monthly</option>
                                    <option value="Weekly">Weekly</option>
                                </select>
                            </div>
                            <div className="col-conf-3">
                                <select className="form-control" id="selected">
                                    {optionResult}
                                </select>
                            </div>
                            <div className="col-md-1 col-md-offset-2">
                                Amount*
                            </div>
                            <div className="col-md-3">
                                <input type="text" className="form-control" id="amount"/>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
        );
    }
}

export default AddFixedAdditionForm;