import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import EmployeeDetailsLong from '../Employee/EmployeeDetailsLong.jsx';
import EditFixedAdditionForm from './EditFixedAdditionForm.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';;

class FixedAdditionDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      response: [],
      transfer: false,
      isComplete: false,
      emptyField: "",
      invalidField: "",
      logout: false,
      fixedAddition: {}
   };
  }

  cancelSave() {
	 this.setState({transfer: true});
  }

  checkInputs(empId) {
  	  if(document.getElementById("category").value == ""){
          this.setState({emptyField: "Category"}, () => { 
              alert(this.state.emptyField + " should not be empty.");
              return "";
          });
      } else if(document.getElementById("type").value == ""){
        this.setState({emptyField: "Frequency"}, () => { 
            alert(this.state.emptyField + " should not be empty.");
            return "";
        });
      } else if(document.getElementById("selected").value == ""){
        this.setState({emptyField: "Frequency"}, () => { 
            alert(this.state.emptyField + " should not be empty.");
            return "";
        });
      } else if(document.getElementById("description").value == ""){
          this.setState({emptyField: "Description"}, () => { 
              alert(this.state.emptyField + " should not be empty.");
              return "";
          });
      } else if(document.getElementById("amount").value == ""){
          this.setState({emptyField: "Amount"}, () => { 
              alert(this.state.emptyField + " should not be empty.");
              return "";
          });
      } else if(isNaN(document.getElementById("amount").value)){
            this.setState({invalidField: "Amount"}, () => { 
                alert(this.state.invalidField + " should be a valid number.");
                return "";
            });
      } else{
  		  this.setState({isComplete: true}, () => { 
      		this.updateFixedAddition(empId);
    		});
  	}
  }  

  updateFixedAddition(empId) {
    var url = window.location.href;
    var fixedAdditionId = url.split('=').pop();

    var fixedAddition = {};
    var schedule = "";
    var selected = document.getElementById("selected").value;

    if(document.getElementById("type").value == "Monthly"){
        schedule = "Monthly (" + selected + ")";
    } else{
        schedule = "Weekly (" + selected + ")";
    }
        
    fixedAddition.category = document.getElementById("category").value;
    fixedAddition.schedule = schedule;
    fixedAddition.description = document.getElementById("description").value;
    fixedAddition.amount = (document.getElementById("amount").value);

    if (document.getElementById('btnActive').checked) {
      fixedAddition.status = document.getElementById("btnActive").value;
    } else{
      fixedAddition.status = document.getElementById("btnInactive").value;
    }
   
    var data = {"employee": empId, "category": fixedAddition.category, "schedule": fixedAddition.schedule, "description": fixedAddition.description,
                "amount": fixedAddition.amount, "status": fixedAddition.status}
    
    var self = this;
    $.ajax({
        url: Utils.basePath + "/fixed-additions/" + fixedAdditionId + "/",
        type: "PUT",
        contentType: "application/json",
        data: JSON.stringify(data),
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        statusCode: {
           400: function (response) {
              alert(JSON.parse(response.responseText));
           },
           401: function (response) {
              alert("User is not authorized. Please login.");
              self.setState({logout: true});
           },
           500: function (response) {
              alert("HTTP error 500, please check your connection.");
           }
        },
        success: function (data) {
            alert("Fixed Addition updated!");
            self.setState({transfer: true});
        },
        error: function (response) {
            console.log(response);
        }
    });
  }

  componentWillMount(){
    var url = window.location.href;
    var id = url.split('=').pop();
    var self = this;
    var fixedAddition = {};

    $.ajax({
      url: Utils.basePath + "/fixed-additions/" + id + "/",
      type:"GET",
      contentType: "application/json",
      async : false,
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
           400: function (response) {
              alert(JSON.parse(response.responseText).id);
           },
           401: function (response) {
              alert("User is not authorized. Please login.");
              self.setState({logout: true});
           },
           500: function (response) {
              alert("HTTP error 500, please check your connection.");
           }
        },
      success: function (data) {
        fixedAddition.id = data.id;
        fixedAddition.category = data.category;
        fixedAddition.status = data.status;
        fixedAddition.schedule = data.schedule;
        fixedAddition.description = data.description;
        fixedAddition.amount = data.amount;
      }
    });

    this.setState({fixedAddition: fixedAddition})
  }

	render(){
		var empId = localStorage.getItem("empId");
    var link = "/employee/details/additions/id=" + empId;
    var fixedAddition = this.state.fixedAddition;

    if (this.state.transfer) {
      return <Redirect push to={link} />;
    }

    if (this.state.logout) {
            return <Redirect push to="/" />;
    }
  
    return(
      <div id="page-wrapper">
        <Menu active="Employee"/>
        <div className="row" id="page-wrapper-inner">
          <Breadcrumb page="edit-fixedaddition" id={empId}/>
          <PageTitle pageTitle="Employee Details"/>
            <div className="row" id="page-wrapper-inner3">
              <EmployeeDetailsLong empId={empId}/>
              <hr />
              <form>
                <EditFixedAdditionForm fixedAddition={fixedAddition}/>
                <div className="row">
                  <div className="col-md-3 pull-right">
                    <div className="pull-right">
                        <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs(empId)}>Save</button>
                        <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
                    </div>
                  </div>
                </div>
              </form>
              <br />
            </div>
          </div>
          <br />  
        </div>
      );
    } 
}

export default FixedAdditionDetails;

