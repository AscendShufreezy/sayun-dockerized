import React from 'react';
import ReactDOM from 'react-dom';
import Preloader from '../Utils/Preloader.jsx';
import EmptyTable from '../Utils/EmptyTable.jsx';
import Utils from '../Utils/Utils.jsx';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class FixedAdditionsListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         preloader: "",
         empty: "",
         logout: false,
         filter1: "",
         filter2: "",
         filter3: "",
         filter4: "",
         filter5: ""
      };
   }

   checkAllCheckbox2(name) {
      var array = document.getElementsByName(name);
      for(var i=0; i<array.length; i++){
         var cb = document.getElementById(array[i].id);
         cb.checked = document.getElementById('checkAll2').checked;
      }
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#fixedAddListTable').html());
      location.href=url
      return false
   }

   componentDidMount(){
      var self = this;
      var url = window.location.href;
      var empId = url.split('=').pop();

      this.setState({preloader: <Preloader/>});
      $.ajax({
         url: Utils.basePath + "/employees/" + empId + "/fixed_additions/",
         type:"GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
            400: function (response) {
               alert(JSON.parse(response.responseText));
            },
            401: function (response) {
               alert("User is not authorized. Please login.");
               self.setState({logout: true});
            },
            500: function (response) {
               alert("HTTP error 500, please check your connection.");
            }
         },
         success: function(response) {
            const additionsList = [];
            for(var i=0; i<response.length; i++){
               var addition = {};
               addition.checkboxId = response[i].id;
               addition.id = response[i].id;
               addition.employee = response[i].employee;
               addition.category = response[i].category;
               addition.description = response[i].description;
               addition.schedule = response[i].schedule;
               addition.amount = response[i].amount;
               addition.status = response[i].status;
               additionsList.push(addition);
            }
            if(additionsList.length == 0){
               this.setState({empty: <EmptyTable text="Fixed Additions" span="6"/>});
            }

            this.setState({response: additionsList});
            this.setState({preloader: ""});
         }.bind(this),
         error: function(xhr, status, err) {
            const additionsList = [];
            this.setState({empty: <EmptyTable text="Temporary Deductions" span="7"/>});
            this.setState({response: additionsList});
            this.setState({preloader: ""});
         }.bind(this)
      });
   }

   idFormatter(cell, row){
      var link = "/fixed-addition/id=" + row.id;
      return (
         <Link to={link}><u>{cell}</u></Link>
      );
   }

   amountFormatter(cell, row){
      var amount = cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return (
         amount
      );
   }

   catFormatter(cell, row){
      var self = this;
      var category = "";
      $.ajax({
         url: Utils.basePath + "/categories/" + cell + "/",
         type:"GET",
         contentType: "application/json",
         async: false,
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
            400: function (response) {
               alert(JSON.parse(response.responseText).id);
            },
            401: function (response) {
               alert("User is not authorized. Please login.");
               self.setState({logout: true});
            },
            500: function (response) {
               alert("HTTP error 500, please check your connection.");
            }
         },
         success: function(response) {
            category = response.description;
         }
      })

      return (
         <a><u>{category}</u></a>
      );
   }

   checkboxFormatter(cell, row){
      var id = "fixed" + row.checkboxId;

      return (
         <input type="checkbox" name="checkbox2" id={id} />
      );
   }

   filterShow1(){
      if(this.state.filter1 == ""){
         this.setState({filter1: 'RegexFilter'});
      } else{
         this.setState({filter1: ''});
      }
   }

   filterShow2(){
      if(this.state.filter2 == ""){
         this.setState({filter2: 'RegexFilter'});
      } else{
         this.setState({filter2: ''});
      }
   }

   filterShow3(){
      if(this.state.filter3 == ""){
         this.setState({filter3: 'RegexFilter'});
      } else{
         this.setState({filter3: ''});
      }
   }

   filterShow4(){
      if(this.state.filter4 == ""){
         this.setState({filter4: 'RegexFilter'});
      } else{
         this.setState({filter4: ''});
      }
   }

   filterShow5(){
      if(this.state.filter5 == ""){
         this.setState({filter5: 'RegexFilter'});
      } else{
         this.setState({filter5: ''});
      }
   }

   render(){
      const preloader = this.state.preloader;
      if (this.state.logout) {
         return <Redirect push to="/" />;
      }

      var filter1 = this.state.filter1;
      var filter2 = this.state.filter2;
      var filter3 = this.state.filter3;
      var filter4 = this.state.filter4;
      var filter5 = this.state.filter5;

      return(
         <div className="row">
           <div className="row" id="page-wrapper-inner5">
              <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
           </div>
            <div className="col-md-12" id="fixedAddListTable">
               {preloader}
               <BootstrapTable data={this.state.response} striped hover pagination>
                  <TableHeaderColumn width="30px" dataField='checkboxId' isKey dataFormat={ this.checkboxFormatter }>
                     <input type="checkbox" id="checkAll2" onChange={(e) => this.checkAllCheckbox2('checkbox2')} />
                  </TableHeaderColumn>
                  <TableHeaderColumn dataField='category' filter={ { type: filter1 } } dataFormat={ this.catFormatter } dataSort={ true }>Category <i className="fa fa-search search-size" onMouseOver={() => this.filterShow1()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='description' filter={ { type: filter2 } }  dataFormat={ this.idFormatter } dataSort={ true }>Description <i className="fa fa-search search-size" onMouseOver={() => this.filterShow2()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='schedule' filter={ { type: filter3 } } dataSort={ true }>Payroll Schedule <i className="fa fa-search search-size" onMouseOver={() => this.filterShow3()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='amount' filter={ { type: filter4 } } dataFormat={ this.amountFormatter } dataSort={ true }>Amount <i className="fa fa-search search-size" onMouseOver={() => this.filterShow4()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='status' filter={ { type: filter5 } } dataSort={ true }>Status <i className="fa fa-search search-size" onMouseOver={() => this.filterShow5()}></i></TableHeaderColumn>
               </BootstrapTable>
            </div>
         </div>
      );
    }
}

export default FixedAdditionsListTable;
