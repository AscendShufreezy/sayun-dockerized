import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import AdditionsListTable from './AdditionsListTable.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class AdditionsListPage extends React.Component {
	constructor(){
		super();
		this.state = {
			logout: false
		}
	}
	deleteTempAddition(){
	  	var numberOfTempAdditions = document.getElementsByName("checkbox");
	  	var tempAdditionsToDelete = [];

	  	for(var i=0; i<numberOfTempAdditions.length;i++){
	  		if(numberOfTempAdditions[i].checked){
	  			tempAdditionsToDelete.push(numberOfTempAdditions[i].id);
	  		}
	  	}

	  	if(tempAdditionsToDelete.length > 0){
	  		var deleteMessage = confirm("Are you sure you want to delete the selected temporary addition/s?");
	  		if (deleteMessage == true) {
	  			var successCounter = 0;
	  			var failCounter = 0;
	  			for(var i=0; i<tempAdditionsToDelete.length;i++){
	  				$.ajax({
			            url: Utils.basePath + "/temporary-additions/" + tempAdditionsToDelete[i] + "/",
			            type:"DELETE",
			            async: false,
			            contentType: "application/json",
			            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
			            statusCode: {
			               400: function (response) {
			                  alert(JSON.parse(response.responseText).id);
			               },
			               401: function (response) {
			                  alert("User is not authorized. Please login.");
			                  self.setState({logout: true});
			               },
			               500: function (response) {
			                  alert("HTTP error 500, please check your connection.");
			               }
						},

			            success: function (data) {
			            	successCounter++;
			            },
			            error: function (response) {
			            	failCounter++;
			            }
			        });

	            	if(successCounter == tempAdditionsToDelete.length){
		  				alert("Deletion of temporary addition/s is successful!");
		  				location.reload();
		  			} else if(failCounter == tempAdditionsToDelete.length){
	                	alert("Temporary Additions with processed payroll schedules cannot be deleted!");
	                } else if((successCounter + failCounter) == tempAdditionsToDelete.length){
	                	alert("Temporary Additions with processed payroll schedules cannot be deleted. Only selected temporary additions with processed payroll schedules are deleted.");
	                	location.reload();
	                }
	  			}
		    }
	  	} else{
	  		alert('There are no temporary addition/s selected.');
	  	}
    }
    componentWillMount() {
	    var group = sessionStorage.getItem('group');
	        if (group != "admin-group" && group != "hr-group") {
	            alert("You are not authorized to access this page.");
	            this.setState({logout: true});
	        }
	 }

    render(){
    	if (this.state.logout) {
            return <Redirect push to="/" />;
        }
      	return (
	      	<div>
      			<PageTitle pageTitle="Temporary Additions"/>
      			<br />
      			<form>
	      			<AdditionsListTable />
	      			<ButtonGroup buttons="addDelete" link1="/temp-addition/add-temp-addition" click2={() => this.deleteTempAddition()} type1="button"/>
      			</form>
      			<br />
		    </div>
     	);
   }
}

export default AdditionsListPage;

