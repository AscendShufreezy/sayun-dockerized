import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import EmployeeDetailsLong from '../Employee/EmployeeDetailsLong.jsx';
import AddFixedAdditionForm from './AddFixedAdditionForm.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class AddFixedAddition extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          response: [],
          transfer: false,
          isComplete: false,
          logout: false,
          emptyField: "",
          fixedAddition: {},
          invalidField: ""
       };
    }

    cancelSave() {
        this.setState({transfer: true});
    }

    checkInputs(empId) {
        if(document.getElementById("category").value == ""){
            this.setState({emptyField: "Category"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("type").value == ""){
            this.setState({emptyField: "Frequency"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("selected").value == ""){
            this.setState({emptyField: "Frequency"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("description").value == ""){
            this.setState({emptyField: "Description"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("amount").value == ""){
            this.setState({emptyField: "Amount"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(isNaN(document.getElementById("amount").value)){
            this.setState({invalidField: "Amount"}, () => { 
                alert(this.state.invalidField + " should be a valid number.");
                return "";
            });
        } else{
            this.setState({isComplete: true}, () => { 
                var fixedAddition = {};
                var schedule = "";
                var selected = document.getElementById("selected").value;

                if(document.getElementById("type").value == "Monthly"){
                    schedule = "Monthly (" + selected + ")";
                } else{
                    schedule = "Weekly (" + selected + ")";
                }
            
                fixedAddition.category = document.getElementById("category").value;
                fixedAddition.schedule = schedule;
                fixedAddition.description = document.getElementById("description").value;
                fixedAddition.amount = document.getElementById("amount").value;
                fixedAddition.status = "Active";
                this.setState({fixedAddition: fixedAddition}, () => this.saveFixedAddition(empId)); //for async calls
            });
        }
    }

    saveFixedAddition(empId) {
        var self = this;
        var fixedAddition = this.state.fixedAddition;
        var data = {"employee": empId, "category": fixedAddition.category,"schedule": fixedAddition.schedule,"description": fixedAddition.description, 
        			"amount": fixedAddition.amount, "status": "Active"}
                    
        $.ajax({
            url: Utils.basePath + "/fixed-additions/",
            type:"POST",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
                alert("Fixed Addition saved!");
                self.setState({transfer: true});
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    render(){
    	var empId = localStorage.getItem("empId");
        var link = "/employee/details/additions/id=" + empId;
    	
        if (this.state.transfer) {
            return <Redirect push to={link} />;
        }

        if (this.state.logout) {
            return <Redirect push to="/" />;
        }

        return (
            <div id="page-wrapper">
                <Menu active="Employee"/>
                <div className="row" id="page-wrapper-inner">
                    <Breadcrumb page="add-fixedaddition" id={empId}/>
                    <PageTitle pageTitle="Employee Details"/>
                    <div className="row" id="page-wrapper-inner3">
                        <EmployeeDetailsLong empId={empId}/>
                        <hr />
                        <form>
                            <AddFixedAdditionForm />
                            <div className="row">
                                <div className="col-md-3 pull-right">
                                  <div className="pull-right">
                                    <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs(empId)}>Save</button>
                                    <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
                                  </div>
                                </div>
                            </div>
                        </form>
                        <br />
                    </div>
                </div>
                <br />  
            </div>
        );
   }
}

export default AddFixedAddition;