import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import FixedAdditionsListTable from './FixedAdditionsListTable.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class FixedAdditionsListPage extends React.Component {
	constructor(){
		super();
		this.state = {
			logout: false
		}
	}
	deleteFixedAddition(){
	  	var numberOfFixedAdditions = document.getElementsByName("checkbox2");
	  	var fixedAdditionsToDelete = [];

	  	for(var i=0; i<numberOfFixedAdditions.length;i++){
	  		if(numberOfFixedAdditions[i].checked){
	  			numberOfFixedAdditions[i].id = numberOfFixedAdditions[i].id.replace('fixed', '');
	  			fixedAdditionsToDelete.push(numberOfFixedAdditions[i].id);
	  		}
	  	}

	  	if(fixedAdditionsToDelete.length > 0){
	  		var deleteMessage = confirm("Are you sure you want to delete the selected fixed addition/s?");
	  		if (deleteMessage == true) {
	  			var successCounter = 0;
	  			for(var i=0; i<fixedAdditionsToDelete.length;i++){
	  				$.ajax({
			            url: Utils.basePath + "/fixed-additions/" + fixedAdditionsToDelete[i] + "/",
			            type:"DELETE",
			            contentType: "application/json",
			            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
			            statusCode: {
			               400: function (response) {
			                  alert(JSON.parse(response.responseText).id);
			               },
			               401: function (response) {
			                  alert("User is not authorized. Please login.");
			                  self.setState({logout: true});
			               },
			               500: function (response) {
			                  alert("HTTP error 500, please check your connection.");
			               }
						},
			            success: function (data) {
			            	successCounter++;

			            	if(successCounter == fixedAdditionsToDelete.length){
				  				alert("Deletion of fixed addition/s is successful!");
				  				location.reload();
				  			}
			            },
			            error: function (response) {
			                alert("Failed !!!");
			            }
			        });
	  			}
		    }
	  	} else{
	  		alert('There are no fixed addition/s selected.');
	  	}
    }

    render(){
    	if (this.state.logout) {
            return <Redirect push to="/" />;
        }
      	return (
	      	<div>
      			<PageTitle pageTitle="Fixed Additions"/>
      			<br />
      			<form>
	      			<FixedAdditionsListTable />
	      			<ButtonGroup buttons="addDelete" link1="/fixed-addition/add-fixed-addition" click2={() => this.deleteFixedAddition()} type1="button"/>
      			</form>
      			<br />
		    </div>
     	);
   }
}

export default FixedAdditionsListPage;

