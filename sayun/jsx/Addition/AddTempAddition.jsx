import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import EmployeeDetailsLong from '../Employee/EmployeeDetailsLong.jsx';
import AddTempAdditionForm from './AddTempAdditionForm.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class AddTempAddition extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          response: [],
          transfer: false,
          isComplete: false,
          emptyField: "",
          invalidField: "",
          tempAddition: {},
          logout: false,
          tempId: 0
       };
    }

    cancelSave() {
        this.setState({transfer: true});
    }

    checkInputs(empId) {
        if(document.getElementById("category").value == ""){
            this.setState({emptyField: "Category"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("date").value == ""){
            this.setState({emptyField: "Date"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("description").value == ""){
            this.setState({emptyField: "Description"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("total").value == ""){
            this.setState({emptyField: "Total"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(isNaN(document.getElementById("total").value)){
            this.setState({invalidField: "Total"}, () => { 
                alert(this.state.invalidField + " should be a valid number.");
                return "";
            });
        } else{
            this.setState({isComplete: true}, () => { 
                var tempAddition = {};
                tempAddition.category = document.getElementById("category").value;
                tempAddition.date = document.getElementById("date").value;
                tempAddition.description = document.getElementById("description").value;
                tempAddition.total = document.getElementById("total").value;
                this.setState({tempAddition: tempAddition}, () => this.saveTempAddition(empId)); //for async calls
            });
        }
    }

    saveTempAddition(empId) {
        var self = this;
        var tempAddition = this.state.tempAddition;
        var data = {"employee": empId, "category": tempAddition.category,"date": tempAddition.date,"description": tempAddition.description, 
        			"total": tempAddition.total, "pending": tempAddition.total, "processed": 0.00}
                    
        $.ajax({
            url: Utils.basePath + "/temporary-additions/",
            type:"POST",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
                self.setState({tempId: data.id});
                alert("Temporary Addition saved!");
                self.setState({transfer: true});
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    render(){
    	var empId = localStorage.getItem("empId");
        var tempId = this.state.tempId;
        //var link = "/employee/details/additions/id=" + empId;
    	var link = "/temp-addition/id=" + tempId;

        if (this.state.transfer) {
            return <Redirect push to={link} />;
        }

        if (this.state.logout) {
            return <Redirect push to="/" />;
        }

        return (
            <div id="page-wrapper">
                <Menu active="Employee"/>
                <div className="row" id="page-wrapper-inner">
                    <Breadcrumb page="add-tempaddition" id={empId}/>
                    <PageTitle pageTitle="Employee Details"/>
                    <div className="row" id="page-wrapper-inner3">
                        <EmployeeDetailsLong empId={empId}/>
                        <hr />
                        <form>
                            <AddTempAdditionForm />
                            <div className="row">
                                <div className="col-md-3 pull-right">
                                  <div className="pull-right">
                                    <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs(empId)}>Save</button>
                                    <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
                                  </div>
                                </div>
                            </div>
                        </form>
                        <br />
                    </div>
                </div>
                <br />  
            </div>
       );
    }
}

export default AddTempAddition;