import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Breadcrumb from '../Utils/Breadcrumb.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import AddGroupForm from './AddGroupForm.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class AddGroup extends React.Component {
  constructor(props) {
      super(props);

      this.state = {
        response: [],
        transfer: false,
        isComplete: false,
        emptyField: "",
        group: {},
        logout: false
     };
  }

  cancelSave() {
      this.setState({transfer: true});
  }

  checkInputs() {
      var permissions = document.getElementById('hidden').value;
      permissions = permissions.split(',');
      if(document.getElementById("name").value == ""){
          this.setState({emptyField: "Name"}, () => { 
              alert(this.state.emptyField + " should not be empty.");
              return "";
          });
      } else if(JSON.stringify(permissions) == '[""]' ){
          alert("Please select at least one (1) permission. ");
      } else{
          this.setState({isComplete: true}, () => { 
            var group = {};
            group.name = document.getElementById("name").value;
            group.permissions = permissions;
            this.setState({group: group}, () => this.saveGroup()); //for async calls
          });
      }
  }

  saveGroup() {
      var self = this;
      var group = this.state.group;
      var data = {"name": group.name, "permissions": group.permissions}
                  
      $.ajax({
          url: Utils.basePath + "/groups/",
          type:"POST",
          contentType: "application/json",
          data: JSON.stringify(data),
          headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
          statusCode: {
             400: function (response) {
                alert(JSON.parse(response.responseText).name);
             },
             401: function (response) {
                alert("User is not authorized. Please login.");
                self.setState({logout: true});
             },
             500: function (response) {
                alert("HTTP error 500, please check your connection.");
             }
          },
          success: function(data) {
              alert("Group saved!");
              self.setState({transfer: true});
          },
          error: function (response) {
              console.log(response);
          }
      });
  }

  componentWillMount(){
      var group = sessionStorage.getItem('group');
      var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

      if (group != "admin-group" && sessionPermission.indexOf(9) == -1) {
          alert("You are not authorized to access this page. Please login as admin user.");
          this.setState({logout: true});
      } 
  }

  render(){
    if (this.state.logout) {
      return <Redirect push to="/" />;
      }

    if (this.state.transfer) {
      return <Redirect push to="/group" />;
    }

    return (
      <div id="page-wrapper">
        <Menu active="Group"/>
        <div className="row" id="page-wrapper-inner">
          <Breadcrumb page="add-group"/>
          <PageTitle pageTitle="Add Group"/>
          <div className="row" id="page-wrapper-inner3">
            <form>
                <AddGroupForm />
                <div className="row">
                    <div className="col-md-3 pull-right">
                      <div className="pull-right">
                        <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
                        <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
                      </div>
                    </div>
                </div>
            </form>
            <br />
          </div>
        </div>
        <br />  
      </div>
    );
  }
}

export default AddGroup;