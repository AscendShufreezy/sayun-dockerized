import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Breadcrumb from '../Utils/Breadcrumb.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import EditGroupForm from './EditGroupForm.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class GroupDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          response: [],
          transfer: false,
          isComplete: false,
          emptyField: "",
          group: {},
          logout: false
       };
    }

    cancelSave() {
		  this.setState({transfer: true});
    }

    checkInputs() {
        var permissions = document.getElementById('hidden').value;
        permissions = permissions.split(',');
        if(document.getElementById("name").value == ""){
            this.setState({emptyField: "Name"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(JSON.stringify(permissions) == '[""]' ){
            alert("Please select at least one (1) permission. ");
        } else{
            this.setState({isComplete: true}, () => { 
                this.updateGroup();
            });
        }
    }

    updateGroup() {
        var self = this;
        var url = window.location.href;
        var id = url.split('=').pop();        

        var permissions = document.getElementById('hidden').value;
        permissions = permissions.split(',');
        console.log(permissions);

        var group = this.state.group;
        group.name = document.getElementById("name").value;
        group.permissions = permissions;

        var data = {"name": group.name, "permissions": group.permissions}

		    $.ajax({
            url: Utils.basePath + "/groups/" + id + "/",
            type: "PUT",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).name);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function (data) {
                alert("Group updated!");
                self.setState({transfer: true});
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    componentWillMount(){
      var group = sessionStorage.getItem('group');
      var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

      if (group != "admin-group" && sessionPermission.indexOf(12) == -1) {
          alert("You are not authorized to access this page. Please login as admin user.");
          this.setState({logout: true});
      }

      var url = window.location.href;
      var id = url.split('=').pop();
      var self = this;
      var group = {};

      $.ajax({
          url: Utils.basePath + "/groups/" + id + "/",
          type:"GET",
          contentType: "application/json",
          async: false,
          headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
          statusCode: {
             400: function (response) {
                alert(JSON.parse(response.responseText).name);
             },
             401: function (response) {
                alert("User is not authorized. Please login.");
                self.setState({logout: true});
             },
             500: function (response) {
                alert("HTTP error 500, please check your connection.");
             }
          },
          success: function (data) {
              group.name = data.name;
              group.permissions = data.permissions;
          },
          error: function (response) {
              alert(response);
          }
      });
      this.setState({group: group});
    }

	render(){
    if (this.state.logout) {
      return <Redirect push to="/" />;
    }

		if (this.state.transfer) {
		  return <Redirect push to="/group" />;
		}

    var group = this.state.group;
  	return (
    	<div id="page-wrapper">
      	<Menu active="Group"/>
      	<div className="row" id="page-wrapper-inner">
      		<Breadcrumb page="edit-group"/>
      		<PageTitle pageTitle="Group"/>
      		<div className="row" id="page-wrapper-inner3">
      			<form>
	      			<EditGroupForm group={group}/>
	      			<div className="row">
			            <div className="col-md-3 pull-right">
			              <div className="pull-right">
			                <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
			                <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
			              </div>
			            </div>
			        </div>
		        </form>
      			<br />
      		</div>
      	</div>	
        <br />  
      </div>
    );
  }
}

export default GroupDetails;

