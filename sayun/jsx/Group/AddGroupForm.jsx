import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import Input from '../Utils/Input.jsx';
import InputCheckbox from '../Utils/InputCheckbox.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class AddGroupForm extends React.Component {
    constructor() {
        super();

        this.state = {
            checkedEmployee: [false, false, false],
            checkedAttendance: [false, false, false],
            checkedSalary: [false, false, false],
            checkedPayroll: [false, false, false],
            checkedHrisReports: [false, false],
            checkedWarehouse: [false, false, false],
            checkedCustomer: [false, false, false],
            checkedItem: [false, false, false],
            checkedRequest: [false, false, false],
            checkedReceipt: [false, false, false],
            checkedIisReports: [false, false],
            checkedUser: [false, false, false],
            checkedGroup: [false, false, false],
            checkedCategory: [false, false, false],
            pushtogroup: []
       };

        this.addToGroupPermission = this.addToGroupPermission.bind(this);
    }

    addToGroupPermission(e){
        console.log("before add: " + this.state.pushtogroup);
        var permissions = this.state.pushtogroup;
        if(e.target.checked){
            if(permissions.indexOf(e.target.id) == -1) {
                permissions.push(parseInt(e.target.id));
            }
        } else{
            var index = permissions.indexOf(parseInt(e.target.id));
            if(index >= 0){
                permissions.splice(index, 1);
            }
        }
        console.log("after add: " + permissions);
        this.setState({pushtogroup: permissions});  
    }

    selectAllEmployee(e){
        var employeePermissions = [52, 49, 51];
        console.log("before selectAllEmployee: " + this.state.pushtogroup);
        var permissions = this.state.pushtogroup;

        if(this.state.checkedEmployee[0] == false && this.state.checkedEmployee[1] == false && this.state.checkedEmployee[2] == false){
           this.setState({checkedEmployee: [true, true, true]});
           for(var i=0; i<employeePermissions.length; i++){
                if(permissions.indexOf(employeePermissions[i]) == -1) {
                    permissions.push(employeePermissions[i]);
                }
           }
        } else{
            this.setState({checkedEmployee: [false, false, false]});
            for(var i=0; i<employeePermissions.length; i++){
                var index = permissions.indexOf(employeePermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }
            }
        }
        console.log("after selectAllEmployee: " + permissions);
        this.setState({pushtogroup: permissions});  
    }

    selectAllAttendance(e){
        var attendancePermissions = [32, 32, 32];
        console.log("before selectAllEmployee: " + this.state.pushtogroup);
        var permissions = this.state.pushtogroup;

        if(this.state.checkedAttendance[0] == false && this.state.checkedAttendance[1] == false && this.state.checkedAttendance[2] == false){
            this.setState({checkedAttendance: [true, true, true]});
            permissions.push(attendancePermissions[0]);
        } else{
            this.setState({checkedAttendance: [false, false, false]});
            var index = permissions.indexOf(attendancePermissions[0]);
            console.log(attendancePermissions[0]);
            console.log(index);
            if(index >= 0){
                permissions.splice(index, 1);
            }        
        }
        console.log("after selectAllEmployee: " + permissions);
        this.setState({pushtogroup: permissions});  
    }

    selectAllSalary(e){
        if(this.state.checkedSalary[0] == false && this.state.checkedSalary[1] == false && this.state.checkedSalary[2] == false){
            this.setState({checkedSalary: [true, true, true]});
        } else{
            this.setState({ checkedSalary: [false, false, false]});
        }
    }

    selectAllPayroll(e){
        var payrollPermissions = [88, 85, 87];
        console.log("before selectAllEmployee: " + this.state.pushtogroup);
        var permissions = this.state.pushtogroup;

        if(this.state.checkedPayroll[0] == false && this.state.checkedPayroll[1] == false && this.state.checkedPayroll[2] == false){
            this.setState({checkedPayroll: [true, true, true]});
            for(var i=0; i<payrollPermissions.length; i++){
                if(permissions.indexOf(payrollPermissions[i]) == -1) {
                    permissions.push(payrollPermissions[i]);
                }
            }
        } else{
            this.setState({checkedPayroll: [false, false, false]});
            for(var i=0; i<payrollPermissions.length; i++){
                var index = permissions.indexOf(payrollPermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            }
        }
        console.log("after selectAllEmployee: " + permissions);
        this.setState({pushtogroup: permissions}); 
    }

    selectAllHris(e){
        var payrollPermissions = [88, 85, 87];
        var attendancePermissions = [32, 32, 32];
        var employeePermissions = [52, 49, 51];

        console.log("before selectAllEmployee: " + this.state.pushtogroup);
        var permissions = this.state.pushtogroup;

        if(this.state.checkedEmployee[0] == false && this.state.checkedEmployee[1] == false && this.state.checkedEmployee[2] == false &&
            this.state.checkedAttendance[0] == false && this.state.checkedAttendance[1] == false && this.state.checkedAttendance[2] == false &&
            this.state.checkedSalary[0] == false && this.state.checkedSalary[1] == false && this.state.checkedSalary[2] == false &&
            this.state.checkedPayroll[0] == false && this.state.checkedPayroll[1] == false && this.state.checkedPayroll[2] == false){

            this.setState({checkedEmployee: [true, true, true]});
            for(var i=0; i<employeePermissions.length; i++){
                if(permissions.indexOf(employeePermissions[i]) == -1) {
                    permissions.push(employeePermissions[i]);
                }
            }

            this.setState({checkedAttendance: [true, true, true]});
            permissions.push(attendancePermissions[0]);

            this.setState({checkedSalary: [true, true, true]});

            this.setState({checkedPayroll: [true, true, true]});
            for(var i=0; i<payrollPermissions.length; i++){
                if(permissions.indexOf(payrollPermissions[i]) == -1) {
                    permissions.push(payrollPermissions[i]);
                }
            }
        } else{
            this.setState({checkedEmployee: [false, false, false]});
            for(var i=0; i<employeePermissions.length; i++){
                var index = permissions.indexOf(employeePermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }
            }

            this.setState({checkedAttendance: [false, false, false]});
            var index = permissions.indexOf(attendancePermissions[0]);
            console.log(attendancePermissions[0]);
            console.log(index);
            if(index >= 0){
                permissions.splice(index, 1);
            }   

            this.setState({ checkedSalary: [false, false, false]});

            this.setState({checkedPayroll: [false, false, false]});
            for(var i=0; i<payrollPermissions.length; i++){
                var index = permissions.indexOf(payrollPermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            }
        }

        console.log("after selectAllEmployee: " + permissions);
        this.setState({pushtogroup: permissions});   
    }

    selectAllWarehouse(e){
        var warehousePermissions = [124, 121, 123];
        console.log("before selectAllEmployee: " + this.state.pushtogroup);
        var permissions = this.state.pushtogroup;

        if(this.state.checkedWarehouse[0] == false && this.state.checkedWarehouse[1] == false && this.state.checkedWarehouse[2] == false){
            this.setState({checkedWarehouse: [true, true, true]});
            for(var i=0; i<warehousePermissions.length; i++){
                if(permissions.indexOf(warehousePermissions[i]) == -1) {
                    permissions.push(warehousePermissions[i]);
                }
            }
        } else{
            this.setState({checkedWarehouse: [false, false, false]});
            for(var i=0; i<warehousePermissions.length; i++){
                var index = permissions.indexOf(warehousePermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            }
        }
        console.log("after selectAllEmployee: " + permissions);
        this.setState({pushtogroup: permissions}); 
    }

    selectAllCustomer(e){
        var customerPermissions = [48, 45, 47];
        console.log("before selectAllEmployee: " + this.state.pushtogroup);
        var permissions = this.state.pushtogroup;

        if(this.state.checkedCustomer[0] == false && this.state.checkedCustomer[1] == false && this.state.checkedCustomer[2] == false){
            this.setState({checkedCustomer: [true, true, true]});
            for(var i=0; i<customerPermissions.length; i++){
                if(permissions.indexOf(customerPermissions[i]) == -1) {
                    permissions.push(customerPermissions[i]);
                }
            }
        } else{
            this.setState({checkedCustomer: [false, false, false]});
            for(var i=0; i<customerPermissions.length; i++){
                var index = permissions.indexOf(customerPermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            }
        }
        console.log("after selectAllEmployee: " + permissions);
        this.setState({pushtogroup: permissions}); 
    }

    selectAllItem(e){
        var itemPermissions = [64, 61, 63];
        console.log("before selectAllEmployee: " + this.state.pushtogroup);
        var permissions = this.state.pushtogroup;

        if(this.state.checkedItem[0] == false && this.state.checkedItem[1] == false && this.state.checkedItem[2] == false){
            this.setState({checkedItem: [true, true, true]});
            for(var i=0; i<itemPermissions.length; i++){
                if(permissions.indexOf(itemPermissions[i]) == -1) {
                    permissions.push(itemPermissions[i]);
                }
            }
        } else{
            this.setState({checkedItem: [false, false, false]});
            for(var i=0; i<itemPermissions.length; i++){
                var index = permissions.indexOf(itemPermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            }
        }
        console.log("after selectAllEmployee: " + permissions);
        this.setState({pushtogroup: permissions}); 
    }

    selectAllRequest(e){
        var requestPermissions = [104, 101, 103];
        console.log("before selectAllEmployee: " + this.state.pushtogroup);
        var permissions = this.state.pushtogroup;

        if(this.state.checkedRequest[0] == false && this.state.checkedRequest[1] == false && this.state.checkedRequest[2] == false){
           this.setState({checkedRequest: [true, true, true]});
           for(var i=0; i<requestPermissions.length; i++){
                if(permissions.indexOf(requestPermissions[i]) == -1) {
                    permissions.push(requestPermissions[i]);
                }
            }
        } else{
            this.setState({checkedRequest: [false, false, false]});
            for(var i=0; i<requestPermissions.length; i++){
                var index = permissions.indexOf(requestPermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            }
        }
        console.log("after selectAllEmployee: " + permissions);
        this.setState({pushtogroup: permissions});        
    }

    selectAllReceipt(e){
        var receiptPermissions = [100, 97, 99];
        console.log("before selectAllEmployee: " + this.state.pushtogroup);
        var permissions = this.state.pushtogroup;

        if(this.state.checkedReceipt[0] == false && this.state.checkedReceipt[1] == false && this.state.checkedReceipt[2] == false){
           this.setState({checkedReceipt: [true, true, true]});
           for(var i=0; i<receiptPermissions.length; i++){
                if(permissions.indexOf(receiptPermissions[i]) == -1) {
                    permissions.push(receiptPermissions[i]);
                }
            }
        } else{
            this.setState({checkedReceipt: [false, false, false]});
            for(var i=0; i<receiptPermissions.length; i++){
                var index = permissions.indexOf(receiptPermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            }
        }
        console.log("after selectAllEmployee: " + permissions);
        this.setState({pushtogroup: permissions});   
    }

    selectAllIisReports(e){
        var reportsPermission = [125, 125];
        var permissions = this.state.pushtogroup;

        if(this.state.checkedIisReports[0] == false && this.state.checkedIisReports[1] == false){
           this.setState({checkedIisReports: [true, true]});
           permissions.push(reportsPermission[0]);
        } else{
            this.setState({
                checkedIisReports: [false, false]
            });
            var index = permissions.indexOf(reportsPermission[0]);
            console.log(reportsPermission[0]);
            console.log(index);
            if(index >= 0){
                permissions.splice(index, 1);
            }   
        }
        console.log("after selectAllEmployee: " + permissions);
        this.setState({pushtogroup: permissions});    
    }

    selectAllIis(e){
        var receiptPermissions = [100, 97, 99];
        var requestPermissions = [104, 101, 103];
        var itemPermissions = [64, 61, 63];
        var customerPermissions = [48, 45, 47];
        var warehousePermissions = [124, 121, 123];
        var reportsPermission = [125, 125];

        console.log("before selectAllEmployee: " + this.state.pushtogroup);
        var permissions = this.state.pushtogroup;

        if(this.state.checkedReceipt[0] == false && this.state.checkedReceipt[1] == false && this.state.checkedReceipt[2] == false &&
            this.state.checkedRequest[0] == false && this.state.checkedRequest[1] == false && this.state.checkedRequest[2] == false &&
            this.state.checkedItem[0] == false && this.state.checkedItem[1] == false && this.state.checkedItem[2] == false &&
            this.state.checkedCustomer[0] == false && this.state.checkedCustomer[1] == false && this.state.checkedCustomer[2] == false &&
            this.state.checkedWarehouse[0] == false && this.state.checkedWarehouse[1] == false && this.state.checkedWarehouse[2] == false &&
            this.state.checkedIisReports[0] == false && this.state.checkedIisReports[1] == false){
            
            this.setState({checkedReceipt: [true, true, true]});
            for(var i=0; i<receiptPermissions.length; i++){
                if(permissions.indexOf(receiptPermissions[i]) == -1) {
                    permissions.push(receiptPermissions[i]);
                }
            }

            this.setState({checkedRequest: [true, true, true]});
            for(var i=0; i<requestPermissions.length; i++){
                if(permissions.indexOf(requestPermissions[i]) == -1) {
                    permissions.push(requestPermissions[i]);
                }
            }

            this.setState({checkedItem: [true, true, true]});
            for(var i=0; i<itemPermissions.length; i++){
                if(permissions.indexOf(itemPermissions[i]) == -1) {
                    permissions.push(itemPermissions[i]);
                }
            }

            this.setState({checkedCustomer: [true, true, true]});
            for(var i=0; i<customerPermissions.length; i++){
                if(permissions.indexOf(customerPermissions[i]) == -1) {
                    permissions.push(customerPermissions[i]);
                }
            }

            this.setState({checkedWarehouse: [true, true, true]});
            for(var i=0; i<warehousePermissions.length; i++){
                if(permissions.indexOf(warehousePermissions[i]) == -1) {
                    permissions.push(warehousePermissions[i]);
                }
            }

            this.setState({checkedIisReports: [true, true]});
            permissions.push(reportsPermission[0]);
        } else{
            this.setState({checkedReceipt: [false, false, false]});
            for(var i=0; i<receiptPermissions.length; i++){
                var index = permissions.indexOf(receiptPermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            
            }

            this.setState({checkedRequest: [false, false, false]});
            for(var i=0; i<requestPermissions.length; i++){
                var index = permissions.indexOf(requestPermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            }

            this.setState({checkedItem: [false, false, false]});
            for(var i=0; i<itemPermissions.length; i++){
                var index = permissions.indexOf(itemPermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            }

            this.setState({checkedCustomer: [false, false, false]});
            for(var i=0; i<customerPermissions.length; i++){
                var index = permissions.indexOf(customerPermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            }

            this.setState({checkedWarehouse: [false, false, false]});
            for(var i=0; i<warehousePermissions.length; i++){
                var index = permissions.indexOf(warehousePermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            }

            this.setState({checkedIisReports: [false, false]});
            var index = permissions.indexOf(reportsPermission[0]);
            if(index >= 0){
                permissions.splice(index, 1);
            } 
        }
        
        console.log("after selectAllEmployee: " + permissions);
        this.setState({pushtogroup: permissions});   
    }

    selectAllUser(e){
        var userPermissions = [16, 13, 15];
        console.log("before selectAllEmployee: " + this.state.pushtogroup);
        var permissions = this.state.pushtogroup;

        if(this.state.checkedUser[0] == false && this.state.checkedUser[1] == false && this.state.checkedUser[2] == false){
           this.setState({checkedUser: [true, true, true]});
           for(var i=0; i<userPermissions.length; i++){
                if(permissions.indexOf(userPermissions[i]) == -1) {
                    permissions.push(userPermissions[i]);
                }
            }
        } else{
            this.setState({checkedUser: [false, false, false]});
            for(var i=0; i<userPermissions.length; i++){
                var index = permissions.indexOf(userPermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            }
        }
        console.log("after selectAllEmployee: " + permissions);
        this.setState({pushtogroup: permissions});   
    }

    selectAllGroup(e){
        var groupPermissions = [12, 9, 11];
        console.log("before selectAllEmployee: " + this.state.pushtogroup);
        var permissions = this.state.pushtogroup;

        if(this.state.checkedGroup[0] == false && this.state.checkedGroup[1] == false && this.state.checkedGroup[2] == false){
           this.setState({checkedGroup: [true, true, true]});
           for(var i=0; i<groupPermissions.length; i++){
                if(permissions.indexOf(groupPermissions[i]) == -1) {
                    permissions.push(groupPermissions[i]);
                }
            }
        } else{
            this.setState({checkedGroup: [false, false, false]});
            for(var i=0; i<groupPermissions.length; i++){
                var index = permissions.indexOf(groupPermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            }
        }
        console.log("after selectAllEmployee: " + permissions);
        this.setState({pushtogroup: permissions});   
    }

    selectAllAccount(e){
        var userPermissions = [16, 13, 15];
        var groupPermissions = [12, 9, 11];
        console.log("before selectAllEmployee: " + this.state.pushtogroup);
        var permissions = this.state.pushtogroup;

        if(this.state.checkedUser[0] == false && this.state.checkedUser[1] == false && this.state.checkedUser[2] == false && 
            this.state.checkedGroup[0] == false && this.state.checkedGroup[1] == false && this.state.checkedGroup[2] == false){
           
            this.setState({checkedUser: [true, true, true]});
            for(var i=0; i<userPermissions.length; i++){
                if(permissions.indexOf(userPermissions[i]) == -1) {
                    permissions.push(userPermissions[i]);
                }
            }

            this.setState({checkedGroup: [true, true, true]});
            for(var i=0; i<groupPermissions.length; i++){
                if(permissions.indexOf(groupPermissions[i]) == -1) {
                    permissions.push(groupPermissions[i]);
                }
            }
        } else{
            this.setState({checkedUser: [false, false, false]});
            for(var i=0; i<userPermissions.length; i++){
                var index = permissions.indexOf(userPermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            
            }

            this.setState({checkedGroup: [false, false, false]});
            for(var i=0; i<groupPermissions.length; i++){
                var index = permissions.indexOf(groupPermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            
            }
        }

        console.log("after selectAllEmployee: " + permissions);
        this.setState({pushtogroup: permissions});   
    }

    selectAllCategory(e){
        var categoryPermissions = [44, 41, 43];
        console.log("before selectAllEmployee: " + this.state.pushtogroup);
        var permissions = this.state.pushtogroup;

        if(this.state.checkedCategory[0] == false && this.state.checkedCategory[1] == false && this.state.checkedCategory[2] == false){
           this.setState({checkedCategory: [true, true, true]});
           for(var i=0; i<categoryPermissions.length; i++){
                if(permissions.indexOf(categoryPermissions[i]) == -1) {
                    permissions.push(categoryPermissions[i]);
                }
            }
        } else{
            this.setState({checkedCategory: [false, false, false]});
            for(var i=0; i<categoryPermissions.length; i++){
                var index = permissions.indexOf(categoryPermissions[i]);
                if(index >= 0){
                    permissions.splice(index, 1);
                }            }
        }
        console.log("after selectAllEmployee: " + permissions);
        this.setState({pushtogroup: permissions});   
    }

    handleChangeEmployee(index, e) {
        var checked = this.state.checkedEmployee;   
        checked[index] = e.target.checked;
        this.setState({checked: checked});
    }

    handleChangeAttendance(index, e) {
        var checked = this.state.checkedAttendance;   
        checked[index] = e.target.checked;
        this.setState({checked: checked})
    }

    handleChangeSalary(index, e) {
        var checked = this.state.checkedSalary;   
        checked[index] = e.target.checked;
        this.setState({checked: checked})
    }

    handleChangePayroll(index, e) {
        var checked = this.state.checkedPayroll;   
        checked[index] = e.target.checked;
        this.setState({checked: checked})
    }

    handleChangeWarehouse(index, e) {
        var checked = this.state.checkedWarehouse;   
        checked[index] = e.target.checked;
        this.setState({ checked: checked})
    }

    handleChangeCustomer(index, e) {
        var checked = this.state.checkedCustomer;   
        checked[index] = e.target.checked;
        this.setState({ checked: checked})
    }

    handleChangeItem(index, e) {
        var checked = this.state.checkedItem;   
        checked[index] = e.target.checked;
        this.setState({ checked: checked})
    }

    handleChangeRequest(index, e) {
        var checked = this.state.checkedRequest;   
        checked[index] = e.target.checked;
        this.setState({ checked: checked})
    }

    handleChangeReceipt(index, e) {
        var checked = this.state.checkedReceipt;   
        checked[index] = e.target.checked;
        this.setState({ checked: checked})
    }

    handleChangeIisReports(index, e) {
        var checked = this.state.checkedIisReports;   
        checked[index] = e.target.checked;
        this.setState({ checked: checked})
    }

    handleChangeUser(index, e) {
        var checked = this.state.checkedUser;   
        checked[index] = e.target.checked;
        this.setState({ checked: checked})
    }

    handleChangeGroup(index, e) {
        var checked = this.state.checkedGroup;   
        checked[index] = e.target.checked;
        this.setState({ checked: checked})
    }

    handleChangeCategory(index, e) {
        var checked = this.state.checkedCategory;   
        checked[index] = e.target.checked;
        this.setState({ checked: checked})
    }

    render(){
        var isAllEmployeeChecked = this.state.checkedEmployee.filter(function(c) {
            return c;
        }).length === this.state.checkedEmployee.length;

        var isAllAttendanceChecked = this.state.checkedAttendance.filter(function(c) {
            return c;
        }).length === this.state.checkedAttendance.length;

        var isAllSalaryChecked = this.state.checkedSalary.filter(function(c) {
            return c;
        }).length === this.state.checkedSalary.length;

        var isAllPayrollChecked = this.state.checkedPayroll.filter(function(c) {
            return c;
        }).length === this.state.checkedPayroll.length;

        var isAllWarehouseChecked = this.state.checkedWarehouse.filter(function(c) {
            return c;
        }).length === this.state.checkedWarehouse.length;

        var isAllCustomerChecked = this.state.checkedCustomer.filter(function(c) {
            return c;
        }).length === this.state.checkedCustomer.length;

        var isAllItemChecked = this.state.checkedItem.filter(function(c) {
            return c;
        }).length === this.state.checkedItem.length;

        var isAllRequestChecked = this.state.checkedRequest.filter(function(c) {
            return c;
        }).length === this.state.checkedRequest.length;

        var isAllReceiptChecked = this.state.checkedReceipt.filter(function(c) {
            return c;
        }).length === this.state.checkedReceipt.length;

        var isAllIisReportsChecked = this.state.checkedIisReports.filter(function(c) {
            return c;
        }).length === this.state.checkedIisReports.length;

        var isAllUserChecked = this.state.checkedUser.filter(function(c) {
            return c;
        }).length === this.state.checkedUser.length;

        var isAllGroupChecked = this.state.checkedGroup.filter(function(c) {
            return c;
        }).length === this.state.checkedGroup.length;

        var isAllCategoryChecked = this.state.checkedCategory.filter(function(c) {
            return c;
        }).length === this.state.checkedCategory.length;

        return (
            <div>
          	    <div className="row" id="page-wrapper-inner3">
                    <div className="row">
                        <div className="col-md-2">
                            Name*
                        </div>
                        <div className="col-md-3">
                            <Input id="name" type="text"/>            
                        </div>
                    </div>
                </div> 

                <hr />

                <div>
                    <h3>Access Rights</h3>
                </div>
                <div>
                    <InputCheckbox onChange={this.selectAllHris.bind(this)} checked={isAllEmployeeChecked && isAllAttendanceChecked && isAllSalaryChecked && isAllPayrollChecked} id="allHris"/> Human Resource Information System
                </div>
                <br />
                <input type="hidden" id="hidden" value={this.state.pushtogroup} />
                <div className="well well-lg access-right-font-size">
                    <div className="row">
                        <div className="col-md-5">
                            <InputCheckbox onChange={this.selectAllEmployee.bind(this)} checked={isAllEmployeeChecked} id="allEmployee"/> Employee Management
                        </div>
                        <div className="col-md-5 col-md-offset-1">
                            <InputCheckbox onChange={this.selectAllAttendance.bind(this)} checked={isAllAttendanceChecked} id="allAttendance" /> Attendance Management
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-5">
                            <table className="table table-striped table-bordered table-responsive">
                                <thead className="thead-font-size">
                                    <tr > 
                                        <th className="checkbox-width"><InputCheckbox onChange={this.selectAllEmployee.bind(this)} checked={isAllEmployeeChecked} id="allEmployee"/></th>
                                        <th className="col-lg-width">Function</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedEmployee[0]} onClick={this.addToGroupPermission} onChange={this.handleChangeEmployee.bind(this, 0)} id="52"/></td>
                                        <td>List Employees</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedEmployee[1]}  onClick={this.addToGroupPermission} onChange={this.handleChangeEmployee.bind(this, 1)} id="49"/></td>
                                        <td>Add Employees</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedEmployee[2]}  onClick={this.addToGroupPermission} onChange={this.handleChangeEmployee.bind(this, 2)} id="51"/></td>
                                        <td>Delete Employees</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-5 col-md-offset-1">
                            <table className="table table-striped table-bordered table-responsive">
                                <thead className="thead-font-size">
                                    <tr > 
                                        <th className="checkbox-width"><InputCheckbox onChange={this.selectAllAttendance.bind(this)} checked={isAllAttendanceChecked} id="allAttendance"/></th>
                                        <th className="col-lg-width">Function</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedAttendance[0]}  onClick={this.addToGroupPermission} onChange={this.handleChangeAttendance.bind(this, 0)} id="32"/></td>
                                        <td>Attendance List by Employees</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedAttendance[1]}  onClick={this.addToGroupPermission} onChange={this.handleChangeAttendance.bind(this, 1)} id="32"/></td>
                                        <td>Attendance List by Date</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedAttendance[2]}  onClick={this.addToGroupPermission} onChange={this.handleChangeAttendance.bind(this, 2)} id="32"/></td>
                                        <td>View Attendance</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-5">
                            <InputCheckbox onChange={this.selectAllSalary.bind(this)} checked={isAllSalaryChecked} id="allSalary"/> Salary Management
                        </div>
                        <div className="col-md-5 col-md-offset-1">
                            <InputCheckbox onChange={this.selectAllPayroll.bind(this)} checked={isAllPayrollChecked} id="allPayroll"/> Payroll Management
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-5">
                            <table className="table table-striped table-bordered table-responsive">
                                <thead className="thead-font-size">
                                    <tr > 
                                        <th className="checkbox-width"><InputCheckbox onChange={this.selectAllSalary.bind(this)} checked={isAllSalaryChecked} id="allSalary"/></th>
                                        <th className="col-lg-width">Function</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedSalary[0]}  onClick={this.addToGroupPermission} onChange={this.handleChangeSalary.bind(this, 0)} id="0"/></td>
                                        <td>List Salary</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedSalary[1]}  onClick={this.addToGroupPermission} onChange={this.handleChangeSalary.bind(this, 1)} id="0"/></td>
                                        <td>Add Salary</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedSalary[2]}  onClick={this.addToGroupPermission} onChange={this.handleChangeSalary.bind(this, 2)} id="0"/></td>
                                        <td>Delete Salary</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-5 col-md-offset-1">
                            <table className="table table-striped table-bordered table-responsive">
                                <thead className="thead-font-size">
                                    <tr > 
                                        <th className="checkbox-width"><InputCheckbox onChange={this.selectAllPayroll.bind(this)} checked={isAllPayrollChecked} id="allPayroll"/></th>
                                        <th className="col-lg-width">Function</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedPayroll[0]}  onClick={this.addToGroupPermission} onChange={this.handleChangePayroll.bind(this, 0)} id="88"/></td>
                                        <td>List Payroll Calendar</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedPayroll[1]}  onClick={this.addToGroupPermission} onChange={this.handleChangePayroll.bind(this, 1)} id="85"/></td>
                                        <td>Add Payroll Calendar</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedPayroll[2]}  onClick={this.addToGroupPermission} onChange={this.handleChangePayroll.bind(this, 2)} id="89"/></td>
                                        <td>Delete Payroll Calendar</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>                    
                </div>

                <div>
                    <InputCheckbox onChange={this.selectAllIis.bind(this)} checked={isAllWarehouseChecked && isAllCustomerChecked && isAllItemChecked && isAllRequestChecked && isAllReceiptChecked && isAllIisReportsChecked} id="allIis"/> Item Inventory System
                </div>
                <br /> 
                <div className="well well-lg access-right-font-size">
                    <div className="row">
                        <div className="col-md-5">
                                <InputCheckbox onChange={this.selectAllWarehouse.bind(this)} checked={isAllWarehouseChecked} id="allWarehouse"/> Warehouse Management
                        </div>
                        <div className="col-md-5 col-md-offset-1">
                                <InputCheckbox onChange={this.selectAllCustomer.bind(this)} checked={isAllCustomerChecked} id="allCustomer"/> Customer Management
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-5">
                            <table className="table table-striped table-bordered table-responsive">
                                <thead className="thead-font-size">
                                    <tr > 
                                        <th className="checkbox-width"><InputCheckbox onChange={this.selectAllWarehouse.bind(this)} checked={isAllWarehouseChecked} id="allWarehouse"/></th>
                                        <th className="col-lg-width">Function</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedWarehouse[0]}  onClick={this.addToGroupPermission} onChange={this.handleChangeWarehouse.bind(this, 0)} id="124"/></td>
                                        <td>List Warehouses</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedWarehouse[1]}  onClick={this.addToGroupPermission} onChange={this.handleChangeWarehouse.bind(this, 1)} id="121"/></td>
                                        <td>Add Warehouses</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedWarehouse[2]}  onClick={this.addToGroupPermission} onChange={this.handleChangeWarehouse.bind(this, 2)} id="123"/></td>
                                        <td>Delete Warehouses</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-5 col-md-offset-1">
                            <table className="table table-striped table-bordered table-responsive">
                                <thead className="thead-font-size">
                                    <tr > 
                                        <th className="checkbox-width"><InputCheckbox onChange={this.selectAllCustomer.bind(this)} checked={isAllCustomerChecked} id="allCustomer"/></th>
                                        <th className="col-lg-width">Function</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedCustomer[0]}  onClick={this.addToGroupPermission} onChange={this.handleChangeCustomer.bind(this, 0)} id="48"/></td>
                                        <td>List Customers</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedCustomer[1]}  onClick={this.addToGroupPermission} onChange={this.handleChangeCustomer.bind(this, 1)} id="45"/></td>
                                        <td>Add Customers</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedCustomer[2]}  onClick={this.addToGroupPermission} onChange={this.handleChangeCustomer.bind(this, 2)} id="47"/></td>
                                        <td>Delete Customers</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-5">
                            <InputCheckbox onChange={this.selectAllItem.bind(this)} checked={isAllItemChecked} id="allItem"/> Item Management
                        </div>
                        <div className="col-md-5 col-md-offset-1">
                            <InputCheckbox onChange={this.selectAllRequest.bind(this)} checked={isAllRequestChecked} id="allRequest"/> Item Out Management
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-5">
                            <table className="table table-striped table-bordered table-responsive">
                                <thead className="thead-font-size">
                                    <tr > 
                                        <th className="checkbox-width"><InputCheckbox onChange={this.selectAllItem.bind(this)} checked={isAllItemChecked} id="allItem"/></th>
                                        <th className="col-lg-width">Function</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedItem[0]}  onClick={this.addToGroupPermission} onChange={this.handleChangeItem.bind(this, 0)} id="64"/></td>
                                        <td>List Items</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedItem[1]}  onClick={this.addToGroupPermission} onChange={this.handleChangeItem.bind(this, 1)} id="61"/></td>
                                        <td>Add Items</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedItem[2]}  onClick={this.addToGroupPermission} onChange={this.handleChangeItem.bind(this, 2)} id="63"/></td>
                                        <td>Delete Items</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-5 col-md-offset-1">
                            <table className="table table-striped table-bordered table-responsive">
                                <thead className="thead-font-size">
                                    <tr > 
                                        <th className="checkbox-width"><InputCheckbox onChange={this.selectAllRequest.bind(this)} checked={isAllRequestChecked} id="allRequest"/></th>
                                        <th className="col-lg-width">Function</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedRequest[0]}  onClick={this.addToGroupPermission} onChange={this.handleChangeRequest.bind(this, 0)} id="104"/></td>
                                        <td>List Items Out</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedRequest[1]}  onClick={this.addToGroupPermission} onChange={this.handleChangeRequest.bind(this, 1)} id="101"/></td>
                                        <td>Add Items Out</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedRequest[2]}  onClick={this.addToGroupPermission} onChange={this.handleChangeRequest.bind(this, 2)} id="103"/></td>
                                        <td>Delete Items Out</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-5">
                            <InputCheckbox onChange={this.selectAllReceipt.bind(this)} checked={isAllReceiptChecked} id="allReceipt"/> Item In Management
                        </div>
                        <div className="col-md-5 col-md-offset-1">
                            <InputCheckbox onChange={this.selectAllIisReports.bind(this)} checked={isAllIisReportsChecked} id="allIisReports"/> Reports
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-5">
                            <table className="table table-striped table-bordered table-responsive">
                                <thead className="thead-font-size">
                                    <tr > 
                                        <th className="checkbox-width"><InputCheckbox onChange={this.selectAllReceipt.bind(this)} checked={isAllReceiptChecked} id="allReceipt"/></th>
                                        <th className="col-lg-width">Function</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedReceipt[0]}  onClick={this.addToGroupPermission} onChange={this.handleChangeReceipt.bind(this, 0)} id="100"/></td>
                                        <td>List Items In</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedReceipt[1]}  onClick={this.addToGroupPermission} onChange={this.handleChangeReceipt.bind(this, 1)} id="97"/></td>
                                        <td>Add Items In</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedReceipt[2]}  onClick={this.addToGroupPermission} onChange={this.handleChangeReceipt.bind(this, 2)} id="99"/></td>
                                        <td>Delete Items In</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-5 col-md-offset-1">
                            <table className="table table-striped table-bordered table-responsive">
                                <thead className="thead-font-size">
                                    <tr > 
                                        <th className="checkbox-width"><InputCheckbox onChange={this.selectAllIisReports.bind(this)} checked={isAllIisReportsChecked} id="allIisReports"/></th>
                                        <th className="col-lg-width">Function</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedIisReports[0]}  onClick={this.addToGroupPermission} onChange={this.handleChangeIisReports.bind(this, 0)} id="125"/></td>
                                        <td>Generate Report</td>
                                    </tr>
                                    <tr>
                                        <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedIisReports[1]}  onClick={this.addToGroupPermission} onChange={this.handleChangeIisReports.bind(this, 1)} id="125"/></td>
                                        <td>Print Report</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div>
                        <InputCheckbox onChange={this.selectAllAccount.bind(this)} checked={isAllUserChecked && isAllGroupChecked} id="allAccount"/> Account Management
                    </div>
                    <br /> 
                    <div className="well well-lg access-right-font-size">
                        <div className="row">
                            <div className="col-md-5">
                            <InputCheckbox onChange={this.selectAllUser.bind(this)} checked={isAllUserChecked} id="allUser"/> User Management
                            </div>
                            <div className="col-md-5 col-md-offset-1">
                            <InputCheckbox onChange={this.selectAllGroup.bind(this)} checked={isAllGroupChecked} id="allGroup"/> Group Management
                            </div>
                        </div>
                        <div className="row form-row-padding-top">
                            <div className="col-md-5">
                                <table className="table table-striped table-bordered table-responsive">
                                    <thead className="thead-font-size">
                                        <tr > 
                                        <th className="checkbox-width"><InputCheckbox onChange={this.selectAllUser.bind(this)} checked={isAllUserChecked} id="allUser"/></th>
                                            <th className="col-lg-width">Function</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedUser[0]}  onClick={this.addToGroupPermission} onChange={this.handleChangeUser.bind(this, 0)} id="16"/></td>
                                            <td>List Users</td>
                                        </tr>
                                        <tr>
                                            <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedUser[1]}  onClick={this.addToGroupPermission} onChange={this.handleChangeUser.bind(this, 1)} id="13"/></td>
                                            <td>Add Users</td>
                                        </tr>
                                        <tr>
                                            <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedUser[2]}  onClick={this.addToGroupPermission} onChange={this.handleChangeUser.bind(this, 2)} id="15"/></td>
                                            <td>Delete Users</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="col-md-5 col-md-offset-1">
                                <table className="table table-striped table-bordered table-responsive">
                                    <thead className="thead-font-size">
                                        <tr > 
                                        <th className="checkbox-width"><InputCheckbox onChange={this.selectAllGroup.bind(this)} checked={isAllGroupChecked} id="allGroup"/></th>
                                            <th className="col-lg-width">Function</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedGroup[0]}  onClick={this.addToGroupPermission} onChange={this.handleChangeGroup.bind(this, 0)} id="12"/></td>
                                            <td>List Groups</td>
                                        </tr>
                                        <tr>
                                            <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedGroup[1]}  onClick={this.addToGroupPermission} onChange={this.handleChangeGroup.bind(this, 1)} id="9"/></td>
                                            <td>Add Groups</td>
                                        </tr>
                                        <tr>
                                            <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedGroup[2]}  onClick={this.addToGroupPermission} onChange={this.handleChangeGroup.bind(this, 2)} id="11"/></td>
                                            <td>Delete Groups</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div>
                        <InputCheckbox onChange={this.selectAllCategory.bind(this)} checked={isAllCategoryChecked} id="allCategory"/> System Management
                    </div>
                    <br /> 
                    <div className="well well-lg access-right-font-size">
                        <div className="row">
                            <div className="col-md-5">
                                <InputCheckbox onChange={this.selectAllCategory.bind(this)} checked={isAllCategoryChecked} id="allCategory"/> Category Management
                            </div> 
                        </div>
                        <div className="row form-row-padding-top">
                            <div className="col-md-5">
                                <table className="table table-striped table-bordered table-responsive">
                                    <thead className="thead-font-size">
                                        <tr > 
                                        <th className="checkbox-width"><InputCheckbox onChange={this.selectAllCategory.bind(this)} checked={isAllCategoryChecked} id="allCategory"/></th>
                                            <th className="col-lg-width">Function</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedCategory[0]}  onClick={this.addToGroupPermission} onChange={this.handleChangeCategory.bind(this, 0)} id="44"/></td>
                                            <td>List Categories</td>
                                        </tr>
                                        <tr>
                                            <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedCategory[1]}  onClick={this.addToGroupPermission} onChange={this.handleChangeCategory.bind(this, 1)} id="41"/></td>
                                            <td>Add Categories</td>
                                        </tr>
                                        <tr>
                                            <td><InputCheckbox name="checkboxPermission" checked={this.state.checkedCategory[2]}  onClick={this.addToGroupPermission} onChange={this.handleChangeCategory.bind(this, 2)} id="43"/></td>
                                            <td>Delete Categories</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AddGroupForm;

