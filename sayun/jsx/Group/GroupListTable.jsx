import React from 'react';
import ReactDOM from 'react-dom';
import EmptyTable from '../Utils/EmptyTable.jsx';
import Preloader from '../Utils/Preloader.jsx';
import moment from 'moment';
import Utils from '../Utils/Utils.jsx';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class GroupListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         date: moment(),
         preloader: "",
         empty: "",
         logout: false,
         filter1: ""
      };
   }

   checkAllCheckbox(name) {
      var array = document.getElementsByName(name);
      for(var i=0; i<array.length; i++){
         var cb = document.getElementById(array[i].id);
         cb.checked = document.getElementById('checkAll').checked;
      }
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#groupListTable').html());
      location.href=url
      return false
   }

   componentDidMount(){
      var self = this;
      this.setState({preloader: <Preloader/>});
      $.ajax({
         url: Utils.basePath + "/groups/",
         type:"GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
            400: function (response) {
               alert(JSON.parse(response.responseText).name);
            },
            401: function (response) {
               alert("User is not authorized. Please login.");
               self.setState({logout: true});
            },
            500: function (response) {
               alert("HTTP error 500, please check your connection.");
            }
         },
         success: function(response) {
            const groupList = [];
            for(var i=0; i<response.length; i++){
               var group = {};
               group.checkboxId = response[i].id;
               group.id = response[i].id;
               group.name = response[i].name;
               group.username = response[i].username;
               group.group = response[i].group;
               groupList.push(group);
            }

            if(groupList.length == 0){
               this.setState({empty: <EmptyTable text="Groups" span="2"/>});
            }

            this.setState({response: groupList});
            this.setState({preloader: ""});
         }.bind(this),
         error: function(xhr, status, err) {
         }.bind(this)
     });
   }

   idFormatter(cell, row){
      var link = "/group/id=" + row.id
      return (
         <Link to={link}><u>{cell}</u></Link>
      );
   }

   checkboxFormatter(cell, row){
      return (
         <input type="checkbox" name="checkbox" id={cell} />
      );
   }

   filterShow1(){
      if(this.state.filter1 == ""){
         this.setState({filter1: 'RegexFilter'});
      } else{
         this.setState({filter1: ''});
      }
   }

   render(){
      const preloader = this.state.preloader;
      var filter1 = this.state.filter1;

      if (this.state.logout) {
             return <Redirect push to="/" />;
      }

      return(
         <div className="row">
           <div className="row" id="page-wrapper-inner5">
              <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
           </div>
            <div className="col-md-12" id="groupListTable">
               {preloader}
               <BootstrapTable data={this.state.response} striped hover pagination>
                  <TableHeaderColumn width="10px" dataField='checkboxId' isKey dataFormat={ this.checkboxFormatter }>
                     <input type="checkbox" id="checkAll" onChange={(e) => this.checkAllCheckbox('checkbox')} />
                  </TableHeaderColumn>
                  <TableHeaderColumn dataField='name' width="350px" filter={ { type: filter1 } } dataFormat={ this.idFormatter } dataSort={ true }>Name <i className="fa fa-search search-size" onMouseOver={() => this.filterShow1()}></i></TableHeaderColumn>
               </BootstrapTable>
            </div>
         </div>
      );
    }
}

export default GroupListTable;
