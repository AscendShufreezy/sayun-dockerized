import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import GroupListTable from './GroupListTable.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class GroupListPage extends React.Component {
	constructor(){
		super();
		this.state = {
			logout: false
		}
	}

	deleteGroup(){
	  	var numberOfGroups = document.getElementsByName("checkbox");
	  	var groupsToDelete = [];

	  	for(var i=0; i<numberOfGroups.length;i++){
	  		if(numberOfGroups[i].checked){
	  			groupsToDelete.push(numberOfGroups[i].id);
	  		}
	  	}

	  	if(groupsToDelete.length > 0){
	  		var deleteMessage = confirm("Are you sure you want to delete the selected group/s?");
	  		if (deleteMessage == true) {
	  			var successCounter = 0;
	  			var failCounter = 0;
	  			for(var i=0; i<groupsToDelete.length;i++){
	  				$.ajax({
			            url: Utils.basePath + "/groups/" + groupsToDelete[i] + "/",
			            type:"DELETE",
			            async: false,
			            contentType: "application/json",
			            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
			            statusCode: {
			               400: function (response) {
			                 // alert(JSON.parse(response.responseText).id);
			               },
			               401: function (response) {
			                  alert("User is not authorized. Please login.");
			                  self.setState({logout: true});
			               },
			               500: function (response) {
			                  alert("HTTP error 500, please check your connection.");
			               }
			            },
			            success: function (data) {
			            	successCounter++;
			            },
			            error: function (response) {
			            	failCounter++;
			            }
			        });

			        if(successCounter == groupsToDelete.length){
		  				alert("Deletion of group/s is successful!");
		  				location.reload();
		  			} else if(failCounter == groupsToDelete.length){
	                	alert("Groups with child record cannot be deleted!");
	                } else if((successCounter + failCounter) == groupsToDelete.length){
	                	alert("Groups with child record cannot be deleted. Only selected groups with no child records are deleted.");
	                	location.reload();
	                }
	  			}
		    }
	  	} else{
	  		alert('There are no group/s selected.');
	  	}
    }

    componentWillMount(){
        var group = sessionStorage.getItem('group');    	
        var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

    	if (group != "admin-group" && sessionPermission.indexOf(12) == -1) {
        alert("You are not authorized to access this page. Please login as admin user.");
          this.setState({logout: true});
        } 
    }    

    render(){
    	if (this.state.logout) {
             return <Redirect push to="/" />;
        }

		var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");
        var hide1 = "";
        var hide2 = "";
        if(sessionPermission.indexOf(9) == -1){
        	hide1 = "hidden";
        } if(sessionPermission.indexOf(11) == -1){
        	hide2 = "hidden";
        }

      	return (
	      	<div id="page-wrapper">
		      	<Menu active="Group"/>
		      	<div className="row" id="page-wrapper-inner">
		      		<PageTitle pageTitle="Group List"/>
		      		<div className="row" id="page-wrapper-inner3">
		      			<form>
			      			<GroupListTable />
			      			<ButtonGroup buttons="addDelete" link1="/group/add-group" click2={() => this.deleteGroup()} type1="button" hide1={hide1} hide2={hide2}/>
		      			</form>
		      			<br />
		      		</div>
		      	</div>	
		      	<br />
		    </div>
     	);
   }
}

export default GroupListPage;

