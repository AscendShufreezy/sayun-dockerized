import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Breadcrumb from '../Utils/Breadcrumb.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import AddCategoryForm from './AddCategoryForm.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class AddCategory extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          response: [],
          transfer: false,
          isComplete: false,
          emptyField: "",
          category: {},
          logout: false
       };
    }

    cancelSave() {
        this.setState({transfer: true});
    }

    checkInputs() {
        if(document.getElementById("id").value == ""){
            this.setState({emptyField: "ID"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("type").value == ""){
            this.setState({emptyField: "Type"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("description").value == ""){
            this.setState({emptyField: "Description"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else{
            this.setState({isComplete: true}, () => { 
                var category = {};
                category.id = document.getElementById("id").value;
                category.type = document.getElementById("type").value;
                category.description = document.getElementById("description").value;
                this.setState({category: category}, () => this.saveCategory()); //for async calls
            });
        }
    }

    saveCategory() {
        var self = this;
        var category = this.state.category;
        var data = {"id": category.id,"type": category.type,"description": category.description}
                    
        $.ajax({
            url: Utils.basePath + "/categories/",
            type:"POST",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
                alert("Category saved!");
                self.setState({transfer: true});
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    componentWillMount(){
        var group = sessionStorage.getItem('group');
        var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

        if (group != "admin-group" && sessionPermission.indexOf(41) == -1) {
            alert("You are not authorized to access this page. Please login as admin user.");
            this.setState({logout: true});
        } 
    }

    render(){
        if (this.state.transfer) {
            return <Redirect push to="/category" />;
        }
        
        if (this.state.logout) {
           return <Redirect push to="/" />;
        }

        return (
            <div id="page-wrapper">
                <Menu active="Category"/>
                <div className="row" id="page-wrapper-inner">
                    <Breadcrumb page="add-category"/>
                    <PageTitle pageTitle="Add Category"/>
                    <div className="row" id="page-wrapper-inner3">
                        <form>
                            <AddCategoryForm />
                            <div className="row">
                                <div className="col-md-3 pull-right">
                                  <div className="pull-right">
                                    <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
                                    <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
                                  </div>
                                </div>
                            </div>
                        </form>
                        <br />
                    </div>
                </div>
                <br />  
            </div>
      );
   }
}

export default AddCategory;