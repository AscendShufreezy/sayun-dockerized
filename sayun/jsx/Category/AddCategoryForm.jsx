import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';
import moment from 'moment';
import Input from '../Utils/Input.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class AddCategoryForm extends React.Component {
    render(){
      return (
        <div>
            <div className="row">
                <div className="col-md-6">
                    <div className="row">
                        <div className="col-md-4">
                            Type*
                        </div>
                        <div className="col-md-8">
                            <select className="form-control" id="type">
                                <option selected disabled> </option>
                                <option>Fixed Deduction</option>
                                <option>Temporary Deduction</option>
                                <option>Fixed Addition</option>
                                <option>Temporary Addition</option>
                            </select>
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-4">
                            ID*
                        </div>
                        <div className="col-md-8">
                            <Input id="id" type="text"/> 
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-4">
                            Description*
                        </div>
                        <div className="col-md-8">
                            <Input id="description" type="text"/> 
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    );
  }
}

export default AddCategoryForm;

