import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';
import Utils from '../Utils/Utils.jsx';

import Breadcrumb from '../Utils/Breadcrumb.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import CategoryListTable from './CategoryListTable.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class CategoryListPage extends React.Component {
	constructor(props) {
	    super(props);
	    this.state = {
	    	logout: false
	    }
	}

	deleteCategory(){
	  	var numberOfCategories = document.getElementsByName("checkbox");
	  	var categoriesToDelete = [];

	  	for(var i=0; i<numberOfCategories.length;i++){
	  		if(numberOfCategories[i].checked){
	  			categoriesToDelete.push(numberOfCategories[i].id);
	  		}
	  	}

	  	if(categoriesToDelete.length > 0){
	  		var deleteMessage = confirm("Are you sure you want to delete the selected category/s?");
	  		if (deleteMessage == true) {
	  			var successCounter = 0;
	  			for(var i=0; i<categoriesToDelete.length;i++){
	  				$.ajax({
			            url: Utils.basePath + "/categories/" + categoriesToDelete[i] + "/",
			            type:"DELETE",
			            contentType: "application/json",
			            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
			            statusCode: {
				               400: function (response) {
				                  alert(JSON.parse(response.responseText).id);
				               },
				               401: function (response) {
				                  alert("User is not authorized. Please login.");
				                  self.setState({logout: true});
				               },
				               500: function (response) {
				                  alert("HTTP error 500, please check your connection.");
				               }
						},

			            success: function (data) {
			            	successCounter++;

			            	if(successCounter == categoriesToDelete.length){
				  				alert("Deletion of category/s is successful!");
				  				location.reload();
				  			}
			            },
			            error: function (response) {
			                alert("Failed !!!");
			            }
			        });
	  			}
		    }
	  	} else{
	  		alert('There are no category/s selected.');
	  	}
    }

    componentWillMount(){
    	var group = sessionStorage.getItem('group');
    	var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

    	if (group != "admin-group" && sessionPermission.indexOf(44) == -1) {
    		alert("You are not authorized to access this page. Please login as admin user.");
    		this.setState({logout: true});
    	} 
    }    

    render(){
    	if (this.state.logout) {
             return <Redirect push to="/" />;
        }

        var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");
        var hide1 = "";
        var hide2 = "";
        if(sessionPermission.indexOf(41) == -1){
        	hide1 = "hidden";
        } if(sessionPermission.indexOf(43) == -1){
        	hide2 = "hidden";
        }

      	return (
	      	<div id="page-wrapper">
		      	<Menu active="Category"/>
		      	<div className="row" id="page-wrapper-inner">
		      		<PageTitle pageTitle="Category List"/>
		      		<div className="row" id="page-wrapper-inner3">
		      			<form>
			      			<CategoryListTable />
			      			<ButtonGroup buttons="addDelete" link1="/category/add-category" click2={() => this.deleteCategory()} type1="button" hide1={hide1} hide2={hide2}/>
		      			</form>
		      			<br />
		      		</div>
		      	</div>	
		      	<br />
		    </div>
     	);
   }
}

export default CategoryListPage;

