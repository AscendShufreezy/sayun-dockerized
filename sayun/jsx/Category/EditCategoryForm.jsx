import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Input from '../Utils/Input.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class EditCategoryForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            type: props.category.type,
            description: props.category.description
        };

        this.handleDescription = this.handleDescription.bind(this);
        this.handleType = this.handleType.bind(this);
    }

    handleDescription(e) {
        this.setState({
            description: e.target.value
        });
    }

    handleType(e) {
        this.setState({
            type: e.target.value
        });
    }

    render(){
        var category = this.props.category;
        var type = this.state.type;
        console.log(type);

        return (
            <div>
            <div className="row">
                <div className="col-md-6">
                    <div className="row">
                        <div className="col-md-4">
                            Type*
                        </div>
                        <div className="col-md-8">
                            <select className="form-control" id="type" value={type} onChange={this.handleType} >
                                <option value="Fixed Deduction">Fixed Deduction</option>
                                <option value="Temporary Deduction">Temporary Deduction</option>
                                <option value="Fixed Addition">Fixed Addition</option>
                                <option value="Temporary Addition">Temporary Addition</option>
                            </select>
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-4">
                            ID*
                        </div>
                        <div className="col-md-8">
                        <input id="id" type="text" className="form-control" value={category.id} disabled />
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-4">
                            Description*
                        </div>
                        <div className="col-md-8">
                            <Input id="description" defaultValue={this.state.description} onChange={this.handleDescription} type="text"/> 
                        </div>
                    </div>
                </div>
            </div> 
            </div>
        );
    }
}   

export default EditCategoryForm;

