import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import EditReceiptForm from './EditReceiptForm.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';;

class ReceiptDetails extends React.Component {
  constructor(props) {
      super(props);

      this.state = {
        response: [],
        transfer: false,
        isComplete: false,
        emptyField: "",
        receipt: {},
        logout: false
     };
  }

  cancelSave() {
	  this.setState({transfer: true});
  }

  checkInputs() {
  	  if(document.getElementById("receiptNo").value == "" && document.getElementById("invoiceNo").value == ""){
          this.setState({emptyField: "Receipt No"}, () => { 
              alert(this.state.emptyField + " should not be empty.");
              return "";
          });
      } else if(document.getElementById("item").value == ""){
          this.setState({emptyField: "Item"}, () => { 
              alert(this.state.emptyField + " should not be empty.");
              return "";
          });
      } else if(document.getElementById("date").value == ""){
          this.setState({emptyField: "Date"}, () => { 
              alert(this.state.emptyField + " should not be empty.");
              return "";
          });
      } else if(document.getElementById("supplier").value == ""){
          this.setState({emptyField: "Supplier"}, () => { 
              alert(this.state.emptyField + " should not be empty.");
              return "";
          });
      } else if(document.getElementById("warehouse").value == ""){
          this.setState({emptyField: "Warehouse"}, () => { 
              alert(this.state.emptyField + " should not be empty.");
              return "";
          });
      } else if(document.getElementById("unitCost").value == ""){
          this.setState({emptyField: "Unit Cost"}, () => { 
              alert(this.state.emptyField + " should not be empty.");
              return "";
          });
      } else if(document.getElementById("qty").value == ""){
          this.setState({emptyField: "Quantity"}, () => { 
              alert(this.state.emptyField + " should not be empty.");
              return "";
          });
      } else{
    		this.setState({isComplete: true}, () => { 
    			this.updateReceipt();
			});
  	}
  }  

  updateReceipt() {
      var url = window.location.href;
      var recId = url.split('=').pop();
      var receipt = this.state.receipt;
      receipt.id = recId;
      receipt.date = document.getElementById("date").value;
      receipt.receiptNo = document.getElementById("receiptNo").value;
      receipt.invoiceNo = document.getElementById("invoiceNo").value;      
      receipt.item = document.getElementById("item").value;
      receipt.supplier = document.getElementById("supplier").value;
      receipt.warehouse = document.getElementById("warehouse").value;
      receipt.unitCost = document.getElementById("unitCost").value;
      receipt.qty = document.getElementById("qty").value;
      receipt.total = (receipt.qty * receipt.unitCost).toFixed(2);
     
      var data = {"id": receipt.id, "date": receipt.date,"receipt_number": receipt.receiptNo, "invoice_number": receipt.invoiceNo, 
      "name": receipt.item, "supplier": receipt.supplier, "warehouse": receipt.warehouse,"unit_cost": receipt.unitCost, "quantity": receipt.qty, "total_cost": receipt.total}

      var self = this;
	    $.ajax({
          url: Utils.basePath + "/receipts/" + recId + "/",
          type: "PUT",
          contentType: "application/json",
          data: JSON.stringify(data),
          headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
          statusCode: {
             400: function (response) {
                alert(JSON.parse(response.responseText));
             },
             401: function (response) {
                alert("User is not authorized. Please login.");
                self.setState({logout: true});
             },
             500: function (response) {
                alert("HTTP error 500, please check your connection.");
             }
          },
          success: function (data) {
              alert("Item In updated!");
              self.setState({transfer: true});
          },
          error: function (response) {
              console.log(response);
          }
      });
  }

  componentWillMount(){
    var group = sessionStorage.getItem('group');
        var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

    if (group != "admin-group" && group != 'hr-group' && group != 'warehouse-group' && group != 'pos-group' && sessionPermission.indexOf(100) == -1) {
        alert("You are not authorized to access this page. Please login as admin user.");
        this.setState({logout: true});
    }
  } 

	render(){
    if (this.state.logout) {
             return <Redirect push to="/" />;
    }

		if (this.state.transfer) {
		    return <Redirect push to="/receipt" />;
		}

    var url = window.location.href;
    var id = url.split('=').pop();
    var self = this;
    var receipt = {};

    $.ajax({
        url: Utils.basePath + "/receipts/" + id + "/",
        type:"GET",
        contentType: "application/json",
        async : false,
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        statusCode: {
           400: function (response) {
              alert(JSON.parse(response.responseText).id);
           },
           401: function (response) {
              alert("User is not authorized. Please login.");
              self.setState({logout: true});
           },
           500: function (response) {
              alert("HTTP error 500, please check your connection.");
           }
        },

        success: function (data) {
            receipt.id = data.id;
            receipt.date = data.date;
            receipt.receiptNo = data.receipt_number;
            receipt.invoiceNo = data.invoice_number;
            receipt.item = data.name;
            receipt.supplier = data.supplier;
            receipt.warehouse = data.warehouse;
            receipt.unitCost = data.unit_cost;
            receipt.qty = data.quantity;
            receipt.total = (receipt.qty * receipt.unitCost).toFixed(2);
        }
    });

  	return (
    	<div id="page-wrapper">
      	<Menu active="Receipt"/>
      	<div className="row" id="page-wrapper-inner">
      		<Breadcrumb page="edit-receipt"/>
      		<PageTitle pageTitle="Item In"/>
      		<div className="row" id="page-wrapper-inner3">
      			<form>
	      			<EditReceiptForm receipt={receipt}/>
	      			<div className="row">
			            <div className="col-md-3 pull-right">
			              <div className="pull-right">
			                <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
			                <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
			              </div>
			            </div>
			        </div>
		        </form>
      			<br />
      		</div>
      	</div>	
        <br />  
      </div>
    );
  }
}

export default ReceiptDetails;

