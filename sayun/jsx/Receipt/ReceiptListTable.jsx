import React from 'react';
import ReactDOM from 'react-dom';
import Preloader from '../Utils/Preloader.jsx';
import EmptyTable from '../Utils/EmptyTable.jsx';
import Utils from '../Utils/Utils.jsx';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class ReceiptListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         warehouse: "",
         preloader: "",
         empty: "",
         logout: false,
         filter1: "",
         filter2: "",
         filter3: "",
         filter4: "",
         filter5: "",
         filter6: "",
         filter7: ""
      };
   }

   checkAllCheckbox(name) {
      var array = document.getElementsByName(name);
      for(var i=0; i<array.length; i++){
         var cb = document.getElementById(array[i].id);
         cb.checked = document.getElementById('checkAll').checked;
      }
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#receiptListTable').html());
      location.href=url
      return false
   }

   componentWillMount(){
      var self = this;
      this.setState({preloader: <Preloader/>});
      $.ajax({
         url: Utils.basePath + "/receipts/",
         type:"GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
            400: function (response) {
               alert(JSON.parse(response.responseText));
            },
            401: function (response) {
               alert("User is not authorized. Please login.");
               self.setState({logout: true});
            },
            500: function (response) {
               alert("HTTP error 500, please check your connection.");
            }
         },
         success: function(response) {
            const receiptList = [];
            for(var i=0; i<response.length; i++){
               var receipt = {};
               this.getWarehouseName(response[i].warehouse);

               receipt.checkboxId = response[i].id;
               receipt.id = response[i].id;
               receipt.name = this.getItemName(response[i].name);
               receipt.date = response[i].date;
               receipt.supplier  = response[i].supplier;
               receipt.warehouse = this.state.warehouse;
               receipt.qty = response[i].quantity;
               receipt.unitCost = response[i].unit_cost;
               receipt.total = response[i].total_cost;
               receipt.receiptNo = response[i].receipt_number;

               if(sessionStorage.getItem('group') == "pos-group"){
                   if(sessionStorage.getItem('warehouse') == response[i].warehouse){
                     receiptList.push(receipt);
                   }
                 } else{
                   receiptList.push(receipt);
                 }
            }

            if(receiptList.length == 0){
               self.setState({empty: <EmptyTable text="Receipts" span="9"/>});
            }

            self.setState({response: receiptList});
            self.setState({preloader: ""});
         }.bind(this),
         error: function(xhr, status, err) {
         }.bind(this)
      })
   }

   getItemName(id){
      var self = this;
      var name = "";
      $.ajax({
         url: Utils.basePath + "/items/" + id + "/",
         type: "GET",
         contentType: "application/json",
         async: false,
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
         },

         success: function(response) {
            name = response.name;
         }
      });
      return name;
   }

   getWarehouseName(warehouseId){
      var self = this;
      $.ajax({
         url: Utils.basePath + "/warehouses/" + warehouseId + "/",
         type: "GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
         },

         async: false,
         success: function(response) {
            self.setState({warehouse: response.name});
         }
      });
   }

   idFormatter(cell, row){
      var link = "/receipt/id=" + row.id;

      return (
         <Link to={link}><u>{row.name}</u></Link>
      );
   }

   amountFormatter(cell, row){
      var amount = cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return (
         amount
      );
   }

   checkboxFormatter(cell, row){
      return (
         <input type="checkbox" name="checkbox" id={cell} />
      );
   }

   filterShow1(){
      if(this.state.filter1 == ""){
         this.setState({filter1: 'RegexFilter'});
      } else{
         this.setState({filter1: ''});
      }
   }

   filterShow2(){
      if(this.state.filter2 == ""){
         this.setState({filter2: 'RegexFilter'});
      } else{
         this.setState({filter2: ''});
      }
   }

   filterShow3(){
      if(this.state.filter3 == ""){
         this.setState({filter3: 'RegexFilter'});
      } else{
         this.setState({filter3: ''});
      }
   }

   filterShow4(){
      if(this.state.filter4 == ""){
         this.setState({filter4: 'RegexFilter'});
      } else{
         this.setState({filter4: ''});
      }
   }

   filterShow5(){
      if(this.state.filter5 == ""){
         this.setState({filter5: 'RegexFilter'});
      } else{
         this.setState({filter5: ''});
      }
   }

   filterShow6(){
      if(this.state.filter6 == ""){
         this.setState({filter6: 'RegexFilter'});
      } else{
         this.setState({filter6: ''});
      }
   }

   filterShow7(){
      if(this.state.filter7 == ""){
         this.setState({filter7: 'RegexFilter'});
      } else{
         this.setState({filter7: ''});
      }
   }

   render(){
      const preloader = this.state.preloader;
      if (this.state.logout) {
             return <Redirect push to="/" />;
      }

      var filter1 = this.state.filter1;
      var filter2 = this.state.filter2;
      var filter3 = this.state.filter3;
      var filter4 = this.state.filter4;
      var filter5 = this.state.filter5;
      var filter6 = this.state.filter6;
      var filter7 = this.state.filter7;

      return(
         <div className="row">
           <div className="row" id="page-wrapper-inner5">
              <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
           </div>
            <div className="col-md-12" id="receiptListTable">
               {preloader}
               <BootstrapTable data={this.state.response} striped hover pagination>
                  <TableHeaderColumn width="20px" dataField='checkboxId' isKey dataFormat={ this.checkboxFormatter }>
                     <input type="checkbox" id="checkAll" onChange={(e) => this.checkAllCheckbox('checkbox')} />
                  </TableHeaderColumn>
                  <TableHeaderColumn dataField='receiptNo' width="120px" filter={ { type: filter1 } } dataFormat={ this.idFormatter } dataSort={ true }>Item Name <i className="fa fa-search search-size" onMouseOver={() => this.filterShow1()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='date' width="150px" filter={ { type: filter2 } }  dataSort={ true }>Date <i className="fa fa-search search-size" onMouseOver={() => this.filterShow2()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='supplier' width="200px" filter={ { type: filter3} } dataSort={ true }>Supplier <i className="fa fa-search search-size" onMouseOver={() => this.filterShow3()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='warehouse' width="200px" filter={ { type: filter4 } } dataSort={ true }>Warehouse <i className="fa fa-search search-size" onMouseOver={() => this.filterShow4()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='qty' width="100px" filter={ { type: filter5 } } dataSort={ true }>Qty <i className="fa fa-search search-size" onMouseOver={() => this.filterShow5()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='unitCost' width="100px" filter={ { type: filter6 } } dataSort={ true }>Unit Cost <i className="fa fa-search search-size" onMouseOver={() => this.filterShow6()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='total' width="100px" filter={ { type: filter7 } } dataFormat={ this.amountFormatter }  dataSort={ true }>Total <i className="fa fa-search search-size" onMouseOver={() => this.filterShow7()}></i></TableHeaderColumn>
               </BootstrapTable>
            </div>
         </div>
      );
    }
}

export default ReceiptListTable;
