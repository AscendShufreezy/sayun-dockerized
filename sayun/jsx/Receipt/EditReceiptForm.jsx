import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Input from '../Utils/Input.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class EditReceiptForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            id: props.receipt.id,
            receiptNo: props.receipt.receiptNo,
            invoiceNo: props.receipt.invoiceNo,
            item: props.receipt.item,
            warehouse: props.receipt.warehouse,
            supplier: props.receipt.supplier,
            qty: props.receipt.qty,
            unitCost: props.receipt.unitCost,
            items: [],
            warehouses: [],
            date: moment(props.receipt.date, "MM/DD/YYYY"),
            logout: false
        };

        this.handleReceiptNo = this.handleReceiptNo.bind(this);
        this.handleInvoiceNo = this.handleInvoiceNo.bind(this);
        this.handleItem = this.handleItem.bind(this);
        this.handleWarehouse = this.handleWarehouse.bind(this);
        this.handleSupplier = this.handleSupplier.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleQty = this.handleQty.bind(this);
        this.handleUnitCost = this.handleUnitCost.bind(this);
    }

    handleReceiptNo(e) {
        this.setState({
            receiptNo: e.target.value
        });
    }    

    handleInvoiceNo(e) {
        this.setState({
            invoiceNo: e.target.value
        });
    }   

    handleItem(e) {
        this.setState({
            item: e.target.value
        });
    }    

    handleWarehouse(e) {
        this.setState({
            warehouse: e.target.value
        });
    }    

    handleSupplier(e) {
        this.setState({
            supplier: e.target.value
        });
    }    

    handleChange(date) {
        this.setState({
            date: date
        });
    }

    handleQty(e) {
        this.setState({
            qty: e.target.value
        });
    }

    handleUnitCost(e) {
        this.setState({
            unitCost: e.target.value
        });
    }

    componentWillMount(){
        var self = this;
        $.ajax({
            url: Utils.basePath + "/items/",
            type:"GET",
            contentType: "application/json",
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
                const itemList = [];
                for(var i=0;i<data.length;i++){
                    itemList.push(data[i]);
                }
                self.setState({items: itemList});
            },
            error: function (response) {
                console.log(response);
            }
        });

        $.ajax({
            url: Utils.basePath + "/warehouses/",
            type:"GET",
            contentType: "application/json",
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
                const warehouseList = [];
                for(var i=0;i<data.length;i++){
                    warehouseList.push(data[i]);
                }
                self.setState({warehouses: warehouseList});
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    render(){
        if (this.state.logout) {
             return <Redirect push to="/" />;
        }

        const itemsResult = this.state.items.map((items) =>
            <option key={items.name} value={items.id}>{items.name}</option>
        );       

        const warehousesResult = this.state.warehouses.map((warehouses) =>
            <option key={warehouses.id} value={warehouses.id}>{warehouses.name}</option>
        );

        var warehouse = this.props.warehouse;
        var total = "0.00";
        if(this.state.qty != "" && this.state.unitCost != ""){ //4 is cashier-group id
           total = (this.state.qty * this.state.unitCost).toFixed(2);
           total = total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        var addItemLink = <div><i className="fa fa-plus-circle"></i><Link to="/item/add-item" target="_blank"><u> Add New Item</u></Link></div>;
        if(sessionStorage.getItem('group') == "pos-group"){
            addItemLink = "";
        }

        return (
            <div>
                <div className="row" id="page-wrapper-inner3">
                    <div className="row form-row-padding-top">
                        <div className="col-conf-3">
                            ID*
                        </div>
                        <div className="col-md-3">
                            <input id="id" type="text" className="form-control" value={this.state.id} disabled="disabled" />
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-conf-3">
                            Supplier Receipt No
                        </div>
                        <div className="col-md-3">
                            <Input id="receiptNo" type="text" defaultValue={this.state.receiptNo} onChange={this.handleReceiptNo}/>
                        </div>
                        <div className="col-md-1 col-md-offset-2">
                            Warehouse*
                        </div>
                        <div className="col-md-3">
                            <select className="form-control" id="warehouse" value={this.state.warehouse} onChange={this.handleWarehouse}>
                                {warehousesResult}
                            </select>
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-conf-3">
                            Supplier Invoice No
                        </div>
                        <div className="col-md-3">
                            <Input id="invoiceNo" type="text" defaultValue={this.state.invoiceNo} onChange={this.handleInvoiceNo}/>
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-conf-3">
                            Item*
                        </div>
                        <div className="col-md-3">
                            <select className="form-control" id="item" value={this.state.item} onChange={this.handleItem}>
                                {itemsResult}
                            </select>
                        </div>
                        <div className="col-md-2 form-row-padding-top">
                            {addItemLink}
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-conf-3">
                            Date Received*
                        </div>
                        <div className="col-md-3">
                            <DatePicker selected={this.state.date} onChange={this.handleChange} id="date" className="form-control"/>
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-conf-3">
                            Supplier*
                        </div>
                        <div className="col-md-4">
                            <Input id="supplier" type="text" defaultValue={this.state.supplier} onChange={this.handleSupplier}/>
                        </div>
                     </div>
                    <div className="row form-row-padding-top">
                        <div className="col-conf-3">
                            Unit Cost*
                        </div>
                        <div className="col-conf-3">
                            <Input id="unitCost" type="text" defaultValue={this.state.unitCost} onChange={this.handleUnitCost}/>
                        </div>
                        <div className="col-md-1">
                            <div className="pull-right">Qty*</div>
                        </div>
                        <div className="col-conf-3">
                            <Input id="qty" type="text" defaultValue={this.state.qty} onChange={this.handleQty}/>
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-conf-3">
                            Total
                        </div>
                        <div className="col-conf-3">
                            <p id="total">{total}</p>
                        </div>
                    </div>
                </div>
                <br />
            </div>
        );
    }
}   

export default EditReceiptForm;