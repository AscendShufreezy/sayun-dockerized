import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Breadcrumb from '../Utils/Breadcrumb.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import EditCustomerForm from '../Customer/EditCustomerForm.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class CustomerDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          response: [],
          transfer: false,
          isComplete: false,
          emptyField: "",
          customer: {},
          logout: false
       };
    }

    cancelSave() {
		this.setState({transfer: true});
    }

    checkInputs() {
        if(document.getElementById("name").value == ""){
            this.setState({emptyField: "Name"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else{
            this.setState({isComplete: true}, () => { 
                this.updateCustomer();
            });
        }
    } 

    updateCustomer() {
        var url = window.location.href;
        var custId = url.split('=').pop();
        var customer = this.state.customer;
        var description;

        if(document.getElementById("description") == null){
            description = "";
        } else{
            description = document.getElementById("description").value;
        }

        customer.name = document.getElementById('name').value;
        customer.description = description;
       
        var data = {"id": custId, "name": customer.name,"description": customer.description, "type": "Truck"}
        var self = this;

		$.ajax({
            url: Utils.basePath + "/customers/" + custId + "/",
            type: "PUT",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },

            success: function (data) {
                alert("Customer updated!");
                self.setState({transfer: true});
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    componentWillMount(){
      var group = sessionStorage.getItem('group');
      var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

      if (group != "admin-group" && group != 'hr-group' && group != 'warehouse-group' && sessionPermission.indexOf(48) == -1) {
          alert("You are not authorized to access this page. Please login as admin user.");
          this.setState({logout: true});
      }

      var self = this;
      var url = window.location.href;
      var custId = url.split('=').pop();
      var customer = {};

      $.ajax({
          url: Utils.basePath + "/customers/" + custId + "/",
          type:"GET",
          contentType: "application/json",
          async : false,
          headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
          statusCode: {
             400: function (response) {
                alert(JSON.parse(response.responseText).id);
             },
             401: function (response) {
                alert("User is not authorized. Please login.");
                self.setState({logout: true});
             },
             500: function (response) {
                alert("HTTP error 500, please check your connection.");
             }
          },

          success: function (data) {
              customer.id = custId;
              customer.name = data.name;
              customer.type = data.type;
              customer.description = data.description;
              self.setState({customer: customer});
          }
      });
    }

	render(){
		if (this.state.transfer) {
		    return <Redirect push to="/customer/type=Employee" />;
		}

    if (this.state.logout) {
       return <Redirect push to="/" />;
    }

    var customer = this.state.customer;        
    var customerType = customer.type;

    var test = ""
    if(customer.type != "Employee"){
        var test = <div className="row"><div className="col-md-3 pull-right"><div className="pull-right"><button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button><button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button></div></div></div>
    }
    
  	return (
    	<div id="page-wrapper">
      	<Menu active="Customer"/>
      	<div className="row" id="page-wrapper-inner">
      		<Breadcrumb page="edit-customer" type={customerType} />
      		<PageTitle pageTitle="Customer"/>
      		<div className="row" id="page-wrapper-inner3">
      			<form>
	      			<EditCustomerForm customer={customer}/>
                        {test}
		        </form>
      			<br />
      		</div>
      	</div>	
        <br />  
      </div>
    );
  }
}

export default CustomerDetails;

