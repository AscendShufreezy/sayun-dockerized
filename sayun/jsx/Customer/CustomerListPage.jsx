import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Breadcrumb from '../Utils/Breadcrumb.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import Utils from '../Utils/Utils.jsx';
import CustomerListTable from './CustomerListTable.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class CustomerListPage extends React.Component {
    constructor() {
        super();

        var url = window.location.href;
        var type = url.split('=').pop();

        this.state = {
          selectedType: type,
          logout: false
        };

        this.handleSelectedType = this.handleSelectedType.bind(this);
    }

    handleSelectedType(e) {
        this.setState({selectedType: e.target.value});
        if(e.target.value == "Employee"){
        	window.history.pushState("object", "test", "/#/customer/type=Employee");
        } else{
        	window.history.pushState("object", "test", "/#/customer/type=Truck");
        }
    }

	deleteCustomer(){
	  	var numberOfCustomers = document.getElementsByName("checkbox");
	  	var customersToDelete = [];

	  	for(var i=0; i<numberOfCustomers.length;i++){
	  		if(numberOfCustomers[i].checked){
	  			customersToDelete.push(numberOfCustomers[i].id);
	  		}
	  	}

	  	if(customersToDelete.length > 0){
	  		var deleteMessage = confirm("Are you sure you want to delete the selected customer/s?");
	  		if (deleteMessage == true) {
	  			var successCounter = 0;
	  			var failCounter = 0;
	  			for(var i=0; i<customersToDelete.length;i++){
	  				$.ajax({
			            url: Utils.basePath + "/customers/" + customersToDelete[i] + "/",
			            type:"DELETE",
			            async: false,
			            contentType: "application/json",
			            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
			            success: function (data) {
			            	successCounter++;			            
			            },
			            error: function (response) {
			            	failCounter++;			            	
			            }
			        });

			        if(successCounter == customersToDelete.length){
		  				alert("Deletion of customer/s is successful!");
		  				location.reload();
		  			} else if(failCounter == customersToDelete.length){
	                	alert("Customers with child record cannot be deleted!");
	                } else if((successCounter + failCounter) == customersToDelete.length){
	                	alert("Customers with child record cannot be deleted. Only selected customers with no child records are deleted.");
	                	location.reload();
	                }
	  			}
		    }
	  	} else{
	  		alert('There are no customer/s selected.');
	  	}
    }

    componentWillMount(){
    	var group = sessionStorage.getItem('group');
    	var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

    	if (group != "admin-group" && group != "hr-group" && group != "warehouse-group" && sessionPermission.indexOf(48) == -1) {
    		alert("You are not authorized to access this page. Please login as admin user.");
    		this.setState({logout: true});
    	} 
    }    
    
    render(){
    	if (this.state.logout) {
             return <Redirect push to="/" />;
        }

        var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");
        var hide1 = "";
        var hide2 = "";
        if(sessionPermission.indexOf(45) == -1){
        	hide1 = "hidden";
        } if(sessionPermission.indexOf(47) == -1){
        	hide2 = "hidden";
        }

      	return (
	      	<div id="page-wrapper">
		      	<Menu active="Customer"/>
		      	<div className="row" id="page-wrapper-inner">
		      		<PageTitle pageTitle="Customers List"/>
		      		<div className="row">
                        <div className="col-conf-3 form-row-padding-top">
                            Customer Type
                        </div>
                        <div className="col-md-2">
                            <select id="type" className="form-control" value={this.state.selectedType} onChange={this.handleSelectedType}>
                                <option value="Employee">Employee</option>
                                <option value="Truck">Truck</option>
                            </select>
                        </div>
                    </div>
                    <br />
		      		<div className="row" id="page-wrapper-inner3">
		      			<form>
			      			<CustomerListTable filter={this.state.selectedType}/>
			      			<ButtonGroup buttons="addDelete" link1="/customer/add-customer" click2={() => this.deleteCustomer()} type1="button" hide1={hide1} hide2={hide2}/>
		      			</form>
		      			<br />
		      		</div>
		      	</div>	
		      	<br />
		    </div>
     	);
   }
}

export default CustomerListPage;

