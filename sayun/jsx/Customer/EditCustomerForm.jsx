import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';
import Input from '../Utils/Input.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class EditCustomerForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            name: props.customer.name,
            description: props.customer.description,
            logout: false
        };

        this.handleDescription = this.handleDescription.bind(this);
        this.handleName = this.handleName.bind(this);
    }

    handleDescription(e) {
        this.setState({
            description: e.target.value
        });
    }

    handleName(e) {
        this.setState({
            name: e.target.value
        });
    }

    render(){
        if (this.state.logout) {
             return <Redirect push to="/" />;
        }

        var customer = this.props.customer;
        if(customer.type == 'Truck'){
            return (
                <div>
                    <div className="row" id="page-wrapper-inner3">
                        <div className="row">
                            <div className="col-md-2">
                                Customer ID*
                            </div>
                            <div className="col-md-3 form-control-height">
                                {customer.id}
                            </div>
                        </div>
                        <div className="row form-row-padding-top">
                            <div className="col-md-2">
                                Name*
                            </div>
                            <div className="col-md-3">
                                <Input id="name" type="text" defaultValue={this.state.name} onChange={this.handleName}/>
                            </div>
                        </div>
                        <div className="row form-row-padding-top">
                            <div className="col-md-2">
                                Description
                            </div>
                            <div className="col-md-10">
                                <Input id="description" type="text" defaultValue={this.state.description} onChange={this.handleDescription}/>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>
            );
        } else{ //if type is Employee
            var self = this;
            var employee = {};
            $.ajax({
                url: Utils.basePath + "/employees/" + customer.id + "/",
                type: "GET",
                contentType: "application/json",
                async: false,
                headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
                statusCode: {
                       400: function (response) {
                          alert(JSON.parse(response.responseText).id);
                       },
                       401: function (response) {
                          alert("User is not authorized. Please login.");
                          self.setState({logout: true});
                       },
                       500: function (response) {
                          alert("HTTP error 500, please check your connection.");
                       }
                },

                success: function (data) {
                    employee.title = data.title;
                    employee.gender = data.gender;
                },
                error: function (response) {
                    console.log(response);
                }
            });

            return (
                <div>
                    <div className="row" id="page-wrapper-inner3">
                        <div className="row">
                            <div className="col-md-2">
                                Customer ID
                            </div>
                            <div className="col-md-3 form-control-height">
                                {customer.id}
                            </div>
                            <div className="col-md-1 col-md-offset-2">
                                Title
                            </div>
                            <div className="col-md-3">
                                {employee.title}
                            </div>
                        </div>
                        <div className="row form-row-padding-top">
                            <div className="col-md-2">
                                Name
                            </div>
                            <div className="col-md-3">
                                {customer.name}
                            </div>
                            <div className="col-md-1 col-md-offset-2">
                                Gender
                            </div>
                            <div className="col-md-3">
                                {employee.gender}
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}   

export default EditCustomerForm;

