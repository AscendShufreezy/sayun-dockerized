import React from 'react';
import ReactDOM from 'react-dom';
import EmptyTable from '../Utils/EmptyTable.jsx';
import Preloader from '../Utils/Preloader.jsx';
import Utils from '../Utils/Utils.jsx';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class CustomerListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         filter: props.filter,
         test: "",
         showCheckbox: true,
         preloader: <Preloader />,
         logout: false,
         filter1: "",
         filter2: ""
      };
   }

   checkAllCheckbox(name) {
      var array = document.getElementsByName(name);
      for(var i=0; i<array.length; i++){
         var cb = document.getElementById(array[i].id);
         cb.checked = document.getElementById('checkAll').checked;
      }
   }

   componentWillReceiveProps(nextProps) {
      if(nextProps.filter != this.state.filter){
         this.setState({
            filter: nextProps.filter
          });
      }
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#customerListTable').html());
      location.href=url
      return false
   }

   componentWillMount(){
      var self = this;
      $.ajax({
         url: Utils.basePath + "/customers/?sort=id",
         type:"GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
         },

         success: function(response) {
            const customerList = [];
            for(var i=0; i<response.length; i++){
               if(response[i].type == self.state.filter){
                  var customer = {};
                  customer.checkboxId = response[i].id;
                  customer.id = response[i].id;
                  customer.type = response[i].type;
                  customer.name = response[i].name;
                  customerList.push(customer);
               }
            }
            if(customerList.length == 0){
               this.setState({test: <EmptyTable text="Customers" span="4"/>});
            }
            this.setState({response: customerList});
            this.setState({preloader: ""});
         }.bind(this),
         error: function(xhr, status, err) {
         }.bind(this)
      });
   }

   componentDidUpdate(){
      var self = this;
      $.ajax({
         url: Utils.basePath + "/customers/?sort=id",
         type:"GET",
         contentType: "application/json",
         async: false,
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
         },

         success: function(response) {
            const customerList = [];
            for(var i=0; i<response.length; i++){
               if(response[i].type == self.state.filter){
                  var customer = {};
                  customer.checkboxId = response[i].id;
                  customer.id = response[i].id;
                  customer.type = response[i].type;
                  customer.name = response[i].name;
                  customerList.push(customer);
               }
            }
            if(JSON.stringify(this.state.response) !=  JSON.stringify(customerList)){
               if(customerList.length == 0){
                  this.setState({test: <EmptyTable text="Customers" span="4"/>});
               }

               this.setState({response: customerList});
            }
         }.bind(this)
      });
   }

   idFormatter(cell, row){
      var link = "/customer/id=" + cell;
      return (
         <Link to={link}><u>{cell}</u></Link>
      );
   }

   checkboxFormatter(cell, row){
      var self = this;

      var isShowCheckbox;
      if(row.type == 'Employee'){
         isShowCheckbox = null;
      } else{
         isShowCheckbox = <input type="checkbox" name="checkbox" id={cell} />;
      }

      return (
         isShowCheckbox
      );
   }

   filterShow1(){
      if(this.state.filter1 == ""){
         this.setState({filter1: 'RegexFilter'});
      } else{
         this.setState({filter1: ''});
      }
   }

   filterShow2(){
      if(this.state.filter2 == ""){
         this.setState({filter2: 'RegexFilter'});
      } else{
         this.setState({filter2: ''});
      }
   }

   render(){
      const preloader = this.state.preloader;
      if (this.state.logout) {
             return <Redirect push to="/" />;
      }

      var filter1 = this.state.filter1;
      var filter2 = this.state.filter2;
      var checkbox = null;
      if(this.state.filter != "Employee"){
         checkbox = <input type="checkbox" id="checkAll" onChange={(e) => this.checkAllCheckbox('checkbox')} />
      }

      var hidden = false;
      if(this.state.filter == "Employee"){
         hidden = true;
      }

      return(
         <div className="row">
           <div className="row" id="page-wrapper-inner5">
              <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
           </div>
            <div className="col-md-12" id="customerListTable">
               {preloader}
               <BootstrapTable data={this.state.response} striped hover pagination>
                  <TableHeaderColumn width="20px" hidden={hidden} dataField='checkboxId' dataFormat={ this.checkboxFormatter }>
                     <input type="checkbox" id="checkAll" onChange={(e) => this.checkAllCheckbox('checkbox')} />
                  </TableHeaderColumn>
                  <TableHeaderColumn dataField='id' width="150px" isKey filter={ { type: filter1 } } dataFormat={ this.idFormatter } dataSort={ true }>Customer ID <i className="fa fa-search search-size" onMouseOver={() => this.filterShow1()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='name' width="200px" filter={ { type: filter2 } }  dataSort={ true }>Name <i className="fa fa-search search-size" onMouseOver={() => this.filterShow2()}></i></TableHeaderColumn>
               </BootstrapTable>



            </div>
         </div>
      );
    }
}

export default CustomerListTable;
