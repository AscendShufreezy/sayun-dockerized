import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';
import moment from 'moment';
import Utils from '../Utils/Utils.jsx';
import SearchBarcodeModal from './SearchBarcodeModal.jsx';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import AddRequestForm from './AddRequestForm.jsx';
import UserForm from './UserForm.jsx';
import PosTable from './PosTable.jsx';
import JSZipUtils from '../../node_modules/jszip-utils/dist/jszip-utils.min.js';
import JSZip from '../../node_modules/jszip/dist/jszip.js';

import '../../node_modules/docxtemplater/build/docxtemplater.js';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

import '../../node_modules/jszip/dist/jszip.js';
import '../../node_modules/jszip-utils/dist/jszip-utils.min.js';
import '../../docxtemplater-latest.js';

import '../../node_modules/jszip/vendor/FileSaver.js';

class AddRequestPos extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          cash: 0.00,
          currectDateTime: moment(),
          emptyField: "",
          idCounter: 0,
          invalidField: "",
          isComplete: false,
          item: "", 
          itemList: "",
          logout: false,
          posList: "",
          response: [],
          total: 0,
          totalQty: 0,
          transfer: false,
          unitPrice: 0,
          loanChecked: "",
          cashChecked: "",
          hashIdBarcode: {},
          invoiceNumber: "",
          invoiceDate: "",
          test: [{}],
          counter: 0
       };

      this.handleItem = this.handleItem.bind(this);
      this.handleCash = this.handleCash.bind(this);
      this.handleType = this.handleType.bind(this);
      this.handleHidden = this.handleHidden.bind(this);
    }

    handleType(e) {
      var status = e.target.value;
      var pendingChecked = "";
      var processedChecked = "";

      if(status == "Loan"){
          this.setState({loanChecked: "checked"});
          this.setState({cashChecked:""});
          this.setState({status:"Loan"});
          this.setState({cash: 0.00});

      } else{
          this.setState({loanChecked: ""});
          this.setState({cashChecked: "checked"});
          this.setState({status: "Cash"});
      }
    }

    handleItem(e) {
      this.setState({item: e.target.value});
      document.getElementById('qty').value = "";

      var unitPrice = 0;
      var self = this;
      $.ajax({
        url: Utils.basePath + "/items/" + e.target.value + "/",
        type:"GET",
        contentType: "application/json",
        async : false,
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        success: function (data) {
            unitPrice = data.unit_price;
        }
      });

      this.setState({unitPrice: unitPrice});
    }

    handleCash(e) {
      this.setState({cash: e.target.value});
    }

    handleHidden(e) {
      this.setState({hidden: e.target.value});
    }

    addToCart(unitcost, qty){
      var posList = [];
      var total = this.state.total;
      var totalQty = this.state.totalQty;

      if(this.state.posList != ""){
        posList = this.state.posList;
      }

      var pos = {};
      pos.id = this.state.idCounter;
      pos.item = document.getElementById('item').value;
      pos.itemName = document.getElementById('item').options[document.getElementById('item').selectedIndex].text;
      pos.unitPrice = document.getElementById('unitPrice').value;
      pos.qty = qty;
      pos.status = "Draft";
      pos.total = (pos.unitPrice * pos.qty).toFixed(2);
      pos.date = this.formatDate(moment());

      total = total + parseFloat(pos.total);
      totalQty = totalQty + parseInt(pos.qty);
      posList.push(pos);

      var incCounter = parseInt(this.state.idCounter) + 1;
      this.setState({total: total});
      this.setState({totalQty: totalQty});
      this.setState({posList: posList});
      this.setState({idCounter: incCounter});
    }

    cancelSave() {
      this.setState({transfer: true});
    }

    checkInputs() {
      var payType = "Cash";
      if (this.state.loanChecked == "checked") {
        payType = "Loan";
      }

      if (document.getElementById("cash").value == 0 && payType == "Cash") {
        alert("Please input a non-zero amount for cash value.");
      } else if (document.getElementById("cash").value < this.state.total && payType == "Cash") {
        alert("Cash is less than the total amount.");
      } else {
        this.saveRequest();
      }

    }

    checkQty(unitPrice) {
      if(document.getElementById("qty").value == ""){
          this.setState({emptyField: "Name"}, () => { 
              alert("Quantity should not be empty.");
              return "";
          });
      } else{
          //this.setState({isComplete: true}, () => { 
            var qty = document.getElementById("qty").value;
            this.addToCart(unitPrice, qty);
         // });
      }
    }

    deleteItems(){
      var posList = this.state.posList;
      var numberOfItems = document.getElementsByName("checkbox");
      var itemsToDelete = [];

      for(var i=0; i<numberOfItems.length;i++){
        if(numberOfItems[i].checked){
          itemsToDelete.push(numberOfItems[i].id);
        }
      }

      if(itemsToDelete.length > 0){
        var deleteMessage = confirm("Are you sure you want to delete the selected request item/s?");
        if (deleteMessage == true) {
          var successCounter = 0;
          var total = this.state.total;
          for(var i=0; i<itemsToDelete.length;i++){
              var index = -1;
              for(var x=0; x<posList.length; x++){
                if(posList[x].id == itemsToDelete[i]){
                  total = total - posList[x].total;
                  index = x;
                  break;
                }
              }

              if(index != -1)
                posList.splice(index, 1);
          }
          this.setState({total: total});         
          this.setState({posList: posList});
          alert("Deletion of request item/s is successful!");
        }
      } else{
        alert('There are no request item/s selected.');
      }

    }

    formatDate(selectedDay){
      var today = new Date(selectedDay);
      var month = today.getMonth() + 1;
      var date = today.getDate();

      if(month < 10){
         month = "0" + month;
      }if(date < 10){
         date = "0" + date;
      }

      return month + "/" + date + "/" + today.getFullYear();
    }

    formatTime(){
      var date = new Date();
      var description = "am";
      var hours = date.getHours();
      var mins = date.getMinutes();

      if(hours > 12){
        hours = hours - 12;
        description = "pm";
      } else if(hours == 12 && mins > 0){
        description = "pm";
      } 

      if(mins < 10){
        mins = "0" + mins;
      }

      return hours + ":" + mins + " " + description;
    }

    getFormattedDate(input){
      var pattern=/(.*?)\/(.*?)\/(.*?)$/;
      var result = input.replace(pattern,function(match,p1,p2,p3){
          var months=['January','February','March','April','May','June','July','August','September','October','November','December'];
          return months[(p1-1)] + " " + (p2)+", " + p3;
      });
      return result;
    }

    saveRequest() {
      var self = this;
      var payType = "Cash";
      if(this.state.loanChecked == "checked"){
        payType = "Loan";
      }
      var posList = this.state.posList;
      var loanTotal = (this.state.total).toFixed(2);
      var dateFormat = this.formatDate(moment());
      var total = this.state.total;
      var loanId = "";

      var request = {};
      request.warehouse = document.getElementById('warehouse').value;
      request.customer = document.getElementById('customer').value;
      request.date = this.formatDate(moment());
      request.total = (this.state.total).toFixed(2);

      var data = {"warehouse": request.warehouse, "customer": request.customer, 
                  "date": request.date, "total": request.total, "status": "Issued"}

      if(total != 0){
        var self2 = this;
        $.ajax({
          url: Utils.basePath + "/requests/",
          type:"POST",
          async: false,
          contentType: "application/json",
          data: JSON.stringify(data),
          headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
          statusCode: {
             400: function (response) {
                alert(JSON.parse(response.responseText).date);
             },
             401: function (response) {
                alert("User is not authorized. Please login.");
                self.setState({logout: true});
             },
             500: function (response) {
                alert("HTTP error 500, please check your connection.");
             }
          },
          success: function(data) {
            var self = this;
            loanId = data.id;
            for(var i=0; i<posList.length; i++){
              posList[i].request = data.id;
              
              var item = {"request": posList[i].request, "item": posList[i].item, "item_name": posList[i].itemName,
                  "unit_price": posList[i].unitPrice, "quantity": posList[i].qty, "status": "Issued",
                  "total": posList[i].total, "date": posList[i].date}

              $.ajax({
                url: Utils.basePath + "/item-requests/",
                type:"POST",
                async: false,
                contentType: "application/json",
                data: JSON.stringify(item),
                headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
                success: function(data) {
                  
                }
              });
            }

            //if payment type is Loan, automatically add a new temporary deduction and payroll schedule
            if(payType == "Loan"){
              var self = this;
              var tempDeduction = {};
              tempDeduction.employee = document.getElementById('customer').value;
              tempDeduction.category = "loan";
              tempDeduction.date = dateFormat;
              tempDeduction.description = "Loan from POS";
              tempDeduction.total = loanTotal;

              var data = {"employee": tempDeduction.employee, "loan_id": loanId, "category": tempDeduction.category,"date": tempDeduction.date,"description": tempDeduction.description, 
                          "total": tempDeduction.total, "processed": 0.00, "pending": tempDeduction.total}
                          
              $.ajax({
                url: Utils.basePath + "/temporary-deductions/",
                type:"POST",
                async: false,
                contentType: "application/json",
                data: JSON.stringify(data),
                headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
                success: function(data) {
                  var self = this;
                  var tempId = data.id;
                  var payroll = {};
                  payroll.employee = data.employee;
                  payroll.type = "deduction";
                  payroll.typeId = tempId;
                  payroll.date = dateFormat;
                  payroll.payrollDate = dateFormat; //this.getPayrollDate(payroll.date);
                  payroll.amount = data.total;

                  var data = [{'employee': payroll.employee ,'type': payroll.type, 'typeid': payroll.typeId, 'date': payroll.date, 
                  'status': "Pending", 'payroll_date': payroll.payrollDate, 'amount': payroll.amount}];

                  $.ajax({
                    url: Utils.basePath + "/payroll-schedules/",
                    type:"POST",
                    async: false,
                    contentType: "application/json",
                    data: JSON.stringify(data),
                    headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
                    success: function(data) {

                    }
                  });
                }
              });
            }
            var printAsk = confirm('Request and Item Requests successfully saved. Do you want to print receipt?');
            if(printAsk){
              var date = new Date();
              var monthNames = ["January", "February", "March", "April", "May", "June","July", "August","September", "October", "November", "December"];
              var time = self2.formatTime();
              var dateTime = monthNames[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear() + " " + time;
              var dataList = [];      
              var payType = "CASH"; 

              if(self2.state.loanChecked == "checked"){
                payType = "LOAN";
              } 

              //loop posList
              var posList2 = self2.state.posList;
              for(var i=0; i<posList2.length; i++){
                var item = {};
                item.name = posList2[i].itemName;
                item.price = posList2[i].unitPrice;
                item.qty = posList2[i].qty;
                item.total = posList2[i].total;

                var sample = self2.test(item.name, item.price, item.qty, item.total);
                dataList.push(sample)
              }

              localStorage.setItem('dateTime', dateTime);
              localStorage.setItem('cashier', sessionStorage.getItem('username'));
              localStorage.setItem('customer', $("#customer>option:selected").html());
              localStorage.setItem('total', (self2.state.total).toFixed(2));
              localStorage.setItem('payType', payType);
              localStorage.setItem('itemsBought', JSON.stringify(dataList));

              var print = window.open("sample-ver2.html");     
              setTimeout(function () { print.close(); }, 5000);
            } 
            location.reload();
          },
          error: function (response) {
            console.log(response);
          }
        });
      }  else{
        alert('Items bought should not be empty!');
      }
    }

    componentWillMount(){
      window.name="wew";
      var group = sessionStorage.getItem('group');
      if (group != "admin-group" && group != "hr-group" && group != "warehouseapprover-group" && group != "pos-group") {
        alert("You are not authorized to access this page. Please login as admin user.");
        this.setState({logout: true});
      } 

      this.setState({loanChecked: "checked"});
      var self = this;
      $.ajax({
        url: Utils.basePath + "/items/",
        type:"GET",
        contentType: "application/json",
        async : false,
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        success: function (data) {
            var itemList = [];
            var hashIdBarcode = self.state.hashIdBarcode;

            for(var i=0; i<data.length; i++){
              var item = {};
              item.id = data[i].id;
              item.name = data[i].name;
              item.unitPrice = data[i].unit_price;
              item.barcode = data[i].barcode;

              //set hashMap and push to itemList
              hashIdBarcode[data[i].id] = data[i].barcode;
              itemList.push(item);
            }
            self.setState({itemList: itemList});
            self.setState({hashIdBarcode: hashIdBarcode});
            self.setState({item: itemList[0].id});
            self.setState({unitPrice: itemList[0].unitPrice});
        }
      });
    }

    test(name, price, qty, total){
      var test = this.state.test;
      test = {'itemName': name ,'itemPrice': price, 'itemQuantity': qty, 'itemTotal': total};
      return test;
    }

    formatTime() {
      var date = new Date();
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12;
      minutes = minutes < 10 ? ('0'+minutes) : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm.toUpperCase();
      return strTime;
    }

    printHtml(){
      var date = new Date();
      var monthNames = ["January", "February", "March", "April", "May", "June","July", "August","September", "October", "November", "December"];
      var time = this.formatTime();
      var dateTime = monthNames[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear() + " " + time;
      var dataList = [];      
      var payType = "CASH"; 

      if(this.state.loanChecked == "checked"){
        payType = "LOAN";
      } 

      //loop posList
      var posList = this.state.posList;
      for(var i=0; i<posList.length; i++){
        var item = {};
        item.name = posList[i].itemName;
        item.price = posList[i].unitPrice;
        item.qty = posList[i].qty;
        item.total = posList[i].total;

        var sample = this.test(item.name, item.price, item.qty, item.total);
        dataList.push(sample)
      }

      localStorage.setItem('dateTime', dateTime);
      localStorage.setItem('cashier', sessionStorage.getItem('username'));
      localStorage.setItem('customer', $("#customer>option:selected").html());
      localStorage.setItem('total', (this.state.total).toFixed(2));
      localStorage.setItem('payType', payType);
      localStorage.setItem('itemsBought', JSON.stringify(dataList));

      var print = window.open("sample-ver2.html");     
      setTimeout(function () { print.close(); }, 5000);
    }

    printReceipt(){
      document.getElementById('printHtml').click();

       /*var obj = {};
      var date = new Date();
      var nextDate = new Date(Date.now() + (30 * 24 * 60 * 60 * 1000));
      var dataList = [];

      //loop posList
      var posList = this.state.posList;
      for(var i=0; i<posList.length; i++){
        var item = {};
        item.name = posList[i].itemName;
        item.price = posList[i].unitPrice;
        item.qty = posList[i].qty;
        item.total = posList[i].total;

        var sample = this.test(item.name, item.price, item.qty, item.total);
        dataList.push(sample)
      }
      
      obj.restaurantName = "Sayun Canteen";
      obj.restaurantLocation = "";
      obj.totalTax = "0.00";
      obj.cashierName = sessionStorage.getItem('username');
      obj.invoiceNumber = itemOutId;
      obj.invoiceDate = (date.getMonth()+1) + "/" + date.getDate() + "/" + date.getFullYear();
      obj.dueDate = obj.invoiceDate;
      obj.employeeId = document.getElementById('customer').value;
      obj.totalPrice = this.state.total;
      obj.totalGST = this.state.total;
      obj.totalQuantity = (this.state.posList).length;
      obj.itemsBought = JSON.stringify(dataList);
      obj = JSON.stringify(obj);

      if(this.state.total != 0){
        var url = window.location.href;
        url = url.substring(0, url.indexOf('/#/'));
        location.reload();
        window.open(`${url}/#/print-receipt/?data=${obj}`);
      } else{
        alert('Items bought should not be empty!');
      }*/
    }

    componentDidMount(){
      $('#searchbarcodeModal').on('shown.bs.modal', function() {
        $(this).find('input#barcode').focus();
        $(this).find('input#barcode').val("");
      });
    }

    render(){
      var time = this.formatTime();
      var date = this.formatDate(this.state.currectDateTime);
      date = this.getFormattedDate(date);

      var itemResult = "";
      var unitPrice = (this.state.unitPrice);
      var total = (this.state.total).toFixed(2);
      var change = (0).toFixed(2);
      var disabled = "";
      var showDom = this.state.showDom;
      var hiddenDom = "";

      if (this.state.logout) {
        return <Redirect push to="/" />;
      }

      else if (this.state.transfer) {
          return <Redirect push to="/request" />;
      }
     
      if(this.state.itemList != ""){
          itemResult = this.state.itemList.map((item) =>
             <option key={item.id} value={item.id}>{item.name}</option>
          ); 
      }

      if(this.state.cash != 0.00){
        change = (this.state.cash - total).toFixed(2);
      }

      if(this.state.loanChecked == "checked"){
        disabled = "disabled";
      }

      return (
        <div id="page-wrapper">
            <Menu active="Request"/>
            <div className="row" id="page-wrapper-inner">
                <PageTitle pageTitle="Item Out"/>
                <Breadcrumb page="add-request"/>
                <br />
                <div className="row" id="page-wrapper-inner3">
                    <div className="row">
                        <div className="col-md-2">  
                          {date} {time}
                        </div>
                    </div>
                </div>
                <br />
                <UserForm />
                <div className="row" id="page-wrapper-inner5">
                  <div className="row" id="page-wrapper-inner-full">
                    <br />
                    <div className="col-md-7">
                      <div className="row" id="page-wrapper-inner5">
                        <div className="row" id="page-wrapper-inner-full">
                          <div className="row">
                            <br />
                            <div className="col-md-1 form-row-padding-top">
                                Item
                            </div>
                            <div className="col-md-6">
                              <select className="form-control" id="item" value={this.state.item} onChange={this.handleItem}>
                                  <option selected disabled> </option>
                                  {itemResult}
                              </select>
                            </div>
                            <div className="input-group col-md-5 ">
                                <button type="button" className="btn btn-default btn-primary btn-search-barcode" data-toggle="modal" data-target="#searchbarcodeModal"><i className="fa fa-search"></i> Search Barcode</button>
                                <SearchBarcodeModal hashIdBarcode={this.state.hashIdBarcode}/>
                            </div>
                          </div>
                          <div className="row form-row-padding-top">
                            <div className="col-md-1 form-row-padding-top">
                                Qty
                            </div>
                            <div className="col-md-2">
                                <input type="text" className="form-control" id="qty"/>
                            </div>
                            <div className="col-md-2 col-md-offset-1 form-row-padding-top">
                                Unit Price: 
                            </div>
                            <div className="col-md-2">
                                <input type="text" className="form-control input-basic" id="unitPrice" value={unitPrice} disabled/>
                            </div>
                          </div>
                          <div className="row form-row-padding-top">
                            <div className="col-md-12">
                                <button className="btn btn-default btn-primary pull-right" onClick={() => this.checkQty(this.state.unitPrice)}><i className="fa fa-shopping-cart"></i> Add to Cart</button>
                            </div>
                          </div>
                          <br />
                          <div className="row" id="page-wrapper-inner3">
                              <PosTable posList={this.state.posList} total={total} totalQty={this.state.totalQty}/>
                          </div>
                          <br />
                          <div className="row form-row-padding-top">
                            <div className="col-md-12">
                                <button className="btn btn-default btn-warning pull-right" onClick={() => this.deleteItems()}>Delete</button>
                            </div>
                          </div>
                          <br/>
                        </div>
                        <br />
                      </div>
                    </div>
                    {/*second column*/}
                    <div className="col-md-4 pull-right">
                      <div className="row">
                        Total
                      </div>
                      <div className="row pos-total">
                          {total}&nbsp;
                      </div>

                      <div className="row form-row-padding-top">
                        <br/><br/>Payment Type
                      </div>
                      <div className="row payment-mover2">
                          <div className="form-row-padding-top col-md-5 col-md-offset-2 payment-mover">
                             <input type="radio" name="paymentType" value="Loan" id="btnLoan" checked={this.state.loanChecked} onChange={this.handleType}/> Loan
                          </div>
                      </div>
                      <div className="row">
                          <div className="form-row-padding-top col-md-5 col-md-offset-2 payment-mover">
                             <input type="radio" name="paymentType" value="Cash" id="btnCash" checked={this.state.cashChecked} onChange={this.handleType}/> Cash
                          </div>
                      </div>

                      <div className="row form-row-padding-top">
                        <br/><br/><br/>Cash Payment
                      </div>
                      <div className="row form-row-padding-top">
                          <div className="form-row-padding-top col-md-3 col-md-offset-1">
                             Cash
                          </div>
                          <div className="col-md-6">
                             <input type="text" className="form-control" id="cash" value={this.state.cash} onChange={this.handleCash} disabled={disabled} />
                          </div>
                      </div>
                      <div className="row">
                          <div className="form-row-padding-top col-md-3 col-md-offset-1">
                             Change
                          </div>
                          <div className="col-md-6 form-row-padding-top">
                             {change}
                          </div>
                      </div>

                      <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                      <div className="row ">
                          <div className="col-md-12">
                              <div className="pull-right">
                                  <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
                                  <button type="button" className="btn btn-default btn-primary" onClick={() => this.printReceipt()}>Print Receipt</button>
                                  <button type="hiidden" className="btn btn-default btn-primary" id="printHtml" onClick={() => this.printHtml()}>Wew</button>
                              </div>
                          </div>
                      </div>
                      <div className="row form-row-padding-top">
                          <div className="col-md-12">
                              <div className="pull-right">
                                  <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
                              </div>
                          </div>
                      </div>
                      <br />
                    </div>
                  </div>
                  <br />
                </div>
            </div>
            <br />  
        </div>
      );
   }
}

export default AddRequestPos;