import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import EditRequestForm from './EditRequestForm.jsx';
import RequestItemPage from '../RequestItem/RequestItemPage.jsx';
import Utils from '../Utils/Utils.jsx';
import moment from 'moment';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';;

class RequestDetails extends React.Component {
  constructor(props) {
      super(props);

      this.state = {
        response: [],
        transfer: false,
        isComplete: false,
        emptyField: "",
        invalidField: "",
        request: {},
        logout: false
     };
  }

  cancelSave() {
	  this.setState({transfer: true});
  }

  checkInputs() {
  	if(document.getElementById("date").value == ""){
          this.setState({emptyField: "Date"}, () => { 
              alert(this.state.emptyField + " should not be empty.");
              return "";
          });
      } else if(!moment(document.getElementById("date").value,"MM/DD/YYYY",true).isValid()){
          this.setState({invalidField: "Date"}, () => { 
              alert(this.state.invalidField + " should be a valid date.");
              return "";
          });
      } else if(document.getElementById("customer").value == ""){
          this.setState({emptyField: "Customer"}, () => { 
              alert(this.state.emptyField + " should not be empty.");
              return "";
          });
      } else{
    		this.setState({isComplete: true}, () => { 
    			this.updateRequest();
			});
  	}
  }  

  updateRequest() {
    var url = window.location.href;
    var reqId = url.split('=').pop();
    var request = this.state.request;
    var warehouse;
    if(document.getElementById("warehouse") == null){
        warehouse = "";
    } else{
        warehouse = document.getElementById("warehouse").value;
    }

    request.id = reqId;
    request.date = document.getElementById("date").value;
    request.customer = document.getElementById("customer").value;
    request.warehouse = warehouse;
    
    var data = {"warehouse": request.warehouse, "customer": request.customer, 
                "date": request.date, "total": 0.00, "status": "Draft"}
      
    var self = this;
    $.ajax({
        url: Utils.basePath + "/requests/" + reqId + "/",
        type: "PUT",
        contentType: "application/json",
        data: JSON.stringify(data),
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        statusCode: {
           400: function (response) {
              alert(JSON.parse(response.responseText));
           },
           401: function (response) {
              alert("User is not authorized. Please login.");
              self.setState({logout: true});
           },
           500: function (response) {
              alert("HTTP error 500, please check your connection.");
           }
        },
        success: function (data) {
            alert("Item Out updated!");
            self.setState({transfer: true});
        },
        error: function (response) {
            console.log(response);
        }
    });
  }

  componentWillMount(){
    var group = sessionStorage.getItem('group');
    var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

    if (group != "admin-group" && group != "hr-group" && group != "warehouseapprover-group" && group != "warehouse-group" && group != "pos-group" && sessionPermission.indexOf(104) == -1) {
      alert("You are not authorized to access this page. Please login as admin user.");
      this.setState({logout: true});
    } 
  }

	render(){
    if (this.state.logout) {
             return <Redirect push to="/" />;
    }

		if (this.state.transfer) {
		    return <Redirect push to="/request" />;
		}

    var url = window.location.href;
    var id = url.split('=').pop();
    var self = this;
    var request = {};

   $.ajax({
      url: Utils.basePath + "/requests/" + id + "/",
      type:"GET",
      contentType: "application/json",
      async : false,
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
         400: function (response) {
            alert(JSON.parse(response.responseText).id);
         },
         401: function (response) {
            alert("User is not authorized. Please login.");
            self.setState({logout: true});
         },
         500: function (response) {
            alert("HTTP error 500, please check your connection.");
         }
      },

      success: function (data) {
          request.id = data.id;
          request.date = data.date;
          request.customer = data.customer;
          request.warehouse = data.warehouse;
      }
    });

    return (
    	<div id="page-wrapper">
      	<Menu active="Request"/>
      	<div className="row" id="page-wrapper-inner">
      		<Breadcrumb page="edit-request"/>
      		<PageTitle pageTitle="Item Out"/>
      		<div className="row" id="page-wrapper-inner3">
      			<form>
	      			<EditRequestForm request={request}/>
	      			<div className="row">
			            <div className="col-md-3 pull-right">
			              <div className="pull-right">
			                <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
			                <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
			              </div>
			            </div>
			        </div>
		        </form>
            <hr />
            <RequestItemPage request={request.id}/>
      		</div>
      	</div>	
        <br />  
	    </div>
    );
  }
}

export default RequestDetails;

