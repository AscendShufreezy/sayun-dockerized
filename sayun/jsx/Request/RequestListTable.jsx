import React from 'react';
import ReactDOM from 'react-dom';
import Preloader from '../Utils/Preloader.jsx';
import EmptyTable from '../Utils/EmptyTable.jsx';
import Utils from '../Utils/Utils.jsx';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../vendor/morrisjs/morris.css';

import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class RequestListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         request: "",
         warehouse: "",
         customer: "",
         preloader: "",
         empty: "",
         logout: false,
         filter1: "",
         filter2: "",
         filter3: "",
         filter4: "",
         filter5: "",
         filter6: "",
         filter7: ""
      };
   }

   checkAllCheckbox(name) {
      var array = document.getElementsByName(name);
      for(var i=0; i<array.length; i++){
         var cb = document.getElementById(array[i].id);
         cb.checked = document.getElementById('checkAll').checked;
      }
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#requestListTable').html());
      location.href=url
      return false
   }

   componentDidMount(){
     var self = this;
     this.setState({preloader: <Preloader/>});
     $.ajax({
        url: Utils.basePath + "/requests/",
        type:"GET",
        contentType: "application/json",
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        statusCode: {
           400: function (response) {
              alert(JSON.parse(response.responseText));
           },
           401: function (response) {
              alert("User is not authorized. Please login.");
              self.setState({logout: true});
           },
           500: function (response) {
              alert("HTTP error 500, please check your connection.");
           }
        },
        success: function(response) {
           const requestList = [];
           for(var i=0; i<response.length; i++){
              var request = {};
              this.getWarehouseName(response[i].warehouse);
              this.getCustomer(response[i].customer);

              request.checkboxId = response[i].id;
              request.id = response[i].id;
              request.customer = this.state.customer.name;
              request.customerType  = this.state.customer.type;
              request.date = response[i].date;
              request.warehouse = this.state.warehouse;
              request.total = response[i].total;
              request.status = response[i].status;

              if(sessionStorage.getItem('group') == "pos-group"){
                if(sessionStorage.getItem('warehouse') == response[i].warehouse){
                  requestList.push(request);
                }
              } else{
                requestList.push(request);
              }
           }

           if(requestList.length == 0){
              self.setState({empty: <EmptyTable text="Requests" span="8"/>});
           }

           self.setState({response: requestList});
           self.setState({preloader: ""});
        }.bind(this),
        error: function(xhr, status, err) {
        }.bind(this)
     })
   }

   getWarehouseName(warehouseId){
      var self = this;
      $.ajax({
         url: Utils.basePath + "/warehouses/" + warehouseId + "/",
         type: "GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
         },

         async: false,
         success: function(response) {
            self.setState({warehouse: response.name});
         }
      });
 }

 getCustomer(id){
    var self = this;
    $.ajax({
       url: Utils.basePath + "/customers/" + id + "/",
       type: "GET",
       contentType: "application/json",
       headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
       statusCode: {
             400: function (response) {
                alert(JSON.parse(response.responseText).id);
             },
             401: function (response) {
                alert("User is not authorized. Please login.");
                self.setState({logout: true});
             },
             500: function (response) {
                alert("HTTP error 500, please check your connection.");
             }
       },
       async: false,
       success: function(response) {
          self.setState({customer: response});
      }
    });
   }

   idFormatter(cell, row){
      var link = "/request/id=" + cell;
      return (
         <Link to={link}><u>{cell}</u></Link>
      );
   }

   totalFormatter(cell, row){
    var total = (cell).toFixed(2);
    var amount = total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return (
       amount
    );
   }


   checkboxFormatter(cell, row){
      var group = sessionStorage.getItem('group');
      var input;
      var disabled = "";

      if(group == "warehouseapprover-group"){
         disabled = "disabled";
      }

      var isShowCheckbox;
      if(group == "warehouseapprover-group"){
        isShowCheckbox = null;
      } else{
         isShowCheckbox = <input type="checkbox" name="checkbox" id={cell} disabled={disabled} />;
       }

        return (
         isShowCheckbox
       );
   }

   filterShow1(){
      if(this.state.filter1 == ""){
         this.setState({filter1: 'RegexFilter'});
      } else{
         this.setState({filter1: ''});
      }
   }

   filterShow2(){
      if(this.state.filter2 == ""){
         this.setState({filter2: 'RegexFilter'});
      } else{
         this.setState({filter2: ''});
      }
   }

   filterShow3(){
      if(this.state.filter3 == ""){
         this.setState({filter3: 'RegexFilter'});
      } else{
         this.setState({filter3: ''});
      }
   }

   filterShow4(){
      if(this.state.filter4 == ""){
         this.setState({filter4: 'RegexFilter'});
      } else{
         this.setState({filter4: ''});
      }
   }

   filterShow5(){
      if(this.state.filter5 == ""){
         this.setState({filter5: 'RegexFilter'});
      } else{
         this.setState({filter5: ''});
      }
   }

   filterShow6(){
      if(this.state.filter6 == ""){
         this.setState({filter6: 'RegexFilter'});
      } else{
         this.setState({filter6: ''});
      }
   }

  filterShow7(){
      if(this.state.filter7 == ""){
         this.setState({filter7: 'RegexFilter'});
      } else{
         this.setState({filter7: ''});
      }
   }

   render(){
    const preloader = this.state.preloader;
     if (this.state.logout) {
       return <Redirect push to="/" />;
    }

    var filter1 = this.state.filter1;
    var filter2 = this.state.filter2;
    var filter3 = this.state.filter3;
    var filter4 = this.state.filter4;
    var filter5 = this.state.filter5;
    var filter6 = this.state.filter6;
    var filter7 = this.state.filter7;

    var checkbox = null;
    var group = sessionStorage.getItem('group');

    if(group != "warehouseapprover-group"){
       checkbox = <input type="checkbox" id="checkAll" onChange={(e) => this.checkAllCheckbox('checkbox')} />
    }

    return(
       <div className="row">
         <div className="row" id="page-wrapper-inner5">
            <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
         </div>
          <div className="col-md-12" id="requestListTable">
             {preloader}
             <BootstrapTable data={this.state.response} striped hover pagination>
                <TableHeaderColumn width="25px" dataField='checkboxId' dataFormat={ this.checkboxFormatter }>
                   {checkbox}
                </TableHeaderColumn>
                <TableHeaderColumn dataField='id' isKey width="100px" filter={ { type: filter1 } } dataFormat={ this.idFormatter } dataSort={ true }>Item Out ID <i className="fa fa-search search-size" onMouseOver={() => this.filterShow1()}></i></TableHeaderColumn>
                <TableHeaderColumn dataField='date' width="150px" filter={ { type: filter2 } }  dataSort={ true }>Item Out Date <i className="fa fa-search search-size" onMouseOver={() => this.filterShow2()}></i></TableHeaderColumn>
                <TableHeaderColumn dataField='customer' width="250px" filter={ { type: filter3 } } dataSort={ true }>Customer Name <i className="fa fa-search search-size" onMouseOver={() => this.filterShow3()}></i></TableHeaderColumn>
                <TableHeaderColumn dataField='customerType' width="200px" filter={ { type: filter4 } } dataSort={ true }>Customer Type <i className="fa fa-search search-size" onMouseOver={() => this.filterShow4()}></i></TableHeaderColumn>
                <TableHeaderColumn dataField='warehouse' width="200px" filter={ { type: filter5} } dataSort={ true }>Warehouse <i className="fa fa-search search-size" onMouseOver={() => this.filterShow5()}></i></TableHeaderColumn>
                <TableHeaderColumn dataField='total' width="100px" filter={ { type: filter6 } } dataFormat={ this.totalFormatter }  dataSort={ true }>Total <i className="fa fa-search search-size" onMouseOver={() => this.filterShow6()}></i></TableHeaderColumn>
                <TableHeaderColumn dataField='status' width="100px" filter={ { type: filter7 } } dataSort={ true }>Status <i className="fa fa-search search-size" onMouseOver={() => this.filterShow7()}></i></TableHeaderColumn>
             </BootstrapTable>
          </div>
       </div>
    );
  }
}

export default RequestListTable;
