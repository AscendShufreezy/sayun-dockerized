import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class Request extends React.Component {
   render() {
      var request = this.props.request;
      var linkRequest = "/request/id=" + request.id;
      var total = (request.total).toFixed(2);
      var input;
      var group = sessionStorage.getItem('group');

      if(group == "warehouseapprover-group"){
         input = <td><input type="checkbox" name="checkbox" id={request.checkboxId} disabled /></td>
      } else{
         input = <td><input type="checkbox" name="checkbox" id={request.checkboxId} /></td>
      }

      var isShowCheckbox;
      if(group == "warehouseapprover-group"){
        isShowCheckbox = null;
      } else{
         isShowCheckbox = <td>{input}</td>;
      }

      return (
         <tr> 
            {isShowCheckbox}
            <td><Link to={linkRequest}><u>{request.id}</u></Link></td>
            <td>{request.date}</td>
            <td>{request.customer}</td>
            <td>{request.customerType}</td>
            <td>{request.warehouse}</td>
            <td>{total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
            <td>{request.status}</td>
         </tr>

      ); 
   }
}

export default Request;