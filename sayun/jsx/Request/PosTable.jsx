import React from 'react';
import ReactDOM from 'react-dom';
import Request from './Request.jsx';
import Preloader from '../Utils/Preloader.jsx';
import EmptyTable from '../Utils/EmptyTable.jsx';
import Utils from '../Utils/Utils.jsx';
import PosItem from './PosItem.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class PosTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         posList: props.posList,
         total: props.total,
         totalQty: props.totalQty,
         request: "",
         warehouse: "",
         customer: "",
         preloader: "",
         empty: "",
         logout: false
      };
   }

   componentWillReceiveProps(nextProps) {
      if(this.state.posList != nextProps.posList || this.state.total != nextProps.total || this.state.totalQty != nextProps.totalQty){
         this.setState({posList: nextProps.posList, total: nextProps.total, totalQty: nextProps.totalQty}); //for async calls
      }
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#posTable').html());
      location.href=url
      return false
   }

   checkAllCheckbox(name) {
      var array = document.getElementsByName(name);
      for(var i=0; i<array.length; i++){
         var cb = document.getElementById(array[i].id);
         cb.checked = document.getElementById('checkAll').checked;
      }
   }

   render(){
      if (this.state.logout) {
         return <Redirect push to="/" />;
      }

      const preloader = this.state.preloader;

      var result = this.state.empty;
      if(this.state.posList.length != 0){
         result = this.state.posList.map((pos) =>
            <PosItem key={pos.id} pos={pos}/>
         );
      }

      var placeholder = "";
      var addHolder = 0;
      if(this.state.posList.length < 10){
         addHolder = 10 - this.state.posList.length;
         for(var i=0; i<addHolder; i++){
            placeholder = <tr><td></td><td></td><td></td><td></td><td></td></tr>;
         }
      }

      return(
         <div className="row">
           <div className="row" id="page-wrapper-inner5">
              <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
           </div>
            <div className="col-md-12" id="posTable">
               {preloader}
               <table className="table table-striped table-bordered table-responsive">
                  <thead className="thead-font-size">
                     <tr>
                        <th className="checkbox-width"><input type="checkbox" id="checkAll" onChange={(e) => this.checkAllCheckbox('checkbox')} /></th>
                        <th className="col-lg-width">Item Type</th>
                        <th className="col-sm-width">Unit Price</th>
                        <th className="col-sm-width">Qty</th>
                        <th className="col-sm-width">Total</th>
                     </tr>
                  </thead>
                  <tbody>
                     {result}
                     {placeholder}
                     <tr>
                        <td></td>
                        <td className="pull-right">Subtotal</td>
                        <td></td>
                        <td>{this.state.totalQty}</td>
                        <td>{this.state.total}</td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
      );
    }
}

export default PosTable;
