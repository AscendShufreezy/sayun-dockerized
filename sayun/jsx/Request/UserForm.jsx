import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import Input from '../Utils/Input.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class UserForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            employeeId: props.employeeId,
            employee: {},
            cashierList: "",
            customerList: "",
            warehouseList: "",
            warehouse: {},
            customer: {},
            logout: false
        };

        this.handleCustomer = this.handleCustomer.bind(this);
    }

    handleCustomer(e) {
        this.setState({
            customer: e.target.value
        });
    }

    handleWarehouse(e) {
        this.setState({
            warehouse: e.target.value
        });
    }

    componentWillMount(){
        var self = this;
        $.ajax({
            url: Utils.basePath + "/customers/?sort=name",
            type:"GET",
            contentType: "application/json",
            async : false,
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },

            success: function (data) {
                var customerList = [];
                for(var i=0; i<data.length; i++){
                    if(data[i].type == "Employee"){
                        var customer = {};
                        customer.id = data[i].id;
                        customer.name = data[i].name;
                        customerList.push(customer);
                    }
                }
                self.setState({customerList: customerList});
                self.setState({customer: customerList[0]})
            }
        });
        
        $.ajax({
            url: Utils.basePath + "/warehouses/",
            type:"GET",
            contentType: "application/json",
            async : false,
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },

            success: function (data) {
                var warehouseList = [];
                for(var i=0; i<data.length; i++){
                    var warehouse = {};
                    warehouse.id = data[i].id;
                    warehouse.name = data[i].name;
                    warehouseList.push(warehouse);
                }
                self.setState({warehouseList: warehouseList});
                var warehouseSelected = sessionStorage.getItem('warehouse');
                if(warehouseSelected == ""){
                    self.setState({warehouse: warehouseList[0]});
                } else{
                    self.setState({warehouse: warehouseSelected});
                }
            }
        });
    }

    render(){
        if (this.state.logout) {
             return <Redirect push to="/" />;
        }

        var employee = this.state.employee;
        var user = sessionStorage.getItem('name');

        var customersResult = "";
        if(this.state.items != ""){
            customersResult = this.state.customerList.map((customer) =>
               <option key={customer.id} value={customer.id}>{customer.name}</option>
            ); 
        }

        var warehouseResult = "";
        if(this.state.wa != ""){
            warehouseResult = this.state.warehouseList.map((warehouse) =>
               <option key={warehouse.id} value={warehouse.id}>{warehouse.name}</option>
            ); 
        }

        return (
            <div>
                <div className="row" id="page-wrapper-inner3">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="row">
                                <div className="col-md-3">
                                    Customer Name
                                </div>
                                <div className="col-md-6">
                                    <select className="form-control" id="customer" value={this.state.customer} onChange={this.handleCustomer}>
                                        {customersResult}
                                    </select>
                                </div>
                            </div>
                        </div>   
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-6">
                            <div className="row">
                                <div className="col-md-3">
                                    Cashier
                                </div>
                                <div className="col-md-6">
                                     <select className="form-control" id="cashier" value={user} disabled>
                                        <option value="user">{user}</option>
                                    </select>
                                </div>
                            </div>
                        </div>   
                        <div className="col-md-5 col-md-offset-1">
                            <div className="row">
                                <div className="col-md-3">
                                    Warehouse
                                </div>
                                <div className="col-md-6">
                                    <select className="form-control" id="warehouse" value={this.state.warehouse} onChange={this.handleWarehouse}>
                                        {warehouseResult}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
            </div>
        );
    }
}   

export default UserForm;