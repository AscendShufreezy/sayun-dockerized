import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';
import moment from 'moment';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import AddRequestForm from './AddRequestForm.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class AddRequest extends React.Component {
    constructor(props) {
    super(props);

    this.state = {
      response: [],
      transfer: false,
      isComplete: false,
      emptyField: "",
      invalidField: "",
      request: {},
      requestId: 0,
      logout: false
   };
  }

  cancelSave() {
    this.setState({transfer: true});
  }

  checkInputs() {
    if(document.getElementById("date").value == ""){
        this.setState({emptyField: "Date"}, () => { 
            alert(this.state.emptyField + " should not be empty.");
            return "";
        });
    } else if(!moment(document.getElementById("date").value,"MM/DD/YYYY",true).isValid()){
        this.setState({invalidField: "Date"}, () => { 
            alert(this.state.invalidField + " should be a valid date.");
            return "";
        });
    } else if(document.getElementById("customer").value == ""){
        this.setState({emptyField: "Customer"}, () => { 
            alert(this.state.emptyField + " should not be empty.");
            return "";
        });
    } else{
      this.setState({isComplete: true}, () => { 
        var request = {};
        var warehouse;
        if(document.getElementById("warehouse") == null){
            warehouse = "";
        } else{
            warehouse = document.getElementById("warehouse").value;
        }

        request.date = document.getElementById("date").value;
        request.customer = document.getElementById("customer").value;
        request.warehouse = warehouse;
        this.setState({request: request}, () => this.saveRequest()); //for async calls
      });
    }
  }

  saveRequest() {
    var self = this;
    var request = this.state.request;
    var data = {"warehouse": request.warehouse, "customer": request.customer, 
                "date": request.date, "total": 0, "status": "Draft"}
                
    $.ajax({
        url: Utils.basePath + "/requests/",
        type:"POST",
        contentType: "application/json",
        data: JSON.stringify(data),
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        statusCode: {
           400: function (response) {
              alert(JSON.parse(response.responseText).customer);
           },
           401: function (response) {
              alert("User is not authorized. Please login.");
              self.setState({logout: true});
           },
           500: function (response) {
              alert("HTTP error 500, please check your connection.");
           }
        },
        success: function(data) {
            self.setState({requestId: data.id});
            alert("Item Out saved!");
            self.setState({transfer: true});
        },
        error: function (response) {
            console.log(response);
        }
    });
  }

  componentWillMount(){
    var group = sessionStorage.getItem('group');
    var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

    if (group != "admin-group" && group != "hr-group" && group != "warehouse-group" && sessionPermission.indexOf(101) == -1) {
      alert("You are not authorized to access this page. Please login as admin user.");
      this.setState({logout: true});
    } 
  }  

  render(){
    var requestId = this.state.requestId;
    var link = "/request/id=" + requestId;
    if (this.state.transfer) {
      return <Redirect push to={link} />;
    }

    if (this.state.logout) {
      return <Redirect push to="/" />;
    }

    return (
      <div id="page-wrapper">
          <Menu active="Request"/>
          <div className="row" id="page-wrapper-inner">
              <Breadcrumb page="add-request"/>
              <PageTitle pageTitle="Add Item Out"/>
              <div className="row" id="page-wrapper-inner3">
                  <form>
                      <AddRequestForm />
                      <div className="row">
                          <div className="col-md-3 pull-right">
                            <div className="pull-right">
                              <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
                              <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
                            </div>
                          </div>
                      </div>
                  </form>
                  <br />
              </div>
          </div>
          <br />  
      </div>
    );
  }
}

export default AddRequest;