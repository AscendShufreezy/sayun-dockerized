import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import RequestListTable from './RequestListTable.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class RequestListPage extends React.Component {
	constructor(){
	    super();
	}

	deleteRequest(){
	  	var numberOfRequests = document.getElementsByName("checkbox");
	  	var requestsToDelete = [];

	  	for(var i=0; i<numberOfRequests.length;i++){
	  		if(numberOfRequests[i].checked){
	  			requestsToDelete.push(numberOfRequests[i].id);
	  		}
	  	}

	  	if(requestsToDelete.length > 0){
	  		var deleteMessage = confirm("Are you sure you want to delete the selected request/s?");
	  		if (deleteMessage == true) {
	  			var successCounter = 0;
	  			var failCounter = 0;
	  			for(var i=0; i<requestsToDelete.length;i++){
	  				$.ajax({
			            url: Utils.basePath + "/requests/" + requestsToDelete[i] + "/",
			            type:"DELETE",
			            async: false,
			            contentType: "application/json",
			            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
			            statusCode: {
			               401: function (response) {
			                  alert("User is not authorized. Please login.");
			                  self.setState({logout: true});
			               },
			               500: function (response) {
			                  alert("HTTP error 500, please check your connection.");
			               }
						},
			            success: function (data) {
			            	successCounter++;			            	
			            },
			            error: function (response) {
			            	failCounter++;
			            }
			        });

			        if(successCounter == requestsToDelete.length){
		  				alert("Deletion of request/s is successful!");
		  				location.reload();
		  			} else if(failCounter == requestsToDelete.length){
	                	alert("Items Out with child record cannot be deleted!");
	                } else if((successCounter + failCounter) == requestsToDelete.length){
	                	alert("Items Out with child record cannot be deleted. Only selected items out with no child records are deleted.");
	                	location.reload();
	                }
	  			}
		    }
	  	} else{
	  		alert('There are no request/s selected.');
	  	}
    }

    componentWillMount(){
    	var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

    	if (sessionPermission.indexOf(104) == -1) {
    		alert("You are not authorized to access this page. Please login as admin user.");
    		this.setState({logout: true});
    	}
    }

    render(){
    	var buttons = "";
    	var group = sessionStorage.getItem('group');
    	if(group == "warehouse-group" || group == "admin-group"){
    		buttons = <ButtonGroup buttons="addDelete" link1="/request/add-request" click2={() => this.deleteRequest()} type1="button"/>;
    	} else if(group == "pos-group" || group == "cashier-group"){
    		buttons = <ButtonGroup buttons="add" link1="/pos" type1="button"/>;
    	} else if(group == "warehouseapprover-group"){
    		buttons = null;
    	}

      	return (
	      	<div id="page-wrapper">
		      	<Menu active="Request"/>
		      	<div className="row" id="page-wrapper-inner">
		      		<PageTitle pageTitle="Item Out List"/>
		      		<div className="row" id="page-wrapper-inner3">
		      			<form>
			      			<RequestListTable />
			      			{buttons}
		      			</form>
		      			<br />
		      		</div>
		      	</div>	
		      	<br />
		    </div>
     	);
   }
}

export default RequestListPage;

