import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class PosItem extends React.Component {
   render() {
      var pos = this.props.pos;

      return (
         <tr> 
            <td><input type="checkbox" name="checkbox" id={pos.id}/></td>
            <td>{pos.itemName}</td>
            <td>{pos.unitPrice}</td>
            <td>{pos.qty}</td>
            <td>{pos.total}</td>
         </tr>

      ); 
   }
}

export default PosItem;