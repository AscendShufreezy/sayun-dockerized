import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Input from '../Utils/Input.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class EditRequestForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            customers: [],
            warehouses: [],
            customer: props.request.customer,
            warehouse: props.request.warehouse,
            date: moment(props.request.date, "MM/DD/YYYY"),
            logout: false
        };

        this.handleWarehouse = this.handleWarehouse.bind(this);
        this.handleCustomer = this.handleCustomer.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleCustomer(e) {
        this.setState({
            customer: e.target.value
        });
    }

    handleWarehouse(e) {
        this.setState({
            warehouse: e.target.value
        });
    }    

    handleChange(date) {
        this.setState({
            date: date
        });
    }

    componentWillMount(){
        var self = this;
        $.ajax({
            url: Utils.basePath + "/customers/?sort=name",
            type:"GET",
            contentType: "application/json",
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
                const customerList = [];
                for(var i=0;i<data.length;i++){
                    //if(data[i].type == "Truck"){
                        customerList.push(data[i]);
                    //}
                }
                self.setState({customers: customerList});
            },
            error: function (response) {
                console.log(response);
            }
        });

        $.ajax({
            url: Utils.basePath + "/warehouses/",
            type:"GET",
            contentType: "application/json",
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
                const warehouseList = [];
                for(var i=0;i<data.length;i++){
                    warehouseList.push(data[i]);
                }
                self.setState({warehouses: warehouseList});
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    render(){
        if (this.state.logout) {
             return <Redirect push to="/" />;
        }

        const customersResult = this.state.customers.map((customers) =>
            <option key={customers.id} value={customers.id}>{customers.name}</option>
        );       

        const warehousesResult = this.state.warehouses.map((warehouses) =>
            <option key={warehouses.id} value={warehouses.id}>{warehouses.name}</option>
        );

        var addCustLink = <div><i className="fa fa-plus-circle"></i><Link to="/customer/add-customer" target="_blank"><u> Add New Customer</u></Link></div>;
        if(sessionStorage.getItem('group') == "pos-group"){
            addCustLink = "";
        }

        return (
            <div>
                <div className="row" id="page-wrapper-inner3">
                    <div className="row form-row-padding-top">
                        <div className="col-conf-3">
                            Date Requested*
                        </div>
                        <div className="col-md-3">
                            <DatePicker selected={this.state.date} onChange={this.handleChange} id="date" className="form-control"/>
                             <i className="fa fa-calendar date-logo"></i>
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-conf-3">
                            Customer*
                        </div>
                        <div className="col-md-3">
                            <select className="form-control" id="customer" value={this.state.customer} onChange={this.handleCustomer}>
                                {customersResult}
                            </select>
                        </div>
                        <div className="col-md-2 form-row-padding-top">
                            {addCustLink}
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-conf-3">
                            Warehouse
                        </div>
                        <div className="col-md-3">
                            <select className="form-control" id="warehouse" value={this.state.warehouse} onChange={this.handleWarehouse}>
                                {warehousesResult}
                            </select>
                        </div>
                    </div>
                </div>
                <br />
            </div>
        );
    }
}   

export default EditRequestForm;