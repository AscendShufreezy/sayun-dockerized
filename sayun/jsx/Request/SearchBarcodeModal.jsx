import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Link, Switch, Redirect } from 'react-router-dom';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class SearchBarcodeModal extends React.Component {
  constructor(props){
      super(props);

      this.state = {
        hashMap: props.hashIdBarcode,
        barcode: ""
      };

      this.handleBarcode = this.handleBarcode.bind(this);
  }

  handleBarcode(e) {
    this.setState({barcode: e.target.value});
  }

  changeValue(){
    var barcode = (this.state.barcode).replace(/\s+$/, '');
    var hashMap = this.state.hashMap;

    for(var key in hashMap) {
      if(hashMap[key] === barcode) {
        document.getElementById('item').value = key;
        document.getElementById('qty').value = 1;
        document.getElementById('unitPrice').value = 500.00;

        var unitPrice = 0;
        var self = this;
        $.ajax({
          url: Utils.basePath + "/items/" + document.getElementById('item').value + "/",
          type:"GET",
          contentType: "application/json",
          async : false,
          headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
          success: function (data) {
            document.getElementById('unitPrice').value = data.unit_price;
          }
        });
        break;
      }
    }

    document.getElementById("barcode").value = "";
  }

  componentDidMount(){
    document.getElementById("barcode").focus();
  }

  render(){
    return(
      <div className="modal fade modal-position" id="searchbarcodeModal" role="dialog">
        <form>
          <div className="modal-dialog modal-md">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal">&times;</button>
                <h4 className="modal-title">Search Barcode</h4>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-md-3 col-md-offset-1 form-row-padding-top">
                    Scanned Barcode:
                  </div>
                  <div className="col-md-4">
                    <input type="text" autoFocus id="barcode" className="form-control" value={this.state.barcode} onChange={this.handleBarcode}/>
                  </div>
                </div>
                <br />
              </div>
              <div className="modal-footer">
                <button type="submit" className="btn btn-default btn-primary" onClick={() => this.changeValue()} data-dismiss="modal">Add to Request</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBarcodeModal;

