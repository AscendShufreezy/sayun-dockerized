import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import AddItemForm from './AddItemForm.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class AddItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          response: [],
          transfer: false,
          isComplete: false,
          emptyField: "",
          invalidField: "",
          item: {},
          logout: false
       };
    }

    cancelSave() {
        this.setState({transfer: true});
    }

    checkInputs() {
        if(document.getElementById("name").value == ""){
            this.setState({emptyField: "Name"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("description").value == ""){
            this.setState({emptyField: "Description"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("barcode").value == ""){
            this.setState({emptyField: "Barcode"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("unitPrice").value == ""){
            this.setState({emptyField: "Unit Price"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(isNaN(document.getElementById("unitPrice").value)){
            this.setState({invalidField: "Unit Price"}, () => { 
                alert(this.state.invalidField + " should be a valid number.");
                return "";
            });
        } else{
            this.setState({isComplete: true}, () => { 
                var item = {};
                item.name = document.getElementById("name").value;
                item.description = document.getElementById("description").value;
                item.unitPrice = document.getElementById("unitPrice").value;
                item.barcode = document.getElementById("barcode").value;
                this.setState({item: item}, () => this.saveItem()); //for async calls
            });
        }
    }

    saveItem() {
        var self = this;
        var item = this.state.item;
        var data = {"name": item.name,"description": item.description, "unit_price": item.unitPrice, "stock": 0, "barcode": item.barcode}
                    
        $.ajax({
            url: Utils.basePath + "/items/",
            type:"POST",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
                alert("Item saved!");
                self.setState({transfer: true});
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    componentWillMount(){
      var group = sessionStorage.getItem('group');
      var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

      if (group != "admin-group" && group != "hr-group" && group != "warehouse-group" && sessionPermission.indexOf(61) == -1) {
        alert("You are not authorized to access this page. Please login as admin user.");
        this.setState({logout: true});
      } 
    } 

    render(){
        if (this.state.transfer) {
            return <Redirect push to="/item" />;
        }

        if (this.state.logout) {
             return <Redirect push to="/" />;
        }

        return (
            <div id="page-wrapper">
                <Menu active="Item"/>
                <div className="row" id="page-wrapper-inner">
                    <Breadcrumb page="add-item"/>
                    <PageTitle pageTitle="Add Item"/>
                    <div className="row" id="page-wrapper-inner3">
                        <form>
                            <AddItemForm />
                            <div className="row">
                                <div className="col-md-3 pull-right">
                                  <div className="pull-right">
                                    <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
                                    <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
                                  </div>
                                </div>
                            </div>
                        </form>
                        <br />
                    </div>
                </div>
                <br />  
            </div>
        );
    }
}

export default AddItem;