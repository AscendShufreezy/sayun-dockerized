import React from 'react';
import ReactDOM from 'react-dom';
import ItemHistory from './ItemHistory.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import EmptyTable from '../Utils/EmptyTable.jsx';
import Preloader from '../Utils/Preloader.jsx';
import Utils from '../Utils/Utils.jsx';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class ItemHistoryList extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         empty: "", 
         preloader: "",
         logout: false,
         filter1: "",
         filter2: "",
         filter3: "",
         filter4: "",
         filter5: "",
         filter6: ""
      };
   }

   componentWillMount(){
      var self = this;
      this.setState({preloader: <Preloader/>}); 
      var itemId = this.props.item.id;
      $.ajax({
         url: Utils.basePath + "/items/" + itemId + "/history/",
         type:"GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
            400: function (response) {
               alert(JSON.parse(response.responseText));
            },
            401: function (response) {
               alert("User is not authorized. Please login.");
               self.setState({logout: true});
            },
            500: function (response) {
               alert("HTTP error 500, please check your connection.");
            }
         },
         success: function(response) {
            const itemHistoryList = [];
            for(var i=0; i<response.length; i++){
               var itemHistory = {};
               itemHistory.id = response[i].id;
               itemHistory.pseudoId = response[i].pseudo_id;
               itemHistory.item = response[i].item;
               itemHistory.transactionType = response[i].transaction_type;
               itemHistory.date = response[i].date;
               itemHistory.qty = response[i].quantity;
               itemHistory.unitPrice = response[i].unit_price;
               itemHistory.total = response[i].total;
               itemHistory.receipt = response[i].receipt;
               itemHistoryList.push(itemHistory);
            }

            if(itemHistoryList.length == 0){
               this.setState({empty: <EmptyTable text="Item Histories" span="6"/>});
            }

            this.setState({response: itemHistoryList}); 
            this.setState({preloader: ""}); 
         }.bind(this),
         error: function(xhr, status, err) {
         }.bind(this)
      }); 
   }

   idFormatter(cell, row){
      console.log(row);
      var link;
      if(row.transactionType == "Item Out"){
         link = "/request/id=" + row.pseudoId;
      } else{
         link = "/receipt/id=" + row.receipt;
      }

      return (
         <Link to={link}><u>{cell}</u></Link>
      );
   }

   amountFormatter(cell, row){
      var amount = cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return (
         amount
      );
   }

   filterShow1(){
      if(this.state.filter1 == ""){
         this.setState({filter1: 'RegexFilter'});
      } else{
         this.setState({filter1: ''});
      }
   }

   filterShow2(){
      if(this.state.filter2 == ""){
         this.setState({filter2: 'RegexFilter'});
      } else{
         this.setState({filter2: ''});
      }
   }

   filterShow3(){
      if(this.state.filter3 == ""){
         this.setState({filter3: 'RegexFilter'});
      } else{
         this.setState({filter3: ''});
      }
   }

   filterShow4(){
      if(this.state.filter4 == ""){
         this.setState({filter4: 'RegexFilter'});
      } else{
         this.setState({filter4: ''});
      }
   }

   filterShow5(){
      if(this.state.filter5 == ""){
         this.setState({filter5: 'RegexFilter'});
      } else{
         this.setState({filter5: ''});
      }
   }

   filterShow6(){
      if(this.state.filter6 == ""){
         this.setState({filter6: 'RegexFilter'});
      } else{
         this.setState({filter6: ''});
      }
   }

   render(){
      const preloader = this.state.preloader;
      if (this.state.logout) {
             return <Redirect push to="/" />;
      }

      var filter1 = this.state.filter1;
      var filter2 = this.state.filter2;
      var filter3 = this.state.filter3;
      var filter4 = this.state.filter4;
      var filter5 = this.state.filter5;
      var filter6 = this.state.filter6;
      
      return(
         <div className="row">
            <div className="col-md-12">
               <PageTitle pageTitle="History"/>
               {preloader}
               <BootstrapTable data={this.state.response} striped hover pagination>
                  <TableHeaderColumn dataField='transactionType' filter={ { type: filter1 } }  dataSort={ true }>Transaction Type <i className="fa fa-search search-size" onMouseOver={() => this.filterShow1()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='date' filter={ { type: filter2 } }  dataSort={ true }>Date <i className="fa fa-search search-size" onMouseOver={() => this.filterShow2()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='pseudoId' isKey filter={ { type: filter3} } dataFormat={ this.idFormatter } dataSort={ true }>ID <i className="fa fa-search search-size" onMouseOver={() => this.filterShow3()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='qty' filter={ { type: filter4 } }  dataSort={ true }>Qty <i className="fa fa-search search-size" onMouseOver={() => this.filterShow4()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='unitPrice' filter={ { type: filter5 } } dataSort={ true } dataFormat={ this.amountFormatter }>Unit Price <i className="fa fa-search search-size" onMouseOver={() => this.filterShow5()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='total' filter={ { type: filter6 } } dataSort={ true } dataFormat={ this.amountFormatter }>Total <i className="fa fa-search search-size" onMouseOver={() => this.filterShow6()}></i></TableHeaderColumn>
               </BootstrapTable>
            </div>
         </div>
      );
    }
}

export default ItemHistoryList;

