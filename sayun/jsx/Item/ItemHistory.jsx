import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class ItemHistory extends React.Component {
   render() {
      var itemHistory = this.props.itemHistory;
      var link;
      if(itemHistory.transactionType == "Item Out"){
         link = "/request/id=" + itemHistory.pseudoId;
      } else{
         link = "/receipt/id=" + itemHistory.receipt;
      }

      return (
         <tr> 
            <td>{itemHistory.transactionType}</td>
            <td>{itemHistory.date}</td>
            <td><Link to={link}><u>{itemHistory.pseudoId}</u></Link></td>
            <td>{itemHistory.qty}</td>
            <td>{itemHistory.unitPrice}</td>
            <td>{itemHistory.total}</td>
         </tr>
      ); 
   }
}

export default ItemHistory;