import React from 'react';
import ReactDOM from 'react-dom';
import EmptyTable from '../Utils/EmptyTable.jsx';
import Preloader from '../Utils/Preloader.jsx';
import Utils from '../Utils/Utils.jsx';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class ItemListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         preloader: "",
         empty: "",
         logout: false,
         filter1: "",
         filter2: "",
         filter3: "",
         filter4: "",
         filter5: ""
      };
   }

   checkAllCheckbox(name) {
      var array = document.getElementsByName(name);
      for(var i=0; i<array.length; i++){
         var cb = document.getElementById(array[i].id);
         cb.checked = document.getElementById('checkAll').checked;
      }
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#itemListTable').html());
      location.href=url
      return false
   }

   componentDidMount(){
         var self = this;
         this.setState({preloader: <Preloader/>});
         $.ajax({
            url: Utils.basePath + "/items/",
            type:"GET",
            contentType: "application/json",
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText));
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(response) {
               const itemList = [];
               for(var i=0; i<response.length; i++){
                  var item = {};
                  item.checkboxId = response[i].id;
                  item.id = response[i].id;
                  item.name = response[i].name;
                  item.description = response[i].description;
                  item.unitPrice = response[i].unit_price;
                  item.qty = response[i].stock;
                  itemList.push(item);
               }

               if(itemList.length == 0){
                  this.setState({empty: <EmptyTable text="Items" span="5"/>});
               }

               this.setState({response: itemList});
               this.setState({preloader: ""});
            }.bind(this),
            error: function(xhr, status, err) {
            }.bind(this)
         });
   }

   idFormatter(cell, row){
      var link = "/item/id=" + cell;
      return (
         <Link to={link}><u>{cell}</u></Link>
      );
   }

   checkboxFormatter(cell, row){
      return (
         <input type="checkbox" name="checkbox" id={cell} />
      );
   }

   amountFormatter(cell, row){
      var amount = cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return (
         amount
      );
   }

   filterShow1(){
      if(this.state.filter1 == ""){
         this.setState({filter1: 'RegexFilter'});
      } else{
         this.setState({filter1: ''});
      }
   }

   filterShow2(){
      if(this.state.filter2 == ""){
         this.setState({filter2: 'RegexFilter'});
      } else{
         this.setState({filter2: ''});
      }
   }

   filterShow3(){
      if(this.state.filter3 == ""){
         this.setState({filter3: 'RegexFilter'});
      } else{
         this.setState({filter3: ''});
      }
   }

   filterShow4(){
      if(this.state.filter4 == ""){
         this.setState({filter4: 'RegexFilter'});
      } else{
         this.setState({filter4: ''});
      }
   }

   filterShow5(){
      if(this.state.filter5 == ""){
         this.setState({filter5: 'RegexFilter'});
      } else{
         this.setState({filter5: ''});
      }
   }

   render(){
      const preloader = this.state.preloader;
      if (this.state.logout) {
             return <Redirect push to="/" />;
      }

      var filter1 = this.state.filter1;
      var filter2 = this.state.filter2;
      var filter3 = this.state.filter3;
      var filter4 = this.state.filter4;
      var filter5 = this.state.filter5;

      return(
         <div className="row">
           <div className="row" id="page-wrapper-inner5">
              <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
           </div>
            <div className="col-md-12" id="itemListTable">
               {preloader}
               <BootstrapTable data={this.state.response} striped hover pagination>
                  <TableHeaderColumn width="20px" dataField='checkboxId' dataFormat={ this.checkboxFormatter }>
                     <input type="checkbox" id="checkAll" onChange={(e) => this.checkAllCheckbox('checkbox')} />
                  </TableHeaderColumn>
                  <TableHeaderColumn dataField='id' isKey width="120px" filter={ { type: filter1 } } dataFormat={ this.idFormatter } dataSort={ true }>Item ID <i className="fa fa-search search-size" onMouseOver={() => this.filterShow1()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='name' width="200px" filter={ { type: filter2 } }  dataSort={ true }>Name <i className="fa fa-search search-size" onMouseOver={() => this.filterShow2()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='description' width="250px" filter={ { type: filter3 } }  dataSort={ true }>Description <i className="fa fa-search search-size" onMouseOver={() => this.filterShow3()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='unitPrice' width="100px" filter={ { type: filter4 } }  dataSort={ true } dataFormat={ this.amountFormatter }>Unit Cost <i className="fa fa-search search-size" onMouseOver={() => this.filterShow4()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='qty' width="100px" filter={ { type: filter5 } }  dataSort={ true }>In Stock Qty <i className="fa fa-search search-size" onMouseOver={() => this.filterShow5()}></i></TableHeaderColumn>
               </BootstrapTable> 
            </div>
         </div>
      );
    }
}

export default ItemListTable;
