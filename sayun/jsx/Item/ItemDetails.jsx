import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import EditItemForm from './EditItemForm.jsx';
import ItemHistoryList from './ItemHistoryList.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';;

class ItemDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      response: [],
      transfer: false,
      isComplete: false,
      emptyField: "",
      invalidField: "",
      item: {},
      logout: false
   };
  }

  cancelSave() {
	  this.setState({transfer: true});
  }

  checkInputs() {
	  if(document.getElementById("name").value == ""){
      this.setState({emptyField: "Name"}, () => { 
          alert(this.state.emptyField + " should not be empty.");
          return "";
      });
    } else if(document.getElementById("description").value == ""){
      this.setState({emptyField: "Description"}, () => { 
          alert(this.state.emptyField + " should not be empty.");
          return "";
      });
    } else if(document.getElementById("barcode").value == ""){
      this.setState({emptyField: "Barcode"}, () => { 
          alert(this.state.emptyField + " should not be empty.");
          return "";
      });
    } else if(document.getElementById("unitPrice").value == ""){
      this.setState({emptyField: "Unit Cost"}, () => { 
          alert(this.state.emptyField + " should not be empty.");
          return "";
      });
    } else if(isNaN(document.getElementById("unitPrice").value)){
      this.setState({invalidField: "Unit Price"}, () => { 
          alert(this.state.invalidField + " should be a valid number.");
          return "";
      });
    } else{
  		this.setState({isComplete: true}, () => { 
  			this.updatItem();
		  });
  	}
  }  

  updatItem() {
      var url = window.location.href;
      var itemId = url.split('=').pop();
      var item = this.state.item;

      item.id = itemId;
      item.name = document.getElementById('name').value;
      item.description = document.getElementById('description').value;
      item.unitPrice = document.getElementById('unitPrice').value;
      item.barcode = document.getElementById('barcode').value;
     
      var data = {"id": item.id, "name": item.name, "description": item.description, "unit_price": item.unitPrice, "barcode": item.barcode}
      var self = this;

	    $.ajax({
          url: Utils.basePath + "/items/" + itemId + "/",
          type: "PUT",
          contentType: "application/json",
          data: JSON.stringify(data),
          headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
          statusCode: {
             400: function (response) {
                alert(JSON.parse(response.responseText));
             },
             401: function (response) {
                alert("User is not authorized. Please login.");
                self.setState({logout: true});
             },
             500: function (response) {
                alert("HTTP error 500, please check your connection.");
             }
          },
          success: function (data) {
              alert("Item updated!");
              self.setState({transfer: true});
          },
          error: function (response) {
              console.log(response);
          }
      });
  }

  componentWillMount(){
    var group = sessionStorage.getItem('group');
    var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

    if (group != "admin-group" && group != 'hr-group' && group != 'warehouse-group' && sessionPermission.indexOf(64) == -1) {
        alert("You are not authorized to access this page. Please login as admin user.");
        this.setState({logout: true});
    }
  }

	render(){
    if (this.state.logout) {
             return <Redirect push to="/" />;
    }

		if (this.state.transfer) {
		    return <Redirect push to="/item" />;
		}

    var url = window.location.href;
    var id = url.split('=').pop();
    var self = this;
    var item = {};

    $.ajax({
      url: Utils.basePath + "/items/" + id + "/",
      type:"GET",
      contentType: "application/json",
      async : false,
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
         400: function (response) {
            alert(JSON.parse(response.responseText).id);
         },
         401: function (response) {
            alert("User is not authorized. Please login.");
            self.setState({logout: true});
         },
         500: function (response) {
            alert("HTTP error 500, please check your connection.");
         }
      },
      success: function (data) {
          item.id = id;
          item.name = data.name;
          item.description = data.description;
          item.unitPrice = data.unit_price;
          item.qty = data.stock;
          item.barcode = data.barcode;
      }
    });

  	return (
    	<div id="page-wrapper">
      	<Menu active="Item"/>
      	<div className="row" id="page-wrapper-inner">
      		<Breadcrumb page="edit-item"/>
      		<PageTitle pageTitle="Item"/>
      		<div className="row" id="page-wrapper-inner3">
      			<form>
	      			<EditItemForm item={item}/>
	      			<div className="row">
			            <div className="col-md-3 pull-right">
			              <div className="pull-right">
			                <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
			                <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
			              </div>
			            </div>
			        </div>
		        </form>
            <hr />
            <ItemHistoryList item={item} />
      			<br />
      		</div>
      	</div>	
        <br />  
      </div>
    );
  }
}

export default ItemDetails;

