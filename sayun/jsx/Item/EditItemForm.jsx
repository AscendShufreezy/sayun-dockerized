import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';
import Input from '../Utils/Input.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class EditItemForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            name: props.item.name,
            description: props.item.description,
            unitPrice: props.item.unitPrice,
            barcode: props.item.barcode
        };

        this.handleDescription = this.handleDescription.bind(this);
        this.handleName = this.handleName.bind(this);
        this.handleUnitPrice = this.handleUnitPrice.bind(this);
        this.handleBarcode = this.handleBarcode.bind(this);
    }

    handleDescription(e) {
        this.setState({
            description: e.target.value
        });
    }

    handleBarcode(e) {
        this.setState({
            barcode: e.target.value
        });
    }

    handleName(e) {
        this.setState({
            name: e.target.value
        });
    }

    handleUnitPrice(e) {
        this.setState({
            unitPrice: e.target.value
        });
    }

    render(){
        var item = this.props.item;

        return (
            <div>
                <div className="row" id="page-wrapper-inner3">
                    <div className="row">
                        <div className="col-md-1">
                            Item ID
                        </div>
                        <div className="col-md-5 form-control-height">
                            {item.id}
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-1">
                            Name*
                        </div>
                        <div className="col-md-5">
                            <Input id="name" type="text" defaultValue={this.state.name} onChange={this.handleName}/>
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-1">
                            Description*
                        </div>
                        <div className="col-md-11">
                            <Input id="description" type="text" defaultValue={this.state.description} onChange={this.handleDescription}/>
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-1">
                            Barcode*
                        </div>
                        <div className="col-md-6">
                            <Input id="barcode" type="text" defaultValue={this.state.barcode} onChange={this.handleBarcode}/>
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-1">
                            Unit Cost*
                        </div>
                        <div className="col-md-4">
                            <Input id="unitPrice" type="text" defaultValue={this.state.unitPrice} onChange={this.handleUnitPrice}/>
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-1">
                            In Stock Qty
                        </div>
                        <div className="col-md-11">
                            {item.qty}
                        </div>
                    </div>
                </div>
                <br />
            </div>
        );
    }
}   

export default EditItemForm;