import React from 'react';
import ReactDOM from 'react-dom';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';;

class EmployeeAttendance extends React.Component {
   constructor(props) {
      super(props);

      var amTimeIn = props.employeeAttendance.amTimeIn;
      var amTimeOut = props.employeeAttendance.amTimeOut;
      var pmTimeIn = props.employeeAttendance.pmTimeIn;
      var pmTimeOut = props.employeeAttendance.pmTimeOut;
     
      amTimeIn =amTimeIn.slice(0,5) + " AM";
      amTimeOut = amTimeOut.slice(0,5) + " AM";
      pmTimeIn = pmTimeIn.slice(0,5)  + " PM";
      pmTimeOut = pmTimeOut.slice(0,5) + " PM";

      this.state = {
         amTimeIn: amTimeIn,
         amTimeOut: amTimeOut,
         pmTimeIn: pmTimeIn,
         pmTimeOut: pmTimeOut
      };

      this.handleAmTimein = this.handleAmTimein.bind(this);
      this.handleAmTimeout = this.handleAmTimeout.bind(this);
      this.handlePmTimein = this.handlePmTimein.bind(this);
      this.handlePmTimeout = this.handlePmTimeout.bind(this);
   }

   handleAmTimein(e) {
      this.setState({
         amTimeIn: e.target.value
      });
    }

   handleAmTimeout(e) {
      this.setState({
         amTimeOut: e.target.value
      });
   }

   handlePmTimein(e) {
      this.setState({
         pmTimeIn: e.target.value
      });
    }

   handlePmTimeout(e) {
      this.setState({
         pmTimeOut: e.target.value
      });
   }

   isGreaterThan(timeinId, timeoutId){
      /*var timeIn = "";
      var timeOut = "";
      
      if(timeinId.includes('am')){
         timeinId = timeinId.slice(0,5);
         timeIn = timeinId + ":00";
      } else if(timeinId.includes('pm')){
         timeinId = timeinId.replace(' pm', '');
         var hours = timeinId.slice(0, 2);
         var minutes = timeinId.slice(3);
         hours = parseInt(hours) + 12;
         timeIn = hours + ":" + minutes + ":00";
      }

       if(timeoutId.includes('am')){
         timeoutId = timeoutId.slice(0,5);
         timeOut = timeoutId + ":00";
      } else if(timeoutId.includes('pm')){
         timeoutId = timeoutId.replace(' pm', '');
         var hours = timeoutId.slice(0, 2);
         var minutes = timeoutId.slice(3);
         hours = parseInt(hours) + 12;
         timeOut = hours + ":" + minutes + ":00";
      }

      if((new Date("01/01/01 " + timeIn)) > (new Date("01/01/01 " + timeOut))){
         return true;
      } else{
         return false;
      }*/
      return false;
   }

   checkInputs(amTimein, amTimeOut, pmTimeIn, pmTimeOut){
      var isGreaterThan = this.isGreaterThan(amTimein, amTimeOut);
      if(isGreaterThan){
            alert("Time in should not be greated than Time out.");
        } else{
            this.saveAttendance();
        }
   }

   saveAttendance() {
      var self = this;
      var employeeAttendance = this.props.employeeAttendance
      var date = employeeAttendance.date;

      var amtimein = document.getElementById('amtimeIn' + date).value;
      var amtimeout = document.getElementById('amtimeOut' + date).value;
      var pmtimein = document.getElementById('pmtimeIn' + date).value;
      var pmtimeout = document.getElementById('pmtimeOut' + date).value;

      if(amtimein != ""){
         amtimein = amtimein.slice(0,5) + ":00";
      } if(amtimeout != ""){
         amtimeout = amtimeout.slice(0,5) + ":00";
      } if(pmtimein != ""){
         pmtimein = pmtimein.slice(0,5) + ":00";
      } if(pmtimeout != ""){
         pmtimeout = pmtimeout.slice(0,5) + ":00";
      }

      employeeAttendance.amTimeIn = amtimein;
      employeeAttendance.amTimeOut = amtimeout;
      employeeAttendance.pmTimeIn = pmtimein;
      employeeAttendance.pmTimeOut = pmtimeout;

      if(employeeAttendance.amTimeIn == ""){
         employeeAttendance.amTimeIn = "00:00:00";
      } if(employeeAttendance.amTimeOut == ""){
         employeeAttendance.amTimeOut = "00:00:00";
      } if(employeeAttendance.pmTimeIn == ""){
         employeeAttendance.pmTimeIn = "00:00:00";
      } if(employeeAttendance.pmTimeOut == ""){
         employeeAttendance.pmTimeOut = "00:00:00";
      }

      var data = {"employee": employeeAttendance.employee,"date": employeeAttendance.date,
                  "am_time_in": employeeAttendance.amTimeIn, "am_time_out": employeeAttendance.amTimeOut, 
                  "pm_time_in": employeeAttendance.pmTimeIn, "pm_time_out": employeeAttendance.pmTimeOut}
                    
      
      if(Number.isInteger(employeeAttendance.id)){ //update attendance
         $.ajax({
            url: Utils.basePath + "/attendance/" + employeeAttendance.id + "/",
            type:"PUT",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).detail);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
               alert("Attendance updated!");
            },
            error: function (response) {
               console.log(response);
            }
         })
      } else{ //save new attendance
         $.ajax({
            url: Utils.basePath + "/attendance/",
            type:"POST",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert("Please follow correct time format: HH:MM am/pm");
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
               alert("Attendance saved!");
            },
            error: function (response) {
               console.log(response);
            }
         })
      }
   }

   render() {
      var employeeAttendance = this.props.employeeAttendance;
      var date = employeeAttendance.date;

      var disable = "";
      if(this.state.amTimeIn == "" && this.state.amTimeOut == "" && this.state.pmTimeIn == "" && this.state.pmTimeOut == ""){ 
         disable = "disabled";
      }

      var amtimeinId = "amtimeIn" + date;
      var amtimeoutId = "amtimeOut" + date;
      var pmtimeinId = "pmtimeIn" + date;
      var pmtimeoutId = "pmtimeOut" + date;

      var amTimeIn = this.state.amTimeIn;
      var amTimeOut = this.state.amTimeOut;
      var pmTimeIn = this.state.pmTimeIn;
      var pmTimeOut = this.state.pmTimeOut;

      amTimeIn = amTimeIn.substring(0,5);
      amTimeOut = amTimeOut.substring(0,5);
      pmTimeIn = pmTimeIn.substring(0,5);
      pmTimeOut = pmTimeOut.substring(0,5);

      if(amTimeIn == "00:00"){
         amTimeIn = "";
      } if(amTimeOut == "00:00"){
         amTimeOut = "";
      } if(pmTimeIn == "00:00"){
         pmTimeIn = "";
      } if(pmTimeOut == "00:00"){
         pmTimeOut = "";
      }

      return (
         <tr> 
            <td>{date}</td>
            <td><input type="time" id={amtimeinId} className="form-control" value={amTimeIn} onChange={this.handleAmTimein}/></td>
            <td><input type="time" id={amtimeoutId} className="form-control" value={amTimeOut}  onChange={this.handleAmTimeout}/></td>
            <td><input type="time" id={pmtimeinId} className="form-control" value={pmTimeIn} onChange={this.handlePmTimein}/></td>
            <td><input type="time" id={pmtimeoutId} className="form-control" value={pmTimeOut} onChange={this.handlePmTimeout}/></td>
            <td className="cell-align-center"><button type="button" className="btn btn-default" id={date} onClick={() => this.checkInputs(this.state.amTimeIn, this.state.amTimeOut, this.state.pmTimeIn, this.state.pmTimeOut)} disabled={disable}>Save</button></td>
         </tr>

      ); 
   }
}

export default EmployeeAttendance;
