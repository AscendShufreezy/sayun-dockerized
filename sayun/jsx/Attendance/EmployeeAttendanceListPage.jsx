import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import DateInput from '../Utils/DateInput.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import DateWeekRange from '../Utils/DateWeekRange.jsx';
import Utils from '../Utils/Utils.jsx';

import AttendanceListTable from './AttendanceListTable.jsx';
import EmployeeShortDetails from './EmployeeShortDetails.jsx';
import EmployeeAttendanceListTable from './EmployeeAttendanceListTable.jsx';
import Preloader from '../Utils/Preloader.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class EmployeeAttendanceListPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          response: [],
          transfer: false,
          employee: {}, 
         preloader: ""
       };
    }

    componentWillMount() {
      this.setState({preloader: <Preloader/>}); 
     
      var url = window.location.href;
      var empId = url.split('=').pop();
      var employee = {};  
      var self = this;
      
      $.ajax({
        url: Utils.basePath + "/employees/" + empId + "/",
        type:"GET",
        contentType: "application/json",
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
        },
        success: function (data) {
          employee.id = data.id;
          employee.fName = data.first_name;
          employee.mName = data.middle_name;
          employee.lName = data.last_name;
          self.setState({employee: employee});  
          self.setState({preloader: ""}); 
        },
        error: function (response) {
          alert(response);
        }
      });
    }

    render(){
      const preloader = this.state.preloader;
      return (
      	<div id="page-wrapper">
	      	<Menu active="Attendance"/>
	      	<div className="row" id="page-wrapper-inner">
            <Breadcrumb page="attendance-employee"/>
	      		<PageTitle pageTitle="Attendance by Employee"/>
            {preloader}
            <EmployeeShortDetails data={this.state.employee}/>
            <div>
              <hr />
              <Menu active="RegularEmployee" empId={this.state.employee}/>
              <div id="page-wrapper-inner">
                <div className="row" id="page-wrapper-inner3">
                  <DateWeekRange />
                  <br />
                  <EmployeeAttendanceListTable employee={this.state.employee.id}/>
                </div>
                <br />
              </div>
              <br />
            </div>
	      	</div>
          <br />
	    </div>
      );
   }
}

export default EmployeeAttendanceListPage;

