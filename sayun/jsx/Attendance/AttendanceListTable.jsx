import React from 'react';
import ReactDOM from 'react-dom';
import Attendance from './Attendance.jsx';
import Preloader from '../Utils/Preloader.jsx';
import moment from 'moment';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class AttendanceListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         responseAttendance: [],
         date: props.date,
         preloader: <Preloader />
      };
   }

   componentWillReceiveProps(nextProps) {
      if(this.state.date != nextProps.date){
         this.setState({type: "updated"});
         this.setState({preloader: <Preloader />})
         this.setState({date: nextProps.date}, () => this.updateTable()); //for async calls
      }
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#attendanceListTable').html());
      location.href=url
      return false
   }

   getEmployees(){
      var employeeList = [];
      var self = this;
      $.ajax({
         url: Utils.basePath + "/employees/",
         type:"GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         async: false,
         statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
         },
         success: function(response) {
            for(var i=0; i<response.length; i++){
               if(response[i].status == "Active"){
                  employeeList.push(response[i]);
               }
            }
         }.bind(this)
      });
      return employeeList;
   }

   formatDate(today){
      today = today._d;
      var month = today.getMonth() + 1;
      var date = today.getDate();

      if(month < 10) month = "0" + month;
      if(date < 10)date = "0" + date;

      return month + "/" + date + "/" + today.getFullYear();
   }

   ifExists(attendance, attendanceList){
      var result = false;
      var attendanceVal = attendance.fName + attendance.mName + attendance.lName;
      var sample = "";
      for(var i=0; i<attendanceList.length; i++){
         sample = attendanceList[i].fName + attendanceList[i].mName + attendanceList[i].lName;
         if(attendanceVal == sample){
            if(attendance.amTimeIn != "" || attendance.pmTimeIn != ""){
               attendanceList.splice(i, 1);
            } else{
               result = true;
            }
         }
      }
      return result;
   }

   getAttendance(todayFormat, employeeList){
      var self = this;
      $.ajax({
         url: Utils.basePath + "/attendance/?d=" + todayFormat,
         type:"GET",
         dataType:"json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
         },
         success: function(response) {
            const attendanceList = [];
            if(response.length == 0){
               for(var i=0; i<employeeList.length; i++){
                  var attendance = {};
                  attendance.checkboxId = employeeList[i].id;
                  attendance.id = employeeList[i].id;
                  attendance.empId = employeeList[i].id;
                  attendance.fName = employeeList[i].first_name;
                  attendance.mName = employeeList[i].middle_name;
                  attendance.lName = employeeList[i].last_name;
                  attendance.date = todayFormat;
                  attendance.amTimeIn = "";
                  attendance.amTimeOut = "";
                  attendance.pmTimeIn = "";
                  attendance.pmTimeOut = "";
                  attendanceList.push(attendance);
               }
            } else{
               var empList = employeeList;
               for(var i=0; i<employeeList.length; i++){
                  for(var x=0; x<response.length; x++){
                     if(response[x].employee == employeeList[i].id){
                        var attendance = {};
                        var index = employeeList.indexOf(employeeList[i]);
                        attendance.checkboxId = response[x].id;
                        attendance.id = response[x].id;
                        attendance.empId = response[x].employee;
                        attendance.fName = employeeList[i].first_name;
                        attendance.mName = employeeList[i].middle_name;
                        attendance.lName = employeeList[i].last_name;
                        attendance.date = response[x].date;
                        attendance.amTimeIn = response[x].am_time_in;
                        attendance.amTimeOut = response[x].am_time_out;
                        attendance.pmTimeIn = response[x].pm_time_in;
                        attendance.pmTimeOut = response[x].pm_time_out;
                        var ifExists = self.ifExists(attendance, attendanceList);
                        if(ifExists == false){
                           attendanceList.push(attendance);
                        }
                        //employeeList.splice(index, 1);
                     } else{
                        var attendance = {};
                        attendance.checkboxId = employeeList[i].id;
                        attendance.id = employeeList[i].id;
                        attendance.empId = employeeList[i].id;
                        attendance.fName = employeeList[i].first_name;
                        attendance.mName = employeeList[i].middle_name;
                        attendance.lName = employeeList[i].last_name;
                        attendance.date = todayFormat;
                        attendance.amTimeIn = "";
                        attendance.amTimeOut = "";
                        attendance.pmTimeIn = "";
                        attendance.pmTimeOut = "";
                        var ifExists = self.ifExists(attendance, attendanceList);
                        if(ifExists == false){
                           attendanceList.push(attendance);
                        }
                     }
                  }
               }
            }
            self.setState({responseAttendance: attendanceList});
            self.setState({preloader: ""});
         }
      });
   }

   getAttendance2(todayFormat, employeeList){
      var self = this;
      $.ajax({
         url: Utils.basePath + "/attendance/?d=" + todayFormat,
         type:"GET",
         dataType:"json",
         async: false,
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
         },
         success: function(response) {
            const attendanceList = [];
            if(response.length == 0){
               for(var i=0; i<employeeList.length; i++){
                  var attendance = {};
                  attendance.checkboxId = employeeList[i].id;
                  attendance.id = employeeList[i].id;
                  attendance.empId = employeeList[i].id;
                  attendance.fName = employeeList[i].first_name;
                  attendance.mName = employeeList[i].middle_name;
                  attendance.lName = employeeList[i].last_name;
                  attendance.date = todayFormat;
                  attendance.amTimeIn = "";
                  attendance.amTimeOut = "";
                  attendance.pmTimeIn = "";
                  attendance.pmTimeOut = "";
                  attendanceList.push(attendance);
               }
            } else{
               for(var i=0; i<employeeList.length; i++){
                  for(var x=0; x<response.length; x++){
                     if(response[x].employee == employeeList[i].id){
                        var attendance = {};
                        var index = employeeList.indexOf(employeeList[i]);
                        attendance.checkboxId = response[x].id;
                        attendance.id = response[x].id;
                        attendance.empId = response[x].employee;
                        attendance.fName = employeeList[i].first_name;
                        attendance.mName = employeeList[i].middle_name;
                        attendance.lName = employeeList[i].last_name;
                        attendance.date = response[x].date;
                        attendance.amTimeIn = response[x].am_time_in;
                        attendance.amTimeOut = response[x].am_time_out;
                        attendance.pmTimeIn = response[x].pm_time_in;
                        attendance.pmTimeOut = response[x].pm_time_out;
                        var ifExists = self.ifExists(attendance, attendanceList);
                        if(ifExists == false){
                           attendanceList.push(attendance);
                        }
                        //employeeList.splice(index, 1);
                     } else{
                        var attendance = {};
                        attendance.checkboxId = employeeList[i].id;
                        attendance.id = employeeList[i].id;
                        attendance.empId = employeeList[i].id;
                        attendance.fName = employeeList[i].first_name;
                        attendance.mName = employeeList[i].middle_name;
                        attendance.lName = employeeList[i].last_name;
                        attendance.date = todayFormat;
                        attendance.amTimeIn = "";
                        attendance.amTimeOut = "";
                        attendance.pmTimeIn = "";
                        attendance.pmTimeOut = "";
                        var ifExists = self.ifExists(attendance, attendanceList);
                        if(ifExists == false){
                           attendanceList.push(attendance);
                        }
                     }
                  }
               }
            }
            if(JSON.stringify(attendanceList) !== JSON.stringify(self.state.responseAttendance)){
               self.setState({responseAttendance: attendanceList});
               self.setState({preloader: ""});
            }
         }
      });
   }


   sleep(time) {
     return new Promise((resolve) => setTimeout(resolve, time));
   }

   componentWillMount(){
      var self = this;
      var today = this.props.date;
      var todayFormat = this.formatDate(today);
      var employeeList = this.getEmployees();
      this.getAttendance(todayFormat, employeeList);
   }

   updateTable(){
      var self = this;
      var today = this.props.date;
      var todayFormat = this.formatDate(today);
      var employeeList = this.getEmployees();

      this.getAttendance2(todayFormat, employeeList);
   }

   render(){
      var type = this.state.type;
      var preloader = this.state.preloader;

      const result = this.state.responseAttendance.map((attendance) =>
         <Attendance key={attendance.id} attendance={attendance} />
      );
      return(
         <div className="row">
           <div className="row" id="page-wrapper-inner5">
              <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
           </div>
            <div className="col-md-12" id="attendanceListTable">
               {preloader}
               <table className="table table-striped table-bordered table-responsive">
                  <thead>
                     <tr>
                        <th className="col-sm-width">Employee ID</th>
                        <th className="col-lg-width">Employee</th>
                        <th className="col-sm-width">AM Time in</th>
                        <th className="col-sm-width">AM Time out</th>
                        <th className="col-sm-width">PM Time in</th>
                        <th className="col-sm-width">PM Time out</th>
                     </tr>
                  </thead>
                  <tbody>
                     {result}
                  </tbody>
               </table>
            </div>
         </div>
      );
    }
}

export default AttendanceListTable;
