import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Utils from '../Utils/Utils.jsx';

import Breadcrumb from '../Utils/Breadcrumb.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import DateInput from '../Utils/DateInput.jsx';
import Pagination from '../Utils/Pagination.jsx';
import AttendanceListTable from './AttendanceListTable.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class AttendanceListPage extends React.Component {
  constructor(){
    super();

    this.state = {
      date: moment()
    };

    this.handleDate = this.handleDate.bind(this);
  }

  handleDate(e) {
    this.setState({
      date: e
    });
  }

  arrowLeft(date){
    var date = date;
    date = new Date(date).getTime();
    date = date - 1000*60*60*24;
    date = new Date(date);
    this.setState({date: moment(date)});
  }

  arrowRight(date){
    var date = date;
    date = new Date(date).getTime();
    date = date + 1000*60*60*24;
    date = new Date(date);
    this.setState({date: moment(date)});
  }

  clickFile(){
    document.getElementById('my_file').click();
  }

  checkFile() {
    var img = document.getElementById("my_file").files;
    console.log(img[0]);

    var formData = new FormData();
    formData.append('csv', img[0]);
    $.ajax({
        url: Utils.basePath + "/attendance/import_from_csv/",
        type: 'POST',
        contentType: "multipart/form-data",
        data: formData,
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        async: false,
        success: function (data) {
            alert("Biometrics imported!");
            location.reload();
            //self.setState({transfer: true});
        },
        error: function (data) {
            alert("Something is wrong with the file you imported. Please check the data.");
        },
        cache: false,
        contentType: false,
        processData: false
    });

  }

  render(){
    return (
      	<div id="page-wrapper">
	      	<Menu active="Attendance"/>
	      	<div className="row" id="page-wrapper-inner">
	      		<PageTitle pageTitle="Attendance by Date"/>
	      		<DateInput id="date" selected={this.state.date} value={this.state.date} onChange={this.handleDate} arrowRight={() => this.arrowRight(this.state.date)} arrowLeft={() => this.arrowLeft(this.state.date)}/>
            <div>
              <Menu active="Regular"/>
              <div id="page-wrapper-inner">
                <div className="row" id="page-wrapper-inner3">
                  <br />
                  <AttendanceListTable date={this.state.date}/>
                  <input type="button" id="get_file" className="btn btn-default btn-primary pull-right" value="Grab file" value="Import Biometrics" onClick={() => this.clickFile()}/> 
                  <input type="file" id="my_file" style={{display:'none'}} onChange={() => this.checkFile()} />
                  <br />
                </div>
                <br />
              </div>
              <br />
            </div>
	      	</div>
          <br />
	    </div>
    );
  }
}

export default AttendanceListPage;

