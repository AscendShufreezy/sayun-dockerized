import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Link, Switch, Redirect } from 'react-router-dom';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class Attendance extends React.Component {
   constructor() {
      super();

      this.state = {
         response: [],
         transfer: false,
         amTimeIn: "",
         amTimeOut: "",
         pmTimeIn: "",
         pmTimeOut: "", 
         attendanceId: ""
      };
   }

   getCurrentTime(){
      var time = new Date();
      var hours = time.getHours();
      var minutes = time.getMinutes();

      if(hours > 12){
         hours = hours - 12;
      } if(hours < 10){
         hours = "0" + hours;  
      } if(minutes < 10){
         minutes = "0" + minutes;          
      }

      return hours + ":" + minutes + ":00";
   }

   saveAttendance(data, employeeId, msg){
      var self = this;
      $.ajax({
         url: Utils.basePath + "/attendance/",
         type:"POST",
         contentType: "application/json",
         data: JSON.stringify(data),
         async: false,
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
            400: function (response) {
               alert("Please follow correct time format: HH:MM am/pm");
            },
            401: function (response) {
               alert("User is not authorized. Please login.");
               self.setState({logout: true});
            },
            500: function (response) {
               alert("HTTP error 500, please check your connection.");
            }
         },
         success: function(data) {
            document.getElementById(employeeId).value = data.id;
            alert("Successfully timed " + msg + "!");
            self.setState({attendanceId: data.id});
         },
         error: function (response) {
            console.log(response);
         }
      })
   }

   updateAttendance(data, id, msg){
      var self = this;
      if(document.getElementById(id) != null){
         id = document.getElementById(id).value;
      }

      $.ajax({
         url: Utils.basePath + "/attendance/" + id + "/",
         type:"PUT",
         contentType: "application/json",
         data: JSON.stringify(data),
         async: false,
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
            400: function (response) {
               alert("Please follow correct time format: HH:MM am/pm");
            },
            401: function (response) {
               alert("User is not authorized. Please login.");
               self.setState({logout: true});
            },
            500: function (response) {
               alert("HTTP error 500, please check your connection.");
            }
         },
         success: function(data) {
            alert("Successfully timed " + msg + "!");
         },
         error: function (response) {
            console.log(response);
         }
      })
   }

   amTimeIn(e){
      var time = this.getCurrentTime();
      var attendance = this.props.attendance;
      var self = this;
      console.log(attendance);

      if(((attendance.amTimeOut == "00:00:00" || attendance.amTimeOut == "") && this.state.amTimeOut == "") && 
         ((attendance.pmTimeIn == "00:00:00" || attendance.pmTimeIn == "") && this.state.pmTimeIn == "") &&
         ((attendance.pmTimeOut == "00:00:00" || attendance.pmTimeOut == "") && this.state.pmTimeOut == "")){
         var data = {"employee": attendance.id, "date": attendance.date, "am_time_in": time, "am_time_out": "00:00:00", 
               "pm_time_in":  "00:00:00", "pm_time_out": "00:00:00"}

         this.saveAttendance(data, attendance.id, "in");
         self.setState({amTimeIn: time})
         
      } else{
         var amOut = attendance.amTimeOut;
         var pmIn = attendance.pmTimeIn;
         var pmOut = attendance.pmTimeOut;

         if(amOut == ""){
            amOut = this.state.amTimeOut;
         } if(pmIn == ""){
            pmIn = this.state.pmTimeIn;
         } if(pmOut == ""){
            pmOut = this.state.pmTimeOut;
         }

         if(amOut == ""){
            amOut = "00:00:00";
         } if(pmIn == ""){
            pmIn =  "00:00:00";
         } if(pmOut == ""){
            pmOut =  "00:00:00";
         }

         console.log(attendance);
         var data = {"id": attendance.id, "employee": attendance.empId,"date": attendance.date, "am_time_in": time, 
            "am_time_out": amOut, "pm_time_in": pmIn, "pm_time_out": pmOut}

         this.updateAttendance(data, attendance.id, "in");
         self.setState({amTimeIn: time})
      }
   }

   amTimeOut(e){
      var time = this.getCurrentTime();
      var attendance = this.props.attendance;
      var self = this;

      if(((attendance.amTimeIn == "00:00:00" || attendance.amTimeIn == "") && this.state.amTimeIn == "") && 
         ((attendance.pmTimeIn == "00:00:00" || attendance.pmTimeIn == "") && this.state.pmTimeIn == "") &&
         ((attendance.pmTimeOut == "00:00:00" || attendance.pmTimeOut == "") && this.state.pmTimeOut == "")){
         var data = {"employee": attendance.id, "date": attendance.date, "am_time_in": "00:00:00", "am_time_out": time, 
               "pm_time_in":  "00:00:00", "pm_time_out": "00:00:00"}

         this.saveAttendance(data, attendance.id, "out");
         self.setState({amTimeOut: time})
         
      } else{
         var amIn = attendance.amTimeIn;
         var pmIn = attendance.pmTimeIn;
         var pmOut = attendance.pmTimeOut;

         if(amIn == ""){
            amIn = this.state.amTimeIn;
         } if(pmIn == ""){
            pmIn = this.state.pmTimeIn;
         } if(pmOut == ""){
            pmOut = this.state.pmTimeOut;
         }

         if(amIn == ""){
            amIn = "00:00:00";
         } if(pmIn == ""){
            pmIn =  "00:00:00";
         } if(pmOut == ""){
            pmOut =  "00:00:00";
         }

         var data = {"id": attendance.id, "employee": attendance.empId,"date": attendance.date, "am_time_in": amIn, 
         "am_time_out": time,"pm_time_in": pmIn, "pm_time_out": pmOut}

         this.updateAttendance(data, attendance.id, "out");
         self.setState({amTimeOut: time})
      }
   }

   pmTimeIn(e){
      var time = this.getCurrentTime();
      var attendance = this.props.attendance;
      var self = this;

      if(((attendance.amTimeOut == "00:00:00" || attendance.amTimeOut == "") && this.state.amTimeOut == "") && 
         ((attendance.amTimeIn == "00:00:00" || attendance.amTimeIn == "")&& this.state.amTimeIn == "") &&
         ((attendance.pmTimeOut == "00:00:00" || attendance.pmTimeOut == "") && this.state.pmTimeOut == "")){
         var data = {"employee": attendance.id, "date": attendance.date, "am_time_in": "00:00:00", "am_time_out": "00:00:00", 
               "pm_time_in": time, "pm_time_out": "00:00:00"}

         this.saveAttendance(data, attendance.id, "in");
         self.setState({pmTimeIn: time})
         
      } else{
         var amOut = attendance.amTimeOut;
         var amIn = attendance.amTimeIn;
         var pmOut = attendance.pmTimeOut;

         if(amOut == ""){
            amOut = this.state.amTimeOut;
         } if(amIn == ""){
            amIn = this.state.amTimeIn;
         } if(pmOut == ""){
            pmOut = this.state.pmTimeOut;
         }

         if(amOut == ""){
            amOut = "00:00:00";
         } if(amIn == ""){
            amIn =  "00:00:00";
         } if(pmOut == ""){
            pmOut =  "00:00:00";
         }

         var data = {"id": attendance.id, "employee": attendance.empId,"date": attendance.date, "am_time_in": amIn, 
            "am_time_out": amOut, "pm_time_in": time, "pm_time_out": pmOut}

         this.updateAttendance(data, attendance.id, "in");
         self.setState({pmTimeIn: time})
      }
   }

    pmTimeOut(e){
      var time = this.getCurrentTime();
      var attendance = this.props.attendance;
      var self = this;

      if(((attendance.amTimeIn == "00:00:00" || attendance.amTimeIn == "") && this.state.amTimeIn == "") && 
         ((attendance.pmTimeIn == "00:00:00" || attendance.pmTimeIn == "") && this.state.pmTimeIn == "") &&
         ((attendance.amTimeOut == "00:00:00" || attendance.amTimeOut == "") && this.state.amTimeOut == "")){
         var data = {"employee": attendance.id, "date": attendance.date, "am_time_in": "00:00:00", "am_time_out": "00:00:00", 
               "pm_time_in":  "00:00:00", "pm_time_out": time}

         this.saveAttendance(data, attendance.id, "out");
         self.setState({pmTimeOut: time})
         
      } else{
         var amIn = attendance.amTimeIn;
         var pmIn = attendance.pmTimeIn;
         var amOut = attendance.amTimeOut;

         if(amIn == ""){
            amIn = this.state.amTimeIn;
         } if(pmIn == ""){
            pmIn = this.state.pmTimeIn;
         } if(amOut == ""){
            amOut = this.state.amTimeOut;
         }

         if(amIn == ""){
            amIn = "00:00:00";
         } if(pmIn == ""){
            pmIn =  "00:00:00";
         } if(amOut == ""){
            amOut =  "00:00:00";
         }

         var data = {"id": attendance.id, "employee": attendance.empId,"date": attendance.date, "am_time_in": amIn, 
            "am_time_out": amOut,"pm_time_in": pmIn, "pm_time_out": time}

         this.updateAttendance(data, attendance.id, "out");
         self.setState({pmTimeOut: time})
      }
   }
   
   componentWillMount(){
      var attendance = this.props.attendance;
      var link = "/attendance/?employee=" + attendance.empId;

      this.setState({amTimeIn: attendance.amTimeIn});
      this.setState({amTimeOut: attendance.amTimeOut});
      this.setState({pmTimeIn: attendance.pmTimeIn});
      this.setState({pmTimeOut: attendance.pmTimeOut});
   }

   render() {
      var attendance = this.props.attendance;
      var link = "/attendance/employee=" + attendance.empId;

      var amTimeInButton;
      var amTimeOutButton;
      var pmTimeInButton;
      var pmTimeOutButton;

      var amTimeIn = this.state.amTimeIn;
      var amTimeOut = this.state.amTimeOut;
      var pmTimeIn = this.state.pmTimeIn;
      var pmTimeOut = this.state.pmTimeOut;

      var disabled = "";
      if(((attendance.amTimeIn != "00:00:00" && attendance.amTimeIn != "") || (this.state.amTimeIn != "00:00:00" && this.state.amTimeIn != "")) &&
         ((attendance.pmTimeOut != "00:00:00" && attendance.pmTimeOut != "") || (this.state.pmTimeOut != "00:00:00" && this.state.pmTimeOut != "")) &&
         ((attendance.pmTimeIn == "00:00:00" || attendance.pmTimeIn == "") && (this.state.pmTimeIn == "00:00:00" || this.state.pmTimeIn == ""))){
         disabled = "disabled";
      }
     

      if(amTimeIn != "" && amTimeIn != "00:00:00"){
         amTimeInButton = <td className="cell-align-center">{amTimeIn.slice(0,5) + " am"}</td>

      } else{
         amTimeInButton = <td className="cell-align-center"><button type="button" className="btn btn-default" onClick={(e) => this.amTimeIn()}>Time in</button></td>
      }

      if(amTimeOut != "" && amTimeOut != "00:00:00"){
         amTimeOutButton = <td className="cell-align-center">{amTimeOut.slice(0,5) + " am"}</td>
      } else{
         amTimeOutButton = <td className="cell-align-center"><button type="button" disabled={disabled} className="btn btn-default" onClick={(e) => this.amTimeOut()}>Time out</button></td>
      }

      if(pmTimeIn != "" && pmTimeIn != "00:00:00"){
         pmTimeInButton = <td className="cell-align-center">{pmTimeIn.slice(0,5) + " pm"}</td>
      } else{
         pmTimeInButton = <td className="cell-align-center"><button type="button" disabled={disabled} className="btn btn-default" onClick={(e) => this.pmTimeIn()}>Time in</button></td>
      }

      if(pmTimeOut != "" && pmTimeOut != "00:00:00"){
         pmTimeOutButton = <td className="cell-align-center">{pmTimeOut.slice(0,5) + " pm"}</td>
      } else{
         pmTimeOutButton = <td className="cell-align-center"><button type="button" className="btn btn-default" onClick={(e) => this.pmTimeOut()}>Time out</button></td>
      }

      return (
         <tr> 
            <td><Link to={link}><u>{attendance.empId}</u></Link></td>
            <td>{attendance.lName}, {attendance.fName} {attendance.mName}</td>
            {amTimeInButton}
            {amTimeOutButton}
            {pmTimeInButton}
            {pmTimeOutButton}
            <input type="hidden" id={attendance.empId} />
         </tr>
      ); 
   }
}

export default Attendance;
