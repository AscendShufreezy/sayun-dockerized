import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Breadcrumb from '../Utils/Breadcrumb.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class EmployeeShortDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          response: [],
          transfer: false,
          employee: {}
       };
    }
    
	render(){
		var employee = this.props.data;
      return (
      		<div>
  	      	<div className="row">
              <div className="col-conf-2 form-row-padding-top">
                Employee ID
              </div>
              <div className="col-md-3">
                <p className="text-position">{employee.id}</p>
              </div>
            </div>       
            <div className="row">
              <div className="col-conf-2 form-row-padding-top">
                Employee
              </div>
              <div className="col-md-3">
                <p className="text-position">{employee.lName}, {employee.fName} {employee.mName}</p>
              </div>
            </div>
          </div>
      );
   }
}

export default EmployeeShortDetails;

