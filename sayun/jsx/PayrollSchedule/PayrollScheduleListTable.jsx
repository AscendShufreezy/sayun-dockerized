import React from 'react';
import ReactDOM from 'react-dom';
import PayrollSchedule from './PayrollSchedule.jsx';
import Preloader from '../Utils/Preloader.jsx';
import EmptyTable from '../Utils/EmptyTable.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class PayrollScheduleListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         preloader: "",
         empty: "",
         totalProcessed: 0,
         totalPending: 0,
         logout: false
      };
   }

   checkAllCheckbox(name) {
      var array = document.getElementsByName(name);
      for(var i=0; i<array.length; i++){
         var cb = document.getElementById(array[i].id);
         cb.checked = document.getElementById('checkAll').checked;
      }
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#payrollScheduleListTable').html());
      location.href=url
      return false
   }


   componentDidMount(){
      var self = this;
      var empId = this.props.empId;
      var url = window.location.href;
      var tempId = url.split('=').pop();
      var totalPending = 0;
      var totalProcessed = 0;

      var type = "deduction";
      if(url.includes('addition')){
         type = "addition";
      }

      this.setState({preloader: <Preloader/>});
      $.ajax({
         url: Utils.basePath + "/employees/" + empId + "/payroll_schedules/",
         type:"GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
            400: function (response) {
               alert(JSON.parse(response.responseText));
            },
            401: function (response) {
               alert("User is not authorized. Please login.");
               self.setState({logout: true});
            },
            500: function (response) {
               alert("HTTP error 500, please check your connection.");
            }
         },
         success: function(response) {
            const scheduleList = [];
            for(var i=0; i<response.length; i++){
               if(response[i].type == type && response[i].type_id == tempId){
                  var schedule = {};
                  schedule.checkboxId = response[i].id;
                  schedule.id = response[i].id;
                  schedule.date = response[i].date;
                  schedule.payrollDate = response[i].payroll_date;
                  schedule.status = response[i].status;
                  schedule.amount = response[i].amount;
                  if(schedule.status == "Pending"){
                     totalPending = totalPending + parseInt(schedule.amount);
                  } else{
                     totalProcessed = totalProcessed + parseInt(schedule.amount);
                  }

                  scheduleList.push(schedule);
               }
            }
            if(scheduleList.length == 0){
               this.setState({empty: <EmptyTable text="Payroll Schedules" span="5"/>});
            }

            this.setState({totalPending: totalPending});
            this.setState({totalProcessed: totalProcessed});
            this.setState({response: scheduleList});
            this.setState({preloader: ""});
         }.bind(this),
         error: function(xhr, status, err) {
         }.bind(this)
      });
   }

   render(){
    if (this.state.logout) {
             return <Redirect push to="/" />;
    }

      const preloader = this.state.preloader;
      var result = this.state.empty;

      if(this.state.response.length != 0){
         result = this.state.response.map((schedule) =>
            <PayrollSchedule key={schedule.id} schedule={schedule} />
         );
      }

      var totalProcessed = (this.state.totalProcessed).toFixed(2);
      var totalPending = (this.state.totalPending).toFixed(2);

      return(
         <div>
            <div className="row">
               <div className="col-md-12" id="payrollScheduleListTable">
                  {preloader}
                  <table className="table table-striped table-bordered table-responsive">
                     <thead className="thead-font-size">
                        <tr>
                           <th className="checkbox-width"><input type="checkbox" id="checkAll" onChange={(e) => this.checkAllCheckbox('checkbox')} /></th>
                           <th className="col-md-width">Date</th>
                           <th className="col-md-width">Status</th>
                           <th className="col-md-width">Payroll Date</th>
                           <th className="col-md-width">Amount</th>
                        </tr>
                     </thead>
                     <tbody>
                        {result}
                     </tbody>
                  </table>
               </div>
            </div>
            <div className="row">
                <div className="col-md-11">
                  <div className="pull-right">Total Processed</div>
                </div>
                <div className="col-md-1">
                  <div className="pull-right">{totalProcessed}</div>
                </div>
           </div>
           <br />
           <div className="row">
                <div className="col-md-11">
                  <div className="pull-right">Total Pending</div>
                </div>
                <div className="col-md-1">
                  <div className="pull-right">{totalPending}</div>
                </div>
           </div>
           <br />
        </div>
      );
    }
}

export default PayrollScheduleListTable;
