import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';
import moment from 'moment';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class PayrollScheduleAddModal extends React.Component {
  constructor(props){
      super(props);

      this.state = {
         date: moment(),
         installment: 1,
         test: [{}],
         payrollSchedule: {},
         emptyField: "",
         invalidField: "",
         logout: false
      };

      this.handleChange = this.handleChange.bind(this);
      this.handleInstallment = this.handleInstallment.bind(this);
  }

  handleChange(date) {
      this.setState({
          date: date
      });
  }

  handleInstallment(e) {
      this.setState({
          installment: e.target.value
      });
  }

  checkInputs() {
    if(document.getElementById("date").value == ""){
        this.setState({emptyField: "Date"}, () => { 
            alert(this.state.emptyField + " should not be empty.");
            return "";
        });
    } else if(!moment(document.getElementById("date").value,"MM/DD/YYYY",true).isValid()){
        this.setState({invalidField: "Date"}, () => { 
            alert(this.state.invalidField + " should be a valid date.");
            return "";
        });
    } else if(document.getElementById("installment").value == ""){
        this.setState({emptyField: "Week Installment"}, () => { 
            alert(this.state.emptyField + " should not be empty.");
            return "";
        });
    } else if(document.getElementById("installment").value < 1){
        alert("Weeks installment value should not be less than 0.");
    } else{
        this.setState({isComplete: true}, () => { 
            var url = window.location.href;
            var tempId = url.split('=').pop();

            var payrollSchedule = {};
            payrollSchedule.employee = this.props.empId;

            if(url.includes('deduction')){
              payrollSchedule.type = "deduction";
            } else{
              payrollSchedule.type = "addition";
            }

            payrollSchedule.typeId = tempId;
            payrollSchedule.date = document.getElementById("date").value;
            payrollSchedule.status = "Pending";
            payrollSchedule.payrollDate = "01/01/2017";
            payrollSchedule.amount = 0;
            this.setState({payrollSchedule: payrollSchedule}, () => this.savePayrollSchedule()); //for async calls*/
        });
    }
  }

  getPayrollDate(date){
    var date = date;
    var year = date.split('/').pop();
    var month = date.slice(0,2);
    var day = date.slice(3,5);

    date = day + "/" + month + "/" + year;

    $.ajax({
      url: Utils.basePath + "/payroll/get_payroll_date/?date=" + date,
      type:"GET",
      async: false,
      contentType: "application/json",
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
      },

      success: function(data) {
          var year = data.result.slice(0,4);
          var month = data.result.slice(5,7);
          var day = data.result.split('-').pop();
          data = month + "/" + day + "/" + year;

          date = data;
      },
      error: function (response) {
          console.log(response);
      }
    });
    
    return date;
  }

  formatDate(selectedDay){
    var today = new Date(selectedDay);
    var month = today.getMonth() + 1;
    var date = today.getDate();

    if(month < 10){
       month = "0" + month;
    }if(date < 10){
       date = "0" + date;
    }

    return month + "/" + date + "/" + today.getFullYear();
  }

  test(employee, type, typeId, date, payrollDate){
    var test = this.state.test;
    test = {'employee': employee ,'type': type, 'typeid': typeId, 'date': date, 'status': "Pending",
          'payroll_date': payrollDate, 'amount': 0.00};
    return test;
  }

  savePayrollSchedule(){
    var formattedDate = this.formatDate(this.state.date);
    var rawDate = new Date(this.state.date);         
    var pushToPayrollList = [];
    var dataList = [];

    var url = window.location.href;
    var tempId = url.split('=').pop();

    if(this.state.installment >= 1){  
      for(var i=1; i<= this.state.installment; i++){
        var payroll = {};

        //getting first and last day of the week
        var firstDay = rawDate.getDate() - rawDate.getDay();
        firstDay = new Date(rawDate.setDate(firstDay));
        var lastDay = new Date(firstDay.getTime() + 1000*60*60*24*6);

        //payroll data
        payroll.employee = this.props.empId;
        if(url.includes('deduction')){
          payroll.type = "deduction";
        } else{
          payroll.type = "addition";
        }

        payroll.typeId = tempId;
        payroll.date = formattedDate;
        payroll.status = "Pending";
        payroll.payrollDate = this.getPayrollDate(payroll.date);
        payroll.amount = 0;
        pushToPayrollList.push(payroll);
        console.log(payroll.payrollDate);

        //date formatting
        var mmDate = new Date(new Date(formattedDate).getTime() + 1000*60*60*24*6);
        rawDate = mmDate;
        formattedDate = this.formatDate(mmDate);

        var sample = this.test(payroll.employee, payroll.type, payroll.typeId, payroll.date, payroll.payrollDate);
        dataList.push(sample)
      }
    }

    $.ajax({
      url: Utils.basePath + "/payroll-schedules/",
      type:"POST",
      contentType: "application/json",
      data: JSON.stringify(dataList),
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
         400: function (response) {
            alert("Please check dates selected. Dates should be unique.");
         },
         401: function (response) {
            alert("User is not authorized. Please login.");
            self.setState({logout: true});
         },
         500: function (response) {
            alert("HTTP error 500, please check your connection.");
         }
      },
      success: function(data) {
          alert("Payroll Schedules saved!");
          location.reload();
      },
      error: function (response) {
          console.log(response);
      }
    });
  }

  render(){
    if (this.state.logout) {
             return <Redirect push to="/" />;
    }

      return(
        <div className="modal fade modal-position" id="addPayrollScheduleModal" role="dialog">
            <div className="modal-dialog modal-md">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal">&times;</button>
                  <h4 className="modal-title">Add Payroll Schedule</h4>
                </div>
                <div className="modal-body">
                  <div className="row">
                    <div className="col-md-1 col-md-offset-1 form-row-padding-top">
                      Date*
                    </div>
                    <div className="col-md-3">
                      <DatePicker selected={this.state.date} onChange={this.handleChange} id="date" className="form-control"/>
                    </div>
                    <div className="col-md-2 col-md-offset-1">
                      <input type="number" id="installment" className="form-control" value={this.state.installment} onChange={this.handleInstallment}/>
                    </div>
                    <div className="col-md-4 form-row-padding-top">
                      weekly installment(s)
                    </div>
                  </div>
                  <br />
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()} data-dismiss="modal">Add</button>
                  <button type="button" className="btn btn-default btn-warning" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
        </div>
      );
    }
}

export default PayrollScheduleAddModal;

