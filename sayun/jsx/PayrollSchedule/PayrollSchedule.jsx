import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class PayrollSchedule extends React.Component {
   constructor(props){
      super(props);

      var amount = (props.schedule.amount);

      this.state = {
         amount: amount
      };

      this.handleAmount = this.handleAmount.bind(this);
    }

    handleAmount(e) {
        this.setState({
            amount: e.target.value
        });
    }

   render() {
      var schedule = this.props.schedule;
      var disabled = "";
      if(schedule.status == "Processed"){
         disabled = "disabled";
      }
      return (
         <tr> 
            <td><input type="checkbox" name="checkbox" id={schedule.checkboxId} /></td>
            <td><u>{schedule.date}</u></td>
            <td>{schedule.status}</td>
            <td>{schedule.payrollDate}</td>
            <td><input value={this.state.amount} name="amount" onChange={this.handleAmount} className="form-control" disabled={disabled}/></td>
         </tr>
      ); 
   }
}

export default PayrollSchedule;