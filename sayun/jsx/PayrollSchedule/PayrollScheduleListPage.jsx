import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import PayrollScheduleListTable from './PayrollScheduleListTable.jsx';
import PayrollScheduleAddModal from './PayrollScheduleAddModal.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
//import '../../dist/css/blurt.min.css';
//import '../../dist/css/sweetalert.css';

import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class PayrollScheduleListPage extends React.Component {
	constructor(){
	    super();
	    this.state = {
	          logout: false,
	    }
	}

	deleteSchedule(){
	  	var numberOfSchedules = document.getElementsByName("checkbox");
	  	var scheduulesToDelete = [];

	  	for(var i=0; i<numberOfSchedules.length;i++){
	  		if(numberOfSchedules[i].checked){
	  			scheduulesToDelete.push(numberOfSchedules[i].id);
	  		}
	  	}

	  	if(scheduulesToDelete.length > 0){
	  		var deleteMessage = confirm("Are you sure you want to delete the selected payroll schedule/s?");
	  		if (deleteMessage == true) {
	  			var successCounter = 0;
	  			var failCounter = 0;
	  			for(var i=0; i<scheduulesToDelete.length;i++){
	  				$.ajax({
			            url: Utils.basePath + "/payroll-schedules/" + scheduulesToDelete[i] + "/",
			            type:"DELETE",
			            async: false,
			            contentType: "application/json",
			            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
			            statusCode: {
				               400: function (response) {
				                  alert(JSON.parse(response.responseText).id);
				               },
				               401: function (response) {
				                  alert("User is not authorized. Please login.");
				                  self.setState({logout: true});
				               },
				               500: function (response) {
				                  alert("HTTP error 500, please check your connection.");
				               }
						},

			            success: function (data) {
			            	successCounter++;
			            },
			            error: function (response) {
			            	failCounter++;
			            }
			        });

			        if(successCounter == scheduulesToDelete.length){
		  				alert("Deletion of payroll schedule/s is successful!");
		  				location.reload();
		  			} else if(failCounter == scheduulesToDelete.length){
	                	alert("Only pending payroll schedules can be deleted!");
	                } else if((successCounter + failCounter) == scheduulesToDelete.length){
	                	alert("Processed payroll schedules cannot be deleted. Only pending payroll schedules are deleted.");
	                	location.reload();
	                }
	  			}
		    }
	  	} else{
	  		alert('There are no payroll schedule/s selected.');
	  	}
    }

    getTemp(){
    	var url = window.location.href;
    	var id = url.split('=').pop();
    	var amount = 0;

    	if(url.includes('deduction')){
    		$.ajax({
		      url: Utils.basePath + "/temporary-deductions/" + id + "/",
		      type:"GET",
		      contentType: "application/json",
		      async : false,
		      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
		      statusCode: {
		               400: function (response) {
		                  alert(JSON.parse(response.responseText).id);
		               },
		               401: function (response) {
		                  alert("User is not authorized. Please login.");
		                  self.setState({logout: true});
		               },
		               500: function (response) {
		                  alert("HTTP error 500, please check your connection.");
		               }
				},

		      success: function (data) {
		        amount = data.total;
		      }
		    });
    	} else{
    		$.ajax({
		      url: Utils.basePath + "/temporary-additions/" + id + "/",
		      type:"GET",
		      contentType: "application/json",
		      async : false,
		      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
		      statusCode: {
	               400: function (response) {
	                  alert(JSON.parse(response.responseText).id);
	               },
	               401: function (response) {
	                  alert("User is not authorized. Please login.");
	                  self.setState({logout: true});
	               },
	               500: function (response) {
	                  alert("HTTP error 500, please check your connection.");
	               }
				},

		      success: function (data) {
		        amount = data.total;
		      }
		    });
    	}

    	return amount;
    }

    saveSchedules(){
    	var numberOfAmounts = document.getElementsByName("amount");
    	var idsOfPayrollSchedules = document.getElementsByName("checkbox");

    	var amountsToCompute = [];
    	var idsToUpdate = [];
    	var total = 0;
    	var tempTotal = this.getTemp();
    	var scheduleType = "";

    	for(var i=0; i<numberOfAmounts.length;i++){
	  		total = total + parseFloat(numberOfAmounts[i].value);
	  		amountsToCompute.push(numberOfAmounts[i].value);
	  		idsToUpdate.push(idsOfPayrollSchedules[i].id);
	  	}	

	  	if(tempTotal != total.toFixed(2)){
        	var url = window.location.href;
        	if(url.includes('deduction')){
        		alert("Total amount of Payroll Schedule does not match total amount of Temporary Deduction.",);
        	} else{
        		alert("Total amount of Payroll Schedule does not match total amount of Temporary Addition.");
        	}

	  	} else{
	  		var counter = 0;
	  		for(var i=0; i<idsToUpdate.length; i++){
	  			var schedule = {};
	  			$.ajax({
			      url: Utils.basePath + "/payroll-schedules/" + idsToUpdate[i] + "/",
			      type:"GET",
			      contentType: "application/json",
			      async: false,
			      data: JSON.stringify(data),
			      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
			      statusCode: {
		               400: function (response) {
		                  alert(JSON.parse(response.responseText).id);
		               },
		               401: function (response) {
		                  alert("User is not authorized. Please login.");
		                  self.setState({logout: true});
		               },
		               500: function (response) {
		                  alert("HTTP error 500, please check your connection.");
		               }
					},

			      success: function(data) {
			         schedule = data;
			      }
			    });
 
	  			var data = {"id": idsToUpdate[i], 'employee': schedule.employee ,'type': schedule.type, 'typeid': schedule.type_id, 'date': schedule.date,
	  			 		'status': schedule.status, 'payroll_date': schedule.payroll_date, 'amount': amountsToCompute[i]};

	  			$.ajax({
			      url: Utils.basePath + "/payroll-schedules/" + idsToUpdate[i] + "/",
			      type:"PUT",
			      contentType: "application/json",
			      data: JSON.stringify(data),
			      async: false,
			      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
			      statusCode: {
			         401: function (response) {
			            alert("User is not authorized. Please login.");
			            self.setState({logout: true});
			         },
			         500: function (response) {
			            alert("HTTP error 500, please check your connection.");
			         }
			      },
			      success: function(data) {
			          counter++;
			      },
			      error: function (response) {
			          console.log(response);
			      }
			    });
	  		}
	  		if(counter == idsToUpdate.length){
	  			alert('Payroll Schedules updated!');
	  			location.reload();
	  		}
	  	}
    }

    computeProcessed(){
    	var numberOfProcessed = document.getElementsByName("status");
    	var numberOfAmounts = document.getElementsByName("amount");
    	var total = 0;

    	for(var i=0; i<numberOfProcessed.length;i++){
    		if(numberOfProcessed[i].value == "Processed"){
    			total = total + numberOfAmounts[i].value;
    		}
    	}
    	return total;
    }

    computePending(){
    	var numberOfPending = document.getElementsByName("status");
    	var numberOfAmounts = document.getElementsByName("amount");
    	var total = 0;
    	for(var i=0; i<numberOfPending.length;i++){
    		if(numberOfPending[i].value == "Pending"){
    			total = total + numberOfAmounts[i].value;
    		}
    	}
    	return total;
    }

    render(){
    	if (this.state.logout) {
             return <Redirect push to="/" />;
 		}

    	var empId = this.props.empId;
      	return (
      		<div>
		      	<PageTitle pageTitle="Payroll Schedule"/>
	            <div className="row" id="page-wrapper-inner3">
                	<br />
	                <form>
	                    <PayrollScheduleListTable empId={empId}/>
	                    <div className="row">
		                    <div className="col-md-3 pull-right">
		                        <div className="pull-right">
                           			<button type="button" className="btn btn-default btn-primary" data-toggle="modal" data-target="#addPayrollScheduleModal">Add</button>
		                            <button type="button" className="btn btn-default btn-primary" onClick={() => this.saveSchedules()}>Save</button>
		                            <button type="button" className="btn btn-default btn-warning" onClick={() => this.deleteSchedule()}>Delete</button>
		                        	<PayrollScheduleAddModal empId={empId}/>
		                        </div>
		                    </div>
	                    </div>
	                </form>
                	<br />
                </div>
                <br />
            </div>
     	);
    }
}

export default PayrollScheduleListPage;

