import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';
import moment from 'moment';
import Utils from '../Utils/Utils.jsx';
import Input from '../Utils/Input.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class EditUserForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            name: props.user.name,
            username: props.user.username,
            group: props.user.group,
            password: "sample",
            repassword: "sample",
            warehouse: props.user.warehouse,
            groups: [],
            warehouses: []
        };

        this.handleName = this.handleName.bind(this);
        this.handleUsername = this.handleUsername.bind(this);
        this.handleGroup = this.handleGroup.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.handleRepassword = this.handleRepassword.bind(this);
        this.handleWarehouse = this.handleWarehouse.bind(this);
    }

    handleName(e) {
        this.setState({
            name: e.target.value
        });
    }

    handleUsername(e) {
        this.setState({
            username: e.target.value
        });
    }

    handleGroup(e) {
        this.setState({
            group: e.target.value
        });
    }

    handlePassword(e) {
        this.setState({
            password: e.target.value
        });
    }

    handleRepassword(e) {
        this.setState({
            repassword: e.target.value
        });
    }

    handleWarehouse(e) {
        this.setState({
            warehouse: e.target.value
        });
    }

    componentWillMount(){
        var self = this;
        $.ajax({
            url: Utils.basePath + "/groups/",
            type:"GET",
            contentType: "application/json",
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },

            success: function(data) {
                const groupList = [];
                for(var i=0;i<data.length;i++){
                    groupList.push(data[i]);

                    if(data[i].name == "pos-group"){
                        self.setState({posId: data[i].id});
                    }
                }
                self.setState({groups: groupList});
            },
            error: function (response) {
                console.log(response);
            }
        });

        $.ajax({
            url: Utils.basePath + "/warehouses/",
            type:"GET",
            contentType: "application/json",
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },

            success: function(data) {
                const warehouseList = [];
                for(var i=0;i<data.length;i++){
                    warehouseList.push(data[i]);
                }
                self.setState({warehouses: warehouseList});
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    render(){
        var user = this.props.user;

        const groupsResult = this.state.groups.map((groups) =>
            <option value={groups.id} key={groups.id}>{groups.name}</option>
        );       

        const warehousesResult = this.state.warehouses.map((warehouses) =>
            <option value={warehouses.id} key={warehouses.id}>{warehouses.name}</option>
        );

        var disable = "";
        if(this.state.group != this.state.posId){
            disable = "disabled";
        }

        return (
            <div>
                <div className="row" id="page-wrapper-inner3">
                    <div className="row">
                        <div className="col-md-2">
                            Name*
                        </div>
                        <div className="col-md-3">
                            <input id="name" type="text" className="form-control" value={this.state.name} onChange={this.handleName} />
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                         <div className="col-md-2">
                            Username*
                        </div>
                        <div className="col-md-3">
                            <Input id="userusername" type="text" defaultValue={this.state.username} disable="Active" />            
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-2">
                            Group*
                        </div>
                        <div className="col-md-3">
                            <select className="form-control" id="group" value={this.state.group} onChange={this.handleGroup}>
                                {groupsResult}
                            </select>
                        </div>
                        <div className="col-md-2 col-md-offset-1">
                            Default Warehouse
                        </div>
                        <div className="col-md-2">
                            <select className="form-control" id="warehouse" value={this.state.warehouse} onChange={this.handleWarehouse} disabled={disable}>
                                {warehousesResult}
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}   

export default EditUserForm;

