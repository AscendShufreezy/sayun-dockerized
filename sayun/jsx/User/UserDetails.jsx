import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Breadcrumb from '../Utils/Breadcrumb.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import EditUserForm from './EditUserForm.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class UserDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          response: [],
          transfer: false,
          isComplete: false,
          emptyField: "",
          user: {},
          logout: false
       };
    }

    cancelSave() {
		this.setState({transfer: true});
    }

    checkInputs() {
    	if(document.getElementById("name").value == ""){
            this.setState({emptyField: "Name"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("userusername").value == ""){
            this.setState({emptyField: "Username"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("group").value == ""){
            this.setState({emptyField: "Group"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else{
    		this.setState({isComplete: true}, () => { 
    			this.updateUser();
			});
    	}
    }

    updateUser() {
        var url = window.location.href;
        var id = url.split('=').pop();
        var self = this;
        var user = this.state.user;
        var groupList =  [];
        var warehouse = [];
        var admin = false;

        if(document.getElementById("warehouse") != null && document.getElementById("warehouse").value != ""){
            warehouse = [document.getElementById("warehouse").value];
        } 

        if(document.getElementById("group").value == '1'){ //admin-group id ==1
            admin = true;
        }


        user.name = document.getElementById('name').value;
        user.username = document.getElementById('userusername').value;
        groupList.push(document.getElementById('group').value);
        user.group = groupList;
        user.admin = admin;
        user.warehouse = warehouse;
       
        var data = {"name": user.name,"username": user.username,"groups": user.group,
                    "admin": user.admin, "warehouses": user.warehouse}

		$.ajax({
            url: Utils.basePath + "/users/" + id + "/",
            type: "PUT",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },

            success: function (data) {
                alert("User updated!");
                self.setState({transfer: true});
            },
            error: function (response) {
                console.log(response);
            }
        });
  }

  componentWillMount(){
    var group = sessionStorage.getItem('group');
    var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

    if (group != "admin-group" && sessionPermission.indexOf(16) == -1) {
        alert("You are not authorized to access this page. Please login as admin user.");
        this.setState({logout: true});
    }
  }

	render(){
    if (this.state.logout) {
      return <Redirect push to="/" />;
    }

		if (this.state.transfer) {
		    return <Redirect push to="/user" />;
		}

    var url = window.location.href;
    var id = url.split('=').pop();
    var self = this;
    var user = {};

    $.ajax({
      url: Utils.basePath + "/users/" + id + "/",
      type:"GET",
      contentType: "application/json",
      async: false,
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
         400: function (response) {
            alert(JSON.parse(response.responseText).id);
         },
         401: function (response) {
            alert("User is not authorized. Please login.");
            self.setState({logout: true});
         },
         500: function (response) {
            alert("HTTP error 500, please check your connection.");
         }
      },

      success: function (data) {
          console.log("retrieved: " + JSON.stringify(data));
          user.name = data.name;
          user.username = data.username;
          user.group = data.groups[0].id;
          user.warehouse = [];
          if(data.warehouses.length > 0){
              user.warehouse = data.warehouses[0].id;
          }
      },
      error: function (response) {
          alert(response);
      }
    });

  	return (
    	<div id="page-wrapper">
      	<Menu active="User"/>
      	<div className="row" id="page-wrapper-inner">
      		<Breadcrumb page="edit-user"/>
      		<PageTitle pageTitle="User"/>
      		<div className="row" id="page-wrapper-inner3">
      			<form>
	      			<EditUserForm user={user}/>
	      			<div className="row">
			            <div className="col-md-3 pull-right">
			              <div className="pull-right">
			                <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
			                <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
			              </div>
			            </div>
			        </div>
		        </form>
      			<br />
      		</div>
      	</div>	
        <br />  
      </div>
    );
  }
}

export default UserDetails;

