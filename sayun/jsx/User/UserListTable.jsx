import React from 'react';
import ReactDOM from 'react-dom';
import EmptyTable from '../Utils/EmptyTable.jsx';
import Preloader from '../Utils/Preloader.jsx';
import moment from 'moment';
import Utils from '../Utils/Utils.jsx';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class UserListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         date: moment(),
         preloader: "",
         empty: "",
         filter1: "",
         filter2: "",
         filter3: ""
      };
   }

   checkAllCheckbox(name) {
      var array = document.getElementsByName(name);
      for(var i=0; i<array.length; i++){
         var cb = document.getElementById(array[i].id);
         cb.checked = document.getElementById('checkAll').checked;
      }
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#userTable').html());
      location.href=url
      return false
   }

   componentDidMount(){
         var self = this;
         this.setState({preloader: <Preloader/>});
         $.ajax({
            url: Utils.basePath + "/users/",
            type:"GET",
            contentType: "application/json",
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(response) {
               const userList = [];
               for(var i=0; i<response.length; i++){
                  var user = {};

                  user.checkboxId = response[i].id;
                  user.id = response[i].id;
                  user.name = response[i].name;
                  user.username = response[i].username;

                  if(response[i].groups[0] == null){
                     user.group = "123";
                  } else{
                     user.groupname = response[i].groups[0].name;
                     user.group = response[i].groups[0].id;
                  }

                  userList.push(user);
               }

               if(userList.length == 0){
                  this.setState({empty: <EmptyTable text="Users" span="4"/>});
               }

               this.setState({response: userList});
               this.setState({preloader: ""});
            }.bind(this),
            error: function(xhr, status, err) {
            }.bind(this)
        });
   }

   userFormatter(cell, row){
      var link = "/user/id=" + row.id;
      return (
         <Link to={link}><u>{cell}</u></Link>
      );
   }

   groupFormatter(cell, row){
      console.log(row);
      var link = "/group/id=" + cell;
      return (
         <Link to={link}><u>{row.groupname}</u></Link>
      );
   }

   checkboxFormatter(cell, row){
      return (
         <input type="checkbox" name="checkbox" id={cell} />
      );
   }

   filterShow1(){
      if(this.state.filter1 == ""){
         this.setState({filter1: 'RegexFilter'});
      } else{
         this.setState({filter1: ''});
      }
   }

   filterShow2(){
      if(this.state.filter2 == ""){
         this.setState({filter2: 'RegexFilter'});
      } else{
         this.setState({filter2: ''});
      }
   }

   filterShow3(){
      if(this.state.filter3 == ""){
         this.setState({filter3: 'RegexFilter'});
      } else{
         this.setState({filter3: ''});
      }
   }

   render(){
      const preloader = this.state.preloader;
      var filter1 = this.state.filter1;
      var filter2 = this.state.filter2;
      var filter3 = this.state.filter3;
      return(
         <div className="row">
            <div className="row" id="page-wrapper-inner5">
               <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
            </div>
            <div className="col-md-12" id="userTable">
               {preloader}
               <BootstrapTable data={this.state.response} striped hover pagination>
                  <TableHeaderColumn width="40px" isKey dataField='checkboxId' dataFormat={ this.checkboxFormatter }>
                     <input type="checkbox" id="checkAll" onChange={(e) => this.checkAllCheckbox('checkbox')} />
                  </TableHeaderColumn>
                  <TableHeaderColumn dataField='name' filter={ { type: filter1 } }  dataSort={ true }>Name <i className="fa fa-search search-size" onMouseOver={() => this.filterShow1()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='username' filter={ { type: filter2 } }  dataFormat={ this.userFormatter } dataSort={ true }>Username <i className="fa fa-search search-size" onMouseOver={() => this.filterShow2()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='group' filter={ { type: filter3 } }  dataFormat={ this.groupFormatter } dataSort={ true }>Group <i className="fa fa-search search-size" onMouseOver={() => this.filterShow3()}></i></TableHeaderColumn>
               </BootstrapTable>
            </div>
         </div>
      );
    }
}

export default UserListTable;
