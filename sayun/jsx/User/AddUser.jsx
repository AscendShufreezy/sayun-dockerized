import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import AddUserForm from './AddUserForm.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class AddUser extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          response: [],
          transfer: false,
          isComplete: false,
          emptyField: "",
          user: {},
          groups: "",
          warehouses: "",
          logout: false
       };
    }

    cancelSave() {
        this.setState({transfer: true});
    }

    checkInputs() {
        if(document.getElementById("name").value == ""){
            this.setState({emptyField: "Name"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("userusername").value == ""){
            this.setState({emptyField: "Username"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("userpassword").value == ""){
            this.setState({emptyField: "Password"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("repassword").value == ""){
            this.setState({emptyField: "Retype Password"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("group").value == ""){
            this.setState({emptyField: "Group"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        }  else if(document.getElementById("repassword").value != document.getElementById("userpassword").value){
                alert("Passwords do not match!");
        } else{
            this.setState({isComplete: true}, () => { 
                var user = {};
                var groupList =  [];
                var warehouse = [];
                var admin = false;

                if(document.getElementById("warehouse") != null && document.getElementById("warehouse").value != ""){
                    warehouse = [document.getElementById("warehouse").value];
                } 
                if(document.getElementById("group").value == '1'){ //admin-group id == 1
                    admin = true;
                }

                console.log(warehouse);

                user.name = document.getElementById("name").value;
                user.username = document.getElementById("userusername").value;
                user.password = document.getElementById("userpassword").value;
                groupList.push(document.getElementById('group').value);
                user.group = groupList;
                user.admin = admin;
                user.warehouse = warehouse;
                this.setState({user: user}, () => this.saveUser()); //for async calls
            });
        }
    }

    saveUser() {
        var self = this;
        var user = this.state.user;
        var data = {
            "name": user.name,
            "username": user.username,
            "password": user.password,
            "admin": user.admin,
            "groups": user.group,
            "warehouses": user.warehouse
        }
                    
        $.ajax({
            url: Utils.basePath + "/users/",
            type:"POST",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).username);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
                alert("User saved!");
                self.setState({transfer: true});
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    componentWillMount(){
        var group = sessionStorage.getItem('group');
        var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

        if (group != "admin-group" && sessionPermission.indexOf(13) == -1) {
            alert("You are not authorized to access this page. Please login as admin user.");
            this.setState({logout: true});
        } 
    }

    render(){
        if (this.state.logout) {
          return <Redirect push to="/" />;
        }

        if (this.state.transfer) {
            return <Redirect push to="/user" />;
        }

        return (
            <div id="page-wrapper">
                <Menu active="User"/>
                <div className="row" id="page-wrapper-inner">
                    <Breadcrumb page="add-user"/>
                    <PageTitle pageTitle="Add User"/>
                    <div className="row" id="page-wrapper-inner3">
                        <form >
                            <AddUserForm />
                            <div className="row">
                                <div className="col-md-3 pull-right">
                                  <div className="pull-right">
                                    <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
                                    <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
                                  </div>
                                </div>
                            </div>
                        </form>
                        <br />
                    </div>
                </div>
                <br />  
            </div>
      );
   }
}

export default AddUser;