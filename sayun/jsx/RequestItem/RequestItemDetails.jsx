import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import EditRequestItemForm from './EditRequestItemForm.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';;

class RequestItemDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          response: [],
          transfer: false,
          isComplete: false,
          emptyField: "",
          requestItem: {}
       };
    }

    cancelSave() {
		  this.setState({transfer: true});
    }

    checkInputs() {
    	if(document.getElementById("date").value == ""){
            this.setState({emptyField: "Date"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("customer").value == ""){
            this.setState({emptyField: "Customer"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else{
      		this.setState({isComplete: true}, () => { 
      			this.updateRequest();
  			});
    	}
    }  

    updateRequest() {
        /*var url = window.location.href;
        var reqId = url.split('=').pop();
        var request = this.state.request;
        var warehouse;
        if(document.getElementById("warehouse") == null){
            warehouse = "";
        } else{
            warehouse = document.getElementById("warehouse").value;
        }

        request.id = reqId;
        request.date = document.getElementById("date").value;
        request.customer = document.getElementById("customer").value;
        request.customerType = "Truck";
        request.warehouse = warehouse;
        
        var data = {"date": request.date, "warehouse": request.warehouse,
                    "customer": request.customer,"customerType": request.customerType}

        var self = this;
		    $.ajax({
            url: Utils.basePath + "/requests/" + recId + "/",
            type: "PUT",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText));
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function (data) {
                alert("Item Out updated!");
                self.setState({transfer: true});
            },
            error: function (response) {
                console.log(response);
            }
        });*/

        alert("Item Out updated!");
        this.setState({transfer: true});
    }

	render(){
		if (this.state.transfer) {
		    return <Redirect push to="/request" />;
		}

        var url = window.location.href;
        var id = url.split('=').pop();
        var self = this;
        var request = {};

       /*$.ajax({
            url: Utils.basePath + "/requests/" + id + "/",
            type:"GET",
            contentType: "application/json",
            async : false,
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            success: function (data) {
                request.id = data.id;
                request.date = data.date;
                request.customer = data.customer;
                request.warehouse = data.warehouse;
            }
        });*/
        request.id = "OUT231";
        request.date = "5/25/2017";
        request.customer = "Truck 2";
        request.warehouse = "Warehouse 1";

      	return (
	      	<div id="page-wrapper">
		      	<Menu active="Request"/>
		      	<div className="row" id="page-wrapper-inner">
		      		<Breadcrumb page="edit-request"/>
		      		<PageTitle pageTitle="Item Out"/>
		      		<div className="row" id="page-wrapper-inner3">
		      			<form>
			      			<EditRequestForm request={request}/>
			      			<div className="row">
					            <div className="col-md-3 pull-right">
					              <div className="pull-right">
					                <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
					                <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
					              </div>
					            </div>
					        </div>
				        </form>
                <hr />
                <RequestItemPage request={request.id}/>
		      		</div>
		      	</div>	
            <br />  
		    </div>
      );
   }
}

export default RequestItemDetails;

