import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Link, Switch, Redirect } from 'react-router-dom';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class RequestItemAddModal extends React.Component {
  constructor(props){
      super(props);

      this.state = {
         items: [],
         item: "",
         unitPrice: 0.00,
         stock: 0,
         total: "",
         status: "Draft",
         qty: 0,
         requestItem: {},
         logout: false
      };

      this.handleItem = this.handleItem.bind(this);
      this.handleQty = this.handleQty.bind(this);
  }

  handleItem(e) {
    this.setState({item: e.target.value});
    var itemId = e.target.value;
    var self = this;
    $.ajax({
      url: Utils.basePath + "/items/" + itemId + "/",
      type: "GET",
      contentType: "application/json",
      async: false,
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
         400: function (response) {
            alert(JSON.parse(response.responseText).id);
         },
         401: function (response) {
            alert("User is not authorized. Please login.");
            self.setState({logout: true});
         },
         500: function (response) {
            alert("HTTP error 500, please check your connection.");
         }
      },
      success: function (data) {
        self.setState({stock: data.stock, unitPrice: data.unit_price});
      }
    });

  }

  handleQty(e) {
    this.setState({qty: e.target.value});
  }

  checkInputs(total) {
    if(document.getElementById("item").value == ""){
        this.setState({emptyField: "Item"}, () => { 
            alert(this.state.emptyField + " should not be empty.");
            return "";
        });
    } else if(this.state.qty > this.state.stock){
        alert("Quantity selected should not be greater than stock.");
    } else if(this.state.qty == ""){
        this.setState({emptyField: "Quantity"}, () => { 
            alert(this.state.emptyField + " should not be empty.");
            return "";
        });
    } else if(this.state.qty <= 0){
        this.setState({emptyField: "Quantity"}, () => { 
            alert(this.state.emptyField + " should be greater than 0.");
            return "";
        });
    } else{
        this.setState({isComplete: true}, () => { 
          var requestItem = {};
          var reqId = window.location.href.split('=').pop();
          requestItem.request = reqId;
          requestItem.item = this.state.item;
          requestItem.quantity = this.state.qty;
          requestItem.status = "Draft";
          requestItem.unitPrice = this.state.unitPrice;
          requestItem.total = this.state.total;
          this.setState({requestItem: requestItem}, () => this.saveItemRequest(total)); //for async calls
        });
    }
  }

  formatDate(){
    var today = new Date();
    var month = today.getMonth() + 1;
    var date = today.getDate();

    if(month < 10){
       month = "0" + month;
    }if(date < 10){
       date = "0" + date;
    }

    return month + "/" + date + "/" + today.getFullYear();
   }

  getItemData(itemId){
    $.ajax({
      url: Utils.basePath + "/items/",
      type:"GET",
      contentType: "application/json",
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
         400: function (response) {
            alert(JSON.parse(response.responseText).id);
         },
         401: function (response) {
            alert("User is not authorized. Please login.");
            self.setState({logout: true});
         },
         500: function (response) {
            alert("HTTP error 500, please check your connection.");
         }
      },
      success: function(data) {
          return data;
      }
    });      
  }

  saveItemRequest(total){
    var requestItem = this.state.requestItem;
    var date = this.formatDate();

    var data = {"request": requestItem.request, "item": requestItem.item, "date": date, "quantity": requestItem.quantity,
                "status": requestItem.status, "unit_price": requestItem.unitPrice, "total": total}
            
    $.ajax({
      url: Utils.basePath + "/item-requests/",
      type:"POST",
      contentType: "application/json",
      data: JSON.stringify(data),
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
          400: function (response) {
            alert(JSON.parse(response.responseText).date);
          },
          401: function (response) {
            alert("User is not authorized. Please login.");
            self.setState({logout: true});
          },
          500: function (response) {
            alert("HTTP error 500, please check your connection.");
          }
      },
      success: function(data) {
          alert("Item Request is saved!");
          location.reload();
      },
      error: function (response) {
          console.log(response);
      }
    });            
  }

  componentWillMount(){
    var self = this;
    $.ajax({
      url: Utils.basePath + "/items/",
      type:"GET",
      contentType: "application/json",
      async: false,
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
         400: function (response) {
            alert(JSON.parse(response.responseText).id);
         },
         401: function (response) {
            alert("User is not authorized. Please login.");
            self.setState({logout: true});
         },
         500: function (response) {
            alert("HTTP error 500, please check your connection.");
         }
      },
      success: function(response) {
        const itemList = [];
          for(var i=0; i<response.length; i++){
            itemList.push(response[i]);
          }
          self.setState({items: itemList, item: itemList[0].id});
          self.setState({unitPrice: itemList[0].unit_price, stock: itemList[0].stock});
      }.bind(this),
      error: function(xhr, status, err) {
      }.bind(this)
    })
  }

  render(){
    if (this.state.logout) {
      return <Redirect push to="/" />;
    }

    const itemResult = this.state.items.map((items) =>
      <option key={items.id} value={items.id}>{items.name}</option>
    ); 

    var unitPrice = this.state.unitPrice;
    var total = (this.state.qty * unitPrice).toFixed(2);
    var totalFormat = total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    return(
      <div className="modal fade modal-position" id="addRequestItemModal" role="dialog">
          <div className="modal-dialog modal-md">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal">&times;</button>
                <h4 className="modal-title">Add Item to Request</h4>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-md-1 col-md-offset-1 form-row-padding-top">
                    Item*
                  </div>
                  <div className="col-md-4">
                    <select className="form-control" id="item" value={this.state.item} onChange={this.handleItem}>
                      <option selected disabled></option>
                      {itemResult}
                    </select>
                  </div>
                  <div className="col-md-3 form-row-padding-top">
                    <i className="fa fa-plus-circle"></i><Link to="/item/add-item" target="_blank"><u> Add New Item</u></Link>
                  </div>
                  <div className="col-md-3 form-row-padding-top">
                    Status: {this.state.status}
                  </div>
                </div>
                <div className="row form-row-padding-top">
                  <div className="col-md-1 col-md-offset-1 form-row-padding-top">
                    Qty*
                  </div>
                  <div className="col-md-2">
                    <input type="number" name="qty" id="qty" className="form-control" value={this.state.qty} onChange={this.handleQty}/>                    </div>
                  <div className="col-md-3 form-row-padding-top">
                    Unit Price: {unitPrice}
                  </div>
                  <div className="col-md-3 form-row-padding-top">
                    In-Stock Qty: {this.state.stock}
                  </div>
                </div>
                <div className="row form-row-padding-top">
                  <div className="col-md-1 col-md-offset-1 form-row-padding-top">
                    Total*
                  </div>
                  <div className="col-md-2 form-row-padding-top">
                    {totalFormat}
                  </div>
                </div>
                <br /><br />
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs(total)} data-dismiss="modal">Add</button>
                <button type="button" className="btn btn-default btn-warning" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </div>
      </div>
    );
  }
}

export default RequestItemAddModal;

