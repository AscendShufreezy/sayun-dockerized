import React from 'react';
import ReactDOM from 'react-dom';
import RequestItemTable from './RequestItemTable.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import RequestItemAddModal from './RequestItemAddModal.jsx';
import RequestItemEditModal from './RequestItemEditModal.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class RequestItemPage extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      total: (0).toFixed(2),
      logout: false
    };
  }

  deleteRequest(){
      var numberOfRequestItems = document.getElementsByName("checkbox");
      var requestItemsToDelete = [];

      for(var i=0; i<numberOfRequestItems.length;i++){
         if(numberOfRequestItems[i].checked){
            requestItemsToDelete.push(numberOfRequestItems[i].id);
         }
      }

      if(requestItemsToDelete.length > 0){
         var deleteMessage = confirm("Are you sure you want to delete the selected request item/s?");
         if (deleteMessage == true) {
            var successCounter = 0;
            for(var i=0; i<requestItemsToDelete.length;i++){
               $.ajax({
                     url: Utils.basePath + "/item-requests/" + requestItemsToDelete[i] + "/",
                     type:"DELETE",
                     contentType: "application/json",
                     headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
                     statusCode: {
                           400: function (response) {
                              alert(JSON.parse(response.responseText).detail);
                           },
                           401: function (response) {
                              alert("User is not authorized. Please login.");
                              self.setState({logout: true});
                           },
                           500: function (response) {
                              alert("HTTP error 500, please check your connection.");
                           }
                    },

                     success: function (data) {
                        successCounter++;

                        if(successCounter == requestItemsToDelete.length){
                        alert("Deletion of request item/s is successful!");
                        location.reload();
                     }
                     },
                     error: function (response) {

                     }
                 });
            }
          }
      } else{
         alert('There are no request item/s selected.');
      }
   }

   render(){
    if (this.state.logout) {
      return <Redirect push to="/" />;
    }
    var requestItem = {};
    var requestId = this.props.request;
    return(
       <div className="row">
          <div className="col-md-12">
             <PageTitle pageTitle="List of Items Requested"/>
              <form>
                <RequestItemTable requestId={requestId} />
                <br />
                <div className="row">
                    <div className="col-md-3 pull-right">
                      <div className="pull-right">
                        <button type="button" className="btn btn-default btn-primary" data-toggle="modal" data-target="#addRequestItemModal">Add</button>
                        <button type="button" className="btn btn-default btn-warning" onClick={() => this.deleteRequest()}>Delete</button>
                        <RequestItemAddModal />
                        
                      </div>
                    </div>
                </div>
              </form>
              <br />
          </div>
       </div>
    );
  }
}

export default RequestItemPage;

