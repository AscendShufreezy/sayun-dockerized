import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Link, Switch, Redirect } from 'react-router-dom';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class RequestItemEditModal extends React.Component {
  constructor(props){
    super(props);

    var item = "";
    var unitPrice = 0;
    var status = "";
    var qty = 0;
    var total = 0;

    this.state = {
       items: [],
       item: item,
       unitPrice: unitPrice,
       status: status,
       qty: qty,
       total: total,
       requestItem: {},
       logout: false
    };

    this.handleItem = this.handleItem.bind(this);
    this.handleQty = this.handleQty.bind(this);
  }

  handleItem(e) {
    this.setState({item: e.target.value});
    var itemId = e.target.value;
    var self = this;
    $.ajax({
      url: Utils.basePath + "/items/" + itemId + "/",
      type: "GET",
      contentType: "application/json",
      async: false,
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
         400: function (response) {
            alert(JSON.parse(response.responseText).id);
         },
         401: function (response) {
            alert("User is not authorized. Please login.");
            self.setState({logout: true});
         },
         500: function (response) {
            alert("HTTP error 500, please check your connection.");
         }
      },

      success: function (data) {
        self.setState({unitPrice: data.unit_price});
        document.getElementById('unitPrice').value = data.unit_price;
        document.getElementById('total').value = "0.00";
      }
    });
  }

  handleQty(e) {
    var item = document.getElementById('item').value;
    this.setState({qty: e.target.value});
    this.setState({item: item});
    document.getElementById('total').value = (document.getElementById('unitPrice').value * e.target.value).toFixed(2);
  }

  checkInputs(total, typeEdit) {
    /*if(document.getElementById("item").value == ""){
        this.setState({emptyField: "Item"}, () => { 
            alert(this.state.emptyField + " should not be empty.");
            return "";
        });
    } else if(document.getElementById("qty").value == ""){
        this.setState({emptyField: "Quantity"}, () => { 
            alert(this.state.emptyField + " should not be empty.");
            return "";
        });
    } else if(document.getElementById("qty").value <= 0){
        this.setState({emptyField: "Quantity"}, () => { 
            alert(this.state.emptyField + " should be greater than 0");
            return "";
        });
    } else{*/
      var requestItem = {};
      var reqId = window.location.href.split('=').pop();
      requestItem.id = document.getElementById('id').value;
      requestItem.request = reqId;
      requestItem.item = document.getElementById('item').value;
      requestItem.quantity = document.getElementById('qty').value;
      requestItem.status = document.getElementById('status').value;
      requestItem.unitPrice = document.getElementById('unitPrice').value;
      requestItem.total = document.getElementById('total').value;

      if (typeEdit == "approve"){
        this.setState({requestItem: requestItem}, () => this.approve()); //for async calls
      } else if (typeEdit == "issue"){
        this.setState({requestItem: requestItem}, () => this.issue()); //for async calls
      } else if (typeEdit == "update"){
        this.setState({requestItem: requestItem}, () => this.updatedRequest(total)); //for async calls
      } 
    //}
  }

  formatDate(){
    var today = new Date();
    var month = today.getMonth() + 1;
    var date = today.getDate();

    if(month < 10){
       month = "0" + month;
    }if(date < 10){
       date = "0" + date;
    }

    return month + "/" + date + "/" + today.getFullYear();
  }

  approve(){
    var requestItem = this.state.requestItem;
    var date = this.formatDate();

    var id = requestItem.id;
    var data = {"request": requestItem.request, "item": requestItem.item, "date": date, "quantity": requestItem.quantity,
                "status": 'Approved', "unit_price": requestItem.unitPrice, "total": requestItem.total}
            
    $.ajax({
      url: Utils.basePath + "/item-requests/" + id + "/",
      type:"PUT",
      async: false,
      contentType: "application/json",
      data: JSON.stringify(data),
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
          400: function (response) {
            alert(JSON.parse(response.responseText).date);
          },
          401: function (response) {
            alert("User is not authorized. Please login.");
            self.setState({logout: true});
          },
          500: function (response) {
            alert("HTTP error 500, please check your connection.");
          }
      },
      success: function(data) {
          alert("Item Request is approved!");
          location.reload();
      },
      error: function (response) {
          console.log(response);
      }
    });
  }

  issue(){
    //iminus sa stock quantity !!!
    var requestItem = this.state.requestItem;
    var date = this.formatDate();

    var id = requestItem.id;
    var data = {"request": requestItem.request, "item": requestItem.item, "date": date, "quantity": requestItem.quantity,
                "status": 'Issued', "unit_price": requestItem.unitPrice, "total": requestItem.total}
            
    $.ajax({
      url: Utils.basePath + "/item-requests/" + id + "/",
      type:"PUT",
      async: false,
      contentType: "application/json",
      data: JSON.stringify(data),
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
          400: function (response) {
            alert(JSON.parse(response.responseText).date);
          },
          401: function (response) {
            alert("User is not authorized. Please login.");
            self.setState({logout: true});
          },
          500: function (response) {
            alert("HTTP error 500, please check your connection.");
          }
      },
      success: function(data) {
          alert("Item Request is issued!");
          location.reload();
      },
      error: function (response) {
          console.log(response);
      }
    });
  }

  updatedRequest(total){
    var requestItem = this.state.requestItem;
    var date = this.formatDate();

    var id = requestItem.id;
    var data = {"request": requestItem.request, "item": requestItem.item, "date": date, "quantity": requestItem.quantity,
                "status": requestItem.status, "unit_price": requestItem.unitPrice, "total": requestItem.total}
            
    $.ajax({
      url: Utils.basePath + "/item-requests/" + id + "/",
      type:"PUT",
      async: false,
      contentType: "application/json",
      data: JSON.stringify(data),
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
        400: function (response) {
          alert(JSON.parse(response.responseText).date);
        },
        401: function (response) {
          alert("User is not authorized. Please login.");
          self.setState({logout: true});
        },
        500: function (response) {
          alert("HTTP error 500, please check your connection.");
        }
      },
      success: function(data) {
        alert("Item Request is updated!");
        location.reload();
      },
      error: function (response) {
        console.log(response);
      }
    });      
  }

  componentWillMount(){
    var self = this;
    $.ajax({
      url: Utils.basePath + "/items/",
      type:"GET",
      contentType: "application/json",
      async: false,
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
         400: function (response) {
            alert(JSON.parse(response.responseText).id);
         },
         401: function (response) {
            alert("User is not authorized. Please login.");
            self.setState({logout: true});
         },
         500: function (response) {
            alert("HTTP error 500, please check your connection.");
         }
      },

      success: function(response) {
        const itemList = [];
          for(var i=0; i<response.length; i++){
            itemList.push(response[i]);
          }
          self.setState({items: itemList}); 
      }.bind(this),
      error: function(xhr, status, err) {
      }.bind(this)
    })
  }

   render(){
      if (this.state.logout) {
        return <Redirect push to="/" />;
      }

      const itemResult = this.state.items.map((items) =>
            <option key={items.id} value={items.id}>{items.name}</option>
      ); 

      var unitPrice = this.state.unitPrice;
      var total = (this.state.qty * unitPrice).toFixed(2);

      var btnApprove = "";
      var btnIssue = "";

      var status = this.state.status;
     
      return(
        <div className="modal fade modal-position" id="editRequestItemModal" role="dialog">
            <div className="modal-dialog modal-md">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal">&times;</button>
                  <h4 className="modal-title">Edit Item in Request List</h4>
                </div>
                <div className="modal-body">
                  <div className="row">
                    <input type="hidden" id="id"/>
                    <div className="col-md-1 col-md-offset-1 form-row-padding-top">
                      Item*
                    </div>
                    <div className="col-md-3">
                      <select className="form-control" id="item" value={this.state.item} onChange={this.handleItem} >
                        {itemResult}
                      </select>
                    </div>
                    <div className="col-md-3 form-row-padding-top">
                      <i className="fa fa-plus-circle"></i><Link to="/item/add-item" target="_blank"><u> Add New Item</u></Link>
                    </div>
                    <div className="col-md-1 form-row-padding-top">
                      Status:
                    </div>
                    <div className="col-md-2">
                      <input id="status" className="form-control" disabled/>
                    </div>
                  </div>
                  <div className="row form-row-padding-top">
                    <div className="col-md-1 col-md-offset-1 form-row-padding-top">
                      Qty*
                    </div>
                    <div className="col-md-2">
                      <input type="number" id="qty" className="form-control" value={this.state.qty} onChange={this.handleQty}/>
                    </div>
                    <div className="col-md-2 form-row-padding-top">
                      Unit Price: 
                    </div>
                    <div className="col-md-2">
                      <input id="unitPrice" className="form-control" disabled/>
                    </div>
                  </div>
                  <div className="row form-row-padding-top">
                    <div className="col-md-1 col-md-offset-1 form-row-padding-top">
                      Total*
                    </div>
                    <div className="col-md-2 form-row-padding-top">
                      <input id="total" className="form-control" disabled/>
                    </div>
                  </div>
                  <br /><br />
                </div>
                <div className="modal-footer">
                  <button type="button" id="btnApprove" className="btn btn-default btn-info" onClick={() => this.checkInputs(total, "approve")} data-dismiss="modal"><i className="fa fa-thumbs-o-up"></i> Approve</button>
                  <button type="button" id="btnIssue" className="btn btn-default btn-success" onClick={() => this.checkInputs(total, "issue")} data-dismiss="modal"><i className="fa fa-check-circle-o"></i> Issue</button>             
                  <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs(total, "update")} data-dismiss="modal">Save</button>
                  <button type="button" className="btn btn-default btn-warning" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
        </div>
      );
    }
}

export default RequestItemEditModal;

