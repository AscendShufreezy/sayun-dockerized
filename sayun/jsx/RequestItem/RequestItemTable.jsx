import React from 'react';
import ReactDOM from 'react-dom';
import Preloader from '../Utils/Preloader.jsx';
import EmptyTable from '../Utils/EmptyTable.jsx';
import Utils from '../Utils/Utils.jsx';
import RequestItemEditModal from './RequestItemEditModal.jsx';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class RequestItemTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         requestItem: "",
         preloader: "",
         empty: "",
         total: 0.00,
         logout: false,
         filter1: "",
         filter2: "",
         filter3: "",
         filter4: "",
         filter5: ""
      };
   }

   checkAllCheckbox(name) {
      var array = document.getElementsByName(name);
      for(var i=0; i<array.length; i++){
         var cb = document.getElementById(array[i].id);
         cb.checked = document.getElementById('checkAll').checked;
      }
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#requestItemTable').html());
      location.href=url
      return false
   }


   componentDidMount(){
      var self = this;
      this.setState({preloader: <Preloader/>});
      var reqId = this.props.requestId;
      $.ajax({
         url: Utils.basePath + "/requests/" + reqId + "/item_requests/",
         type:"GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
            400: function (response) {
               alert(JSON.parse(response.responseText));
            },
            401: function (response) {
               alert("User is not authorized. Please login.");
               self.setState({logout: true});
            },
            500: function (response) {
               alert("HTTP error 500, please check your connection.");
            }
         },
         success: function(response) {
            const requestItemList = [];
            var total = this.state.total;
            for(var i=0; i<response.length; i++){
               var requestItem = {};

               $.ajax({
                  url: Utils.basePath + "/items/" + response[i].item + "/",
                  type:"GET",
                  contentType: "application/json",
                  async: false,
                  headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
                  statusCode: {
                        400: function (response) {
                           alert(JSON.parse(response.responseText).id);
                        },
                        401: function (response) {
                           alert("User is not authorized. Please login.");
                           self.setState({logout: true});
                        },
                        500: function (response) {
                           alert("HTTP error 500, please check your connection.");
                        }
                  },

                  success: function(data) {
                     requestItem.name = data.name;
                     requestItem.itemId = data.id;
                  }.bind(this)
                })
               requestItem.checkboxId = response[i].id;
               requestItem.id = response[i].id;
               requestItem.status  = response[i].status;
               requestItem.qty = response[i].quantity;
               requestItem.unitPrice = response[i].unit_price;
               requestItem.total = response[i].total;
               total = total + parseFloat(requestItem.total);
               requestItemList.push(requestItem);
            }

            if(requestItemList.length == 0){
               self.setState({empty: <EmptyTable text="Request Items" span="8"/>});
            }
            self.setState({total: (total).toFixed(2)});
            self.setState({response: requestItemList});
            self.setState({preloader: ""});
         }.bind(this),
         error: function(xhr, status, err) {
         }.bind(this)
      })

      $('#editRequestItemModal').on("show.bs.modal", function (e) {
         console.log(sessionStorage.getItem('group'));
         console.log($(e.relatedTarget).data('status'));
         if((sessionStorage.getItem('group') == "warehouseapprover-group") && ($(e.relatedTarget).data('status') == "Draft")){
           $('#editRequestItemModal ').find('button#btnApprove').prop("hidden", false);
         } else{
            $('#editRequestItemModal ').find('button#btnApprove').prop("hidden", true);
         }

         if((sessionStorage.getItem('group') == "warehouse-group") && ($(e.relatedTarget).data('status') == "Approved")){
            $('#editRequestItemModal ').find('button#btnIssue').prop("hidden", false);
         } else{
             $('#editRequestItemModal ').find('button#btnIssue').prop("hidden", true);
         }


         /*if((sessionStorage.getItem('group') != "warehouseapprover-group") || ($(e.relatedTarget).data('status') != "Draft")){
            $('#editRequestItemModal ').find('button#btnApprove').prop("hidden", true);
         } if((sessionStorage.getItem('group') != "warehouse-group") || ($(e.relatedTarget).data('status') != "Approved")){
            $('#editRequestItemModal ').find('button#btnIssue').prop("hidden", true);
         }*/

         if($(e.relatedTarget).data('status') == "Issued"){
            $('#editRequestItemModal ').find('button#btnApprove').prop("hidden", "disabled");
            $('#editRequestItemModal ').find('button#btnIssue').prop("hidden", "disabled");
            $('#editRequestItemModal ').find('select#item').prop("disabled", "disabled");
            $('#editRequestItemModal ').find('input#qty').prop("disabled", "disabled");
         } else{
            $('#editRequestItemModal ').find('select#item').prop("disabled", false);
            $('#editRequestItemModal ').find('input#qty').prop("disabled", false);
         }

         $('#editRequestItemModal ').find('input#id').val($(e.relatedTarget).data('id'))
         $('#editRequestItemModal ').find('select#item').val($(e.relatedTarget).data('item'))
         $('#editRequestItemModal ').find('input#status').val($(e.relatedTarget).data('status'))
         $('#editRequestItemModal ').find('input#qty').val($(e.relatedTarget).data('qty'))
         $('#editRequestItemModal ').find('input#unitPrice').val($(e.relatedTarget).data('unit'))
         $('#editRequestItemModal ').find('input#total').val($(e.relatedTarget).data('total'))
      });
   }


   idFormatter(cell, row){
      var link = "/request/item/id=" + cell;
      return (
         <u><a data-toggle="modal" data-target="#editRequestItemModal" data-id={row.id} data-item={row.itemId} data-qty={row.qty} data-unit={row.unitPrice} data-total={row.total} data-status={row.status} className="cursor-pointer">{cell}</a></u>
      );
   }

   checkboxFormatter(cell, row){
      return (
         <input type="checkbox" name="checkbox" id={cell} />
      );
   }

   amountFormatter(cell, row){
      var amount = cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return (
         amount
      );
   }

   filterShow1(){
      if(this.state.filter1 == ""){
         this.setState({filter1: 'RegexFilter'});
      } else{
         this.setState({filter1: ''});
      }
   }

   filterShow2(){
      if(this.state.filter2 == ""){
         this.setState({filter2: 'RegexFilter'});
      } else{
         this.setState({filter2: ''});
      }
   }

   filterShow3(){
      if(this.state.filter3 == ""){
         this.setState({filter3: 'RegexFilter'});
      } else{
         this.setState({filter3: ''});
      }
   }

   filterShow4(){
      if(this.state.filter4 == ""){
         this.setState({filter4: 'RegexFilter'});
      } else{
         this.setState({filter4: ''});
      }
   }

   filterShow5(){
      if(this.state.filter5 == ""){
         this.setState({filter5: 'RegexFilter'});
      } else{
         this.setState({filter5: ''});
      }
   }

   render(){
      const preloader = this.state.preloader;
      if (this.state.logout) {
             return <Redirect push to="/" />;
      }

      var filter1 = this.state.filter1;
      var filter2 = this.state.filter2;
      var filter3 = this.state.filter3;
      var filter4 = this.state.filter4;
      var filter5 = this.state.filter5;

      var total = this.state.total;
      total = total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      
      return(
         <div>
            <div className="row">
              <div className="row" id="page-wrapper-inner5">
                 <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
              </div>
               <RequestItemEditModal requestItem={""}/>
               <div className="col-md-12" id="requestItemTable">
                  {preloader}
                  <BootstrapTable data={this.state.response} striped hover pagination>
                     <TableHeaderColumn width="25px" dataField='checkboxId' isKey dataFormat={ this.checkboxFormatter }>
                        <input type="checkbox" id="checkAll" onChange={(e) => this.checkAllCheckbox('checkbox')} />
                     </TableHeaderColumn>
                     <TableHeaderColumn dataField='name' width="300px" filter={ { type: filter1 } } dataFormat={ this.idFormatter } dataSort={ true }>Item Name <i className="fa fa-search search-size" onMouseOver={() => this.filterShow1()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='status' width="200px" filter={ { type: filter2 } } dataSort={ true }>Status <i className="fa fa-search search-size" onMouseOver={() => this.filterShow2()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='qty' width="200px" filter={ { type: filter3 } }  dataSort={ true }>Qty <i className="fa fa-search search-size" onMouseOver={() => this.filterShow3()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='unitPrice' width="200px" filter={ { type: filter4 } } dataSort={ true } dataFormat={ this.amountFormatter }>Unit Cost <i className="fa fa-search search-size" onMouseOver={() => this.filterShow4()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='total' width="200px" filter={ { type: filter5 } } dataSort={ true } dataFormat={ this.amountFormatter }>Total <i className="fa fa-search search-size" onMouseOver={() => this.filterShow5()}></i></TableHeaderColumn>
                  </BootstrapTable>
               </div>
            </div>
            <div className="row">
               <div className="col-md-4 col-md-offset-6">
                  <div className="pull-right">
                     <div className="col-md-2">
                        Total
                     </div>
                     <div className="col-md-2 pull-right">
                        &nbsp;&nbsp;&nbsp;{total}
                     </div>
                  </div>
              </div>
            </div>
         </div>
      );
    }
}

export default RequestItemTable;
