import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class PayrollCalculation extends React.Component {
   render() {
      var calculation = this.props.calculation;
      var link = "/id=" + calculation.id;

      if(calculation.category == "Fixed Addition"){
         link = "/fixed-addition" + link;
      } else if(calculation.category == "Temporary Addition"){
         link = "/temp-addition" + link;
      } else if(calculation.category == "Fixed Deduction"){
         link = "/fixed-deduction" + link;
      } else if(calculation.category == "Temporary Deduction"){
         link = "/temp-deduction" + link;
      }

      var amount = (calculation.amount).toFixed(2);
      var folder = <i className="fa fa-folder-open"></i>

      return (
         <tr> 
            <td>{calculation.date}</td>
            <td>{calculation.category}</td>  
            <td>{calculation.description}</td>              
            <td><center>{folder}<Link to={link}><u> open record</u></Link></center></td>
            <td><div className="pull-right">{amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</div></td> 
         </tr>

      ); 
   }
}

export default PayrollCalculation;