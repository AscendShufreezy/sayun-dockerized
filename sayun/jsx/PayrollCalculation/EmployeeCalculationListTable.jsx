import React from 'react';
import ReactDOM from 'react-dom';
import PayrollCalculation from './PayrollCalculation.jsx';
import EmptyTable from '../Utils/EmptyTable.jsx';
import Preloader from '../Utils/Preloader.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class EmployeeCalculationListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         preloader: "",
         empty: "",
         employeeId: props.employeeId,
         payrollDate: props.payrollDate,
         totalOfTotal: 0
      };
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#empCalcListTable').html());
      location.href=url
      return false
   }

   componentWillMount(){
      var self = this;
      var employeeId = this.state.employeeId;
      var payrollDate = this.state.payrollDate;
      var totalOfTotal = 0;
      this.setState({preloader: <Preloader/>});
      $.ajax({
         url: Utils.basePath + "/employees/" + employeeId + "/payroll_breakdown/?date=" + payrollDate,
         type:"GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
            400: function (response) {
               alert(JSON.parse(response.responseText));
            },
            401: function (response) {
               alert("User is not authorized. Please login.");
               self.setState({logout: true});
            },
            500: function (response) {
               alert("HTTP error 500, please check your connection.");
            }
         },
         success: function(response) {
            const calculationList = [];
            for(var i=0; i<response.length; i++){
               var calculation = {};
               calculation.date = response[i].date;
               calculation.category = response[i].category;
               calculation.type = response[i].type;
               calculation.description = response[i].description;
               calculation.id = response[i].id;
               calculation.amount = response[i].amount;

               if((calculation.category).includes('Deduction')){
                  totalOfTotal = totalOfTotal - calculation.amount;
               } else if((calculation.category).includes('Addition')){
                  totalOfTotal = totalOfTotal + calculation.amount;
               }
               calculationList.push(calculation);
            }

            if(calculationList.length == 0){
               this.setState({empty: <EmptyTable text="Employee Calculations" span="5"/>});
            }
            this.setState({totalOfTotal: totalOfTotal});
            this.setState({response: calculationList});
            this.setState({preloader: ""});
         }.bind(this),
         error: function(xhr, status, err) {
         }.bind(this)
      });
   }

   render(){
      const preloader = this.state.preloader;
      var result = this.state.empty;

      if(this.state.response.length != 0){
         result = this.state.response.map((calculation) =>
            <PayrollCalculation key={calculation.id} calculation={calculation} />
         );
      }

      var total = (this.state.totalOfTotal).toFixed(2);

      return(
         <div>
            <div className="row">
              <div className="row" id="page-wrapper-inner5">
                 <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
              </div>
               <div className="col-md-12" id="empCalcListTable">
                  {preloader}
                  <table className="table table-striped table-bordered table-responsive">
                     <thead className="thead-font-size">
                        <tr>
                           <th className="col-md-width">Date</th>
                           <th className="col-md-width">Category</th>
                           <th className="col-md-width">Description</th>
                           <th className="col-md-width"></th>
                           <th className="col-md-width"><div className="pull-right">Amount</div></th>
                        </tr>
                     </thead>
                     <tbody>
                        {result}
                     </tbody>
                  </table>
               </div>
            </div>
            <div className="row">
              <div className="pull-right">
                  <div className="pull-right">
                     <div className="col-md-1 employee-total2">
                           Total
                     </div>
                     <div className="col-md-2 employee-total">
                           {total}
                     </div>
                  </div>
              </div>
          </div>
         </div>
      );
    }
}

export default EmployeeCalculationListTable;
