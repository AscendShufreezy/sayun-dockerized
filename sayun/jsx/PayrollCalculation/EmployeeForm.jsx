import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import Input from '../Utils/Input.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class EmployeeForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            employeeId: props.employeeId,
            employee: {},
            logout: false
        };
    }

    componentWillMount(){
        var employeeId = this.state.employeeId;
        var self = this;
        $.ajax({
            url: Utils.basePath + "/employees/" + employeeId + "/",
            type:"GET",
            contentType: "application/json",
            async : false,
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },

            success: function (data) {
                var employee = {};
                employee.id = data.id;
                employee.fName = data.first_name;
                employee.mName = data.middle_name;
                employee.lName = data.last_name;
                employee.type = data.type;
                self.setState({employee: employee});
            }
        });
    }

    render(){
        if (this.state.logout) {
             return <Redirect push to="/" />;
        }

        var employee = this.state.employee;
        return (
            <div>
                <div className="row" id="page-wrapper-inner3">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="row">
                                <div className="col-md-3">
                                    Employee ID:
                                </div>
                                <div className="col-md-6">
                                     {employee.id}
                                </div>
                            </div>
                        </div>   
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-6">
                            <div className="row">
                                <div className="col-md-3">
                                    Name
                                </div>
                                <div className="col-md-6">
                                     {employee.lName}, {employee.fName} {employee.mName}
                                </div>
                            </div>
                        </div>   
                        <div className="col-md-5 col-md-offset-1">
                            <div className="row">
                                <div className="col-md-3">
                                    Employee Type
                                </div>
                                <div className="col-md-6">
                                    {employee.type}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
            </div>
        );
    }
}   

export default EmployeeForm;