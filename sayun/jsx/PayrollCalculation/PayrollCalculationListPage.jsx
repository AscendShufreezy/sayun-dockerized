import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import PayrollForm from './PayrollForm.jsx';
import EmployeeForm from './EmployeeForm.jsx';
import EmployeeCalculationListTable from './EmployeeCalculationListTable.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';;

class PayrollCalculationListPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      response: [],
      transfer: false,
      payroll: {},
      calendar: 0,
      calendarId: 0,
      employeeId: 0,
      payrollDate: "",
      payrollCoverage: ""
   };
  }

  cancelSave() {
    this.setState({transfer: true});
  }

  componentWillMount(){
    var calendarId = 0;
    var date = "";
    var payrollCoverage = "";
    var url = window.location.href;
    var employeeId = url.split('id=').pop();
    var payroll = url.substring(0, url.indexOf('id=') + 'id='.length);
    payroll = payroll.replace('/id=', '');
    payroll = payroll .split('=').pop();

    $.ajax({
      url: Utils.basePath + "/payroll/" + payroll + "/",
      type:"GET",
      contentType: "application/json",
      async : false,
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
         400: function (response) {
            alert(JSON.parse(response.responseText).id);
         },
         401: function (response) {
            alert("haha.");
            self.setState({logout: true});
         },
         500: function (response) {
            alert("HTTP error 500, please check your connection.");
         }
      },

      success: function (data) {
          calendarId =  data.calendar;
          date = data.date;
          payrollCoverage = data.coverage;
      }
    });

    this.setState({employeeId: employeeId, payrollCoverage: payrollCoverage, payrollDate: date, calendar: payroll, calendarId: calendarId}); //employee id and payroll id
  }

	render(){
    var payroll = this.state.calendar;
    var payrollDate = this.state.payrollDate;
    var calendarId = this.state.calendarId;
    var employeeId = this.state.employeeId;
    var payrollCoverage = this.state.payrollCoverage;

    if (this.state.transfer) {
        var link = "/payroll/id=" + payroll;
        return <Redirect push to={link} />;
    }

    return (
      <div id="page-wrapper">
        <Menu active="Payroll"/>
        <div className="row" id="page-wrapper-inner">
          <Breadcrumb page="payrollcalculation" calendarId={calendarId} payrollId={payroll}/>
          <PageTitle pageTitle="Payroll"/>
          <PayrollForm payrollDate={payrollDate} payrollCoverage={payrollCoverage} calendarId={calendarId}/>
          <hr />
          <PageTitle pageTitle="Employee Calculation"/>
          <EmployeeForm employeeId={employeeId}/>
          <div className="row">
              <div className="col-md-3 pull-right">
                <div className="pull-right">
                  <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
                </div>
              </div>
          </div>
          <br />
          <EmployeeCalculationListTable employeeId={employeeId} payrollDate={payrollDate}/>
        </div>  
        <br />  
      </div>
    );
  }
}

export default PayrollCalculationListPage;