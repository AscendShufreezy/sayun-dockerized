import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';
import Input from '../Utils/Input.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class PayrollForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            payrollDate: props.payrollDate,
            payrollCoverage: props.payrollCoverage,
            calendarId: props.calendarId,
            calendar: {},
            logout: false
        };
    }

    componentWillMount(){
        var calendarId = this.state.calendarId;
        var self = this;
        $.ajax({
            url: Utils.basePath + "/payroll-calendars/" + calendarId + "/",
            type:"GET",
            contentType: "application/json",
            async : false,
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },

            success: function (data) {
                self.setState({calendar: data});
            }
        });
    }

    render(){
        if (this.state.logout) {
             return <Redirect push to="/" />;
        }

        var calendar = this.state.calendar;
        var payrollDate = this.state.payrollDate;
        var payrollCoverage = this.state.payrollCoverage;
        return (
            <div>
                <div className="row" id="page-wrapper-inner3">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="row">
                                <div className="col-md-4">
                                    Payroll Calendar Name
                                </div>
                                <div className="col-md-6">
                                     {calendar.name}
                                </div>
                            </div>
                        </div>   
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-6">
                            <div className="row">
                                <div className="col-md-4">
                                    Payroll Date
                                </div>
                                <div className="col-md-6">
                                     {payrollDate}
                                </div>
                            </div>
                        </div>   
                        <div className="col-md-5 col-md-offset-1">
                            <div className="row">
                                <div className="col-md-3">
                                    Coverage Period
                                </div>
                                <div className="col-md-6">
                                    {payrollCoverage}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
            </div>
        );
    }
}   

export default PayrollForm;