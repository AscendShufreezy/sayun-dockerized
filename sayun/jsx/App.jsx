import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import Footer from './Utils/Footer.jsx';
import Header from './Utils/Header.jsx';
import LoginForm from './LoginForm.jsx';

import EmployeeListPage from './Employee/EmployeeListPage.jsx';
import EmployeeDetails from './Employee/EmployeeDetails.jsx';
import AddEmployee from './Employee/AddEmployee.jsx';
import EmployeeDetailsAdditions from './Employee/EmployeeDetailsAdditions.jsx';

import AttendanceListPage from './Attendance/AttendanceListPage.jsx';
import EmployeeAttendanceListPage from './Attendance/EmployeeAttendanceListPage.jsx';

import OvertimeListPage from './Overtime/OvertimeListPage.jsx';
import EmployeeOvertimeListPage from './Overtime/EmployeeOvertimeListPage.jsx';

import AddTempDeduction from './Deduction/AddTempDeduction.jsx';
import AddFixedDeduction from './Deduction/AddFixedDeduction.jsx';
import FixedDeductionDetails from './Deduction/FixedDeductionDetails.jsx';
import TempDeductionDetails from './Deduction/TempDeductionDetails.jsx';

import AddTempAddition from './Addition/AddTempAddition.jsx';
import AddFixedAddition from './Addition/AddFixedAddition.jsx';
import FixedAdditionDetails from './Addition/FixedAdditionDetails.jsx';
import TempAdditionDetails from './Addition/TempAdditionDetails.jsx';

import PayrollCalendarListPage from './PayrollCalendar/PayrollCalendarListPage.jsx';
import AddPayrollCalendar from './PayrollCalendar/AddPayrollCalendar.jsx';
import PayrollCalendarDetails from './PayrollCalendar/PayrollCalendarDetails.jsx';
import PayrollEmployeeDetails from './PayrollEmployee/PayrollEmployeeDetails.jsx';
import PayrollCalculationListPage from './PayrollCalculation/PayrollCalculationListPage.jsx';

import UserListPage from './User/UserListPage.jsx';
import AddUser from './User/AddUser.jsx';
import UserDetails from './User/UserDetails.jsx';

import GroupListPage from './Group/GroupListPage.jsx';
import AddGroup from './Group/AddGroup.jsx';
import GroupDetails from './Group/GroupDetails.jsx';

import CategoryListPage from './Category/CategoryListPage.jsx';
import AddCategory from './Category/AddCategory.jsx';
import CategoryDetails from './Category/CategoryDetails.jsx';

import WarehouseListPage from './Warehouse/WarehouseListPage.jsx';
import AddWarehouse from './Warehouse/AddWarehouse.jsx';
import WarehouseDetails from './Warehouse/WarehouseDetails.jsx';

import CustomerListPage from './Customer/CustomerListPage.jsx';
import AddCustomer from './Customer/AddCustomer.jsx';
import CustomerDetails from './Customer/CustomerDetails.jsx';

import ItemListPage from './Item/ItemListPage.jsx';
import AddItem from './Item/AddItem.jsx';
import ItemDetails from './Item/ItemDetails.jsx';

import ReceiptListPage from './Receipt/ReceiptListPage.jsx';
import AddReceipt from './Receipt/AddReceipt.jsx';
import ReceiptDetails from './Receipt/ReceiptDetails.jsx';

import RequestListPage from './Request/RequestListPage.jsx';
import AddRequest from './Request/AddRequest.jsx';
import RequestDetails from './Request/RequestDetails.jsx';
import AddRequestPos from './Request/AddRequestPos.jsx';

import InventoryListPage from './ReportsIis/InventoryListPage.jsx';
import SummaryListPage from './ReportsIis/SummaryListPage.jsx';
import CustRequestListPage from './ReportsIis/CustRequestListPage.jsx';

import ChangePasswordPage from './System/ChangePasswordPage.jsx';
import Preloader from './Utils/Preloader.jsx';
import PrintReceipt from './Utils/PrintReceipt.jsx'

class App extends React.Component {
   render() {
      var header;
      if((window.location.href).includes("employee")){
         header = <Header active="HRIS"/>
      } else if((window.location.href).includes("/attendance")){
         header = <Header active="HRIS"/>
      } else if((window.location.href).includes("/payroll")){
         header = <Header active="HRIS"/>
      } else if((window.location.href).includes("deduction/")){
         header = <Header active="HRIS"/>
      } else if((window.location.href).includes("addition/")){
         header = <Header active="HRIS"/>
      }  else if((window.location.href).includes("/warehouse")){
         header = <Header active="IIS"/>
      } else if((window.location.href).includes("/customer")){
         header = <Header active="IIS"/>
      } else if((window.location.href).includes("/item")){
         header = <Header active="IIS"/>
      } else if((window.location.href).includes("/receipt")){
         header = <Header active="IIS"/>
      } else if((window.location.href).includes("/request")){
         header = <Header active="IIS"/>
      } else if((window.location.href).includes("/pos")){
         header = <Header active="IIS"/>
      }else if((window.location.href).includes("/user")){
         header = <Header active="Account"/>
      } else if((window.location.href).includes("/group")){
         header = <Header active="Account"/>
      } else if((window.location.href).includes("/category")){
         header = <Header active="System"/>
      } else if((window.location.href).includes("/iis")){
         header = <Header active="IIS"/>
      } 

      return (
         <div>
            {header}
            <Switch>              
               <Route exact path='/' component={LoginForm}/>

               <Route exact path='/warehouse' component={WarehouseListPage}/>
               <Route exact path='/warehouse/add-warehouse' component={AddWarehouse}/>
               <Route path='/warehouse/' component={WarehouseDetails}/>

               <Route exact path='/customer/type=Truck' component={CustomerListPage}/>
               <Route exact path='/customer/type=Employee' component={CustomerListPage}/>
               <Route exact path='/customer/add-customer' component={AddCustomer}/>
               <Route path='/customer/' component={CustomerDetails}/>

               <Route exact path='/item' component={ItemListPage}/>
               <Route exact path='/item/add-item' component={AddItem}/>
               <Route path='/item/' component={ItemDetails}/>

               <Route exact path='/receipt' component={ReceiptListPage}/>
               <Route exact path='/receipt/add-receipt' component={AddReceipt}/>
               <Route path='/receipt/' component={ReceiptDetails}/>

               <Route exact path='/request' component={RequestListPage}/>
               <Route exact path='/request/add-request' component={AddRequest}/>
               <Route exact path='/pos' component={AddRequestPos}/>
               <Route path='/request/' component={RequestDetails}/>

               <Route exact path='/user' component={UserListPage}/>
               <Route exact path='/user/add-user' component={AddUser}/>
               <Route path='/user/' component={UserDetails}/>

               <Route exact path='/group' component={GroupListPage}/>
               <Route exact path='/group/add-group' component={AddGroup}/>
               <Route path='/group/' component={GroupDetails}/>

               <Route exact path='/category' component={CategoryListPage}/>
               <Route exact path='/category/add-category' component={AddCategory}/>
               <Route path='/category/' component={CategoryDetails}/>

               <Route exact path='/employee' component={EmployeeListPage}/>
               <Route exact path='/employee/add-employee' component={AddEmployee}/>
               <Route path='/employee/details/additions/' component={EmployeeDetailsAdditions}/>
               <Route path='/employee/details/' component={EmployeeDetails}/>

               <Route exact path='/attendance' component={AttendanceListPage}/>
               <Route exact path='/attendance/overtime' component={OvertimeListPage}/>
               <Route path='/attendance/' component={EmployeeAttendanceListPage}/>
               <Route path='/attendances/overtime/' component={EmployeeOvertimeListPage}/>

               <Route exact path='/payroll' component={PayrollCalendarListPage}/>
               <Route exact path='/payroll/add-payroll-calendar' component={AddPayrollCalendar}/>
               <Route path='/payroll/calendar' component={PayrollCalendarDetails}/>
               <Route path='/payroll/calculation' component={PayrollCalculationListPage}/>
               <Route path='/payroll' component={PayrollEmployeeDetails}/>

               <Route exact path='/temp-deduction/add-temp-deduction' component={AddTempDeduction}/>
               <Route exact path='/fixed-deduction/add-fixed-deduction' component={AddFixedDeduction}/>
               <Route path='/fixed-deduction/' component={FixedDeductionDetails}/>
               <Route path='/temp-deduction/' component={TempDeductionDetails}/>

               <Route exact path='/temp-addition/add-temp-addition' component={AddTempAddition}/>
               <Route exact path='/fixed-addition/add-fixed-addition' component={AddFixedAddition}/>
               <Route path='/fixed-addition/' component={FixedAdditionDetails}/>
               <Route path='/temp-addition/' component={TempAdditionDetails}/>

               <Route exact path='/reports/iis/inventory' component={InventoryListPage}/>
               <Route exact path='/reports/iis/summary' component={SummaryListPage}/>
               <Route exact path='/reports/iis/requests' component={CustRequestListPage}/>

               <Route exact path='/payroll' component={EmployeeListPage}/>
               <Route exact path='/report/hris' component={EmployeeListPage}/>
               <Route path='/print-receipt/' component={PrintReceipt}/>

               <Route exact path='/change-password' component={ChangePasswordPage}/>
            </Switch>
            <Footer />
         </div>
      );
   }	
}

export default App;