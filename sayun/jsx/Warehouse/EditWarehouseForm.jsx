import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';
import Input from '../Utils/Input.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class EditWarehouseForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            name: props.warehouse.name,
            location: props.warehouse.location,
            description: props.warehouse.description
        };

        this.handleDescription = this.handleDescription.bind(this);
        this.handleName = this.handleName.bind(this);
        this.handleLocation = this.handleLocation.bind(this);
    }

    handleDescription(e) {
        this.setState({
            description: e.target.value
        });
    }

    handleName(e) {
        this.setState({
            name: e.target.value
        });
    }

    handleLocation(e) {
        this.setState({
            location: e.target.value
        });
    }

    render(){
        var warehouse = this.props.warehouse;
        console.log(warehouse);
        return (
            <div>
                <input type="hidden" id="owner" value={warehouse.owner}/>
                <div className="row" id="page-wrapper-inner3">
                    <div className="row">
                        <div className="col-md-2">
                            Name*
                        </div>
                        <div className="col-md-5">
                            <Input id="name" type="text" defaultValue={this.state.name} onChange={this.handleName}/>
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-2">
                            Location*
                        </div>
                        <div className="col-md-10">
                            <Input id="location" type="text" defaultValue={this.state.location} onChange={this.handleLocation}/>
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-2">
                            Description*
                        </div>
                        <div className="col-md-10">
                            <Input id="description" type="text" defaultValue={this.state.description} onChange={this.handleDescription}/>
                        </div>
                    </div>
                </div>
                <br />
            </div>
        );
    }
}   

export default EditWarehouseForm;