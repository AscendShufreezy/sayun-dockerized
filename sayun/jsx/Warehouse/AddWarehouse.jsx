import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import AddWarehouseForm from './AddWarehouseForm.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class AddWarehouse extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          response: [],
          transfer: false,
          isComplete: false,
          emptyField: "",
          warehouse: {},
          logout: false
       };
    }

    cancelSave() {
        this.setState({transfer: true});
    }

    checkInputs() {
        if(document.getElementById("name").value == ""){
            this.setState({emptyField: "Name"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("location").value == ""){
            this.setState({emptyField: "Location"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else if(document.getElementById("description").value == ""){
            this.setState({emptyField: "Description"}, () => { 
                alert(this.state.emptyField + " should not be empty.");
                return "";
            });
        } else{
            this.setState({isComplete: true}, () => { 
                var warehouse = {};
                warehouse.name = document.getElementById("name").value;
                warehouse.location = document.getElementById("location").value;
                warehouse.description = document.getElementById("description").value;
                this.setState({warehouse: warehouse}, () => this.saveWarehouse()); //for async calls
            });
        }
    }

    saveWarehouse() {
        var self = this;
        var warehouse = this.state.warehouse;
        var data = {"name": warehouse.name,"location": warehouse.location,"description": warehouse.description, "owner": sessionStorage.getItem('id')}
                    
        $.ajax({
            url: Utils.basePath + "/warehouses/",
            type:"POST",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
                alert("Warehouse saved!");
                self.setState({transfer: true});
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    componentWillMount(){
      var group = sessionStorage.getItem('group');
      var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

      if (group != "admin-group" && group != "hr-group" && group != "warehouse-group" && sessionPermission.indexOf(121) == -1) {
        alert("You are not authorized to access this page. Please login as admin user.");
        this.setState({logout: true});
      } 
    }    

    render(){
        if (this.state.logout) {
             return <Redirect push to="/" />;
        }

        if (this.state.transfer) {
            return <Redirect push to="/warehouse" />;
        }

        return (
            <div id="page-wrapper">
                <Menu active="Warehouse"/>
                <div className="row" id="page-wrapper-inner">
                    <Breadcrumb page="add-warehouse"/>
                    <PageTitle pageTitle="Add Warehouse"/>
                    <div className="row" id="page-wrapper-inner3">
                        <form>
                            <AddWarehouseForm />
                            <div className="row">
                                <div className="col-md-3 pull-right">
                                  <div className="pull-right">
                                    <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs()}>Save</button>
                                    <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
                                  </div>
                                </div>
                            </div>
                        </form>
                        <br />
                    </div>
                </div>
                <br />  
            </div>
      );
   }
}

export default AddWarehouse;