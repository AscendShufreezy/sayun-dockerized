import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';
import Input from '../Utils/Input.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class AddWarehouseForm extends React.Component {
    render(){
      return (
        <div>
            <div className="row" id="page-wrapper-inner3">
                <div className="row">
                    <div className="col-md-2">
                        Name*
                    </div>
                    <div className="col-md-5">
                        <Input id="name" type="text"/>
                    </div>
                </div>
                <div className="row form-row-padding-top">
                    <div className="col-md-2">
                        Location*
                    </div>
                    <div className="col-md-10">
                        <Input id="location" type="text"/>
                    </div>
                </div>
                <div className="row form-row-padding-top">
                    <div className="col-md-2">
                        Description*
                    </div>
                    <div className="col-md-10">
                        <Input id="description" type="text"/>
                    </div>
                </div>
            </div>
            <br />
        </div>
    );
  }
}

export default AddWarehouseForm;