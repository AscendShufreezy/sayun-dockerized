import React from 'react';
import ReactDOM from 'react-dom';
import EmptyTable from '../Utils/EmptyTable.jsx';
import Preloader from '../Utils/Preloader.jsx';
import Utils from '../Utils/Utils.jsx';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class WarehouseListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         preloader: "",
         empty: "",
         logout: false,
         filter1: "",
         filter2: "",
         filter3: "",
         filter4: ""

      };
   }

   checkAllCheckbox(name) {
      var array = document.getElementsByName(name);
      for(var i=0; i<array.length; i++){
         var cb = document.getElementById(array[i].id);
         cb.checked = document.getElementById('checkAll').checked;
      }
   }

   componentWillMount(){
      var self = this;
      this.setState({preloader: <Preloader/>});
      $.ajax({
         url: Utils.basePath + "/warehouses/",
         type:"GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
            400: function (response) {
               alert(JSON.parse(response.responseText));
            },
            401: function (response) {
               alert("User is not authorized. Please login.");
               self.setState({logout: true});
            },
            500: function (response) {
               alert("HTTP error 500, please check your connection.");
            }
         },
         success: function(response) {
            const warehouseList = [];
            for(var i=0; i<response.length; i++){
               var warehouse = {};
               warehouse.checkboxId = response[i].id;
               warehouse.id = response[i].id;
               warehouse.name = response[i].name;
               warehouse.location = response[i].location;
               warehouse.description = response[i].description;
               warehouse.owner = response[i].owner;
               warehouseList.push(warehouse);
            }

            if(warehouseList.length == 0){
               this.setState({empty: <EmptyTable text="Warehouses" span="5"/>});
            }

            this.setState({response: warehouseList});
            this.setState({preloader: ""});
         }.bind(this),
         error: function(xhr, status, err) {
         }.bind(this)
      });
   }

   idFormatter(cell, row){
      var link = "/warehouse/id=" + cell;
      return (
         <Link to={link}><u>{cell}</u></Link>
      );
   }

   checkboxFormatter(cell, row){
      return (
         <input type="checkbox" name="checkbox" id={cell} />
      );
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#warehouseTable').html());
      location.href=url
      return false
   }

   filterShow1(){
      if(this.state.filter1 == ""){
         this.setState({filter1: 'RegexFilter'});
      } else{
         this.setState({filter1: ''});
      }
   }

   filterShow2(){
      if(this.state.filter2 == ""){
         this.setState({filter2: 'RegexFilter'});
      } else{
         this.setState({filter2: ''});
      }
   }

   filterShow3(){
      if(this.state.filter3 == ""){
         this.setState({filter3: 'RegexFilter'});
      } else{
         this.setState({filter3: ''});
      }
   }

   filterShow4(){
      if(this.state.filter4 == ""){
         this.setState({filter4: 'RegexFilter'});
      } else{
         this.setState({filter4: ''});
      }
   }

   render(){
      const preloader = this.state.preloader;
      if (this.state.logout) {
             return <Redirect push to="/" />;
      }
      var filter1 = this.state.filter1;
      var filter2 = this.state.filter2;
      var filter3 = this.state.filter3;
      var filter4 = this.state.filter4;

      return(
         <div>
            <div className="row" id="page-wrapper-inner3">
               <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
            </div>
            <div className="row" id="warehouseTable">
               <div className="col-md-12">
                  {preloader}
                  <BootstrapTable data={this.state.response} striped hover pagination>
                     <TableHeaderColumn width="20px" dataField='checkboxId' dataFormat={ this.checkboxFormatter }>
                        <input type="checkbox" id="checkAll" onChange={(e) => this.checkAllCheckbox('checkbox')} />
                     </TableHeaderColumn>
                     <TableHeaderColumn dataField='id' width="150px" isKey filter={ { type: filter1 } } dataFormat={ this.idFormatter } dataSort={ true }>Warehouse <i className="fa fa-search search-size" onMouseOver={() => this.filterShow1()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='name' width="200px" filter={ { type: filter2 } }  dataSort={ true }>Name <i className="fa fa-search search-size" onMouseOver={() => this.filterShow2()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='location' width="300px" filter={ { type: filter3 } } dataSort={ true }>Location <i className="fa fa-search search-size" onMouseOver={() => this.filterShow3()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='description' width="300px" filter={ { type: filter4 } } dataSort={ true }>Description <i className="fa fa-search search-size" onMouseOver={() => this.filterShow4()}></i></TableHeaderColumn>
                  </BootstrapTable>
               </div>
            </div>
         </div>
      );
    }
}

export default WarehouseListTable;
