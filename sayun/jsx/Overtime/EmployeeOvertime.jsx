import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class EmployeeOvertime extends React.Component {
   constructor(props) {
      super(props);

      var timeIn = "";
      var timeOut = "";

      if(props.employeeOvertime.timeIn != "" && props.employeeOvertime.timeOut != ""){
         timeIn = props.employeeOvertime.timeIn.slice(0,5) + " PM";
         timeOut = props.employeeOvertime.timeOut.slice(0,5) + " PM";
      }

      this.state = {
         timeIn: timeIn,
         timeOut: timeOut,
         hours: props.employeeOvertime.hours,
         overtimeId: "",
         logout: false
      };

      this.handleTimein = this.handleTimein.bind(this);
      this.handleTimeout = this.handleTimeout.bind(this);
   }

   handleTimein(e) {
      this.setState({
         timeIn: e.target.value
      });
    }

   handleTimeout(e) {
      this.setState({
         timeOut: e.target.value
      });
   }

   isGreaterThan(timeinId, timeoutId){
      var timeIn = "";
      var timeOut = "";
      
      if(timeinId.includes('PM')){
         timeinId = timeinId.replace(' PM', '');
         var hours = timeinId.slice(0, 2);
         var minutes = timeinId.slice(3);
         hours = parseInt(hours);
         timeIn = hours + ":" + minutes + ":00";
      }

      if(timeoutId.includes('PM')){
         timeoutId = timeoutId.replace(' PM', '');
         var hours = timeoutId.slice(0, 2);
         var minutes = timeoutId.slice(3);
         hours = parseInt(hours);
         timeOut = hours + ":" + minutes + ":00";
      }

      if((new Date("01/01/01 " + timeIn)) > (new Date("01/01/01 " + timeOut))){
         return true;
      } else{
         return false;
      }
   }

   checkInputs(timeinId, timeoutId){
      var isGreaterThan = this.isGreaterThan(timeinId, timeoutId);
      
         if(isGreaterThan){
            alert("Time in should not be greated than Time out.");
        } else{
            this.saveAttendance();
        }   
     
   }

   saveAttendance() {
      var self = this;
      var employeeOvertime = this.props.employeeOvertime;
      var date = employeeOvertime.date;

      var timein = document.getElementById('timeIn' + date).value;
      var timeout = document.getElementById('timeOut' + date).value;

      var timeinDesc = timein.split(' ').pop();
      var timeoutDesc = timeout.split(' ').pop();

      timein = timein.slice(0,5) + ":00";
      timeout = timeout.slice(0,5) + ":00";

      var timeInDate = new Date("01/01/2000 " + timein);    
      var timeOutDate = new Date("01/01/2000 " + timeout);    

      var diff = timeOutDate - timeInDate;
      diff = (diff / 3600000).toFixed(2);

      var data = {"employee": employeeOvertime.employee, "date": employeeOvertime.date, "time_in": timein,
                  "time_out": timeout, "total_hours": diff}
                    
      if((employeeOvertime.timeIn != "" && employeeOvertime.timeIn != "00:00:00")){
         var overtimeId = this.state.overtimeId;
         if(overtimeId == ""){
            overtimeId = employeeOvertime.id;
         }
         $.ajax({
            url: Utils.basePath + "/overtimes/" + overtimeId + "/", 
            type: "PUT",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },

            success: function (data) {
               alert("Overtime is updated!");
               self.setState({hours: diff});
            },
            error: function (response) {
               console.log(response);
            }
         });
      } else{
         $.ajax({
            url: Utils.basePath + "/overtimes/",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(data),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },

            success: function (data) {
               alert("Overtime is saved!");
               self.setState({hours: diff});
               self.setState({overtimeId: data.id});
            },
            error: function (response) {
               console.log(response);
            }
         });
      }
   }

   render() {
      if (this.state.logout) {
             return <Redirect push to="/" />;
      }

      var employeeOvertime = this.props.employeeOvertime;
      var date = employeeOvertime.date;

      var disable = "";
      if(this.state.timeIn == "" || this.state.timeOut == ""){ 
         disable = "disabled";
      }

      var timeinId = "timeIn" + date;
      var timeoutId = "timeOut" + date;

      var hours = "";
      if(this.state.hours){
         hours = this.state.hours;
      }

      var timein = this.state.timeIn;
      var timeout = this.state.timeOut;
      timein = timein.substring(0,5);
      timeout = timeout.substring(0,5);

      return (
         <tr> 
            <td>{date}</td>
            <td><input type="time" id={timeinId} className="form-control"  value={timein} onChange={this.handleTimein}/></td>
            <td><input type="time" id={timeoutId} className="form-control" value={timeout} onChange={this.handleTimeout}/></td>
            <td>{hours}</td>
            <td className="cell-align-center"><button type="button" className="btn btn-default" id={date} onClick={() => this.checkInputs(this.state.timeIn, this.state.timeOut)} disabled={disable}>Save</button></td>
         </tr>

      ); 
   }
}

export default EmployeeOvertime;
