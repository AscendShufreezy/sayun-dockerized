import React from 'react';
import ReactDOM from 'react-dom';
import EmployeeOvertime from './EmployeeOvertime.jsx';
import moment from 'moment';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class EmployeeOvertimeListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         date: moment(),
         firstday: "",
         secondday: "",
         thirdday: "",
         fourthday: "",
         fifthday: "",
         sixthday: "",
         seventhday: "",
         logout: false
      };
   }

   formatDate(first){
      var month = first.getMonth() + 1;
      var date = first.getDate();

      if(month < 10){
         month = "0" + month;
      }if(date < 10){
         date = "0" + date;
      }

      var firstFormat = month + "/" + date + "/" + first.getFullYear();
      this.setState({firstday: firstFormat});
   }

   formatSecondDate(second){
      var month = second.getMonth() + 1;
      var date = second.getDate();
      if(month < 10) month = "0" + month;
      if(date < 10) date = "0" + date;

      var secondFormat = month + "/" + date + "/" + second.getFullYear();
      this.setState({secondday: secondFormat});
   }

   formatThirdDate(third){
      var month = third.getMonth() + 1;
      var date = third.getDate();
      if(month < 10) month = "0" + month;
      if(date < 10) date = "0" + date;

      var thirdFormat = month + "/" + date + "/" + third.getFullYear();
      this.setState({thirdday: thirdFormat});
   }

   formatFourthDate(fourth){
      var month = fourth.getMonth() + 1;
      var date = fourth.getDate();
      if(month < 10) month = "0" + month;
      if(date < 10) date = "0" + date;

      var fourthFormat = month + "/" + date + "/" + fourth.getFullYear();
      this.setState({fourthday: fourthFormat});
   }

   formatFifthDate(fifth){
      var month = fifth.getMonth() + 1;
      var date = fifth.getDate();
      if(month < 10) month = "0" + month;
      if(date < 10) date = "0" + date;

      var fifthFormat = month + "/" + date + "/" + fifth.getFullYear();
      this.setState({fifthday: fifthFormat});
   }

   formatSixthDate(sixth){
      var month = sixth.getMonth() + 1;
      var date = sixth.getDate();
      if(month < 10) month = "0" + month;
      if(date < 10) date = "0" + date;

      var sixthFormat = month + "/" + date + "/" + sixth.getFullYear();
      this.setState({sixthday: sixthFormat});
   }

   formatSeventhDate(seventh){
      var month = seventh.getMonth() + 1;
      var date = seventh.getDate();
      if(month < 10) month = "0" + month;
      if(date < 10) date = "0" + date;

      var seventhFormat = month + "/" + date + "/" + seventh.getFullYear();
      this.setState({seventhday: seventhFormat});
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#empOvertimeListTable').html());
      location.href=url
      return false
   }

   componentWillMount(){
      var today = new Date();
      var first = today.getDate() - today.getDay();

      first = new Date(today.setDate(first));
      this.formatDate(first);

         var url = window.location.href;
        var empId = url.split('=').pop();
         var self = this;
         $.ajax({
            url: Utils.basePath + "/employees/" + empId + "/overtimes/",
            type:"GET",
            dataType:"json",
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
         },

            success: function(response) {
               const employeeOvertimeList = [];
               const dateList = [];
               var firstday = this.state.firstday;
               dateList.push(firstday);

               var firstDate = new Date(firstday);
               var secondDate = new Date(firstDate.getTime()+1000*60*60*24);
               var thirdDate = new Date(secondDate.getTime()+1000*60*60*24);
               var fourthDate = new Date(thirdDate.getTime()+1000*60*60*24);
               var fifthDate = new Date(fourthDate.getTime()+1000*60*60*24);
               var sixthDate = new Date(fifthDate.getTime()+1000*60*60*24);
               var sevethDate = new Date(sixthDate.getTime()+1000*60*60*24);

               this.formatSecondDate(secondDate);
               dateList.push(this.state.secondday);
               this.formatThirdDate(thirdDate);
               dateList.push(this.state.thirdday);
               this.formatFourthDate(fourthDate);
               dateList.push(this.state.fourthday);
               this.formatFifthDate(fifthDate);
               dateList.push(this.state.fifthday);
               this.formatSixthDate(sixthDate);
               dateList.push(this.state.sixthday);
               this.formatSeventhDate(sevethDate);
               dateList.push(this.state.seventhday);

               var hitter = 0;

               for(var x=0;x<dateList.length;x++){
                  hitter = 0;
                  for(var i=0; i<response.length; i++){
                     if(response[i].date == dateList[x]){
                        var employeeOvertime = {};
                        employeeOvertime.id = response[i].id;
                        employeeOvertime.employee = empId;
                        employeeOvertime.date = response[i].date;
                        employeeOvertime.timeIn = response[i].time_in;
                        employeeOvertime.timeOut = response[i].time_out;
                        employeeOvertime.hours = response[i].total_hours;
                        employeeOvertimeList.push(employeeOvertime);
                        hitter = 1;
                     }
                  }
                  if(hitter == 0){
                     var employeeOvertime = {};
                     employeeOvertime.id = dateList[x];
                     employeeOvertime.employee = empId;
                     employeeOvertime.date = dateList[x];
                     employeeOvertime.timeIn = "";
                     employeeOvertime.timeOut = "";
                     employeeOvertime.hours = "";
                     employeeOvertimeList.push(employeeOvertime);
                  }
               }
               this.setState({response: employeeOvertimeList});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
   }

   render(){
      if (this.state.logout) {
             return <Redirect push to="/" />;
      }

      const result = this.state.response.map((employeeOvertime) =>
         <EmployeeOvertime key={employeeOvertime.id} employeeOvertime={employeeOvertime} />
      );
      return(
         <div className="row">
           <div className="row" id="page-wrapper-inner5">
              <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
           </div>
            <div className="col-md-12" id="empOvertimeListTable">
               <table className="table table-striped table-bordered table-responsive">
                  <thead>
                     <tr>
                        <th className="col-md-width">Date</th>
                        <th className="col-md-width">Time in</th>
                        <th className="col-md-width">Time out</th>
                        <th className="col-md-width">Total OT hours</th>
                        <th className="col-md-width"></th>
                     </tr>
                  </thead>
                  <tbody>
                     {result}
                  </tbody>
               </table>
            </div>
         </div>
      );
    }
}

export default EmployeeOvertimeListTable;
