import React from 'react';
import ReactDOM from 'react-dom';
import Overtime from './Overtime.jsx';
import Preloader from '../Utils/Preloader.jsx';
import moment from 'moment';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class OvertimeListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         overtime: [],
         date: props.date,
         preloader: <Preloader />,
         employeeList: [],
         todayFormat: this.formatDate(this.props.date),
         overtimeList: []
      };
   }

   componentWillReceiveProps(nextProps) {
      if(this.state.date != nextProps.date){
         this.setState({date: nextProps.date}, () => this.updateOvertime()); //for async calls
      }
   }

   getEmployees(){
      var employeeList = [];
      var self = this;
      $.ajax({
         url: Utils.basePath + "/employees/",
         type:"GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
         },

         async: false,
         success: function(response) {
            for(var i=0; i<response.length; i++){
               if(response[i].status == "Active"){
                  employeeList.push(response[i]);
               }
            }
         }.bind(this)
      });

      return employeeList;
   }

   formatDate(today){
      today = today._d;
      var month = today.getMonth() + 1;
      var date = today.getDate();

      if(month < 10) month = "0" + month;
      if(date < 10)date = "0" + date;

      return month + "/" + date + "/" + today.getFullYear();
   }

   ifExists(overtime, overtimeList){
      var result = false;
      var localOvertimeList = overtimeList;
      for(var i=0; i<localOvertimeList.length; i++){
         if(overtime.id == localOvertimeList[i].empId){
            if(overtime.timeIn != ""){
               localOvertimeList.splice(i, 1);
               this.setState({overtimeList: localOvertimeList});
            } else{
               result = true;
            }
         }
      }
      return result;
   }

   getOvertime(todayFormat, employeeList){
      var self = this;
      $.ajax({
         url: Utils.basePath + "/overtimes/?d=" + todayFormat,
         type:"GET",
         dataType:"json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
         },

         success: function(response) {
            var overtimeList = [];
            if(response.length == 0){
               for(var i=0; i<employeeList.length; i++){
                  var overtime = {};
                  overtime.checkboxId = employeeList[i].id;
                  overtime.id = employeeList[i].id;
                  overtime.empId = employeeList[i].id;
                  overtime.fName = employeeList[i].first_name;
                  overtime.mName = employeeList[i].middle_name;
                  overtime.lName = employeeList[i].last_name;
                  overtime.date = todayFormat;
                  overtime.timeIn = "";
                  overtime.timeOut = "";
                  overtime.hours = "";
                  overtimeList.push(overtime);
               }
            } else{
               for(var i=0; i<employeeList.length; i++){
                  var passed = false;
                  for(var x=0; x<response.length; x++){
                     if(response[x].employee == employeeList[i].id){
                        var overtime = {};
                        var index = employeeList.indexOf(employeeList[i]);
                        overtime.checkboxId = response[x].id;
                        overtime.id = response[x].id;
                        overtime.empId = response[x].employee;
                        overtime.fName = employeeList[i].first_name;
                        overtime.mName = employeeList[i].middle_name;
                        overtime.lName = employeeList[i].last_name;
                        overtime.date = response[x].date;
                        overtime.timeIn = response[x].time_in;
                        overtime.timeOut = response[x].time_out;
                        overtime.hours = response[x].total_hours;
                        var ifExists = self.ifExists(overtime, overtimeList);
                        if(ifExists == false){
                           overtimeList = self.state.overtimeList;
                           overtimeList.push(overtime);
                           passed = true;
                           break;
                        }
                     }
                  }

                  if(passed == false){
                     var overtime = {};
                     overtime.checkboxId = employeeList[i].id;
                     overtime.id = employeeList[i].id;
                     overtime.empId = employeeList[i].id;
                     overtime.fName = employeeList[i].first_name;
                     overtime.mName = employeeList[i].middle_name;
                     overtime.lName = employeeList[i].last_name;
                     overtime.date = todayFormat;
                     overtime.timeIn = "";
                     overtime.timeOut = "";
                     overtime.hours = "";
                     var ifExists = self.ifExists(overtime, overtimeList);
                     if(ifExists == false){
                        overtimeList = self.state.overtimeList;
                        overtimeList.push(overtime);
                     }
                  }
               }
            }
            console.log(overtimeList);
            self.setState({overtime: overtimeList});
            self.setState({preloader: ""});
         }
      });
   }

   getOvertime2(todayFormat, employeeList){
      var self = this;
      $.ajax({
         url: Utils.basePath + "/overtimes/?d=" + todayFormat,
         type:"GET",
         dataType:"json",
         async: false,
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
         },

         success: function(response) {
            var overtimeList = [];
            if(response.length == 0){
               for(var i=0; i<employeeList.length; i++){
                  var overtime = {};
                  overtime.checkboxId = employeeList[i].id;
                  overtime.id = employeeList[i].id;
                  overtime.empId = employeeList[i].id;
                  overtime.fName = employeeList[i].first_name;
                  overtime.mName = employeeList[i].middle_name;
                  overtime.lName = employeeList[i].last_name;
                  overtime.date = todayFormat;
                  overtime.timeIn = "";
                  overtime.timeOut = "";
                  overtime.hours = "";
                  overtimeList.push(overtime);
               }
            } else{
               for(var i=0; i<employeeList.length; i++){
                  var passed = false;
                  for(var x=0; x<response.length; x++){
                     if(response[x].employee == employeeList[i].id){
                        var overtime = {};
                        var index = employeeList.indexOf(employeeList[i]);
                        overtime.checkboxId = response[x].id;
                        overtime.id = response[x].id;
                        overtime.empId = response[x].employee;
                        overtime.fName = employeeList[i].first_name;
                        overtime.mName = employeeList[i].middle_name;
                        overtime.lName = employeeList[i].last_name;
                        overtime.date = response[x].date;
                        overtime.timeIn = response[x].time_in;
                        overtime.timeOut = response[x].time_out;
                        overtime.hours = response[x].total_hours;
                        var ifExists = self.ifExists(overtime, overtimeList);
                        if(ifExists == false){
                           overtimeList = self.state.overtimeList;
                           overtimeList.push(overtime);
                           passed = true;
                           break;
                        }
                     }
                  }
                  if(passed == false){
                     var overtime = {};
                     overtime.checkboxId = employeeList[i].id;
                     overtime.id = employeeList[i].id;
                     overtime.empId = employeeList[i].id;
                     overtime.fName = employeeList[i].first_name;
                     overtime.mName = employeeList[i].middle_name;
                     overtime.lName = employeeList[i].last_name;
                     overtime.date = todayFormat;
                     overtime.timeIn = "";
                     overtime.timeOut = "";
                     overtime.hours = "";
                     var ifExists = self.ifExists(overtime, overtimeList);
                     if(ifExists == false){
                        overtimeList = self.state.overtimeList;
                        overtimeList.push(overtime);
                     }
                  }
               }
            }
            if(JSON.stringify(overtimeList) !== JSON.stringify(self.state.overtime)){
               console.log(overtimeList);
               self.setState({overtime: overtimeList});
            }
         }
      });
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#overtimeListTable').html());
      location.href=url
      return false
   }

   componentWillMount(){
      var self = this;
      var today = this.state.date;
      var todayFormat = this.formatDate(today);
      var employeeList = this.getEmployees();
      this.setState({employeeList: employeeList});

      this.getOvertime(todayFormat, employeeList);
   }

   updateOvertime(){
      var self = this;
      var today = this.state.date;
      var todayFormat = this.formatDate(today);
      var employeeList = this.state.employeeList;

      this.getOvertime2(todayFormat, employeeList);
   }

   render(){
      const preloader = this.state.preloader;
      const result = this.state.overtime.map((overtime) =>
         <Overtime key={overtime.id} overtime={overtime} employeeList={this.state.employeeList}/>
      );
      return(
         <div className="row">
           <div className="row" id="page-wrapper-inner5">
              <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
           </div>
            <div className="col-md-12" id="overtimeListTable">
               {preloader}
               <table className="table table-striped table-bordered table-responsive">
                  <thead>
                     <tr>
                        <th className="col-sm-width">Employee ID</th>
                        <th className="col-lg-width">Employee</th>
                        <th className="col-sm-width">Time in</th>
                        <th className="col-sm-width">Time out</th>
                        <th className="col-sm-width">Total OT hours</th>
                        <th className="col-sm-width"></th>
                     </tr>
                  </thead>
                  <tbody>
                     {result}
                  </tbody>
               </table>
            </div>
         </div>
      );
    }
}

export default OvertimeListTable;
