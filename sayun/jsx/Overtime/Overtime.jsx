import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Link, Switch, Redirect } from 'react-router-dom';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class Overtime extends React.Component {
   constructor(props) {
      super(props);

      var timeIn = "";
      var timeOut = "";

      if(props.overtime.timeIn != ""){
         timeIn = props.overtime.timeIn.slice(0,5) + " PM";
      } if(props.overtime.timeOut != ""){
         timeOut = props.overtime.timeOut.slice(0,5) + " PM";
      }

      this.state = {
         timeIn: timeIn,
         timeOut: timeOut,
         hours: props.overtime.hours,
         overtimeId: "",
         employeeList: props.employeeList,
         logout: false
      };

      this.handleTimein = this.handleTimein.bind(this);
      this.handleTimeout = this.handleTimeout.bind(this);
      this.handleHours = this.handleHours.bind(this);
   }

   handleTimein(e) {
      this.setState({
         timeIn: e.target.value
      });
    }

   handleTimeout(e) {
      this.setState({
         timeOut: e.target.value
      });
    }

   handleHours(e) {
      this.setState({
         hours: e.target.value
      });
   }

   componentWillMount(){
      var overtime = this.props.overtime;
      var link = "/overtime/" + overtime.empId;;
   }

   getEmployee(id){
      var employee = {};
      var employeeList = this.state.employeeList;
      for(var i=0; i<employeeList.length; i++){
         if(employeeList[i].id == id){
            employee = employeeList[i];
         }
      }

      return employee;
   }

   saveOvertime(){
      var self = this;
      var propsOvertime = this.props.overtime;
      console.log(this.state.timeIn);
      var timeIn = this.state.timeIn.slice(0,5) + ":00";
      var timeOut = this.state.timeOut.slice(0,5) + ":00"; 

      var timeInDate = new Date("01/01/2000 " + timeIn);    
      var timeOutDate = new Date("01/01/2000 " + timeOut);    

      var diff = timeOutDate - timeInDate;
      diff = (diff / 3600000).toFixed(2);

      var dataPost = {"id": propsOvertime.empId, "employee": propsOvertime.empId, "date": propsOvertime.date, "time_in": timeIn,
                  "time_out": timeOut, "total_hours": diff}

      var dataPut = {"employee": propsOvertime.empId, "date": propsOvertime.date, "time_in": timeIn,
                  "time_out": timeOut, "total_hours": diff}

      if(propsOvertime.timeIn != "" && propsOvertime.timeIn != "00:00:00"){
         var overtimeId = this.state.overtimeId;
         if(overtimeId == ""){
            overtimeId = propsOvertime.id;
         }
         $.ajax({
            url: Utils.basePath + "/overtimes/" + overtimeId + "/",
            type: "PUT",
            contentType: "application/json",
            data: JSON.stringify(dataPut),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("Please create necessary payroll dates first before adding Overtime.");
               }
         },

            success: function (data) {
               alert("Overtime is updated!");
               self.setState({hours: diff});
            },
            error: function (response) {
               console.log(response);
            }
         });
      } else{
         $.ajax({
            url: Utils.basePath + "/overtimes/",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(dataPost),
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("Please create necessary payroll dates first before adding Overtime.");
               }
         },

            success: function (data) {
               alert("Overtime is saved!");
               self.setState({hours: diff});
               self.setState({overtimeId: data.id});
            },
            error: function (response) {
               console.log(response);
            }
         });
      }
   }

   render() {
      if (this.state.logout) {
             return <Redirect push to="/" />;
      }

      var overtime = this.props.overtime;
      var employee = this.getEmployee(overtime.empId);
      var date = this.state.date;
      var employeeType = employee.type;
      var linkOvertime = "/attendances/overtime/id=" + overtime.empId;
      var overtimeIdPrint = "";

      if(employeeType == "Per Trip"){
         overtimeIdPrint = overtime.empId;
      } else{
         overtimeIdPrint = <Link to={linkOvertime}><u>{overtime.empId}</u></Link>
      }

      var disable = "";
      var disable2 = "";
      if(this.state.timeIn == "" || this.state.timeOut == ""){ 
         disable = "disabled";
      }

      if(employeeType == "Per Trip"){
         disable2 = "disabled";
      }

      var timeinId = "timeIn" + date;
      var timeoutId = "timeOut" + date;

      var hours = "";
      if(this.state.hours){
         hours = this.state.hours;
      }

      var timein = this.state.timeIn;
      var timeout = this.state.timeOut;
      timein = timein.substring(0,5);
      timeout = timeout.substring(0,5);

      return (
         <tr> 
            <td>{overtimeIdPrint}</td>
            <td>{employee.last_name}, {employee.first_name} {employee.middle_name}</td>
            <td><input type="time" id={timeinId} disabled={disable2} className="form-control"  value={timein} onChange={this.handleTimein}/></td>
            <td><input type="time" id={timeoutId} disabled={disable2} className="form-control" value={timeout} onChange={this.handleTimeout}/></td>
            <td>{hours}</td>
            <td className="cell-align-center"><button type="button" className="btn btn-default" id={date} onClick={() => this.saveOvertime()} disabled={disable}>Save</button></td>
         </tr>

      ); 
   }
}

export default Overtime;