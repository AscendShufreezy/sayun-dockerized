import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import DateInput from '../Utils/DateInput.jsx';
import Pagination from '../Utils/Pagination.jsx';
import OvertimeListTable from './OvertimeListTable.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class OvertimeListPage extends React.Component {
  constructor(){
    super();

    this.state = {
      date: moment()
    };

    this.handleDate = this.handleDate.bind(this);
  }

  handleDate(e) {
    this.setState({
      date: e
    });
  }

 arrowLeft(date){
    var date = date;
    date = new Date(date).getTime();
    date = date - 1000*60*60*24;
    date = new Date(date);
    this.setState({date: moment(date)});
  }

  arrowRight(date){
    var date = date;
    date = new Date(date).getTime();
    date = date + 1000*60*60*24;
    date = new Date(date);
    this.setState({date: moment(date)});
  }

  render(){
    return (
        <div id="page-wrapper">
          <Menu active="Attendance"/>
          <div className="row" id="page-wrapper-inner">
            <PageTitle pageTitle="Attendance by Date"/>
            <DateInput id="date" selected={this.state.date} value={this.state.date} onChange={this.handleDate} arrowRight={() => this.arrowRight(this.state.date)} arrowLeft={() => this.arrowLeft(this.state.date)}/>
            <div>
              <Menu active="Overtime"/>
              <div id="page-wrapper-inner">
                <div className="row" id="page-wrapper-inner3">
                  <br />
                  <OvertimeListTable date={this.state.date}/>
                </div>
              </div>
              <br />
            </div>
          </div>
          <br />
      </div>
    );
  }
}

export default OvertimeListPage;

