import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import Input from '../Utils/Input.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class EditPayrollEmployeeForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            status: props.payroll.status,
            calendar: props.payroll.calendar,
            pendingChecked: "",
            processedChecked: "",
            logout: false

        };

        this.handleStatus = this.handleStatus.bind(this);
    }

    handleStatus(e) {
        var status = e.target.value;
        var pendingChecked = "";
        var processedChecked = "";

        if(status == "Pending"){
            this.setState({pendingChecked: "checked"});
            this.setState({processedChecked:""});
            this.setState({status:"Pending"});

        } else{
            this.setState({pendingChecked: ""});
            this.setState({processedChecked: "checked"});
            this.setState({status: "Processed"});
        }
    }

    componentWillMount(){
        var status = this.state.status;
        var calendar = this.state.calendar;
        var pendingChecked = "";
        var processedChecked = "";

        if(status == "Pending"){
            this.setState({pendingChecked: "checked"});
            this.setState({processedChecked:""});
            this.setState({status:"Pending"});

        } else{
            this.setState({pendingChecked: ""});
            this.setState({processedChecked: "checked"});
            this.setState({status: "Processed"});
        }

        var self = this;
        $.ajax({
            url: Utils.basePath + "/payroll-calendars/" + calendar + "/",
            type:"GET",
            contentType: "application/json",
            async : false,
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },

            success: function (data) {
                self.setState({calendar: data.name})
            }
        });
    }

    render(){
        if (this.state.logout) {
             return <Redirect push to="/" />;
        }

        var payroll = this.props.payroll;
        return (
            <div>
                <div className="row" id="page-wrapper-inner3">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="row">
                                <div className="col-md-4">
                                    Payroll Calendar Name
                                </div>
                                <div className="col-md-6">
                                     {this.state.calendar}
                                </div>
                            </div>
                        </div>   
                        <div className="col-md-6">
                            <div className="row">
                                <div className="col-md-3">
                                    Status
                                </div>
                                <div className="col-md-6">
                                    <input type="radio" name="status" value="Pending" id="btnPending" checked={this.state.pendingChecked} onChange={this.handleStatus} /><a className="radio-text">Pending</a> &nbsp;&nbsp;
                                    <input type="radio" name="status" value="Processed" id="btnProcessed" checked={this.state.processedChecked} onChange={this.handleStatus} /><a className="radio-text">Processed</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row form-row-padding-top">
                        <div className="col-md-6">
                            <div className="row">
                                <div className="col-md-4">
                                    Payroll Date
                                </div>
                                <div className="col-md-6">
                                     {payroll.date}
                                </div>
                            </div>
                        </div>   
                        <div className="col-md-6">
                            <div className="row">
                                <div className="col-md-3">
                                    Coverage Period
                                </div>
                                <div className="col-md-6">
                                    {payroll.coverage}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
            </div>
        );
    }
}   

export default EditPayrollEmployeeForm;