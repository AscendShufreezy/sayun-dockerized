import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import EditPayrollEmployeeForm from './EditPayrollEmployeeForm.jsx';
import PayrollEmployeeListPage from './PayrollEmployeeListPage.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';;

class PayrollEmployeeDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      response: [],
      transfer: false,
      payroll: {},
      calendar: "",
      logout: false
   };
  }

  cancelSave() {
		this.setState({transfer: true});
  }
 
	generatePayslip() {
      var payroll = this.state.payroll;
      window.open(`${Utils.basePath}/export-hrr01/?date=${payroll.date}&token=${sessionStorage.getItem('token')}`);
      return false;
	}

	generateReport(){
        var payroll = this.state.payroll;
        window.open(`${Utils.basePath}/export-hrr02/?date=${payroll.date}&token=${sessionStorage.getItem('token')}`);
        return false;
	}

  updatePayrollCalendar() {
      var payroll = this.state.payroll;
      var self = this;

      if (document.getElementById('btnPending').checked) {
        payroll.status = document.getElementById("btnPending").value;
      } else{
        payroll.status = document.getElementById("btnProcessed").value;
      }

      var data = {"id": payroll.id, "calendar": payroll.calendar, "date": payroll.date,"status": payroll.status, 
                "coverage": payroll.coverage}

      if(payroll.status == "Processed"){
        $.ajax({
          url: Utils.basePath + "/payroll/" + payroll.id + "/process/",
          type: "POST",
          contentType: "application/json",
          headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
          statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
          },

          success: function (data) {
              alert("Payroll updated!");
              self.setState({transfer: true});
          }
        });
      } else{
        var self = this;
        $.ajax({
          url: Utils.basePath + "/payroll/" +  payroll.id + "/",
          type: "PUT",
          contentType: "application/json",
          data: JSON.stringify(data),
          headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
          statusCode: {
             400: function (response) {
                alert(JSON.parse(response.responseText));
             },
             401: function (response) {
                alert("User is not authorized. Please login.");
                self.setState({logout: true});
             },
             500: function (response) {
                alert("HTTP error 500, please check your connection.");
             }
          },
          success: function (data) {
              alert("Payroll updated!");
              self.setState({transfer: true});
          },
          error: function (response) {
              console.log(response);
          }
        });
      }
  }

  componentWillMount(){
    var url = window.location.href;
    var id = url.split('=').pop();
    var self = this;
    var payroll = {};

    $.ajax({
        url: Utils.basePath + "/payroll/" + id + "/",
        type:"GET",
        contentType: "application/json",
        async : false,
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
        },

        success: function (data) {
            payroll.id = data.id;
            payroll.calendar = data.calendar;
            payroll.date = data.date;
            payroll.status = data.status;
            payroll.coverage = data.coverage;
            self.setState({calendar: payroll.calendar});
        }
    });

    this.setState({payroll: payroll})
  }

	render(){
    if (this.state.logout) {
      return <Redirect push to="/" />;
    }

    var payroll = this.state.payroll;
    if (this.state.transfer) {
        var link = "/payroll/calendar/id=" + this.state.calendar;
        return <Redirect push to={link} />; //tbd
    }

    return (
      <div id="page-wrapper">
        <Menu active="Payroll"/>
        <div className="row" id="page-wrapper-inner">
          <Breadcrumb page="edit-payrollemployee" id={this.state.calendar}/>
          <PageTitle pageTitle="Payroll"/>
          <div className="row" id="page-wrapper-inner3">
            <form>
              <EditPayrollEmployeeForm payroll={payroll}/>
              <div className="row">
                <button type="button" className="btn btn-default btn-primary" onClick={() => this.generatePayslip(payroll.date)}>Generate Payslip</button>
                <button type="button" className="btn btn-default btn-primary" onClick={() => this.generateReport(payroll.date)}>Generate Bank Payroll Report</button>
                <div className="col-md-3 pull-right">
                  <div className="pull-right">
                    <button type="button" className="btn btn-default btn-primary" onClick={() => this.updatePayrollCalendar()}>Save</button>
                    <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
                  </div>
                </div>
              </div>
              <hr />
              <PayrollEmployeeListPage payroll={payroll} />
            </form>
            <br />
          </div>
        </div>  
        <br />  
      </div>
    );
  }
}

export default PayrollEmployeeDetails;