import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import PayrollEmployeeListTable from './PayrollEmployeeListTable.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class PayrollEmployeeListPage extends React.Component {
	constructor(){
        super();
        this.state = {
              logout: false,
        }
    }

    render(){
    	if (this.state.logout) {
        return <Redirect push to="/" />;
      }
    	var payroll = this.props.payroll;
      	return (
	      	<div>
	      		<PageTitle pageTitle="Employees"/>
	      		<div className="row" id="page-wrapper-inner3">
	      			<div>
		      			<PayrollEmployeeListTable payroll={payroll}/>
	                </div>
	      		</div>
		    </div>
     	);
   }
}

export default PayrollEmployeeListPage;

