import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class PayrollEmployee extends React.Component {
   render() {
      var payrollEmployee = this.props.payrollEmployee;
      var payroll = this.props.payroll;
      var link = "/payroll/calculation/payroll=" + payroll + "/id=" + payrollEmployee.id;

      var amount = (payrollEmployee.total).toFixed(2);
      var error = "";
      if(amount < 0){
         error = <i className="fa fa-warning color-red"></i>
      } 

      return (
         <tr> 
            <td><Link to={link}><u>{payrollEmployee.id}</u></Link></td>
            <td>{payrollEmployee.lName}, {payrollEmployee.fName} {payrollEmployee.mName}</td>
            <td>{payrollEmployee.type}</td>
            <td><div className="pull-right">{error} {amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</div></td> 
         </tr>

      ); 
   }
}

export default PayrollEmployee;