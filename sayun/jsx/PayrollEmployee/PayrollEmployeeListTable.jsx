import React from 'react';
import ReactDOM from 'react-dom';
import PayrollEmployee from './PayrollEmployee.jsx';
import Preloader from '../Utils/Preloader.jsx';
import EmptyTable from '../Utils/EmptyTable.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class PayrollEmployeeListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         preloader: "",
         empty: "",
         totalOfTotal: 0,
         logout: false
      };
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#payrollEmpListTable').html());
      location.href=url
      return false
   }

   componentDidMount(){
         var self = this;
         var payrollDate = this.props.payroll.date;
         this.setState({preloader: <Preloader/>});
         var totalOfTotal = 0;
         $.ajax({
            url: Utils.basePath + "/employees/",
            type:"GET",
            contentType: "application/json",
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText));
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(response) {
               const employeeList = [];
               for(var i=0; i<response.length; i++){
                  if(sessionStorage.getItem('group') == "hr-group"){
                     if(response[i].type == "Per Trip"){
                     	var employee = {};
                     	employee.id = response[i].id;
                     	employee.fName = response[i].first_name;
                     	employee.mName = response[i].middle_name;
                    	   employee.lName = response[i].last_name;
                        employee.type = response[i].type;
                        employee.total = 0;

                        $.ajax({
                           url: Utils.basePath + "/employees/" + employee.id + "/total_payroll/?date=" + payrollDate,
                           type:"GET",
                           contentType: "application/json",
                           async: false,
                           headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
                           statusCode: {
                              400: function (response) {
                                 alert(JSON.parse(response.responseText));
                              },
                              401: function (response) {
                                 alert("User is not authorized. Please login.");
                                 self.setState({logout: true});
                              },
                              500: function (response) {
                                 alert("HTTP error 500, please check your connection.");
                              }
                           },
                           success: function(response) {
                              employee.total = response.total;
                              totalOfTotal = totalOfTotal + employee.total;
                           }.bind(this)
                        });
                        self.setState({totalOfTotal: totalOfTotal});
                     	employeeList.push(employee);
                     }
                  } else{
                     var employee = {};
                     employee.id = response[i].id;
                     employee.fName = response[i].first_name;
                     employee.mName = response[i].middle_name;
                     employee.lName = response[i].last_name;
                     employee.type = response[i].type;
                     employee.total = 0;

                     $.ajax({
                        url: Utils.basePath + "/employees/" + employee.id + "/total_payroll/?date=" + payrollDate,
                        type:"GET",
                        contentType: "application/json",
                        async: false,
                        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
                        statusCode: {
                           400: function (response) {
                              alert(JSON.parse(response.responseText));
                           },
                           401: function (response) {
                              alert("User is not authorized. Please login.");
                              self.setState({logout: true});
                           },
                           500: function (response) {
                              alert("HTTP error 500, please check your connection.");
                           }
                        },
                        success: function(response) {
                           employee.total = response.total;
                           totalOfTotal = totalOfTotal + employee.total;
                        }.bind(this)
                     });
                     self.setState({totalOfTotal: totalOfTotal});
                     employeeList.push(employee);
                  }
               }

               if(employeeList.length == 0){
                  this.setState({empty: <EmptyTable text="Payrolls" span="4"/>});
               }

               this.setState({response: employeeList});
               this.setState({preloader: ""});
            }.bind(this),
            error: function(xhr, status, err) {
            }.bind(this)
         });
   }

   render(){
      if (this.state.logout) {
         return <Redirect push to="/" />;
      }


      const preloader = this.state.preloader;
      var id = window.location.href.split('=').pop();

      var result = this.state.empty;
      var total = (this.state.totalOfTotal).toFixed(2);

      if(this.state.response.length != 0){
         result = this.state.response.map((payrollEmployee) =>
            <PayrollEmployee key={payrollEmployee.id} payroll={id} payrollEmployee={payrollEmployee} />
         );
      }

      return(
         <div>
            <div className="row">
              <div className="row" id="page-wrapper-inner5">
                 <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
              </div>
               <div className="col-md-12" id="payrollEmpListTable">
                  {preloader}
                  <table className="table table-striped table-bordered table-responsive">
                     <thead className="thead-font-size">
                        <tr>
                           <th className="col-md-width">Employee Id</th>
                           <th className="col-lg-width">Name</th>
                           <th className="col-md-width">Employee Type</th>
                           <th className="col-md-width"><div className="pull-right">Amount</div></th>
                        </tr>
                     </thead>
                     <tbody>
                        {result}
                     </tbody>
                  </table>
               </div>
            </div>
            <div className="row">
                  <div className="pull-right">
                     <div className="col-md-1 employee-total2">
                           Total
                     </div>
                     <div className="col-md-2 employee-total">
                           {total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                     </div>
                  </div>
          </div>
         </div>
      );
    }
}

export default PayrollEmployeeListTable;
