import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Utils from '../Utils/Utils.jsx';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import SummaryListTable from './SummaryListTable.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class SummaryListPage extends React.Component {
	constructor(){
	    super();

	    this.state = {
	      from: moment(),
	      to: moment(),
	      items: "",
	      item: {}, 
	      total: 0,
	      totalItem: 0,
	      logout: false,
	    };

	    this.handleFrom = this.handleFrom.bind(this);	    
	    this.handleTo = this.handleTo.bind(this);
	    this.handleItem = this.handleItem.bind(this);
    }

    handleItem(e) {
        this.setState({
            item: e.target.value
        });
    }

	handleTo(e) {
	    this.setState({
	      to: e
	    });
	}

	handleFrom(e) {
	    this.setState({
	      from: e
	    });
	}

	formatDate(today){
	    today = today._d;
	    var month = today.getMonth() + 1;
	    var date = today.getDate();

	    if(month < 10) month = "0" + month;
	    if(date < 10)date = "0" + date;

	    return date + "/" + month + "/" + today.getFullYear();
	}

	componentWillMount(){
		var group = sessionStorage.getItem('group');
		var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

    	if (group != "admin-group" && group != "hr-group" && group != "warehouse-group" && sessionPermission.indexOf(125) == -1) {
    		alert("You are not authorized to access this page. Please login as admin user.");
    		this.setState({logout: true});
    	} 
    	
		var self = this;
		var totalItem = 0;
		var total = 0;
		const itemList = [];

        $.ajax({
            url: Utils.basePath + "/items/",
            type:"GET",
            contentType: "application/json",
            async: false,
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
			},

            success: function(response) {
                for(var i=0;i<response.length;i++){
                    itemList.push(response[i]);
                }
                this.setState({items: itemList});
                this.setState({item: itemList[0].id});
            }.bind(this)
         });
		
		var fromDate = this.formatDate(this.state.from);
        var toDate = this.formatDate(this.state.to);
        var item = itemList[0];

        $.ajax({
            url: Utils.basePath + "/items/" + item + "/receipt_summary/?from=" + fromDate + "&to=" + toDate,
            type:"GET",
            contentType: "application/json",
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            success: function(response) {
                const itemList = [];
                for(var i=0;i<response.length;i++){
                    total = total + response[i].total;
                    totalItem = totalItem + response[i].quantity;
                }
                this.setState({total: total});
                this.setState({totalItem: totalItem})
            }.bind(this)
        });
	}



    render(){
    	if (this.state.logout) {
             return <Redirect push to="/" />;
 		}
 
    	var item = this.state.item;
    	var fromDate = this.state.from;
    	var toDate = this.state.to;
    	var amount = this.state.total;

    	console.log(this.state.item);

    	var itemsResult = "";
    	if(this.state.items != ""){
    		itemsResult = this.state.items.map((items) =>
		       <option key={items.name} value={items.id}>{items.name}</option>
		    ); 
    	}
	   
      	return (
	      	<div id="page-wrapper">
		      	<Menu active="ReportsIis"/>
		      	<div className="row" id="page-wrapper-inner">
		      		<div className="row" id="page-wrapper-inner5">
		      			<form>
			      			<Menu active="Summary"/>
			      			<div className="row" id="page-wrapper-inner">
		      					<div className="row" id="page-wrapper-inner5">
		      						<br />
						            <div className="col-conf-2 form-row-padding-top">
						                Item
						            </div>
						             <div className="col-md-3">
										<select className="form-control" id="item" value={this.state.item} onChange={this.handleItem}>
				                            {itemsResult}
				                        </select>
						            </div>
						            <div className="col-md-1 col-md-offset-1 form-row-padding-top">
						                From
						            </div>
						             <div className="col-md-2">
										<DatePicker selected={this.state.from} onChange={this.handleFrom} id="from" className="form-control cust-req-mover"/>
										<i className="fa fa-calendar cust-req-mover3"></i>						            
						            </div>
						            <div className="col-md-1 form-row-padding-top">
						                To
						            </div>
						             <div className="col-md-2">
										<DatePicker selected={this.state.to} onChange={this.handleTo} id="to" className="form-control cust-req-mover2"/>
										<i className="fa fa-calendar cust-req-mover4"></i>
						            </div>
						            <br /><br />
						            <div className="row" id="page-wrapper-inner5">
							      		<SummaryListTable item={item} from={fromDate} to={toDate}/>
						      			<br />
						      		</div>
		      					</div>
		      				</div>
			      		</form>
		      			<br />
		      		</div>
		      	</div>	
		      	<br />
		    </div>
     	);
   }
}

export default SummaryListPage;

