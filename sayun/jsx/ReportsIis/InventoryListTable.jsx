import React from 'react';
import ReactDOM from 'react-dom';
import EmptyTable from '../Utils/EmptyTable.jsx';
import Preloader from '../Utils/Preloader.jsx';
import Utils from '../Utils/Utils.jsx';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class InventoryListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         preloader: "",
         empty: "",
         date: props.date,
         filter1: "",
         filter2: "",
         filter3: ""
      };
   }

   componentWillReceiveProps(nextProps) {
      if(this.state.date != nextProps.date){
         this.setState({date: nextProps.date}, () => this.updateTable()); //for async calls
      }
   }

   formatDate(today){
      today = today._d;
      var month = today.getMonth() + 1;
      var date = today.getDate();

      if(month < 10) month = "0" + month;
      if(date < 10)date = "0" + date;

      return month + "/" + date + "/" + today.getFullYear();
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#inventoryListTable').html());
      location.href=url
      return false
   }

   componentWillMount(){
      var self = this;
      var date = this.state.date;
      date = this.formatDate(date);

      this.setState({preloader: <Preloader/>});
      $.ajax({
         url: Utils.basePath + "/items/inventory/?date=" + date,
         type:"GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
            400: function (response) {
               alert(JSON.parse(response.responseText));
            },
            401: function (response) {
               alert("User is not authorized. Please login.");
               self.setState({logout: true});
            },
            500: function (response) {
               alert("HTTP error 500, please check your connection.");
            }
         },
         success: function(response) {
            const inventoryList = [];
            for(var i=0; i<response.length; i++){
               var inventory = {};
               inventory.id = response[i].id;
               inventory.name = response[i].name;
               inventory.qty = response[i].quantity;
               inventory.total = response[i].total_cost;
               inventoryList.push(inventory);
            }

            if(inventoryList.length == 0){
               this.setState({empty: <EmptyTable text="Item Inventory" span="3"/>});
            }

            this.setState({response: inventoryList});
            this.setState({preloader: ""});
         }.bind(this),
         error: function(xhr, status, err) {
         }.bind(this)
      });
   }

   updateTable(){
      var self = this;
      var date = this.state.date;
      date = this.formatDate(date);

      this.setState({preloader: <Preloader/>});
      $.ajax({
         url: Utils.basePath + "/items/inventory/?date=" + date,
         type:"GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
            400: function (response) {
               alert(JSON.parse(response.responseText));
            },
            401: function (response) {
               alert("User is not authorized. Please login.");
               self.setState({logout: true});
            },
            500: function (response) {
               alert("HTTP error 500, please check your connection.");
            }
         },
         success: function(response) {
            const inventoryList = [];
            for(var i=0; i<response.length; i++){
               var inventory = {};
               inventory.id = response[i].id;
               inventory.name = response[i].name;
               inventory.qty = response[i].quantity;
               inventory.total = response[i].total_cost;
               inventoryList.push(inventory);
            }

            if(inventoryList.length == 0){
               this.setState({empty: <EmptyTable text="Item Inventory" span="3"/>});
            }

            this.setState({response: inventoryList});
            this.setState({preloader: ""});
         }.bind(this),
         error: function(xhr, status, err) {
         }.bind(this)
      });
   }

   amountFormatter(cell, row){
      cell = (cell).toFixed(2);
      var amount = cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return (
         amount
      );
   }

   filterShow1(){
      if(this.state.filter1 == ""){
         this.setState({filter1: 'RegexFilter'});
      } else{
         this.setState({filter1: ''});
      }
   }

   filterShow2(){
      if(this.state.filter2 == ""){
         this.setState({filter2: 'RegexFilter'});
      } else{
         this.setState({filter2: ''});
      }
   }

   filterShow3(){
      if(this.state.filter3 == ""){
         this.setState({filter3: 'RegexFilter'});
      } else{
         this.setState({filter3: ''});
      }
   }

   render(){
      const preloader = this.state.preloader;
      var filter1 = this.state.filter1;
      var filter2 = this.state.filter2;
      var filter3 = this.state.filter3;

      return(
         <div className="row">
           <div className="row" id="page-wrapper-inner5">
              <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
           </div>
            <div className="col-md-12" id="inventoryListTable">
               {preloader}
               <BootstrapTable data={this.state.response} striped hover pagination>
                  <TableHeaderColumn dataField='name' width="300px" isKey filter={ { type: filter1 } }  dataSort={ true }>Name <i className="fa fa-search search-size" onMouseOver={() => this.filterShow1()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='qty' width="200px" filter={ { type: filter2 } } dataSort={ true }>Qty <i className="fa fa-search search-size" onMouseOver={() => this.filterShow2()}></i></TableHeaderColumn>
                  <TableHeaderColumn dataField='total' width="200px" filter={ { type: filter3 } } dataSort={ true } dataFormat={ this.amountFormatter }>Total Cost <i className="fa fa-search search-size" onMouseOver={() => this.filterShow3()}></i></TableHeaderColumn>
               </BootstrapTable>
            </div>
         </div>
      );
    }
}

export default InventoryListTable;
