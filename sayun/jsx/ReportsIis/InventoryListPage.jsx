import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Utils from '../Utils/Utils.jsx';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import InventoryListTable from './InventoryListTable.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class InventoryListPage extends React.Component {
	constructor(){
	    super();

	    this.state = {
	      date: moment(),
	      totalAmount: 0,
	      totalQty: 0
	    };

	    this.handleDate = this.handleDate.bind(this);
	}

	handleDate(e) {
      this.updateTable(e);
	    this.setState({date: e});
	}

  formatDate(today){
    today = today._d;
    var month = today.getMonth() + 1;
    var date = today.getDate();

    if(month < 10) month = "0" + month;
    if(date < 10)date = "0" + date;

    return month + "/" + date + "/" + today.getFullYear();
  }

	updateTable(e){
		var self = this;
		var totalAmount = 0;
		var totalQty = 0;
    var date = this.formatDate(e);

    $.ajax({
        url: Utils.basePath + "/items/inventory/?date=" + date,
        type:"GET",
        asyc: false,
        contentType: "application/json",
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        statusCode: {
           400: function (response) {
              alert(JSON.parse(response.responseText));
           },
           401: function (response) {
              alert("User is not authorized. Please login.");
              self.setState({logout: true});
           },
           500: function (response) {
              alert("HTTP error 500, please check your connection.");
           }
        },
        success: function(response) {
            const inventoryList = [];
            for(var i=0; i<response.length; i++){
              totalAmount = totalAmount + response[i].total_cost;
              totalQty = totalQty + response[i].quantity;
            }
            self.setState({totalQty: totalQty}); 
            self.setState({totalAmount: totalAmount}); 
            self.setState({date: e});
        }.bind(this)
     });
	}

  componentWillMount(){
    var group = sessionStorage.getItem('group');
    var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");
    if (group != "admin-group" && group != "hr-group" && group != "warehouse-group" && sessionPermission.indexOf(125) == -1) {
      alert("You are not authorized to access this page. Please login as admin user.");
      this.setState({logout: true});
    } 

    var self = this;
    var totalAmount = this.state.totalAmount;
    var totalQty = this.state.totalQty;
    var date = this.state.date;
    date = this.formatDate(date);

    $.ajax({
        url: Utils.basePath + "/items/inventory/?date=" + date,
        type:"GET",
        contentType: "application/json",
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        statusCode: {
           400: function (response) {
              alert(JSON.parse(response.responseText));
           },
           401: function (response) {
              alert("User is not authorized. Please login.");
              self.setState({logout: true});
           },
           500: function (response) {
              alert("HTTP error 500, please check your connection.");
           }
        },
        success: function(response) {
            const inventoryList = [];
            for(var i=0; i<response.length; i++){
              totalAmount = totalAmount + response[i].total_cost;
              totalQty = totalQty + response[i].quantity;
            }
            this.setState({totalQty: totalQty}); 
            this.setState({totalAmount: totalAmount}); 
        }.bind(this)
     });
  }

  render(){
  	var amount = (this.state.totalAmount).toFixed(2);
    var date = this.state.date;
    amount = amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    return (
    	<div id="page-wrapper">
      	<Menu active="ReportsIis"/>
      	<div className="row" id="page-wrapper-inner">
      		<div className="row" id="page-wrapper-inner5">
      			<form>
	      			<Menu active="Inventory"/>
	      			<div className="row" id="page-wrapper-inner">
      					<div className="row" id="page-wrapper-inner5">
      						<br />
				            <div className="col-md-2 form-row-padding-top">
				                Inventory Balance as of
				            </div>
				             <div className="col-md-3">
  										<DatePicker selected={this.state.date} onChange={this.handleDate} id="date" className="form-control" />						                
  										<i className="fa fa-calendar"></i>
				            </div>
				            <br /><br />  
					            <div className="row" id="page-wrapper-inner5">
							      		<InventoryListTable date={date}/>
							      		<div>
							      			<div className="col-md-1 col-md-offset-4">
                            Total
                        </div>
                        <div className="col-md-1 inv-qty">
                            {this.state.totalQty}
                        </div>
                        <div className="col-md-1 inv-amount">
                            {amount}
                        </div>
                      <br />
						      		</div>
					      			<br />
					      		</div>
	      					</div>
	      				</div>
		      		</form>
	      			<br />
	      		</div>
	      	</div>	
	      	<br />
		  </div>
    );
  }
}

export default InventoryListPage;

