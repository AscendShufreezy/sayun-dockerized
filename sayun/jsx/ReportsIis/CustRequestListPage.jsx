import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import CustRequestListTable from './CustRequestListTable.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class CustRequestListPage extends React.Component {
	constructor(){
	    super();

	    this.state = {
	      from: moment(),
	      to: moment(),
	      customers: "",
	      customer: {}, 
	      total: 0,
	      totalItem: 0,
	      logout: false
	    };

	    this.handleFrom = this.handleFrom.bind(this);	    
	    this.handleTo = this.handleTo.bind(this);
        this.handleCustomer = this.handleCustomer.bind(this);
    }

    handleCustomer(e) {
        this.setState({
            customer: e.target.value
        });
    }

	handleTo(e) {
	    this.setState({
	      to: e
	    });
	}

	handleFrom(e) {
	    this.setState({
	      from: e
	    });
	}

	filter(){
		var customer = this.state.customer;
		var fromDate = this.state.from;
		var toDate = this.state.to;

		this.updateTable(customer, fromDate, toDate);
	}

	formatDate(today){
	    today = today._d;
	    var month = today.getMonth() + 1;
	    var date = today.getDate();

	    if(month < 10) month = "0" + month;
	    if(date < 10)date = "0" + date;

	    return date + "/" + month + "/" + today.getFullYear();
	}

	updateTable(customer, fromDate, toDate){
		var total = 0;
		var totalItem = 0;
		fromDate = this.formatDate(fromDate);
		toDate = this.formatDate(toDate);

        $.ajax({
            url: Utils.basePath + "/customers/" + customer + "/requests/?from=" + fromDate + "&to=" + toDate,
            type:"GET",
            async: false,
            contentType: "application/json",
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            success: function(response) {
                const itemList = [];
                for(var i=0;i<response.length;i++){
                    total = total + parseFloat(response[i].total);
                    totalItem = totalItem + response[i].quantity;
                }
                this.setState({total: total});
                this.setState({totalItem: totalItem})
            }.bind(this)
         });

	}

	componentWillMount(){
		var group = sessionStorage.getItem('group');
		var sessionPermission = JSON.parse("[" + sessionStorage.getItem('permissions') + "]");

    	if (group != "admin-group" && group != "hr-group" && group != "warehouse-group" && sessionPermission.indexOf(125) == -1) {
    		alert("You are not authorized to access this page. Please login as admin user.");
    		this.setState({logout: true});
    	} 
		
		var self = this;
		var totalItem = this.state.totalItem;
		var total = this.state.total;
		const customerList = [];

        $.ajax({
            url: Utils.basePath + "/customers/?sort=name",
            type:"GET",
            contentType: "application/json",
            async: false,
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
			},

            success: function(response) {
                for(var i=0;i<response.length;i++){
                	if(response[i].type !== 'Employee'){ //Retrieve truck type only
						customerList.push(response[i].id);
                	}
                }
                this.setState({customers: customerList});
                this.setState({customer: customerList[0]});
            }.bind(this)
         });
        
        var fromDate = this.formatDate(this.state.from);
        var toDate = this.formatDate(this.state.to);
        var customer = customerList[0];

        $.ajax({
            url: Utils.basePath + "/customers/" + customer + "/requests/?from=" + fromDate + "&to=" + toDate,
            type:"GET",
            async: false,
            contentType: "application/json",
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            success: function(response) {
                const itemList = [];
                for(var i=0;i<response.length;i++){
                    total = total + response[i].total;
                    totalItem = totalItem + response[i].quantity;
                }
                this.setState({total: total});
                this.setState({totalItem: totalItem})
            }.bind(this)
        });
	}

    render(){
    	if (this.state.logout) {
             return <Redirect push to="/" />;
 		}

    	var customer = this.state.customer;
    	var fromDate = this.state.from;
    	var toDate = this.state.to;
    	var amount = this.state.total;

    	var customersResult = "";
    	if(this.state.items != ""){
    		customersResult = this.state.customers.map((customer) =>
		       <option key={customer} value={customer}>{customer}</option>
		    ); 
    	}
	   
      	return (
	      	<div id="page-wrapper">
		      	<Menu active="ReportsIis"/>
		      	<div className="row" id="page-wrapper-inner">
		      		<div className="row" id="page-wrapper-inner5">
		      			<form>
			      			<Menu active="CustRequest"/>
			      			<div className="row" id="page-wrapper-inner">
		      					<div className="row" id="page-wrapper-inner5">
		      						<br />
						            <div className="col-conf-2 form-row-padding-top">
						                Customer ID
						            </div>
						             <div className="col-md-3">
										<select className="form-control" id="item" value={this.state.customer} onChange={this.handleCustomer}>
				                            {customersResult}
				                        </select>
						            </div>
						            <div className="col-md-1 col-md-offset-1 form-row-padding-top">
						                From
						            </div>
						             <div className="col-md-2">
										<DatePicker selected={this.state.from} onChange={this.handleFrom} id="from" className="form-control cust-req-mover"/>
										<i className="fa fa-calendar cust-req-mover3"></i>						            
						            </div>
						            <div className="col-md-1 form-row-padding-top">
						                To
						            </div>
						             <div className="col-md-2">
										<DatePicker selected={this.state.to} onChange={this.handleTo} id="to" className="form-control cust-req-mover2"/>
										<i className="fa fa-calendar cust-req-mover4"></i>
						            </div>
									<br /><br />
						            <div className="row" id="page-wrapper-inner5">
							      		<CustRequestListTable customer={customer} from={fromDate} to={toDate}/>
						      			<br />
						      		</div>
		      					</div>
		      				</div>
			      		</form>
		      			<br />
		      		</div>
		      	</div>	
		      	<br />
		    </div>
     	);
   }
}

export default CustRequestListPage;

