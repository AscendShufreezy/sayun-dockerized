import React from 'react';
import ReactDOM from 'react-dom';
import EmptyTable from '../Utils/EmptyTable.jsx';
import Preloader from '../Utils/Preloader.jsx';
import Utils from '../Utils/Utils.jsx';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { HashRouter, Route, Link, Switch } from 'react-router-dom';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class CustRequestListTable extends React.Component {
   constructor(props){
      super(props);

      this.state = {
         response: [],
         preloader: "",
         empty: "",
         customer: props.customer,
         to: props.to,
         from: props.from,
         total: 0,
         totalItem: 0,
         filter1: "",
         filter2: "",
         filter3: "",
         filter4: "",
         filter5: "",
         filter6: ""
      };
   }

   componentWillReceiveProps(nextProps) {
      if(this.state.customer != nextProps.customer || this.state.from != nextProps.from || this.state.to != nextProps.to){
         this.setState({customer: nextProps.customer, from: nextProps.from, to: nextProps.to }, () => this.updateTable()); //for async calls
      }
   }

   formatDate(today){
       today = today._d;
       var month = today.getMonth() + 1;
       var date = today.getDate();

       if(month < 10) month = "0" + month;
       if(date < 10)date = "0" + date;

       return date + "/" + month + "/" + today.getFullYear();
   }

   exportToExcel(){
      var url='data:application/vnd.ms-excel,' + encodeURIComponent($('#custRequestListTable').html());
      location.href=url
      return false
   }

   componentDidMount(){
      var self = this;
      var fromDate = this.formatDate(this.state.from);
      var toDate = this.formatDate(this.state.to);
      var customer = this.state.customer;

      this.setState({preloader: <Preloader/>});
      $.ajax({
         url: Utils.basePath + "/customers/" + customer + "/requests/?from=" + fromDate + "&to=" + toDate,
         type:"GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
            400: function (response) {
               alert(JSON.parse(response.responseText));
            },
            401: function (response) {
               alert("User is not authorized. Please login.");
               self.setState({logout: true});
            },
            500: function (response) {
               alert("HTTP error 500, please check your connection.");
            }
         },
         success: function(response) {
            const requestList = [];
            var total = 0;
            var totalItem = 0;

            for(var i=0; i<response.length; i++){
               var  request = {};
               request.id = response[i].id;
               request.name = response[i].item_name;
               request.date = response[i].date;
               request.unitPrice = response[i].unit_price;
               request.request = response[i].request;
               request.qty = response[i].quantity;
               request.total = response[i].total;
               requestList.push(request);
            }

            if(requestList.length == 0){
               this.setState({empty: <EmptyTable text="Items Out" span="6"/>});
            }

            for(var i=0;i<response.length;i++){
              total = total + parseFloat(response[i].total);
              totalItem = totalItem + response[i].quantity;
            }
            this.setState({total: total});
            this.setState({totalItem: totalItem})

            this.setState({response: requestList});
            this.setState({preloader: ""});
         }.bind(this),
         error: function(xhr, status, err) {
         }.bind(this)
      });
   }

   updateTable(){
      var self = this;
      var fromDate = this.formatDate(this.state.from);
      var toDate = this.formatDate(this.state.to);
      var customer = this.state.customer;

      this.setState({preloader: <Preloader/>});
      $.ajax({
         url: Utils.basePath + "/customers/" + customer + "/requests/?from=" + fromDate + "&to=" + toDate,
         type:"GET",
         contentType: "application/json",
         headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
         statusCode: {
            400: function (response) {
               alert(JSON.parse(response.responseText));
            },
            401: function (response) {
               alert("User is not authorized. Please login.");
               self.setState({logout: true});
            },
            500: function (response) {
               alert("HTTP error 500, please check your connection.");
            }
         },
         success: function(response) {
            const requestList = [];
            var total = 0;
            var totalItem = 0;

            for(var i=0; i<response.length; i++){
               console.log(response);
               var  request = {};
               request.id = response[i].id;
               request.name = response[i].item_name;
               request.date = response[i].date;
               request.request = response[i].request;
               request.unitPrice = response[i].unit_price;
               request.qty = response[i].quantity;
               request.total = response[i].total;
               requestList.push(request);
            }

            if(requestList.length == 0){
               this.setState({empty: <EmptyTable text="Items Out" span="6"/>});
            }

            for(var i=0;i<response.length;i++){
              total = total + parseFloat(response[i].total);
              totalItem = totalItem + response[i].quantity;
            }
            this.setState({total: total});
            this.setState({totalItem: totalItem})

            console.log(requestList);
            this.setState({response: requestList});
            this.setState({preloader: ""});
         }.bind(this),
         error: function(xhr, status, err) {
         }.bind(this)
      });
   }

   amountFormatter(cell, row){
      var amount = cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return (
         amount
      );
   }

   filterShow1(){
      if(this.state.filter1 == ""){
         this.setState({filter1: 'RegexFilter'});
      } else{
         this.setState({filter1: ''});
      }
   }

   filterShow2(){
      if(this.state.filter2 == ""){
         this.setState({filter2: 'RegexFilter'});
      } else{
         this.setState({filter2: ''});
      }
   }

   filterShow3(){
      if(this.state.filter3 == ""){
         this.setState({filter3: 'RegexFilter'});
      } else{
         this.setState({filter3: ''});
      }
   }

   filterShow4(){
      if(this.state.filter4 == ""){
         this.setState({filter4: 'RegexFilter'});
      } else{
         this.setState({filter4: ''});
      }
   }

   filterShow5(){
      if(this.state.filter5 == ""){
         this.setState({filter5: 'RegexFilter'});
      } else{
         this.setState({filter5: ''});
      }
   }

   filterShow6(){
      if(this.state.filter6 == ""){
         this.setState({filter6: 'RegexFilter'});
      } else{
         this.setState({filter6: ''});
      }
   }

   render(){
      const preloader = this.state.preloader;
      var total = (this.state.total).toFixed(2);
      total = total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

      var filter1 = this.state.filter1;
      var filter2 = this.state.filter2;
      var filter3 = this.state.filter3;
      var filter4 = this.state.filter4;
      var filter5 = this.state.filter5;
      var filter6 = this.state.filter6;

      return(
         <div>
            <div className="row">
              <div className="row" id="page-wrapper-inner5">
                 <i className="fa fa-external-link export-move pull-left" onClick={() => this.exportToExcel()}></i>
              </div>
               <div className="col-md-12" id="custRequestListTable">
                  {preloader}
                  <BootstrapTable data={this.state.response} striped hover pagination>
                     <TableHeaderColumn dataField='request' isKey width="200px" filter={ { type: filter1 } }  dataSort={ true }>Item Out ID <i className="fa fa-search search-size" onMouseOver={() => this.filterShow1()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='date' width="100px" filter={ { type: filter2 } } dataSort={ true }>Date <i className="fa fa-search search-size" onMouseOver={() => this.filterShow2()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='name' width="300px" filter={ { type: filter3 } } dataSort={ true }>Item Name <i className="fa fa-search search-size" onMouseOver={() => this.filterShow3()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='qty' width="100px" filter={ { type: filter4 } } dataSort={ true }>Qty <i className="fa fa-search search-size" onMouseOver={() => this.filterShow4()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='unitPrice' width="200px" filter={ { type: filter5 } } dataSort={ true } dataFormat={ this.amountFormatter }>Unit Cost <i className="fa fa-search search-size" onMouseOver={() => this.filterShow5()}></i></TableHeaderColumn>
                     <TableHeaderColumn dataField='total' width="200px" filter={ { type: filter6 } } dataSort={ true } dataFormat={ this.amountFormatter }>Total <i className="fa fa-search search-size" onMouseOver={() => this.filterShow6()}></i></TableHeaderColumn>
                  </BootstrapTable>
               </div>
            </div>
            <div>
               <div className="col-md-1 col-md-offset-6">
                      Total
                  </div>
                  <div className="col-md-1 summary-qty">
                      {this.state.totalItem}
                  </div>
                  <div className="col-md-1 cust-amount">
                      {total}
                  </div>
                  <br />
            </div>
         </div>
      );
    }
}

export default CustRequestListTable;
