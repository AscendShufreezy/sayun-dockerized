import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';
import moment from 'moment';import Input from '../Utils/Input.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class EditTempDeductionForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            category: props.tempDeduction.category,
            description: props.tempDeduction.description,
            date: moment(props.tempDeduction.date, "MM/DD/YYYY"),
            total: props.tempDeduction.total,
            disable: "",
            additions: []
        };

        this.handleCategory = this.handleCategory.bind(this);
        this.handleDescription = this.handleDescription.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleTotal = this.handleTotal.bind(this);
    }

    handleDescription(e) {
        this.setState({
            description: e.target.value
        });
    }

    handleCategory(e) {
        this.setState({
            category: e.target.value
        });
    }

    handleTotal(e) {
        this.setState({
            total: e.target.value
        });
    }

    handleChange(date) {
        this.setState({
            date: date
        });
    }

    componentWillMount(){
        var tempDeduction = this.props.tempDeduction;
        if(tempDeduction.category == "canteen"){
            this.setState({disable: "disabled"})
        }

        var self = this;
        $.ajax({
            url: Utils.basePath + "/categories/",
            type:"GET",
            contentType: "application/json",
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
                const additions = [];
                for(var i=0;i<data.length;i++){
                    if(data[i].type == "Temporary Deduction"){
                        additions.push(data[i]);
                    }
                }
                self.setState({additions: additions});
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    render(){
        const additionsResult = this.state.additions.map((additions) =>
            <option value={additions.id} key={additions.id}>{additions.description}</option>
        );

        var tempDeduction = this.props.tempDeduction;
        var footer = "";
        var viewPos = "";

        if(this.state.disable != ""){
            footer = "Go to Canteen Purchase record:";
        }

        if(tempDeduction.category == "loan"){
            var link = "/request/id=" + tempDeduction.loanId;
            viewPos = <div><br/>Go to POS Request record: <Link to={link}><u>{tempDeduction.loanId}</u></Link></div>
           //var link = "/request/id=OUT-0001";
            //viewPos = <div><br/>Go to POS Request record: <Link to={link}><u>OUT-0001</u></Link></div>
        }
        

        return (
            <div>
                <div>
                    <PageTitle pageTitle="Temporary Deduction"/>
                </div>
                <div>
                    <div className="row" id="page-wrapper-inner3">
                        <div className="row">
                            <div className="col-md-2 form-row-padding-top">
                                Category*
                            </div>
                            <div className="col-md-3">
                                <select className="form-control" id="category" disabled={this.state.disable} value={this.state.category} onChange={this.handleCategory}>
                                    <option selected disabled> </option>
                                    {additionsResult}
                                </select>
                            </div>
                            <div className="col-md-2 col-md-offset-2 form-row-padding-top">
                                Date Entered*
                            </div>
                            <div className="col-md-3">
                                <DatePicker selected={this.state.date} onChange={this.handleChange} disabled={this.state.disable} id="date" className="form-control"/>
                                <i className="fa fa-calendar"></i>
                            </div>
                        </div>
                        <div className="row form-row-padding-top">
                            <div className="col-md-2 form-row-padding-top">
                                Description*
                            </div>
                            <div className="col-md-3">
                                <input type="text" className="form-control" id="description" disabled={this.state.disable} value={this.state.description} onChange={this.handleDescription}/>
                            </div>
                            <div className="col-md-2 col-md-offset-2 form-row-padding-top">
                                Total Amount*
                            </div>
                            <div className="col-md-3">
                                <input type="text" className="form-control" id="total" disabled={this.state.disable} value={this.state.total} onChange={this.handleTotal}/>
                            </div>
                        </div>
                    </div>
                    {viewPos}
                    <br />
                    {footer}
                </div>
            </div>
        );
    }
}   

export default EditTempDeductionForm;