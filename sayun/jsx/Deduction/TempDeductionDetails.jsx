import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import Breadcrumb from '../Utils/Breadcrumb.jsx';
import EmployeeDetailsLong from '../Employee/EmployeeDetailsLong.jsx';
import EditTempDeductionForm from './EditTempDeductionForm.jsx';
import PayrollScheduleListPage from '../PayrollSchedule/PayrollScheduleListPage.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';;

class TempDeductionDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      response: [],
      transfer: false,
      isComplete: false,
      emptyField: "",
      tempDeduction: {},
      logout: false,
      invalidField: ""
   };
}

  cancelSave() {
    this.setState({transfer: true});
  }

  checkInputs(empId) {
    if(document.getElementById("category").value == ""){
        this.setState({emptyField: "Category"}, () => { 
            alert(this.state.emptyField + " should not be empty.");
            return "";
        });
    } else if(document.getElementById("date").value == ""){
        this.setState({emptyField: "Date"}, () => { 
            alert(this.state.emptyField + " should not be empty.");
            return "";
        });
    } else if(document.getElementById("description").value == ""){
        this.setState({emptyField: "Description"}, () => { 
            alert(this.state.emptyField + " should not be empty.");
            return "";
        });
    } else if(document.getElementById("total").value == ""){
        this.setState({emptyField: "Total Amount"}, () => { 
            alert(this.state.emptyField + " should not be empty.");
            return "";
        });
    } else if(isNaN(document.getElementById("total").value)){
        this.setState({invalidField: "Total"}, () => { 
            alert(this.state.invalidField + " should be a valid number.");
            return "";
        });
    } else{
      this.setState({isComplete: true}, () => { 
        this.updateTempDeduction(empId);
     });
    }  
  }  

  updateTempDeduction(empId) {
    var url = window.location.href;
    var tempDeductionId = url.split('=').pop();

    var tempDeduction = {};
    tempDeduction.category = document.getElementById("category").value;
    tempDeduction.date = document.getElementById("date").value;
    tempDeduction.description = document.getElementById("description").value;
    tempDeduction.total = (document.getElementById("total").value);
    var data = {"employee": empId, "loan_id": "", "category": tempDeduction.category,"description": tempDeduction.description, "date": tempDeduction.date,
                "total": tempDeduction.total, "processed": 0.00, "pending": tempDeduction.total} //to be updated when payroll schedule is done
    
    var self = this;
    $.ajax({
        url: Utils.basePath + "/temporary-deductions/" + tempDeductionId + "/",
        type: "PUT",
        contentType: "application/json",
        data: JSON.stringify(data),
        headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
        statusCode: {
           400: function (response) {
              alert(JSON.parse(response.responseText));
           },
           401: function (response) {
              alert("User is not authorized. Please login.");
              self.setState({logout: true});
           },
           500: function (response) {
              alert("HTTP error 500, please check your connection.");
           }
        },
        success: function (data) {
            alert("Temproary Deduction updated!");
            self.setState({transfer: true});
        },
        error: function (response) {
            console.log(response);
        }
    });
  }

  componentWillMount(){
    var url = window.location.href;
    var id = url.split('=').pop();
    var self = this;
    var tempDeduction = {};

    $.ajax({
      url: Utils.basePath + "/temporary-deductions/" + id + "/",
      type:"GET",
      contentType: "application/json",
      async : false,
      headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
      statusCode: {
         400: function (response) {
            alert(JSON.parse(response.responseText).id);
         },
         401: function (response) {
            alert("User is not authorized. Please login.");
            self.setState({logout: true});
         },
         500: function (response) {
            alert("HTTP error 500, please check your connection.");
         }
      },
      success: function (data) {
        tempDeduction.id = data.id;
        tempDeduction.category = data.category;
        tempDeduction.date = data.date;
        tempDeduction.description = data.description;
        tempDeduction.total = data.total;
        tempDeduction.loanId = data.loan_id;
      }
    });

    this.setState({tempDeduction: tempDeduction})
  }

  render(){
    if (this.state.logout) {
             return <Redirect push to="/" />;   
    }

    var empId = localStorage.getItem("empId");
    var link = "/employee/details/id=" + empId;
    if (this.state.transfer) {
      return <Redirect push to={link} />;
    }
  
    return(
      <div id="page-wrapper">
        <Menu active="Employee"/>
        <div className="row" id="page-wrapper-inner">
          <Breadcrumb page="edit-tempdeduction" id={empId}/>
          <PageTitle pageTitle="Employee Details"/>
            <div className="row" id="page-wrapper-inner3">
              <EmployeeDetailsLong empId={empId}/>
              <hr />
              <form>
                <EditTempDeductionForm tempDeduction={this.state.tempDeduction}/>
                <div className="row">
                  <div className="col-md-3 pull-right">
                    <div className="pull-right">
                        <button type="button" className="btn btn-default btn-primary" onClick={() => this.checkInputs(empId)}>Save</button>
                        <button type="button" className="btn btn-default btn-warning" onClick={() => this.cancelSave()}>Cancel</button>
                    </div>
                  </div>
                </div>
              </form>
              <PayrollScheduleListPage empId={empId}/>
            </div>
          </div>
          <br />  
        </div>
      );
  } 
}

export default TempDeductionDetails;

