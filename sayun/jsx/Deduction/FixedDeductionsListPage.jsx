import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import FixedDeductionsListTable from './FixedDeductionsListTable.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class FixedDeductionsListPage extends React.Component {
	constructor(){
		super();
		this.state = {
			logout: false
		}
	}
	deleteFixedDeduction(){
	  	var numberOfFixedDeductions = document.getElementsByName("checkbox2");
	  	var fixedDeductionsToDelete = [];

	  	for(var i=0; i<numberOfFixedDeductions.length;i++){
	  		if(numberOfFixedDeductions[i].checked){
	  			numberOfFixedDeductions[i].id = numberOfFixedDeductions[i].id.replace('fixed', '');
	  			fixedDeductionsToDelete.push(numberOfFixedDeductions[i].id);
	  		}
	  	}

	  	if(fixedDeductionsToDelete.length > 0){
	  		var deleteMessage = confirm("Are you sure you want to delete the selected fixed deduction/s?");
	  		if (deleteMessage == true) {
	  			var successCounter = 0;
	  			for(var i=0; i<fixedDeductionsToDelete.length;i++){
	  				$.ajax({
			            url: Utils.basePath + "/fixed-deductions/" + fixedDeductionsToDelete[i] + "/",
			            type:"DELETE",
			            contentType: "application/json",
			            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
			            statusCode: {
			               400: function (response) {
			                  alert(JSON.parse(response.responseText).id);
			               },
			               401: function (response) {
			                  alert("User is not authorized. Please login.");
			                  self.setState({logout: true});
			               },
			               500: function (response) {
			                  alert("HTTP error 500, please check your connection.");
			               }
			            },
			            success: function (data) {
			            	successCounter++;

			            	if(successCounter == fixedDeductionsToDelete.length){
				  				alert("Deletion of fixed deduction/s is successful!");
				  				location.reload();
				  			}
			            },
			            error: function (response) {
			                alert("Failed !!!");
			            }
			        });
	  			}
		    }
	  	} else{
	  		alert('There are no fixed deduction/s selected.');
	  	}
    }

    render(){
    	if (this.state.logout) {
             return <Redirect push to="/" />;
        }

      	return (
	      	<div>
      			<PageTitle pageTitle="Fixed Deductions"/>
      			<Pagination />
      			<br />
      			<form>
	      			<FixedDeductionsListTable />
	      			<ButtonGroup buttons="addDelete" link1="/fixed-deduction/add-fixed-deduction" click2={() => this.deleteFixedDeduction()} type1="button"/>
      			</form>
      			<br />
		    </div>
     	);
   }
}

export default FixedDeductionsListPage;

