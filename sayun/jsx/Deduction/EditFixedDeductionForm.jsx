import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Match, Miss } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import Input from '../Utils/Input.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';
import '../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.css';

class EditFixedDeductionForm extends React.Component {
    constructor(props){
        super(props);
        var result = [];
        var type = "";
        var selected = "";

        var schedule = props.fixedDeduction.schedule;
        if(schedule.includes('Monthly')){
            schedule = schedule.replace('Monthly (', '');
            type = "Monthly";
            selected = schedule.replace(')', '');
            result = ["First Week", "Second Week", "Third Week", "Fourth Week"];
        } else if(schedule.includes('Weekly')){
            schedule = schedule.replace('Weekly (', '');
            type = "Weekly";
            selected = schedule.replace(')', '');
            result = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        }

        this.state = {
            category: props.fixedDeduction.category,
            description: props.fixedDeduction.description,
            status: props.fixedDeduction.status,
            type: type,
            selected: selected,
            amount: props.fixedDeduction.amount,
            activeChecked: "",
            inactiveChecked: "",
            optionResult: result,
            additions: []            
        };

        this.handleCategory = this.handleCategory.bind(this);
        this.handleDescription = this.handleDescription.bind(this);
        this.handleStatus = this.handleStatus.bind(this);
        this.handleType= this.handleType.bind(this);
        this.handleSelected= this.handleSelected.bind(this);
        this.handleAmount = this.handleAmount.bind(this);
    }

    handleCategory(e) {
        this.setState({
            category: e.target.value
        });
    }

    handleSelected(e) {
        this.setState({
            selected: e.target.value
        });
    }
    
    
    handleDescription(e) {
        this.setState({
            description: e.target.value
        });
    }

    handleType(e) {
        var value = e.target.value;
        this.setState({type: value});

        if(value == "Monthly"){
            this.setState({optionResult: ["First Week", "Second Week", "Third Week", "Fourth Week"]});
        } else{
            this.setState({optionResult: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]});
        }
    }

    handleAmount(e) {
        this.setState({
            amount: e.target.value
        });
    }    

    handleStatus(e) {
        var status = e.target.value;
        var activeChecked = "";
        var inactiveChecked = "";

        if(status == "Active"){
            this.setState({activeChecked: "checked"});
            this.setState({inactiveChecked:""});
            this.setState({status:"Active"});

        } else{
            this.setState({activeChecked: ""});
            this.setState({inactiveChecked: "checked"});
            this.setState({status: "Inactive"});
        }
    }

    componentWillMount(){
        var status = this.state.status;
        var activeChecked = "";
        var inactiveChecked = "";

        if(status == "Active"){
            this.setState({activeChecked: "checked"});
            this.setState({inactiveChecked:""});
            this.setState({status:"Active"});

        } else{
            this.setState({activeChecked: ""});
            this.setState({inactiveChecked: "checked"});
            this.setState({status: "Inactive"});
        }

        var self = this;
        $.ajax({
            url: Utils.basePath + "/categories/",
            type:"GET",
            contentType: "application/json",
            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
            statusCode: {
               400: function (response) {
                  alert(JSON.parse(response.responseText).id);
               },
               401: function (response) {
                  alert("User is not authorized. Please login.");
                  self.setState({logout: true});
               },
               500: function (response) {
                  alert("HTTP error 500, please check your connection.");
               }
            },
            success: function(data) {
                const additions = [];
                for(var i=0;i<data.length;i++){
                    if(data[i].type == "Fixed Deduction"){
                        additions.push(data[i]);
                    }
                }
                self.setState({additions: additions});
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    render(){
        var fixedDeduction = this.props.fixedDeduction;
        const optionResult = this.state.optionResult.map((option) =>
            <option key={option} value={option}>{option}</option>
        ); 

        const additionsResult = this.state.additions.map((additions) =>
            <option value={additions.id} key={additions.id}>{additions.description}</option>
        );
        return (
            <div>
                <div>
                    <PageTitle pageTitle="Fixed Deduction"/>
                </div>
                <div>   
                    <div className="row" id="page-wrapper-inner3">
                        <div className="row">
                            <div className="col-md-2">
                                Category*
                            </div>
                            <div className="col-md-3">
                                <select className="form-control" id="category" value={this.state.category} onChange={this.handleCategory}>
                                    <option selected disabled> </option>
                                    {additionsResult}
                                </select>
                            </div>
                            <div className="col-md-1 col-md-offset-2">
                                Status
                            </div>
                            <div className="col-md-3">
                                <input type="radio" name="status" value="Active" id="btnActive" checked={this.state.activeChecked} onChange={this.handleStatus} /><a className="radio-text">Active</a> &nbsp;&nbsp;
                                <input type="radio" name="status" value="Inactive" id="btnInactive" checked={this.state.inactiveChecked} onChange={this.handleStatus} /><a className="radio-text">Inactive</a>
                            </div>
                        </div>
                        <div className="row form-row-padding-top">
                            <div className="col-md-2">
                                Description*
                            </div>
                            <div className="col-md-4">
                                <input type="text" className="form-control" id="description" value={this.state.description} onChange={this.handleDescription}/>
                            </div>
                        </div>
                        <div className="row form-row-padding-top">
                            <div className="col-md-2">
                                Frequency*
                            </div>
                            <div className="col-conf-3">
                                <select className="form-control" id="type" value={this.state.type} onChange={this.handleType}>
                                    <option value="Monthly">Monthly</option>
                                    <option value="Weekly">Weekly</option>
                                </select>
                            </div>
                            <div className="col-conf-3">
                                <select className="form-control" id="selected" value={this.state.selected} onChange={this.handleSelected}>
                                    {optionResult}
                                </select>
                            </div>
                            <div className="col-md-1 col-md-offset-2">
                                Amount*
                            </div>
                            <div className="col-md-3">
                                <input type="text" className="form-control" id="amount" value={this.state.amount} onChange={this.handleAmount}/>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
        );
    }
}

export default EditFixedDeductionForm;