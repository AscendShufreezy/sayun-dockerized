import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Redirect } from 'react-router-dom';

import Menu from '../Utils/Menu.jsx';
import PageTitle from '../Utils/PageTitle.jsx';
import Pagination from '../Utils/Pagination.jsx';
import ButtonGroup from '../Utils/ButtonGroup.jsx';
import DeductionsListTable from './DeductionsListTable.jsx';
import Utils from '../Utils/Utils.jsx';

import '../../vendor/bootstrap/css/bootstrap.min.css';
import '../../vendor/metisMenu/metisMenu.min.css';
import '../../dist/css/sb-admin-2.css';
import '../../dist/css/index.css';
import '../../vendor/morrisjs/morris.css';
import '../../vendor/font-awesome/css/font-awesome.min.css';
import '../../img/font-awesome/css/font-awesome.min.css';

class DeductionsListPage extends React.Component {
	constructor(){
		super();
		this.state = {
			logout: false
		}
	}
	deleteTempDeduction(){
	  	var numberOfTempDeductions = document.getElementsByName("checkbox");
	  	var tempDeductionsToDelete = [];

	  	for(var i=0; i<numberOfTempDeductions.length;i++){
	  		if(numberOfTempDeductions[i].checked){
	  			tempDeductionsToDelete.push(numberOfTempDeductions[i].id);
	  		}
	  	}

	  	if(tempDeductionsToDelete.length > 0){
	  		var deleteMessage = confirm("Are you sure you want to delete the selected temporary deduction/s?");
	  		if (deleteMessage == true) {
	  			var successCounter = 0;
	  			var failCounter = 0;
	  			for(var i=0; i<tempDeductionsToDelete.length;i++){
	  				$.ajax({
			            url: Utils.basePath + "/temporary-deductions/" + tempDeductionsToDelete[i] + "/",
			            type:"DELETE",
			            async: false,
			            contentType: "application/json",
			            headers: {"Authorization": "Token " + sessionStorage.getItem('token')},
			            statusCode: {
			               400: function (response) {
			                  alert(JSON.parse(response.responseText).id);
			               },
			               401: function (response) {
			                  alert("User is not authorized. Please login.");
			                  self.setState({logout: true});
			               },
			               500: function (response) {
			                  alert("HTTP error 500, please check your connection.");
			               }
			            },
			            success: function (data) {
			            	successCounter++;
			            },
			            error: function (response) {
			            	failCounter++;
			            }
			        });

	            	if(successCounter == tempDeductionsToDelete.length){
		  				alert("Deletion of temporary deduction/s is successful!");
		  				location.reload();
		  			} else if(failCounter == tempDeductionsToDelete.length){
	                	alert("Temporary Deductions with processed payroll schedules cannot be deleted!");
	                } else if((successCounter + failCounter) == tempDeductionsToDelete.length){
	                	alert("Temporary Deductions with processed payroll schedules cannot be deleted. Only selected temporary deductions with processed payroll schedules are deleted.");
	                	location.reload();
	                }
	  			}
		    }
	  	} else{
	  		alert('There are no temporary deduction/s selected.');
	  	}
    }

    render(){
    	if (this.state.logout) {
             return <Redirect push to="/" />;
         }

      	return (
	      	<div>
      			<PageTitle pageTitle="Temporary Deductions"/>
      			<br />
      			<form>
	      			<DeductionsListTable />
	      			<ButtonGroup buttons="addDelete" link1="/temp-deduction/add-temp-deduction" click2={() => this.deleteTempDeduction()} type1="button"/>
      			</form>
      			<br />
		    </div>
     	);
   }
}

export default DeductionsListPage;

