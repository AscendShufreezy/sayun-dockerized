# Prerequisites

1. Docker (click [here](https://docs.docker.com/engine/installation/) for instructions)
2. NodeJS (click [here](https://nodejs.org/en/download/) for instructions)
3. <Check if git is installed by using --git version> 
	If there is no git installed,
		Git (click [here](https://git-scm.com/) for instructions)
4. Working internet connection
5. Create a folder named 'sayun-project' in your local drive.

# How to Build

Navigate to the 'sayun-project' folder.

1. Get a copy of the source codes 
	by opening a terminal (or cmd) then doing this command:
   
   `git clone git@bitbucket.org:nerd-mechatron/sayun-dockerized.git`

2. Open a terminal and go to `sayun` folder by doing this command:

	`cd sayun-dockerized/sayun`

3. Download all the packages needed to run the client-side codes by doing this command:

	`npm install`

4. Go back once the parent directory by doing this command:

	`cd ..`

5. Build a docker image and run it.

	**Warning: This will stop and delete all of the containers you are currently running.**

	`docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q) && docker-compose build && docker-compose up -d`
    
6. Verify server was normally started. There should be 3 running containers with names `sayun-nginx`, `sayun-django-app`, and `sayun-postgres`. All of these containers should have an "up" status. After executing the second command, the last output should be something like `Starting gunicorn...Listening at: http://0.0.0.0:8000`

	`docker ps -a`
  
    `docker-compose logs web`
    
# FAQ

1. Q: One of the containers already exited. 
	
    A: Execute `docker-compose start` and run `docker ps -a` again to verify.

# Importing items and employees

1. Modify *fixtures/employees.json* or *fixtures/items.json* as you see fit. 
    
    *Note: File names should not be changed. These files should not be moved elsewhere.*

2. Make sure all the docker containers are up by executing `docker ps -a`. Run `docker-compose start` if not all of them are up.
3. Run a shell session inside the _sayun-django-app_ container by executing `docker exec -ti sayun-django-app bash`. The current line of your terminal should now be something like `root@477cf9842c19:/src#`.
4. To import the items, run `python import.py fixtures/items.py`. To import the employees, run `python import.py fixtures/employees.py`.
