#!/usr/bin/env python
import os
import django
import sys
import json
import datetime


def import_employees(employees):
    # loop all employees then start adding them to db
    try:
        with transaction.atomic():
            total_count = 0

            for employee in employees:
                if employee['type'] not in [x[0] for x in Employee.TYPE]:
                    raise AttributeError('[ERROR] Invalid type "%s"' % employee['type'])

                if employee['gender'] not in [x[0] for x in Employee.GENDER]:
                    raise AttributeError('[ERROR] Invalid gender "%s"' % employee['gender'])

                if employee['status'] not in [x[0] for x in Employee.STATUS]:
                    raise AttributeError('[ERROR] Invalid status "%s"' % employee['status'])

                username = employee['id']
                email = '%s@sayun.com' % username
                password = 'sayun'
                user = User.objects.create_user(username, email, password, **{
                    'first_name': employee['first_name'],
                    'last_name': employee['last_name'],
                })

                emp = Employee.objects.create(**{
                    'user': user,
                    'id': employee['id'],
                    'middle_name': employee['middle_name'],
                    'birthdate': datetime.datetime.strptime(employee['birthdate'], '%Y-%m-%d'),
                    'gender': employee['gender'],
                    'type': employee['type'],
                    'barcode': employee['id'],
                    'title': employee['title'],
                    'date_hired': datetime.datetime.strptime(employee['date_hired'], '%Y-%m-%d'),
                    'status': 'Active',
                    'remarks': employee['remarks']
                })

                # create customer object for this employee
                customer = Customer.objects.create(**{
                    'id': employee['id'],
                    'name': '%s, %s %s' % (employee['last_name'], employee['first_name'], employee['middle_name']),
                    'type': 'Employee',
                    'barcode': employee['id'],
                })

                # bind customer object to this employee
                emp.customer = customer
                emp.save()

                SalaryInfo.objects.create(**{
                    'employee': emp,
                    'bank_account': employee['id'],
                    'hourly_rate': 0
                })


                total_count += 1

            print('Successfully imported %d employee(s).' % total_count)
    except ValueError:
        transaction.rollback()
        print('[ERROR] Make sure dates are in the form of {year}-{month}-{date}. Ex: 2017-06-23.')
    except IntegrityError as e:
        transaction.rollback()
        print('[ERROR] Make sure ID is unique across all employees.')
        print(e)
    except AttributeError as e:
        print(e)


def import_items(items):
    try:
        with transaction.atomic():
            [Item.objects.create(**item) for item in items]
            print('Successfully imported %d item(s).' % len(items))
    except IntegrityError:
        transaction.rollback()
        print('[ERROR] Make sure ID is unique across all items.')

def import_items_for_bodega(items):
    try:
        with transaction.atomic():
            #IMPORTANT SET THIS TO THE ID OF THE MAIN WAREHOUSE
            id_of_warehouse = 'WH-0004'
            warehouse = Warehouse.objects.get(pk=id_of_warehouse)

            for item in items:

                id = item['id']
                name = item['name']
                description = item['description']
                unit_price = item['unit_price']
                unit = item['unit']
                barcode = item['barcode']
                initial_qty = item['qty']
                
                newitem = Item.objects.create(**{
                    'id': id,
                    'name': name,
                    'unit': unit,
                    'description': description,
                    'unit_price': unit_price,
                    'barcode': barcode
                })

                warehouseitem = WarehouseItems.objects.create(**{
                    'warehouse': warehouse,
                    'item': newitem,
                    'stock': initial_qty
                })

            print('Successfully imported %d item(s).' % len(items))
    except IntegrityError:
        transaction.rollback()
        print('[ERROR] Make sure ID is unique across all items.')

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sayun.settings")

    # Configure settings
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sayun.settings")

    # Setup Django
    django.setup()

    from django.contrib.auth.models import User
    from django.db import IntegrityError, transaction

    from app.models import Employee, Item, SalaryInfo, Customer, Warehouse, WarehouseItems

    # verify a json file was given
    if len(sys.argv) is not 2:
        print('[ERROR] Expecting one argument to be the input JSON file that contains the employee list.')
        sys.exit(1)

    # verify text contains contains a valid JSON format
    try:
        with open(sys.argv[1], 'r') as input_file:
            array = json.load(input_file)
    except json.JSONDecodeError:
        print('[ERROR] Input JSON file has invalid JSON format')
        sys.exit(1)

    # determine whether to import items or employees
    if sys.argv[1].lower().endswith('employees.json'):
        import_employees(array)
    elif sys.argv[1].lower().endswith('items.json'):
        import_items(array)
    elif sys.argv[1].lower().endswith('items_for_bodega.json'):
        import_items_for_bodega(array)
    else:
        print('[ERROR] Filenames should either be "employees.json" or "items.json" or "items_for_bodega.json"')
        sys.exit(1)
