#!/usr/bin/env python
import os
import django

DEFAULT_CATEGORIES = {
    'Temporary Addition': [
        {
            'id': 'basicpertrip',
            'description': 'Basic Pay - Per Trip',
        },
        {
            'id': 'unpaid',
            'description': 'Unpaid Excess By',
        },
        {
            'id': 'allowance',
            'description': 'Allowance',
        },
        {
            'id': 'overtime',
            'description': 'Overtime',
        },
    ],
    'Temporary Deduction': [
        {
            'id': 'paidallowance',
            'description': 'Paid Allowance',
        },
        {
            'id': 'canteen',
            'description': 'Canteen',
        },
        {
            'id': 'loan',
            'description': 'Loan',
        },
        {
            'id': 'load',
            'description': 'Load',
        },
        {
            'id': 'rice',
            'description': 'Rice',
        },
        {
            'id': 'cash',
            'description': 'Cash Allowance',
        },
        {
            'id': 'housing',
            'description': 'Housing Materials',
        },
        {
            'id': 'others',
            'description': 'Negative & Others',
        },
    ],
    'Fixed Addition': [
        {
            'id': 'basicsalary',
            'description': 'Basic Salary',
        },
    ],
    'Fixed Deduction': [
        {
            'id': 'ssscontrib',
            'description': 'SSS Contribution',
        },
        {
            'id': 'sssloan',
            'description': 'SSS Loan',
        },
        {
            'id': 'phiccontrib',
            'description': 'PHIC Contribution',
        },
        {
            'id': 'hdmfcontrib',
            'description': 'HDMF Contribution',
        },
        {
            'id': 'hdmfloan',
            'description': 'HDMF Loan',
        },
        {
            'id': 'tax',
            'description': 'Withholding Tax',
        },
    ]
}

ADDITIONAL_DEFAULT_PERMISSIONS = [
    {
        'model': 'report',
        'name': 'Can view report',
        'codename': 'view_report',
    }
]

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sayun.settings")

    # Configure settings
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sayun.settings")

    # Setup Django
    django.setup()

    from django.contrib.auth.models import User, Group, Permission
    from django.contrib.contenttypes.models import ContentType
    from django.db import IntegrityError

    from app.models import Category, UserMeta

    try:
        # create default user
        user = User.objects.create_superuser('admin', 'admin@sayun.com', 'admin', first_name='Administrator')
        user_meta = UserMeta.objects.create(**{
            'user': user,
            'password': 'admin',
        })

        # create default categories
        for type, categories in DEFAULT_CATEGORIES.items():
            for category in categories:
                # append type to dict key
                category['type'] = type

                # create category
                Category.objects.create(**category)

        # create additional default permissions
        for additional_permission in ADDITIONAL_DEFAULT_PERMISSIONS:
            # create permission if it does not exist yet
            if not Permission.objects.filter(codename=additional_permission['codename']).exists():
                # create or get the needed content type
                content_type, _ = ContentType.objects.get_or_create(model=additional_permission['model'])

                # create the permission object
                Permission.objects.create(**{
                    'content_type': content_type,
                    'name': additional_permission['name'],
                    'codename': additional_permission['codename']
                })

        # create default group
        group = Group.objects.create(name='admin-group')
        group.permissions.add(*list(Permission.objects.all()))
        group.user_set.add(user)
        group.save()
    except IntegrityError:
        # user 'admin' and 'group' already exists
        pass
