from django.conf.urls import url, include
from django.contrib import admin

from rest_framework.authtoken import views as drf_views
from rest_framework.routers import DefaultRouter

from app import views

# REST API router
router = DefaultRouter()

# Human Resources Information System
router.register(r'employees', views.EmployeeViewSet)
router.register(r'attendance', views.AttendanceViewSet, base_name='attendance')
router.register(r'overtimes', views.OvertimeViewSet, base_name='overtimes')
router.register(r'payroll', views.PayrollViewSet)
router.register(r'payroll-schedules', views.PayrollScheduleViewSet)
router.register(r'payroll-calendars', views.PayrollCalendarViewSet)
router.register(r'salary-infos', views.SalaryInfoViewSet)
router.register(r'fixed-additions', views.FixedAdditionViewSet)
router.register(r'fixed-deductions', views.FixedDeductionViewSet)
router.register(r'temporary-additions', views.TemporaryAdditionViewSet)
router.register(r'temporary-deductions', views.TemporaryDeductionViewSet)

# Inventory Information System
router.register(r'warehouses', views.WarehouseViewSet)
router.register(r'customers', views.CustomerViewSet)
router.register(r'items', views.ItemViewSet)
router.register(r'receipts', views.ReceiptViewSet)
router.register(r'item-receipts', views.ItemReceiptViewSet)
router.register(r'item-histories', views.ItemHistoryViewSet)
router.register(r'requests', views.RequestViewSet)
router.register(r'item-requests', views.ItemRequestViewSet)
router.register(r'warehouse-items', views.WarehouseItemsViewSet)

router.register(r'warehouse-users', views.WarehouseUserViewSet)
router.register(r'warehouse-purchasers', views.WarehousePurchaserViewSet)
router.register(r'warehouse-approvers', views.WarehouseApproverViewSet)
router.register(r'warehouse-requesters', views.WarehouseRequesterViewSet)


# Common Functions
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'permissions', views.PermissionViewSet)
router.register(r'content-types', views.ContentTypeViewSet)
router.register(r'categories', views.CategoryViewSet)

urlpatterns = [
    url(r'^api/admin/', admin.site.urls),
    url(r'^api/login/', drf_views.obtain_auth_token),
    url(r'^api/change-password/', views.ChangePassword.as_view()),
    url(r'^api/export-hrr01/', views.ExportHRR01.as_view()),
    url(r'^api/export-hrr02/', views.ExportHRR02.as_view()),
    url(r'^api/', include(router.urls)),
]
