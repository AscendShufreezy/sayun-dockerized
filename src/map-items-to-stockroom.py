#!/usr/bin/env python
import os
import django
import sys
import json
import datetime


def update_items_to_warehouse():
    # query all items
    try:
        with transaction.atomic():

            stockroom_warehouse = Warehouse.objects.get(id='WH-0005')
            canteen_warehouse = Warehouse.objects.get(id='WH-0001')

            all_items = WarehouseItems.objects.filter(warehouse__id=canteen_warehouse.id)

            total_count = 0

            for item in all_items:
                print('item = %s' % item.item.id)
                wi = WarehouseItems.objects.create(**{
                    'item': item.item,
                    'warehouse': stockroom_warehouse,
                    'stock': 0,
                })
                total_count += 1

            print('Successfully updayed %d items(s).' % total_count)
    except ValueError:
        transaction.rollback()
        print('[ERROR] Make sure dates are in the form of {year}-{month}-{date}. Ex: 2017-06-23.')
    except IntegrityError as e:
        transaction.rollback()
        print('[ERROR] Make sure ID is unique across all warehouse items.')
        print(e)
    except AttributeError as e:
        print(e)

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sayun.settings")

    # Configure settings
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sayun.settings")

    # Setup Django
    django.setup()

    from django.contrib.auth.models import User
    from django.db import IntegrityError, transaction

    from app.models import Item, WarehouseItems, Warehouse

    update_items_to_warehouse()
