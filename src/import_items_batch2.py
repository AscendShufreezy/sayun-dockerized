#!/usr/bin/env python
import os
import django
import sys
import json
import datetime


def import_items_for_bodega(items):
    try:
        with transaction.atomic():
            #IMPORTANT!!!
            # Step 1 -  SET THIS TO THE ID OF THE MAIN WAREHOUSE
            id_of_warehouse = 'WH-0003'
            # Step 2 -  SET THIS TO THE NEXT AVAILABLE ITEM ID SEQUENCE
            id_sequence_item_id = 1270
            # Step 3 -  SET THIS TO THE EMPLOYEE ID OF THE PURCHASER
            id_of_purchaser = '1000113'
            # Step 4 -  run using
            # python import_items_batch2.py fixtures/items_batch2_sep14.json
            
            warehouse = Warehouse.objects.get(pk=id_of_warehouse)
            purchaser = WarehousePurchaser.objects.get(purchaser_id=id_of_purchaser, warehouse__id=id_of_warehouse)
            item_id_prefix = 'ITEM-'

            #create item-in record
            receipt = Receipt.objects.create(**{
                'id': 'INITIALINVENTORY2',
                'warehouse': warehouse,
                'supplier': 'INITIAL INVENTORY DATA - BATCH 2',
                'date': datetime.date(2017, 9, 14),
                'purchaser':purchaser,
                'receipt_number': 'INITIAL INVENTORY DATA',
                'invoice_number': 'INITIAL INVENTORY DATA'
            })

            for item in items:

                id = item['id']
                name = item['name']
                description = item['description']
                unit_price = item['unit_price']
                unit = item['unit']
                barcode = item['barcode']
                initial_qty = item['qty']
                
                #create item
                newitem = Item.objects.create(**{
                    'id': item_id_prefix + str(id_sequence_item_id),
                    'name': name,
                    'unit': unit,
                    'description': description,
                    'unit_price': unit_price,
                    'barcode': barcode
                })

                #map item to main bodega
                warehouseitem = WarehouseItems.objects.create(**{
                    'warehouse': warehouse,
                    'item': newitem,
                    'stock': initial_qty
                })

                #create item inside the item in record above
                item_receipt = ItemReceipt.objects.create(**{
                    'receipt': receipt,
                    'item': newitem,
                    'unit_price': newitem.unit_price,
                    'quantity': item['qty'],
                    'total': item['unit_price'],
                    'date': receipt.date,
                })

                #create item history
                itemhistory = ItemHistory.objects.create(**{
                    'pseudo_id': item_receipt.receipt.id,
                    'item': item_receipt.item,
                    'receipt': item_receipt,
                    'date': item_receipt.receipt.date,
                    'transaction_type': 'Item In',
                    'quantity': item_receipt.quantity,
                    'unit_price': item_receipt.unit_price,
                    'total': item_receipt.total,
                })

                id_sequence_item_id += 1

            print('Successfully imported %d item(s).' % len(items))
    except IntegrityError:
        transaction.rollback()
        print('[ERROR] Make sure ID is unique across all items.')

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sayun.settings")

    # Configure settings
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sayun.settings")

    # Setup Django
    django.setup()

    from django.contrib.auth.models import User
    from django.db import IntegrityError, transaction

    from app.models import Item, Warehouse, WarehouseItems, Receipt, ItemReceipt, ItemHistory, WarehousePurchaser

    # verify a json file was given
    if len(sys.argv) is not 2:
        print('[ERROR] Expecting one argument to be the input JSON file that contains the employee list.')
        sys.exit(1)

    # verify text contains contains a valid JSON format
    try:
        with open(sys.argv[1], 'r') as input_file:
            array = json.load(input_file)
    except json.JSONDecodeError:
        print('[ERROR] Input JSON file has invalid JSON format')
        sys.exit(1)

    if sys.argv[1].lower().endswith('items_batch2_sep14.json'):
        import_items_for_bodega(array)
    else:
        print('[ERROR] Filename should be "items_batch2_sep14.json".')
        sys.exit(1)
