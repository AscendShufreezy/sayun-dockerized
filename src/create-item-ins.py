#!/usr/bin/env python
import os
import django
import sys
import json
import datetime

#VERY IMPORTANT!!!
# This script retrieves all the list of items in items_for_bodega.json
# and creates an Item In record called "INTIALINVENTORY" with all
# all items set to the initial quantity. It also creates an Item History
# record for each.
#
# MAKE SURE TO CORRECTLY SET THE id_of_warehouse and id_of_purchaser below.

def create_item_ins(items):
    try:
        with transaction.atomic():
            #IMPORTANT SET THIS TO THE ID OF THE MAIN WAREHOUSE
            id_of_warehouse = 'WH-0003'
            warehouse = Warehouse.objects.get(pk=id_of_warehouse)

            #IMPORTANT SET THIS TO THE ID OF DEFAULT PURCHASER
            #  currently defaulted to 1000113 ADOABO ROSALINDA
            id_of_purchaser = '1000113'
            purchaser = WarehousePurchaser.objects.get(purchaser_id=id_of_purchaser, warehouse__id=id_of_warehouse)
            
            #create receipt
            receipt = Receipt.objects.create(**{
                'id': 'INITIALINVENTORY',
                'warehouse': warehouse,
                'supplier': 'INITIAL INVENTORY DATA',
                'date': datetime.date(2017, 9, 2),
                'purchaser':purchaser,
                'receipt_number': 'INITIAL INVENTORY DATA',
                'invoice_number': 'INITIAL INVENTORY DATA'
            })

            sequence_no = 1

            for item in items:

                id = item['id']
                
                wh_item = Item.objects.get(pk=id)

                print('**** UPDATING %s' % id)

                #create receipt item
                item_receipt = ItemReceipt.objects.create(**{
                    'receipt': receipt,
                    'item': wh_item,
                    'unit_price': wh_item.unit_price,
                    'quantity': item['qty'],
                    'total': item['unit_price'],
                    'date': receipt.date,
                })

                #create item history
                itemhistory = ItemHistory.objects.create(**{
                    'pseudo_id': item_receipt.receipt.id,
                    'item': item_receipt.item,
                    'receipt': item_receipt,
                    'date': item_receipt.receipt.date,
                    'transaction_type': 'Item In',
                    'quantity': item_receipt.quantity,
                    'unit_price': item_receipt.unit_price,
                    'total': item_receipt.total,
                })

                sequence_no += 1

            print('Successfully imported %d item(s).' % len(items))
    except IntegrityError as e:
        transaction.rollback()
        print('[ERROR] Make sure ID is unique across all items. %s' % e.message)

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sayun.settings")

    # Configure settings
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sayun.settings")

    # Setup Django
    django.setup()

    from django.contrib.auth.models import User
    from django.db import IntegrityError, transaction, models

    from app.models import Item, Warehouse, WarehouseItems, Receipt, ItemReceipt, ItemHistory, WarehousePurchaser

    # verify a json file was given
    if len(sys.argv) is not 2:
        print('[ERROR] Expecting one argument to be the input JSON file that contains the employee list.')
        sys.exit(1)

    # verify text contains contains a valid JSON format
    try:
        with open(sys.argv[1], 'r') as input_file:
            array = json.load(input_file)
    except json.JSONDecodeError:
        print('[ERROR] Input JSON file has invalid JSON format')
        sys.exit(1)

    # determine whether to import items or employees
    if sys.argv[1].lower().endswith('items_for_bodega.json'):
        create_item_ins(array)
    else:
        print('[ERROR] Filenames should be "items_for_bodega.json"')
        sys.exit(1)
