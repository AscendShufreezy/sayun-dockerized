from datetime import date, timedelta
from decimal import Decimal

from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.db import transaction, IntegrityError
from django.db.models import Sum

from rest_framework import serializers
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.exceptions import ValidationError
from rest_framework.settings import api_settings

from app.models import Employee, Attendance, Overtime, Category, Warehouse, Customer, Item, PayrollSchedule, Receipt, \
    ItemReceipt, ItemHistory, Request, ItemRequest, TemporaryAddition, SalaryInfo, PayrollCalendar, FixedAddition, FixedDeduction, \
    TemporaryDeduction, Payroll, UserMeta, AvatarUpload, WarehouseItems,\
    WarehouseUser, WarehousePurchaser, WarehouseApprover, WarehouseRequester

EMPLOYEE_EMAIL_FORMAT = '%s@sayun.com'
EMPLOYEE_DEFAULT_PASSWORD = 'sayun'


class EmployeeSerializer(serializers.ModelSerializer):
    birthdate = serializers.DateField(input_formats=[api_settings.DATE_FORMAT])
    date_hired = serializers.DateField(input_formats=[api_settings.DATE_FORMAT])
    exit_date = serializers.DateField(input_formats=[api_settings.DATE_FORMAT], allow_null=True)
    years = serializers.SerializerMethodField()
    make_customer = serializers.BooleanField(write_only=True, required=False, default=False)

    class Meta:
        model = Employee
        fields = ('id', 'first_name', 'middle_name', 'last_name', 'barcode',
                  'birthdate', 'gender', 'type', 'title', 'date_hired', 'status',
                  'exit_date', 'remarks', 'image', 'years', 'barcode','make_customer')

    def create(self, validated_data):
        # do db stuff in a transaction
        with transaction.atomic():
            # create employee's user relation
            username = validated_data['id']
            email = EMPLOYEE_EMAIL_FORMAT % username
            user = User.objects.create_user(username, email, EMPLOYEE_DEFAULT_PASSWORD, **{
                'first_name': self.initial_data['first_name'],
                'last_name': self.initial_data['last_name'],
            })

            # save make_customer flag
            should_make_customer = validated_data['make_customer']
            del validated_data['make_customer']

            # create employee
            validated_data['user'] = user
            employee = Employee.objects.create(**validated_data)

            # create as customer, if necessary
            if should_make_customer:
                # create customer object for this employee
                customer = Customer.objects.create(**{
                    'id': employee.id,
                    'name': '%s, %s %s' % (user.last_name, user.first_name, employee.middle_name),
                    'type': 'Employee',
                    'barcode': employee.barcode,
                })

                # bind customer object to this employee
                employee.customer = customer
                employee.save()

            return employee

    def update(self, instance, validated_data):
        with transaction.atomic():
            instance.user.first_name = self.initial_data['first_name']
            instance.user.last_name = self.initial_data['last_name']
            instance.user.save()

            # update corresponding customer object if it exists
            if instance.customer:
                instance.customer.name = '%s, %s %s' % (instance.user.last_name, instance.user.first_name,
                                                        instance.middle_name)
                instance.customer.barcode = self.initial_data['barcode']
                instance.customer.save()

            if validated_data['make_customer'] and not instance.customer:
                # create customer object for this employee
                customer = Customer.objects.create(**{
                    'id': instance.id,
                    'name': '%s, %s %s' % (instance.user.last_name, instance.user.first_name, instance.middle_name),
                    'type': 'Employee',
                    'barcode': instance.barcode
                })

                # bind customer object to this employee
                instance.customer = customer
                instance.save()
            elif not validated_data['make_customer'] and instance.customer:
                instance.customer.delete()
                instance.customer = None

            return super(EmployeeSerializer, self).update(instance, validated_data)

    def get_years(self, obj):
        today = date.today()
        hired = obj.date_hired
        years = today.year - hired.year

        if years > 0:
            if hired.month > today.month or hired.month == today.month and hired.day > today.day:
                years -= 1

        return years


class PayrollScheduleSerializer(serializers.ModelSerializer):
    date = serializers.DateField(input_formats=[api_settings.DATE_FORMAT])
    payroll_date = serializers.DateField(input_formats=[api_settings.DATE_FORMAT])
    typeid = serializers.IntegerField(write_only=True)
    type_id = serializers.SerializerMethodField()

    class Meta:
        model = PayrollSchedule
        fields = 'id', 'employee', 'type', 'typeid', 'type_id', 'date', 'status', 'payroll_date', 'amount'

    def get_type_id(self, object):
        if object.type == 'addition':
            return object.addition.id

        if object.type == 'deduction':
            return object.deduction.id

        return 0

    def create(self, validated_data):
        kwargs = {}

        if validated_data['type'] == 'addition':
            try:
                kwargs['addition'] = TemporaryAddition.objects.get(pk=validated_data['typeid'])
            except TemporaryAddition.DoesNotExist:
                raise ValidationError("TemporaryAddition #%d was not found." % validated_data['typeid'])
        else:
            try:
                kwargs['deduction'] = TemporaryDeduction.objects.get(pk=validated_data['typeid'])
            except TemporaryDeduction.DoesNotExist:
                raise ValidationError("TemporaryDeduction #%d was not found." % validated_data['typeid'])

        kwargs['employee'] = validated_data['employee']
        kwargs['type'] = validated_data['type']
        kwargs['date'] = validated_data['date']
        kwargs['status'] = validated_data['status']
        kwargs['payroll_date'] = validated_data['payroll_date']
        kwargs['amount'] = validated_data['amount']

        return PayrollSchedule.objects.create(**kwargs)

    def update(self, instance, validated_data):
        with transaction.atomic():
            # perform default behavior of updating payroll schedule
            instance = super(PayrollScheduleSerializer, self).update(instance, validated_data)

            # clear current addition and/or deduction object
            instance.addition = None
            instance.deduction = None

            # update payroll schedule's addition/deduction object
            if validated_data['type'] == 'addition':
                instance.addition_id = validated_data['typeid']
            elif validated_data['type'] == 'deduction':
                instance.deduction_id = validated_data['typeid']

            # save addition/deduction modification
            instance.save()

            # refresh object from DB, to fill in addition/deduction correctly
            instance.refresh_from_db()

            return instance


class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = 'id', 'content_type', 'codename'


class ContentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContentType
        fields = 'id', 'model'


class GroupSerializer(serializers.ModelSerializer):
    permissions = PermissionSerializer(many=True, read_only=True)

    class Meta:
        model = Group
        fields = 'id', 'name', 'permissions'

    def create(self, validated_data):
        for p in self.initial_data['permissions']:
            if not Permission.objects.filter(pk=p).exists():
                raise ValidationError("Permission=%d does not exist" % p)

        group = Group.objects.create(name=validated_data['name'])
        [group.permissions.add(p) for p in self.initial_data['permissions']]
        group.save()

        return group

    def update(self, instance, validated_data):
        instance.name = validated_data['name']
        instance.permissions.clear()
        [instance.permissions.add(p) for p in self.initial_data['permissions']]
        instance.save()

        return instance

class UserSerializer(serializers.ModelSerializer):
    name = serializers.StringRelatedField(source='first_name')
    admin = serializers.BooleanField(source='is_staff')
    groups = GroupSerializer(many=True, read_only=True)
    defaultWarehouse = serializers.StringRelatedField(source='meta.defaultWarehouse.id')
    password = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'name', 'username', 'password', 'admin', 'groups', 'defaultWarehouse')

    def get_password(self, user):
        try:
            return user.meta.password
        except UserMeta.DoesNotExist:
            return None

    def create(self, validated_data):
        # if groups was given, check each item if it exists in DB
        if self.initial_data['groups']:
            for g in self.initial_data['groups']:
                if not Group.objects.filter(pk=g).exists():
                    raise ValidationError("Groupd=%d does not exist." % g)

        # if warehouses was given, check each item if it exists in DB
        if self.initial_data['warehouses']:
            for w in self.initial_data['warehouses']:
                if not Warehouse.objects.filter(pk=w).exists():
                    raise ValidationError("Warehouse=%s does not exist." % w)

        username = validated_data['username']
        password = self.initial_data['password']

        with transaction.atomic():
            # create user, saving it to DB
            user = User.objects.create_user(username, '', password, **{
                'first_name': self.initial_data['name'],
                'is_staff': self.initial_data['admin'],
            })

            # create user meta
            user.meta = UserMeta.objects.create(**{
                'user': user,
                'password': password
            })
            user.save()

            # add each group in user groups
            [user.groups.add(g) for g in self.initial_data['groups']]

            # add each warehouse in user warehouses
            [user.warehouses.add(Warehouse.objects.get(pk=w)) for w in self.initial_data['warehouses']]

            return user

    def update(self, instance, validated_data):
        # if groups was given, check each item if it exists in DB
        if self.initial_data['groups']:
            for g in self.initial_data['groups']:
                if not Group.objects.filter(pk=g).exists():
                    raise ValidationError("Groupd=%d does not exist." % g)

        # if warehouses was given, check each item if it exists in DB
        if self.initial_data['warehouses']:
            for w in self.initial_data['warehouses']:
                if not Warehouse.objects.filter(pk=w).exists():
                    raise ValidationError("Warehouse=%d does not exist." % w)

        instance.first_name = self.initial_data['name']
        instance.admin = self.initial_data['admin']
        instance.meta.defaultWarehouse = Warehouse.objects.get(id=self.initial_data['defaultWarehouse'])

        # add each group in user groups
        instance.groups.clear()
        [instance.groups.add(g) for g in self.initial_data['groups']]

        # add each warehouse in user warehouses
        [instance.warehouses.add(Warehouse.objects.get(pk=w)) for w in self.initial_data['warehouses']]

        # save changes
        instance.meta.save()
        instance.save()

        return instance


class AttendanceSerializer(serializers.ModelSerializer):
    date = serializers.DateField(input_formats=[api_settings.DATE_FORMAT])

    class Meta:
        model = Attendance
        fields = 'id', 'employee', 'date', 'am_time_in', 'am_time_out', 'pm_time_in', 'pm_time_out'


class OvertimeSerializer(serializers.ModelSerializer):
    date = serializers.DateField(input_formats=[api_settings.DATE_FORMAT])

    class Meta:
        model = Overtime
        fields = 'id', 'employee', 'date', 'time_in', 'time_out', 'total_hours'

    @staticmethod
    def get_payroll_date_from(date_subject):
        for payroll in Payroll.objects.order_by('-date'):
            if payroll.date - timedelta(days=7) <= date_subject <= payroll.date:
                return payroll.date

    def sync_temporary_addition(self, overtime):
        # get employee who worked overtime
        employee = overtime.employee

        # make sure 'overtime' category is ready
        category, _ = Category.objects.get_or_create(**{
            'id': 'overtime',
            'type': 'Temporary Addition',
            'description': 'Overtime',
        })

        # retrieve payroll date
        payroll_date = self.get_payroll_date_from(overtime.date)
        if payroll_date is None:
            raise ValidationError('No appropriate payroll could be found with date=%s' % overtime.date)

        # compute total amount for this overtime
        total = overtime.total_hours * employee.salary_info.hourly_rate * Decimal(Overtime.FIXED_RATE)

        # create employee's temporary addition if it does not exist yet
        if not hasattr(overtime, 'temporary_addition'):
            temporary_addition = TemporaryAddition.objects.create(**{
                'overtime': overtime,
                'employee': employee,
                'category': category,
                'description': 'Overtime - %s' % overtime.date.strftime('%B %d, %Y'),
                'date': overtime.date,
                'total': total,
                'pending': total,
                'processed': 0.00,
            })

            # also create PayrollSchedule
            PayrollSchedule.objects.create(**{
                'employee': employee,
                'type': 'addition',
                'addition': temporary_addition,
                'date': overtime.date,
                'status': 'Pending',
                'payroll_date': payroll_date,
                'amount': total
            })
        else:
            temporary_addition = overtime.temporary_addition
            temporary_addition.description = 'Overtime - %s' % overtime.date.strftime('%B %d, %Y')
            temporary_addition.date = overtime.date
            temporary_addition.total = total
            temporary_addition.pending = total
            temporary_addition.save()

            schedule = PayrollSchedule.objects.filter(addition=temporary_addition).first()
            schedule.date = overtime.date
            schedule.amount = total
            schedule.save()

    def create(self, validated_data):
        with transaction.atomic():
            # invoke normal creation of overtime object
            overtime = super(OvertimeSerializer, self).create(validated_data)

            # create temporary addition for this overtime
            self.sync_temporary_addition(overtime)

            # return model for response
            return overtime

    def update(self, instance, validated_data):
        with transaction.atomic():
            # invoke normal update of overtime object
            overtime = super(OvertimeSerializer, self).update(instance, validated_data)

            # update temporary addition for this overtime
            self.sync_temporary_addition(overtime)

            # return model for response
            return overtime


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = 'id', 'type', 'description'


# noinspection PyAbstractClass
class ChangePasswordSerializer(AuthTokenSerializer):
    new_password = serializers.CharField()
    new_password2 = serializers.CharField()

    def validate(self, attrs):
        attrs = super(ChangePasswordSerializer, self).validate(attrs)

        new_password = attrs.get('new_password')
        new_password2 = attrs.get('new_password2')

        if not new_password or not new_password2:
            raise serializers.ValidationError('Must include "new_password" and "new_password2"')

        if new_password != new_password2:
            raise serializers.ValidationError('Passwords are not the same.')

        attrs['new_password'] = new_password
        return attrs


class WarehouseSerializer(serializers.ModelSerializer):
    """
    The serializer used for generic warehouse endpoint
    """

    class Meta:
        model = Warehouse
        fields = 'id', 'name', 'location', 'description', 'owner', 'is_POS', 'require_purchasers', 'require_requesters', 'require_approvers', 'customers'

    def create(self, validated_data):
        return Warehouse.objects.create(**{
            'name': validated_data['name'],
            'location': validated_data['location'],
            'description': validated_data['description'],
            'owner': User.objects.get(pk=self.initial_data['owner']),
            'is_POS': validated_data['is_POS'],
            'require_purchasers': validated_data['require_purchasers'],
            'require_requesters': validated_data['require_requesters'],
            'require_approvers': validated_data['require_approvers'],
            'customers': validated_data['customers'],
        })

    def update(self, instance, validated_data):
        instance.name = validated_data['name']
        instance.location = validated_data['location']
        instance.description = validated_data['description']
        instance.is_POS = validated_data['is_POS']
        instance.require_purchasers = validated_data['require_purchasers']
        instance.require_requesters = validated_data['require_requesters']
        instance.require_approvers = validated_data['require_approvers']
        instance.customers = validated_data['customers']

        if self.initial_data['owner'] is None:
            instance.owner = None
        elif self.initial_data['owner'] is not instance.owner.id:
            instance.owner = User.objects.get(pk=self.initial_data['owner'])

        instance.save()
        return instance

class WarehouseItemsSerializer(serializers.ModelSerializer):
    """
    The serializer used for generic warehouse endpoint
    """
    warehouse_name = serializers.SerializerMethodField()
    item_name = serializers.StringRelatedField(source='item.name', read_only=True)
    item_barcode = serializers.StringRelatedField(source='item.barcode', read_only=True)
    item_price = serializers.StringRelatedField(source='item.unit_price', read_only=True)
    item_unit = serializers.StringRelatedField(source='item.unit', read_only=True)

    class Meta:
        model = WarehouseItems
        fields = 'id', 'warehouse', 'item', 'stock', 'warehouse_name', 'item_name', 'item_barcode', 'item_price', 'item_unit'

    def get_warehouse_name(self, object):
        return object.warehouse.name



class WarehouseUserSerializer(serializers.ModelSerializer):
    user_name = serializers.SerializerMethodField()
    username = serializers.SerializerMethodField()

    class Meta:
        model = WarehouseUser
        fields = 'id', 'user', 'warehouse', 'user_name', 'username'

    def get_user_name(self, object):
        return object.user.first_name

    def get_username(self,object):
        return object.user.username

class WarehousePurchaserSerializer(serializers.ModelSerializer):
    purchaser_name = serializers.SerializerMethodField()

    class Meta:
        model = WarehousePurchaser
        fields = 'id', 'purchaser', 'warehouse', 'purchaser_name'

    def get_purchaser_name(self, object):
        if object.purchaser.middle_name != '' or object.purchaser.middle_name != None:
             return object.purchaser.last_name +",  "+ object.purchaser.first_name + " " + object.purchaser.middle_name
        else:
            return object.purchaser.last_name + ", " +object.purchaser.first_name 

class WarehouseApproverSerializer(serializers.ModelSerializer):
    approver_name = serializers.SerializerMethodField()
    username = serializers.SerializerMethodField()

    class Meta:
        model = WarehouseApprover
        fields = 'id', 'approver', 'warehouse', 'approver_name', 'username'

    def get_approver_name(self, object):
        return object.approver.first_name + " " + object.approver.last_name

    def get_username(self,object):
        return object.approver.username

class WarehouseRequesterSerializer(serializers.ModelSerializer):
    requester_name = serializers.SerializerMethodField()

    class Meta:
        model = WarehouseRequester
        fields = 'id', 'requester', 'warehouse', 'requester_name'

    def get_requester_name(self, object):
        if object.requester.middle_name != '' or object.requester.middle_name != None:
            return object.requester.last_name + ", " + object.requester.first_name + " " + object.requester.middle_name
        else:
            return object.requester.last_name + ", " + object.requester.first_name 

class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = 'id', 'name', 'type', 'description', 'barcode'


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = 'id', 'name', 'unit_price', 'description', 'barcode', 'unit'


class ReceiptSerializer(serializers.ModelSerializer):
    date = serializers.DateField(input_formats=[api_settings.DATE_FORMAT])
    total = serializers.SerializerMethodField()
    warehouse_name = serializers.StringRelatedField(source='warehouse.name', read_only=True)
    purchaser_name = serializers.StringRelatedField(source='purchaser.purchaser.__str__', read_only=True)

    class Meta:
        model = Receipt
        fields = ('id', 'warehouse', 'date', 'supplier', 'purchaser', 'receipt_number', 'invoice_number', 'total', 'warehouse_name', 'purchaser_name')

    def get_total(self, obj):
        result = ItemReceipt.objects.filter(receipt=obj).aggregate(Sum('total'))
        return result['total__sum'] or 0


class ItemHistorySerializer(serializers.ModelSerializer):
    date = serializers.DateField(input_formats=[api_settings.DATE_FORMAT])
    item_name = serializers.StringRelatedField(source='item.name', read_only=True)
    receipt_warehouse = serializers.StringRelatedField(source='receipt.receipt.warehouse.name', read_only=True)
    request_warehouse = serializers.StringRelatedField(source='request.request.warehouse.name', read_only=True)

    class Meta:
        model = ItemHistory
        fields = ('id', 'pseudo_id', 'item', 'receipt', 'request', 'date', 'transaction_type',
                  'quantity', 'unit_price', 'total', 'item_name', 'receipt_warehouse', 'request_warehouse')

class RequestSerializer(serializers.ModelSerializer):
    date = serializers.DateField(input_formats=[api_settings.DATE_FORMAT])
    total = serializers.SerializerMethodField()
    warehouse_name = serializers.StringRelatedField(source='warehouse.name', read_only=True)
    customer_name = serializers.StringRelatedField(source='customer.name', read_only=True)
    requester_name = serializers.StringRelatedField(source='requester.customer.name', read_only=True)

    class Meta:
        model = Request
        fields = ('id', 'warehouse', 'customer', 'requester', 'date', 'total', 'status', 'warehouse_name', 'customer_name', 'requester_name')

    def get_total(self, obj):
        result = ItemRequest.objects.filter(request=obj).aggregate(Sum('total'))
        return result['total__sum'] or 0


class SalaryInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = SalaryInfo
        fields = ('id', 'employee', 'bank_account', 'hourly_rate')


class PayrollCalendarSerializer(serializers.ModelSerializer):
    class Meta:
        model = PayrollCalendar
        fields = ('id', 'name', 'payroll_calendar', 'status')


class PayrollSerializer(serializers.ModelSerializer):
    date = serializers.DateField(input_formats=['%d-%b-%Y'], format='%d-%b-%Y')

    class Meta:
        model = Payroll
        fields = ('id', 'calendar', 'date', 'coverage', 'status')


class FixedAdditionSerializer(serializers.ModelSerializer):
    class Meta:
        model = FixedAddition
        fields = ('id', 'employee', 'category', 'description', 'schedule', 'amount', 'status')


class FixedDeductionSerializer(serializers.ModelSerializer):
    class Meta:
        model = FixedDeduction
        fields = ('id', 'employee', 'category', 'description', 'schedule', 'amount', 'status')


class TemporaryAdditionSerializer(serializers.ModelSerializer):
    date = serializers.DateField(input_formats=[api_settings.DATE_FORMAT])

    class Meta:
        model = TemporaryAddition
        fields = ('id', 'employee', 'category', 'description', 'date', 'total', 'processed', 'pending')


class TemporaryDeductionSerializer(serializers.ModelSerializer):
    date = serializers.DateField(input_formats=[api_settings.DATE_FORMAT])

    class Meta:
        model = TemporaryDeduction
        fields = ('id', 'employee', 'category', 'description', 'date', 'loan_id', 'total', 'processed', 'pending')


class ItemRequestSerializer(serializers.ModelSerializer):
    date = serializers.DateField(input_formats=[api_settings.DATE_FORMAT])
    item_name = serializers.StringRelatedField(source='item.name', read_only=True)
    requested_by = serializers.StringRelatedField(source='request.requester', read_only=True)

    class Meta:
        model = ItemRequest
        fields = ('id', 'request', 'item', 'item_name', 'unit_price', 'quantity', 'status', 'total', 'date', 'type', 'requested_by')

    def create(self, validated_data):
        with transaction.atomic():
            item_request = super(ItemRequestSerializer, self).create(validated_data)

            # update item stock if this request is approved
            # if item_request.status == 'Approved':
            #     if item_request.item.stock - item_request.quantity < 0:
            #         raise serializers.ValidationError("Cannot request quantity of %d, current item quantity is %d" % (
            #             item_request.quantity, item_request.item.stock))

            #     warehouse_items = WarehouseItems.objects.get(warehouse = item_request.request.warehouse, item = item_request.item)

            #     warehouse_items.stock -= item_receipt.quantity
            #     warehouse_items.save()

            # create history for this request

            if item_request.status == 'Issued':
                ItemHistory.objects.create(**{
                    'pseudo_id': item_request.request.id,
                    'item': item_request.item,
                    'request': item_request,
                    'date': item_request.request.date,
                    'transaction_type': 'Item Out',
                    'quantity': item_request.quantity,
                    'unit_price': item_request.unit_price,
                    'total': item_request.total,
                })

            return item_request


class ItemReceiptSerializer(serializers.ModelSerializer):
    date = serializers.DateField(input_formats=[api_settings.DATE_FORMAT])
    item_name = serializers.StringRelatedField(source='item.name', read_only=True)
    purchaser = serializers.StringRelatedField(source='receipt.purchaser.purchaser', read_only=True)

    class Meta:
        model = ItemReceipt
        fields = ('id', 'receipt', 'item', 'item_name', 'unit_price', 'quantity', 'total', 'date', 'purchaser')

    def create(self, validated_data):
        with transaction.atomic():
            item_receipt = super(ItemReceiptSerializer, self).create(validated_data)

            # create history for this request
            ItemHistory.objects.create(**{
                'pseudo_id': item_receipt.receipt.id,
                'item': item_receipt.item,
                'receipt': item_receipt,
                'date': item_receipt.receipt.date,
                'transaction_type': 'Item In',
                'quantity': item_receipt.quantity,
                'unit_price': item_receipt.unit_price,
                'total': item_receipt.total,
            })

            return item_receipt
