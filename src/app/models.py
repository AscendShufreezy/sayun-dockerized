import datetime

from django.contrib.auth.models import User
from django.db import models




"""
Common Functions Model
"""


class Category(models.Model):
    id = models.CharField(max_length=100, primary_key=True, verbose_name='Category ID')
    type = models.CharField(max_length=200)
    description = models.CharField(max_length=200)

    def __str__(self):
        return self.type

    class Meta:
        verbose_name_plural = 'categories'


class Customer(models.Model):
    id = models.CharField(max_length=200, primary_key=True)
    name = models.CharField(max_length=200)
    type = models.CharField(max_length=200)
    barcode =  models.CharField(max_length=200, default='None', blank=True, null=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return '%s: %s' % (self.id, self.name)


"""
Human Resources Information System Models

Employee is a customer. But a customer is not always an employee. Kay si truck pwede ma customer
"""


class Employee(models.Model):
    GENDER = (
        ('Male', 'Male'),
        ('Female', 'Female'),
    )
    TYPE = (
        ('Weekly', 'Weekly'),
        ('Per Trip', 'Per Trip'),
        ('Office', 'Office'),
    )
    STATUS = (
        ('Active', 'Active'),
        ('AWOL', 'Away on Leave'),
        ('Resigned', 'Resigned'),
        ('Terminated', 'Terminated'),
    )
    id = models.CharField(max_length=100, primary_key=True, verbose_name='Employee ID')
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    customer = models.OneToOneField(Customer, on_delete=models.SET_NULL, null=True)
    middle_name = models.CharField(max_length=100)
    barcode =  models.CharField(max_length=200, default='None', blank=True, null=True)
    birthdate = models.DateField()
    gender = models.CharField(max_length=100, choices=GENDER)
    type = models.CharField(max_length=100, choices=TYPE)
    title = models.CharField(max_length=100)
    date_hired = models.DateField('Date Hired')
    status = models.CharField(max_length=100, choices=STATUS, default='active')
    exit_date = models.DateField('Exit Date', blank=True, null=True)
    remarks = models.TextField(max_length=3000, blank=True, null=True)
    image = models.URLField(blank=True, null=True)

    @property
    def first_name(self):
        return self.user.first_name

    @property
    def last_name(self):
        return self.user.last_name

    def __str__(self):
        return '%s %s %s' % (self.user.last_name, self.user.first_name, self.middle_name)


class Attendance(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.PROTECT)
    date = models.DateField(default=datetime.date.today)
    am_time_in = models.TimeField(null=True)
    am_time_out = models.TimeField(null=True)
    pm_time_in = models.TimeField(null=True)
    pm_time_out = models.TimeField(null=True)

    def __str__(self):
        return '%s - %s, %s - %s' % (
            self.am_time_in.strftime('%I:%H %p'),
            self.am_time_out.strftime('%I:%H %p'),
            self.pm_time_in.strftime('%I:%H %p'),
            self.pm_time_out.strftime('%I:%H %p'),
        )


class SalaryInfo(models.Model):
    employee = models.OneToOneField(Employee, on_delete=models.CASCADE, related_name='salary_info')
    bank_account = models.CharField(max_length=200, blank=True)
    hourly_rate = models.DecimalField(max_digits=20, decimal_places=2)


class Overtime(models.Model):
    FIXED_RATE = 1.3

    employee = models.ForeignKey(Employee, on_delete=models.PROTECT)
    date = models.DateField(default=datetime.date.today)
    time_in = models.TimeField()
    time_out = models.TimeField()
    total_hours = models.DecimalField(max_digits=20, decimal_places=2)


class TemporaryAddition(models.Model):
    overtime = models.OneToOneField(Overtime, on_delete=models.CASCADE, related_name='temporary_addition', null=True)
    employee = models.ForeignKey(Employee, on_delete=models.PROTECT)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    description = models.TextField(blank=True)
    date = models.DateField('Date Entered')
    total = models.DecimalField(max_digits=20, decimal_places=2)
    processed = models.DecimalField(max_digits=20, decimal_places=2)
    pending = models.DecimalField(max_digits=20, decimal_places=2)


class OvertimeAddition(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.PROTECT)
    overtime = models.ForeignKey(Overtime, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    date = models.DateField()
    description = models.TextField()


class FixedAddition(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.PROTECT)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    description = models.TextField()
    schedule = models.CharField(max_length=200)
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    status = models.CharField(max_length=200)


class FixedDeduction(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.PROTECT)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    description = models.TextField()
    schedule = models.CharField(max_length=200)
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    status = models.CharField(max_length=200)


class TemporaryDeduction(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.PROTECT)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    description = models.TextField()
    date = models.DateField('Date Entered')
    loan_id = models.CharField(max_length=200, blank=True)
    total = models.DecimalField(max_digits=20, decimal_places=2)
    processed = models.DecimalField(max_digits=20, decimal_places=2)
    pending = models.DecimalField(max_digits=20, decimal_places=2)


class PayrollSchedule(models.Model):
    TYPES = (
        ('addition', 'addition'),
        ('deduction', 'deduction'),
    )
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    type = models.CharField(max_length=200, choices=TYPES)
    addition = models.ForeignKey(TemporaryAddition, on_delete=models.CASCADE, null=True)
    deduction = models.ForeignKey(TemporaryDeduction, on_delete=models.CASCADE, null=True)
    date = models.DateField()
    status = models.CharField(max_length=200)
    payroll_date = models.DateField()
    amount = models.DecimalField(max_digits=20, decimal_places=2)

    def __str__(self):
        return 'PayrollSchedule#%d' % self.id


class CafeteriaDeduction(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.PROTECT)
    description = models.TextField()
    date = models.DateField()
    amount = models.DecimalField(max_digits=20, decimal_places=2)

    def __str__(self):
        return "CafeteriaDeduction #%d" % self.id


class PayrollCalendar(models.Model):
    name = models.CharField(max_length=200)
    payroll_calendar = models.CharField(max_length=200, blank=True)
    status = models.CharField(max_length=200)


class Payroll(models.Model):
    calendar = models.ForeignKey(PayrollCalendar, on_delete=models.PROTECT)
    date = models.DateField()
    coverage = models.CharField(max_length=200)
    status = models.CharField(max_length=200)

    class Meta:
        unique_together = ('calendar', 'date', )


class PayrollComputation(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.PROTECT)
    payroll = models.ForeignKey(Payroll, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    description = models.TextField()
    amount = models.DecimalField(max_digits=20, decimal_places=2)


"""
Inventory Information System
"""


class Warehouse(models.Model):
    CUSTOMER_TYPES = (
        ('employees', 'employees'),
        ('truck', 'truck'),
    )
    id = models.CharField(max_length=200, primary_key=True, blank=True)
    owner = models.ForeignKey(User, related_name='warehouses', on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=200, unique=True, null=True)
    location = models.TextField()
    description = models.TextField()
    created_at = models.DateTimeField('Date Created', auto_now_add=True, editable=False)
    updated_at = models.DateTimeField('Date Updated', auto_now=True, editable=False)
    is_POS = models.BooleanField(default = False)
    require_purchasers = models.BooleanField(default = False)
    require_requesters = models.BooleanField(default = False)
    require_approvers = models.BooleanField(default = False)
    customers = models.CharField(max_length=10, choices=CUSTOMER_TYPES, default='employees')

    def save(self, *args, **kwargs):
        # already contains ID, updating the object
        if self.id:
            super(Warehouse, self).save(*args, **kwargs)
            return

        latest_warehouse = Warehouse.objects.order_by('-created_at').first()
        if not latest_warehouse:
            self.id = 'WH-0001'
            super(Warehouse, self).save(*args, **kwargs)
            return

        last_id = int(latest_warehouse.id[3:])
        self.id = 'WH-%0*d' % (4, last_id + 1)
        super(Warehouse, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


class UserMeta(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='meta')
    password = models.CharField(max_length=200)
    defaultWarehouse = models.ForeignKey(Warehouse, on_delete=models.CASCADE, null=True)

class WarehouseUser(models.Model):
    user = models.ForeignKey(User, related_name='warehouse_user', on_delete=models.CASCADE)
    warehouse = models.ForeignKey(Warehouse, related_name='assigned_warehouse', on_delete=models.CASCADE)

    class Meta:
        unique_together = (("user", "warehouse"),)

    def __str__(self):
        return self.user.first_name+" "+self.user.last_name+" can access "+self.warehouse.name

class WarehouseApprover(models.Model):
    approver = models.ForeignKey(User, related_name='warehouse_approver', on_delete=models.CASCADE)
    warehouse = models.ForeignKey(Warehouse, related_name='approver_warehouse', on_delete=models.CASCADE)

    class Meta:
        unique_together = (("approver", "warehouse"),)

    def __str__(self):
        return self.approver.first_name+" "+self.approver.last_name+" is approver at "+self.warehouse.name

class WarehousePurchaser(models.Model):
    purchaser = models.ForeignKey(Employee, related_name='warehouse_purchaser', on_delete=models.CASCADE)
    warehouse = models.ForeignKey(Warehouse, related_name='purchaser_warehouse', on_delete=models.CASCADE)

    class Meta:
        unique_together = (("purchaser", "warehouse"),)

    def __str__(self):
        return self.purchaser.first_name+" "+ self.purchaser.last_name +" is purchaser at "+self.warehouse.name

class WarehouseRequester(models.Model):
    requester = models.ForeignKey(Employee, related_name='warehouse_requester', on_delete=models.CASCADE)
    warehouse = models.ForeignKey(Warehouse, related_name='requester_warehouse', on_delete=models.CASCADE)

    class Meta:
        unique_together = (("requester", "warehouse"),)

    def __str__(self):
        return self.requester.first_name+" "+self.requester.last_name+" is requester at "+self.warehouse.name


class Request(models.Model):
    id = models.CharField(max_length=200, primary_key=True, blank=True)
    warehouse = models.ForeignKey(Warehouse, on_delete=models.PROTECT, null=True)
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)
    date = models.DateField('Issued Date')
    status = models.CharField(max_length=200)
    created_at = models.DateTimeField('Date Created', auto_now_add=True, editable=False)
    updated_at = models.DateTimeField('Date Updated', auto_now=True, editable=False)
    requester = models.ForeignKey(Employee, on_delete=models.CASCADE, blank=True, null=True)

    def save(self, *args, **kwargs):
        # already contains ID, update object
        if self.id:
            super(Request, self).save(*args, **kwargs)
            return

        latest_request = Request.objects.order_by('-created_at').first()
        if not latest_request:
            self.id = 'OUT-0001'
            super(Request, self).save(*args, **kwargs)
            return

        last_id = int(latest_request.id[4:])
        self.id = 'OUT-%0*d' % (4, last_id + 1)
        super(Request, self).save(*args, **kwargs)


class Item(models.Model):
    id = models.CharField(max_length=200, primary_key=True, blank=True)
    name = models.CharField(max_length=200)
    description = models.TextField()
    unit_price = models.DecimalField(max_digits=20, decimal_places=2)
    barcode = models.CharField(max_length=200)
    created_at = models.DateTimeField('Date Created', auto_now_add=True, editable=False)
    updated_at = models.DateTimeField('Date Updated', auto_now=True, editable=False)
    unit = models.CharField(max_length=15, null=True)

    def save(self, *args, **kwargs):
        # model already has ID (update opr)
        if self.id:
            super(Item, self).save(*args, **kwargs)
            return

        latest_item = Item.objects.order_by('-created_at').first()
        if not latest_item:
            self.id = 'ITEM-0001'
            super(Item, self).save(*args, **kwargs)
            return

        last_id = int(latest_item.id[5:])
        self.id = 'ITEM-%0*d' % (4, last_id + 1)
        super(Item, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

class WarehouseItems(models.Model):
    warehouse = models.ForeignKey(Warehouse, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    stock = models.FloatField(default=0)

    class Meta:
        unique_together = (("warehouse", "item"),)

    def __str__(self):
        return self.item.name + " is available at " + self.warehouse.name

class Receipt(models.Model):
    id = models.CharField(max_length=200, primary_key=True)
    warehouse = models.ForeignKey(Warehouse, on_delete=models.CASCADE)
    date = models.DateField()
    supplier = models.TextField(max_length=200)
    purchaser = models.ForeignKey(WarehousePurchaser, on_delete=models.CASCADE, blank=True, null=True)
    receipt_number = models.CharField(max_length=200, blank=True)
    invoice_number = models.CharField(max_length=200, blank=True)
    created_at = models.DateTimeField('Date Created', auto_now_add=True, editable=False)
    updated_at = models.DateTimeField('Date Updated', auto_now=True, editable=False)

    def save(self, *args, **kwargs):
        super(Receipt, self).save(*args, **kwargs)

    def __str__(self):
        return self.id

class ItemReceipt(models.Model):
    receipt = models.ForeignKey(Receipt, on_delete=models.PROTECT, related_name='item_receipts')
    item = models.ForeignKey(Item, on_delete=models.PROTECT)
    unit_price = models.DecimalField(max_digits=20, decimal_places=2)
    quantity = models.FloatField(default=0)
    # status = models.CharField(max_length=200)
    total = models.DecimalField(max_digits=20, decimal_places=2)
    date = models.DateField()


class ItemRequest(models.Model):
    request = models.ForeignKey(Request, on_delete=models.PROTECT, related_name='item_requests')
    item = models.ForeignKey(Item, on_delete=models.PROTECT)
    unit_price = models.DecimalField(max_digits=20, decimal_places=2)
    quantity = models.FloatField(default=0)
    status = models.CharField(max_length=200)
    total = models.DecimalField(max_digits=20, decimal_places=2)
    date = models.DateField()
    type = models.CharField(max_length=200, default='Other', blank=True, null=True)


class ItemHistory(models.Model):
    TRANSACTION_TYPES = (
        ('Item In', 'Item In'),
        ('Item Out', 'Item Out'),
    )
    pseudo_id = models.CharField(max_length=200, blank=True)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    receipt = models.ForeignKey(ItemReceipt, on_delete=models.CASCADE, null=True)
    request = models.ForeignKey(ItemRequest, on_delete=models.CASCADE, null=True)
    date = models.DateField()
    transaction_type = models.CharField(max_length=200, choices=TRANSACTION_TYPES)
    quantity = models.FloatField(default=0)
    unit_price = models.DecimalField(max_digits=20, decimal_places=2)
    total = models.DecimalField(max_digits=20, decimal_places=2)
    created_at = models.DateTimeField('Date Created', auto_now_add=True, editable=False)
    updated_at = models.DateTimeField('Date Updated', auto_now=True, editable=False)

    class Meta:
        verbose_name_plural = 'item histories'


class PayrollBreakdown(models.Model):
    TYPES = (
        ('fa', 'fixed addition'),
        ('fd', 'fixed deduction'),
        ('ta', 'temporary addition'),
        ('td', 'temporary deduction'),
    )
    payroll = models.ForeignKey(Payroll, on_delete=models.PROTECT)
    type = models.CharField(max_length=2, choices=TYPES)
    breakdown_id = models.IntegerField()
    employee = models.ForeignKey(Employee, on_delete=models.PROTECT)
    date = models.CharField(max_length=11)
    category = models.CharField(max_length=255)
    description = models.TextField()
    amount = models.DecimalField(max_digits=20, decimal_places=2)


class AvatarUpload(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(User, to_field='id')
    image = models.FileField()
