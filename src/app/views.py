import csv
import datetime
import io
import time
import zipfile
import docx
import xlsxwriter

from decimal import Decimal
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.db import transaction, IntegrityError
from django.db.models import Sum, ProtectedError
from django.db.models.functions import Lower
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from django.views import View

from rest_framework import viewsets, permissions, mixins, parsers
from rest_framework.authtoken.models import Token
from rest_framework.decorators import detail_route, list_route, parser_classes
from rest_framework.parsers import MultiPartParser, FormParser, JSONParser
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.views import APIView

from app.models import Employee, Attendance, Overtime, Category, Warehouse, \
    Customer, Item, PayrollSchedule, Receipt, ItemHistory, Request, \
    ItemReceipt, ItemRequest, SalaryInfo, PayrollCalendar, FixedDeduction, FixedAddition, \
    TemporaryAddition, TemporaryDeduction, Payroll, UserMeta, AvatarUpload, PayrollBreakdown, \
    WarehouseUser, WarehousePurchaser, WarehouseRequester, WarehouseApprover, WarehouseItems
from app.serializers import EmployeeSerializer, UserSerializer, \
    GroupSerializer, AttendanceSerializer, OvertimeSerializer, \
    CategorySerializer, ChangePasswordSerializer, WarehouseSerializer, \
    CustomerSerializer, ItemSerializer, PayrollScheduleSerializer, \
    ReceiptSerializer, ItemHistorySerializer, RequestSerializer, \
    ItemReceiptSerializer, ItemRequestSerializer, PermissionSerializer, SalaryInfoSerializer, \
    PayrollCalendarSerializer, FixedDeductionSerializer, \
    FixedAdditionSerializer, TemporaryAdditionSerializer, WarehouseItemsSerializer, \
    TemporaryDeductionSerializer, PayrollSerializer, ContentTypeSerializer, \
    WarehouseUserSerializer, WarehousePurchaserSerializer, WarehouseRequesterSerializer, WarehouseApproverSerializer
from app.services import PayrollService, PermissionService

# Permission string constants
FORBIDDEN = '%s is forbidden to %s %s'


class CategoryViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Category.objects.order_by('id')
    serializer_class = CategorySerializer

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_category'):
            return Response({'details': FORBIDDEN % (request.user.id, 'view', 'categories')}, status=403)

        return super(CategoryViewSet, self).list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_category'):
            return Response({'details': FORBIDDEN % (request.user.id,)}, status=403)

        return super(CategoryViewSet, self).retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_category'):
            return Response({'details': '%s is forbidden to add categories' % request.user.id}, status=403)

        return super(CategoryViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_category'):
            return Response({'details': '%s is forbidden to change categories' % request.user.id}, status=403)

        return super(CategoryViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_category'):
            return Response({'details': '%s is forbidden to delete categories' % request.user.id}, status=403)

        return super(CategoryViewSet, self).destroy(request, *args, **kwargs)


class CustomerViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Customer.objects.order_by(Lower('id'))
    serializer_class = CustomerSerializer

    @detail_route(methods=['get'])
    def loans(self, request, pk=None):
        if not PermissionService.has_permission(request.user, 'view_itemrequest'):
            return Response({'details': '%s is forbidden to view item requests' % request.user.id}, status=403)

        from_date = request.query_params.get('from', None)
        to_date = request.query_params.get('to', None)

        if from_date is None or to_date is None:
            return Response({'detail': "Expected 'from' and 'to' parameters with the \
                format 'DD/MM/YYYY'"}, status=400)

        from_date = datetime.datetime.strptime(from_date, '%d/%m/%Y')
        to_date = datetime.datetime.strptime(to_date, '%d/%m/%Y')

        data = ItemRequest.objects.filter(request__customer_id=pk)
        data = data.filter(date__range=[from_date, to_date],  type__iexact='Loan')
        serializer = ItemRequestSerializer(data, many=True)

        return Response(serializer.data, status=200)

    @detail_route(methods=['get'])
    def requests(self, request, pk=None):
        if not PermissionService.has_permission(request.user, 'view_itemrequest'):
            return Response({'details': '%s is forbidden to view item requests' % request.user.id}, status=403)

        from_date = request.query_params.get('from', None)
        to_date = request.query_params.get('to', None)
        warehouse = request.query_params.get('warehouse', None)

        if from_date is None or to_date is None:
            return Response({'detail': "Expected 'from' and 'to' parameters with the \
                format 'DD/MM/YYYY'"}, status=400)

        if warehouse is None:
            return Response(status=200, data="")

        from_date = datetime.datetime.strptime(from_date, '%d/%m/%Y')
        to_date = datetime.datetime.strptime(to_date, '%d/%m/%Y')

        data = ItemRequest.objects.filter(request__customer_id=pk, request__warehouse_id = warehouse, status='Issued')
        data = data.filter(date__range=[from_date, to_date])
        serializer = ItemRequestSerializer(data, many=True)

        return Response(serializer.data, status=200)
    
    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_customer'):
            return Response({'details': '%s is forbidden to view customers' % request.user.id}, status=403)

        sort_field = request.query_params.get('sort', None)
        inactive_filter = ('AWOL', 'Resigned', 'Terminated',)

        if sort_field is None or sort_field == 'id':
            queryset = Customer.objects.exclude(employee__status__in=inactive_filter).order_by(Lower('id'))
        else:
            queryset = Customer.objects.exclude(employee__status__in=inactive_filter).order_by(Lower('name'))

        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_customer'):
            return Response({'details': '%s is forbidden to view customers' % request.user.id}, status=403)

        return super(CustomerViewSet, self).retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_customer'):
            return Response({'details': '%s is forbidden to add customers' % request.user.id}, status=403)

        return super(CustomerViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_customer'):
            return Response({'details': '%s is forbidden to change customers' % request.user.id}, status=403)

        return super(CustomerViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_customer'):
            return Response({'details': '%s is forbidden to delete customers' % request.user.id}, status=403)

        try:
            return super(CustomerViewSet, self).destroy(request, *args, **kwargs)
        except ProtectedError as e:
            name = e.protected_objects.model.__name__
            return Response({'detail': 'Customer is still referenced in some %s(s).' % name}, status=400)


class ItemViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Item.objects.order_by('name')
    serializer_class = ItemSerializer

    @detail_route(methods=['get'])
    def history(self, request, pk=None):
        if not PermissionService.has_permission(request.user, 'view_itemhistory'):
            return Response({'details': '%s is forbidden to view item histories' % request.user.id}, status=403)

        histories = ItemHistory.objects.filter(item=self.get_object()).order_by('date')
        return Response(ItemHistorySerializer(histories, many=True).data)

    @detail_route(methods=['get'])
    def receipts(self, request, pk=None):
        if not PermissionService.has_permission(request.user, 'view_receipt'):
            return Response({'details': '%s is forbidden to view receipts' % request.user.id}, status=403)

        receipts = Receipt.objects.filter(name=self.get_object())
        return Response(ReceiptSerializer(receipts, many=True).data)

    @list_route()
    def inventory(self, request):
        if not PermissionService.has_permission(request.user, 'view_itemhistory'):
            return Response({'details': '%s is forbidden to view item histories' % request.user.id}, status=403)

        given_date = request.query_params.get('date', None)
        given_warehouse = request.query_params.get('warehouse', None)

        if given_date is None:
            return Response({'detail': "A query parameter named 'date' with a format of 'DD/MM/YYYY' is expected."},
                            status=400)

        if given_warehouse is None:
            return Response({'detail': "A query parameter named 'warehouse' is expected."},
                            status=400)

        try:
            given_date = datetime.datetime.strptime(given_date, '%m/%d/%Y')
        except ValueError:
            return Response({'detail': "A query parameter named 'date' with a format of 'DD/MM/YYYY' is expected."},
                            status=400)

        item_request = ItemRequest.objects.filter(date__lte=given_date, request__warehouse__id=given_warehouse).order_by('item__name')
        item_receipt = ItemReceipt.objects.filter(date__lte=given_date, receipt__warehouse__id=given_warehouse).order_by('item__name')
        result = {}

        # gather up quantities of each item
        for x in item_receipt:
            try:
                row = result[x.item.id]
            except KeyError:
                row = {
                    'item': x.item.id,
                    'name': x.item.name,
                    'quantity': 0,
                }
                result[x.item.id] = row

            row['quantity'] += x.quantity
            recent_price = x.item.unit_price
            row['total_cost'] = Decimal.from_float(row['quantity']) * recent_price

        for x in item_request:
            try:
                row = result[x.item.id]
            except KeyError:
                row = {
                    'item': x.item.id,
                    'name': x.item.name,
                    'quantity': 0,
                }
                result[x.item.id] = row

            row['quantity'] -= x.quantity
            recent_price = x.item.unit_price
            row['total_cost'] = Decimal.from_float(row['quantity']) * recent_price


        return Response(list(result.values()), status=200)


    @detail_route(methods=['get'])
    def receipt_summary(self, request, pk=None):
        if not PermissionService.has_permission(request.user, 'view_receipt'):
            return Response({'details': '%s is forbidden to view receipts' % request.user.id}, status=403)

        from_date = request.query_params.get('from', None)
        to_date = request.query_params.get('to', None)
        warehouse = request.query_params.get('warehouse', None)

        if from_date is None or to_date is None:
            return Response({'detail': "Expected 'from' and 'to' parameters with the format 'DD/MM/YYYY'"}, status=400)

        if warehouse is None:
            return Response(status=200)

        from_date = datetime.datetime.strptime(from_date, '%d/%m/%Y')
        to_date = datetime.datetime.strptime(to_date, '%d/%m/%Y')

        data = ItemReceipt.objects.filter(item__id=pk, receipt__warehouse__id = warehouse)
        data = data.filter(date__range=[from_date, to_date])
        serializer = ItemReceiptSerializer(data, many=True)

        return Response(serializer.data, status=200)

    @detail_route(methods=['get'])
    def items_in_warehouse(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_receipt'):
            return Response({'details': '%s is forbidden to view receipts' % request.user.id}, status=403)

        is_admin = request.user.groups.filter(name="admin-group")

        if is_admin.count() > 0:
            data = Item.objects.all().order_by('-created_at')
            serializer = ItemSerializer(data, many=True)
        else:
            warehouseIds = WarehouseUser.objects.filter(user = request.user).values('warehouse')
            listIds = []

            for warehouseId in warehouseIds:
                listIds.append(warehouseId['warehouse'])

            itemIds = WarehouseItems.objects.filter(warehouse__id__in=listIds).values('item')
            listItems = []

            for itemId in itemIds:
                listItems.append(itemId['item'])

            data = Item.objects.filter(id__in=listItems).order_by('-created_at')
            serializer = ItemSerializer(data, many=True)

        return Response(serializer.data, status=200)

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_item'):
            return Response({'details': '%s is forbidden to view items' % request.user.id}, status=403)

        # return Response(status=400, data="fota")
        return super(ItemViewSet, self).list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_item'):
            return Response({'details': '%s is forbidden to view items' % request.user.id}, status=403)

        return super(ItemViewSet, self).retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_item'):
            return Response({'details': '%s is forbidden to add items' % request.user.id}, status=403)

        return super(ItemViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_item'):
            return Response({'details': '%s is forbidden to change items' % request.user.id}, status=403)

        return super(ItemViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_item'):
            return Response({'details': '%s is forbidden to delete items' % request.user.id}, status=403)

        try:
            return super(ItemViewSet, self).destroy(request, *args, **kwargs)
        except ProtectedError as e:
            name = e.protected_objects.model.__name__
            return Response({'detail': 'Item is still referenced in some %s(s).' % name}, status=400)


class EmployeeViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Employee.objects.order_by('user__last_name', 'user__first_name', 'middle_name')
    serializer_class = EmployeeSerializer

    @detail_route(methods=['get'])
    def overtimes(self, request, pk=None):
        ots = Overtime.objects.filter(employee=self.get_object())
        return Response(OvertimeSerializer(ots, many=True).data)

    @detail_route(methods=['get'])
    def attendance(self, request, pk=None):
        ats = Attendance.objects.filter(employee=self.get_object())
        return Response(AttendanceSerializer(ats, many=True).data)

    @detail_route(methods=['get'])
    def salary_infos(self, request, pk=None):
        salary_infos = SalaryInfo.objects.filter(employee=self.get_object())
        return Response(SalaryInfoSerializer(salary_infos, many=True).data)

    @detail_route(methods=['get'])
    def payroll_schedules(self, request, pk=None):
        payroll_schedules = PayrollSchedule.objects.filter(employee=self.get_object())
        return Response(PayrollScheduleSerializer(payroll_schedules, many=True).data)

    @detail_route(methods=['get'])
    def fixed_deductions(self, request, pk=None):
        fixed_deductions = FixedDeduction.objects.filter(employee=self.get_object())
        return Response(FixedDeductionSerializer(fixed_deductions, many=True).data)

    @detail_route(methods=['get'])
    def fixed_additions(self, request, pk=None):
        fixed_additions = FixedAddition.objects.filter(employee=self.get_object())
        return Response(FixedAdditionSerializer(fixed_additions, many=True).data)

    @detail_route(methods=['get'])
    def temporary_deductions(self, request, pk=None):
        temporary_deductions = TemporaryDeduction.objects.filter(employee=self.get_object())
        return Response(TemporaryDeductionSerializer(temporary_deductions, many=True).data)

    @detail_route(methods=['get'])
    def temporary_additions(self, request, pk=None):
        temporary_additions = TemporaryAddition.objects.filter(employee=self.get_object())
        return Response(TemporaryAdditionSerializer(temporary_additions, many=True).data)

    @detail_route(methods=['get'])
    def payroll_breakdown(self, request, pk=None):
        # get employee object
        employee = self.get_object()

        # get date from query parameters
        date_as_str = request.query_params.get('date', None)

        # parse date to python date
        date = datetime.datetime.strptime(date_as_str, '%d-%b-%Y').date()

        # get payroll object
        try:
            payroll = Payroll.objects.get(date=date)
        except Payroll.DoesNotExist:
            return Response({'details': 'Payroll with date=%s does not exist' % date_as_str},
                            status=404)

        # get payroll breakdown according to payroll status
        if payroll.status.lower() == 'processed':
            rows = [{
                'id': breakdown.breakdown_id,
                'employee': employee.id,
                'date': breakdown.date,
                'category': breakdown.category,
                'description': breakdown.description,
                'amount': breakdown.amount,
            } for breakdown in PayrollBreakdown.objects.filter(payroll=payroll, employee=employee)]
        else:
            rows = PayrollService.get_total_breakdown(employee, date)
            rows = rows['fixed_additions'] + rows['temporary_additions'] + \
                   rows['fixed_deductions'] + rows['temporary_deductions']

        return Response(rows, status=200)

    @detail_route(methods=['get'])
    def total_payroll(self, request, pk=None):
        # get date from query parameters
        date_as_str = request.query_params.get('date', None)

        # parse date to python date
        date = datetime.datetime.strptime(date_as_str, '%d-%b-%Y').date()

        # check payroll with the given date exists
        if not Payroll.objects.filter(date=date).exists():
            return Response({'details': 'Payroll with date=%s does not exist' % date_as_str},
                            status=404)

        return Response(
            {'total': PayrollService.get_total(self.get_object(), date)},
            status=200
        )

    @detail_route(methods=['post', 'delete'])
    def customer(self, request, pk=None):
        employee = self.get_object()

        # API user wants to make this employee a customer
        if request.method == 'POST':
            # employee is not yet a customer
            if not employee.customer:
                # wrap db stuff in a transaction
                with transaction.atomic():
                    # create customer object for this employee
                    customer = Customer.objects.create(**{
                        'id': employee.id,
                        'name': '%s, %s %s' % (employee.user.last_name, employee.user.first_name, employee.middle_name),
                        'type': 'Employee',
                        'barcode': employee.barcode,
                    })

                    # bind customer object to this employee
                    employee.customer = customer
                    employee.save()

            return Response()

        # API user wants to make this employee not a customer
        # employee is a customer
        if employee.customer:
            # remove employee-customer binding
            employee.customer.delete()

        return Response()

    @detail_route(methods=['post'], parser_classes=[MultiPartParser, FormParser])
    def upload_image(self, request, pk=None):
        with transaction.atomic():
            employee = self.get_object()

            upload = AvatarUpload.objects.create(**{
                'owner': employee.user,
                'image': request.data.get('image'),
            })

            employee.image = upload.image.url
            employee.save()

            return Response({'uri': upload.image.url}, status=200)

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_employee'):
            return Response({'details': '%s is forbidden to view employees' % request.user.id}, status=403)

        return super(EmployeeViewSet, self).list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_employee'):
            return Response({'details': '%s is forbidden to view employees' % request.user.id}, status=403)

        return super(EmployeeViewSet, self).retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_employee'):
            return Response({'details': '%s is forbidden to add employees' % request.user.id}, status=403)

        return super(EmployeeViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_employee'):
            return Response({'details': '%s is forbidden to change employees' % request.user.id}, status=403)

        return super(EmployeeViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_employee'):
            return Response({'details': '%s is forbidden to delete employees' % request.user.id}, status=403)

        with transaction.atomic():
            # delete customer object if it exists
            employee = self.get_object()
            if employee.customer:
                employee.customer.delete()

            # remember current employee's userID, to be used for deletion later
            employee_user_id = employee.user.id

            # let viewset's implementation delete the employee, then get its response
            response = super(EmployeeViewSet, self).destroy(request, *args, **kwargs)

            # delete employee's corresponding user object
            User.objects.filter(id=employee_user_id).delete()

            return response


class PayrollScheduleViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = PayrollSchedule.objects.order_by('payroll_date', 'date')
    serializer_class = PayrollScheduleSerializer

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_payrollschedule'):
            return Response({'details': '%s is forbidden to view payroll schedules' % request.user.id}, status=403)

        obj_type = request.query_params.get('type', None)
        queryset = PayrollSchedule.objects.all()

        if obj_type is not None:
            queryset = queryset.filter(type=obj_type)

        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_payrollschedule'):
            return Response({'details': '%s is forbidden to view payroll schedules' % request.user.id}, status=403)

        return super(PayrollScheduleViewSet, self).retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_payrollschedule'):
            return Response({'details': '%s is forbidden to add payroll schedules' % request.user.id}, status=403)

        with transaction.atomic():
            # this code snippet allows creation of multiple objects
            serializer = self.serializer_class(data=request.data, many=True)
            serializer.is_valid(raise_exception=True)
            serializer.save()

            return Response(status=201)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_payrollschedule'):
            return Response({'details': '%s is forbidden to change payroll schedules' % request.user.id}, status=403)

        return super(PayrollScheduleViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_payrollschedule'):
            return Response({'details': '%s is forbidden to delete payroll schedules' % request.user.id}, status=403)

        payroll_schedule = self.get_object()

        if payroll_schedule.status.lower() == 'processed':
            return Response({'detail': 'PayrollSchedule#%d is already processed.' % payroll_schedule.id}, status=400)

        return super(PayrollScheduleViewSet, self).destroy(request, *args, **kwargs)


class PayrollCalendarViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = PayrollCalendar.objects.order_by('name')
    serializer_class = PayrollCalendarSerializer

    @detail_route(methods=['get'])
    def payroll(self, request, pk=None):
        payrolls = Payroll.objects.filter(calendar=self.get_object())
        return Response(PayrollSerializer(payrolls, many=True).data)

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_payrollcalendar'):
            return Response({'details': '%s is forbidden to view payroll calendars' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_payrollcalendar'):
            return Response({'details': '%s is forbidden to view payroll calendars' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_payrollcalendar'):
            return Response({'details': '%s is forbidden to add payroll calendars' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_payrollcalendar'):
            return Response({'details': '%s is forbidden to change payroll calendars' % request.user.id}, status=403)

        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_payrollcalendar'):
            return Response({'details': '%s is forbidden to delete payroll calendars' % request.user.id}, status=403)

        return super().destroy(request, *args, **kwargs)


class PayrollViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Payroll.objects.order_by('date')
    serializer_class = PayrollSerializer

    @staticmethod
    def sync_payroll_calendar(payroll):
        # Update payroll calendar's date range
        payroll_calendar = payroll.calendar

        # retrieve earliest and latest date for the current payroll calendar
        queryset = payroll_calendar.payroll_set.order_by('date')
        earliest = queryset.first().date - datetime.timedelta(days=6)
        latest = queryset.last().date

        # format earliest and latest date
        earliest = datetime.datetime.strftime(earliest, '%b-%d-%Y')
        latest = datetime.datetime.strftime(latest, '%b-%d-%Y')

        # commit
        payroll_calendar.payroll_calendar = '%s - %s' % (earliest, latest)
        payroll_calendar.save()

    @list_route(methods=['get'])
    def get_payroll_date(self, request):
        date_subject = datetime.datetime.strptime(request.query_params.get('date'), '%d/%m/%Y')
        date_subject = datetime.date(date_subject.year, date_subject.month, date_subject.day)

        for payroll in Payroll.objects.order_by('date'):
            if payroll.date - datetime.timedelta(days=6) <= date_subject <= payroll.date:
                return Response({'result': payroll.date}, status=200)

        return Response(status=404)

    @detail_route(methods=['post'])
    def process(self, request, pk=None):
        with transaction.atomic():
            # get payroll object
            payroll = self.get_object()

            # return error if payroll was already processed
            if payroll.status == 'Processed':
                return Response(status=400)

            # update payroll's status to Processed
            payroll.status = 'Processed'
            payroll.save()

            min_date = payroll.date - datetime.timedelta(days=6)
            max_date = payroll.date

            # find all payroll schedules within this payroll's date
            payroll_schedules = PayrollSchedule.objects.filter(payroll_date__range=[min_date, max_date])

            for schedule in payroll_schedules:
                # only process those payroll schedules who weren't processed yet
                if schedule.status != 'Processed':
                    if schedule.type == 'addition':
                        schedule.addition.processed += schedule.amount
                        schedule.addition.pending -= schedule.amount
                        schedule.addition.save()
                    elif schedule.type == 'deduction':
                        schedule.deduction.processed += schedule.amount
                        schedule.deduction.pending -= schedule.amount
                        schedule.deduction.save()

                    schedule.status = 'Processed'
                    schedule.save()

            # Breakdown creator lambda
            def create_breakdown(breakdown, btype):
                PayrollBreakdown.objects.create(**{
                    'payroll': payroll,
                    'type': btype,
                    'breakdown_id': breakdown['id'],
                    'employee': employee,
                    'date': breakdown['date'],
                    'category': breakdown['category'],
                    'description': breakdown['description'],
                    'amount': breakdown['amount']
                })

            # freeze payroll breakdown
            for employee in Employee.objects.all():
                result = PayrollService.get_total_breakdown(employee, payroll.date)

                [create_breakdown(row, 'fa') for row in result['fixed_additions']]
                [create_breakdown(row, 'fd') for row in result['fixed_deductions']]
                [create_breakdown(row, 'ta') for row in result['temporary_additions']]
                [create_breakdown(row, 'td') for row in result['temporary_deductions']]

            return Response(status=200)

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_payroll'):
            return Response({'details': '%s is forbidden to view payrolls' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_payroll'):
            return Response({'details': '%s is forbidden to view payrolls' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_payroll'):
            return Response({'details': '%s is forbidden to add payrolls' % request.user.id}, status=403)

        with transaction.atomic():
            serializer = self.serializer_class(data=request.data, many=True)
            serializer.is_valid(raise_exception=True)

            # save payrolls
            payrolls = serializer.save()

            # sync each payroll calendar's date range
            for payroll in payrolls:
                self.sync_payroll_calendar(payroll)

            return Response(status=201)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_payroll'):
            return Response({'details': '%s is forbidden to change payrolls' % request.user.id}, status=403)

        with transaction.atomic():
            # get payroll object
            payroll = self.get_object()

            # invoke normal update operation
            response = super(PayrollViewSet, self).update(request, *args, **kwargs)

            # refresh payroll to reflect latest changes
            payroll.refresh_from_db()

            # sync payroll calendar's date range
            self.sync_payroll_calendar(payroll)

            return response

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_payroll'):
            return Response({'details': '%s is forbidden to delete payrolls' % request.user.id}, status=403)

        # get payroll object
        payroll = self.get_object()

        # payroll objects can only be deleted if its status is pending
        if payroll.status != "Pending":
            return Response(status=400)

        with transaction.atomic():
            response = super(PayrollViewSet, self).destroy(request, *args, **kwargs)

            # update corresponding payroll calendar's description if this payroll
            # is the latest or the earliest of it
            payroll_calendar = payroll.calendar
            queryset = payroll_calendar.payroll_set.order_by('date')

            # queryset may be empty if user decided to delete all payrolls within a calendar
            if queryset.count() is 0:
                payroll_calendar.payrolPl_calendar = '-'
                payroll_calendar.save()
            else:
                earliest = datetime.datetime.strftime(queryset.first().date - datetime.timedelta(days=6), '%b-%d-%Y')
                latest = datetime.datetime.strftime(queryset.last().date, '%b-%d-%Y')
                payroll_calendar.payroll_calendar = '%s - %s' % (earliest, latest)
                payroll_calendar.save()

            return response


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = User.objects.filter(employee=None).order_by('username')
    serializer_class = UserSerializer

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_user'):
            return Response({'details': '%s is forbidden to view users' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, pk=None):
        if not PermissionService.has_permission(request.user, 'view_user'):
            return Response({'details': '%s is forbidden to view users' % request.user.id}, status=403)

        try:
            user = get_object_or_404(self.queryset, pk=int(pk))
        except ValueError:
            user = get_object_or_404(self.queryset, username=pk)

        serializer = self.serializer_class(user)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_user'):
            return Response({'details': '%s is forbidden to add users' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_user'):
            return Response({'details': '%s is forbidden to change users' % request.user.id}, status=403)

        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_user'):
            return Response({'details': '%s is forbidden to delete users' % request.user.id}, status=403)

        return super().destroy(request, *args, **kwargs)


class GroupViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Group.objects.order_by('name')
    serializer_class = GroupSerializer

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_group'):
            return Response({'details': '%s is forbidden to view groups' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_group'):
            return Response({'details': '%s is forbidden to view groups' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_group'):
            return Response({'details': '%s is forbidden to add groups' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_group'):
            return Response({'details': '%s is forbidden to change groups' % request.user.id}, status=403)

        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_group'):
            return Response({'details': '%s is forbidden to delete groups' % request.user.id}, status=403)

        group = self.get_object()

        if group.user_set.count() is not 0:
            return Response({'detail': 'Group is still referenced in some User(s)'}, status=400)

        return super(GroupViewSet, self).destroy(request, *args, **kwargs)


class PermissionViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Permission.objects.all()
    serializer_class = PermissionSerializer

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_permission'):
            return Response({'details': '%s is forbidden to view permissions' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_permission'):
            return Response({'details': '%s is forbidden to view permissions' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)


class ContentTypeViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = ContentType.objects.all()
    serializer_class = ContentTypeSerializer

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_contenttype'):
            return Response({'details': '%s is forbidden to view content types' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_contenttype'):
            return Response({'details': '%s is forbidden to view content types' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)


class WarehouseViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Warehouse.objects.order_by('name')
    serializer_class = WarehouseSerializer

    @detail_route(methods=['get'])
    def warehouse_users(self, request, pk=None):
        warehouse_users = WarehouseUser.objects.filter(warehouse=self.get_object())
        return Response(WarehouseUserSerializer(warehouse_users, many=True).data)

    @detail_route(methods=['get'])
    def is_pos(self, request, pk=None):
        return Response(self.get_object().is_POS)

    @detail_route(methods=['get'])
    def warehouse_purchasers(self, request, pk=None):
        warehouse_purchasers = WarehousePurchaser.objects.filter(warehouse=self.get_object())
        return Response(WarehousePurchaserSerializer(warehouse_purchasers, many=True).data)

    @detail_route(methods=['get'])
    def warehouse_approvers(self, request, pk=None):
        warehouse_approvers = WarehouseApprover.objects.filter(warehouse=self.get_object())
        return Response(WarehouseApproverSerializer(warehouse_approvers, many=True).data)

    @detail_route(methods=['get'])
    def warehouse_requesters(self, request, pk=None):
        warehouse_requesters = WarehouseRequester.objects.filter(warehouse=self.get_object())
        return Response(WarehouseRequesterSerializer(warehouse_requesters, many=True).data)

    @detail_route(methods=['get'])
    def warehouse_items(self, request, pk=None):
        warehouse_items = WarehouseItems.objects.filter(warehouse=self.get_object()).order_by('item__name')
        return Response(WarehouseItemsSerializer(warehouse_items, many=True).data)

    @detail_route(methods=['get'])
    def available_warehouses(self, request, pk=None):
        userId = request.query_params.get('user', None)

        if userId is None:
            return Response({'detail': "No user Id has been passed."},
                            status=400) 

        warehouseIds = WarehouseUser.objects.filter(user__id=userId).values('warehouse')
        listIds = []

        for warehouseId in warehouseIds:
            listIds.append(warehouseId['warehouse'])

        available_warehouses = Warehouse.objects.filter(id__in=listIds)

        return Response(WarehouseSerializer(available_warehouses, many=True).data)

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_warehouse'):
            return Response({'details': '%s is forbidden to view warehouses' % request.user.id}, status=403)

        isPOS = request.query_params.get('pos', None)

        warehouseIds = WarehouseUser.objects.filter(user = request.user).values('warehouse')
        listIds = []

        for warehouseId in warehouseIds:
            listIds.append(warehouseId['warehouse'])

        if isPOS is not None:
            if isPOS == 'True':
                data = Warehouse.objects.filter(id__in=listIds, is_POS=True)
            elif isPOS == 'False':
                data = Warehouse.objects.filter(id__in=listIds, is_POS=False)
            serializer = WarehouseSerializer(data, many=True)

            return Response(serializer.data)

        elif isPOS is None:
            is_admin = request.user.groups.filter(name="admin-group")

            if is_admin.count() > 0:
                data = Warehouse.objects.all()
            else:
                data = Warehouse.objects.filter(id__in=listIds)

            serializer = WarehouseSerializer(data, many=True)
            return Response(serializer.data)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_warehouse'):
            return Response({'details': '%s is forbidden to view warehouses' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_warehouse'):
            return Response({'details': '%s is forbidden to add warehouses' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'update_warehouse'):
            return Response({'details': '%s is forbidden to update warehouses' % request.user.id}, status=403)

        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'destroy_warehouse'):
            return Response({'details': '%s is forbidden to destroy warehouses' % request.user.id}, status=403)

        return super().destroy(request, *args, **kwargs)



class WarehouseUserViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = WarehouseUser.objects.all()
    serializer_class = WarehouseUserSerializer

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_warehouseuser'):
            return Response({'details': '%s is forbidden to view warehouse users' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_warehouseuser'):
            return Response({'details': '%s is forbidden to add warehouse users' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_warehouseuser'):
            return Response({'details': '%s is forbidden to delete warehouse users' % request.user.id}, status=403)

        return super().destroy(request, *args, **kwargs)


class WarehousePurchaserViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = WarehousePurchaser.objects.all()
    serializer_class = WarehousePurchaserSerializer

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_warehousepurchaser'):
            return Response({'details': '%s is forbidden to view warehouse purchasers' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_warehousepurchaser'):
            return Response({'details': '%s is forbidden to add warehouse purchasers' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_warehousepurchaser'):
            return Response({'details': '%s is forbidden to delete warehouse purchasers' % request.user.id}, status=403)

        return super().destroy(request, *args, **kwargs)


class WarehouseApproverViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = WarehouseApprover.objects.all()
    serializer_class = WarehouseApproverSerializer

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_warehouseapprover'):
            return Response({'details': '%s is forbidden to view warehouse approvers' % request.user.id}, status=403)

        toFilter = request.query_params.get('user', None)
        if toFilter is not None:
            try: 
                toFilter = int(toFilter)
            except ValueError:
                return super().list(request, *args, **kwargs)

            warehouseApprovers = WarehouseApprover.objects.filter(approver__id=toFilter).values('warehouse')

            listIds = []

            for warehouseApprover in warehouseApprovers:
                listIds.append(warehouseApprover['warehouse'])

            data = Warehouse.objects.filter(id__in=listIds)
            serializer = WarehouseSerializer(data, many=True)
            return Response(serializer.data)

        return super().list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_warehouseapprover'):
            return Response({'details': '%s is forbidden to add warehouse approvers' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_warehouseapprover'):
            return Response({'details': '%s is forbidden to delete warehouse approvers' % request.user.id}, status=403)

        return super().destroy(request, *args, **kwargs)


class WarehouseRequesterViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = WarehouseRequester.objects.all()
    serializer_class = WarehouseRequesterSerializer

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_warehouserequester'):
            return Response({'details': '%s is forbidden to view warehouse requesters' % request.user.id}, status=403)

        toFilter = request.query_params.get('warehouse', None)
        if toFilter is not None:

            warehouseRequesters = WarehouseRequester.objects.filter(warehouse__id=toFilter).values('requester')

            listIds = []

            for warehouseRequester in warehouseRequesters:
                listIds.append(warehouseRequester['requester'])

            inactive_filter = ('AWOL', 'Resigned', 'Terminated',)
            queryset = Customer.objects.exclude(employee__status__in=inactive_filter).filter(employee__in=listIds).order_by(Lower('id'))
            serializer = CustomerSerializer(queryset, many=True)
            return Response(serializer.data)

        return super().list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_warehouserequester'):
            return Response({'details': '%s is forbidden to add warehouse requesters' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_warehouserequester'):
            return Response({'details': '%s is forbidden to delete warehouse requesters' % request.user.id}, status=403)

        return super().destroy(request, *args, **kwargs)


class ReceiptViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Receipt.objects.order_by('-date')
    serializer_class = ReceiptSerializer

    @detail_route(methods=['get'])
    def item_receipts(self, request, pk=None):
        item_receipts = ItemReceipt.objects.filter(receipt=self.get_object())
        return Response(ItemReceiptSerializer(item_receipts, many=True).data)

    @detail_route(methods=['get'])
    def total(self, request, pk=None):
        total = ItemReceipt.objects.filter(receipt=self.get_object()).aggregate(Sum('total'))
        return Response({'total': total['total__sum'] or 0}, status=200)

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_receipt'):
            return Response({'details': '%s is forbidden to view receipts' % request.user.id}, status=403)

        from_date = request.query_params.get('from', None)
        to_date = request.query_params.get('to', None)

        if from_date is None or to_date is None:
            return Response({'detail': "Expected 'from' and 'to' parameters with the \
                format 'DD/MM/YYYY'"}, status=400)

        warehouseIds = WarehouseUser.objects.filter(user = request.user).values('warehouse')
        listIds = []

        from_date = datetime.datetime.strptime(from_date, '%d/%m/%Y')
        to_date = datetime.datetime.strptime(to_date, '%d/%m/%Y')

        for warehouseId in warehouseIds:
            listIds.append(warehouseId['warehouse'])

        receipts = Receipt.objects.filter(warehouse__in = listIds)
        receipts = receipts.filter(date__range=[from_date, to_date])
        serializer = ReceiptSerializer(receipts, many=True)

        return Response(serializer.data)


        # return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_receipt'):
            return Response({'details': '%s is forbidden to view receipts' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_receipt'):
            return Response({'details': '%s is forbidden to add receipts' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)


    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_receipt'):
            return Response({'details': '%s is forbidden to change receipts' % request.user.id}, status=403)

        receipt = self.get_object()

        response = super().update(request, *args, **kwargs)

        receipt.refresh_from_db()

        items_history = ItemHistory.objects.filter(pseudo_id = receipt.id).update(date = receipt.date)

        return response

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_receipt'):
            return Response({'details': '%s is forbidden to delete receipts' % request.user.id}, status=403)

        try:
            return super(ReceiptViewSet, self).destroy(request, *args, **kwargs)
        except ProtectedError as e:
            name = e.protected_objects.model.__name__
            return Response({'detail': 'Receipt is still referenced in some %s(s).' % name}, status=400)


class ItemReceiptViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = ItemReceipt.objects.order_by('item__name')
    serializer_class = ItemReceiptSerializer

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_itemreceipt'):
            return Response({'details': '%s is forbidden to view item requests' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_itemreceipt'):
            return Response({'details': '%s is forbidden to view item requests' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def perform_create(self, serializer):
        with transaction.atomic():
            # perform object creation
            item_receipt = serializer.save()

            warehouse_items = WarehouseItems.objects.get(warehouse = item_receipt.receipt.warehouse, item = item_receipt.item)

            warehouse_items.stock += item_receipt.quantity
            warehouse_items.save()

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'create_itemreceipt'):
            return Response({'details': '%s is forbidden to create item requests' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_itemreceipt'):
            return Response({'details': '%s is forbidden to change item requests' % request.user.id}, status=403)

        with transaction.atomic():
            item_receipt = self.get_object()

            # subtract the previous item receipt quantity to the warehouse item stock
            warehouse_item = WarehouseItems.objects.get(warehouse = item_receipt.receipt.warehouse, item = item_receipt.item)
            warehouse_item.stock -= item_receipt.quantity

            response = super(ItemReceiptViewSet, self).update(request, *args, **kwargs)

            # get latest data from db
            item_receipt.refresh_from_db()

            # add the new quantity to the warehouse item stock and save changes
            warehouse_item.stock += item_receipt.quantity
            warehouse_item.save()

            # update item history record
            item_history = ItemHistory.objects.get(receipt = item_receipt)
            item_history.quantity = item_receipt.quantity
            item_history.unit_price = item_receipt.unit_price
            item_history.total = item_receipt.total

            item_history.save()

            item_receipt.receipt.save()

            return response

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_itemreceipt'):
            return Response({'details': '%s is forbidden to delete item requests' % request.user.id}, status=403)

        item_receipt = self.get_object()

        warehouse_item = WarehouseItems.objects.get(warehouse = item_receipt.receipt.warehouse, item = item_receipt.item)

        warehouse_item.stock -= item_receipt.quantity

        warehouse_item.save()

        return super(ItemReceiptViewSet, self).destroy(request, *args, **kwargs)

class WarehouseItemsViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = WarehouseItems.objects.order_by('item__name')
    serializer_class = WarehouseItemsSerializer


    @detail_route(methods=['get'])
    def warehouse_item_stock(self, request, pk=None):
        warehouse_item = WarehouseItems.objects.get(id=pk)
        return Response(WarehouseItemsSerializer(warehouse_item, many=False).data)

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_warehouseitems'):
            return Response({'details': '%s is forbidden to view item-warehouse assignments' % request.user.id}, status=403)

        admin = request.user.groups.filter(name="admin-group")
        listIds = []

        if admin.count() > 0:
            warehouseIds = Warehouse.objects.all().values('id')

            for warehouseId in warehouseIds:
                listIds.append(warehouseId['id'])

        else:
            warehouseIds = WarehouseUser.objects.filter(user = request.user).values('warehouse')

            for warehouseId in warehouseIds:
                listIds.append(warehouseId['warehouse'])

        itemId = request.query_params.get('item', None)
        if itemId is not None:
            data = WarehouseItems.objects.filter(item__id=itemId, warehouse__in=listIds).order_by('item__name')
            serializer = WarehouseItemsSerializer(data, many=True)
            return Response(serializer.data)

        warehouseId = request.query_params.get('warehouse', None)
        if warehouseId is not None:
            data = WarehouseItems.objects.filter(warehouse__id=warehouseId, warehouse__in=listIds).order_by('item__name')
            serializer = WarehouseItemsSerializer(data, many=True)
            return Response(serializer.data)

        data = WarehouseItems.objects.filter(warehouse__in=listIds).order_by('item__name')
        serializer = WarehouseItemsSerializer(data, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_warehouseitems'):
            return Response({'details': '%s is forbidden to view item-warehouse assignments' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'create_warehouseitems'):
            return Response({'details': '%s is forbidden to assign items to a warehouse' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_itemreceipt'):
            return Response({'details': '%s is forbidden to delete item requests' % request.user.id}, status=403)

        return super(WarehouseItemsViewSet, self).destroy(request, *args, **kwargs)



class ItemHistoryViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = ItemHistory.objects.order_by('date')
    serializer_class = ItemHistorySerializer

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_itemhistory'):
            return Response({'details': '%s is forbidden to view item histories' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_itemhistory'):
            return Response({'details': '%s is forbidden to view item histories' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_itemhistory'):
            return Response({'details': '%s is forbidden to add item histories' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_itemhistory'):
            return Response({'details': '%s is forbidden to change item histories' % request.user.id}, status=403)

        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_itemhistory'):
            return Response({'details': '%s is forbidden to delete item histories' % request.user.id}, status=403)

        return super().destroy(request, *args, **kwargs)


class RequestViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Request.objects.order_by('-date')
    serializer_class = RequestSerializer

    
    @list_route()    
    def dailycash(self, request, pk=None):
        if not PermissionService.has_permission(request.user, 'view_itemrequest'):
            return Response({'details': '%s is forbidden to view item requests' % request.user.id}, status=403)

        from_date = request.query_params.get('from', None)
        to_date = request.query_params.get('to', None)
        warehouse = request.query_params.get('warehouse', None)

        if from_date is None or to_date is None:
            return Response({'detail': "Expected 'from' and 'to' parameters with the \
                format 'DD/MM/YYYY'"}, status=400)

        if warehouse is None:
            return Response(status=200, data="")

        from_date = datetime.datetime.strptime(from_date, '%d/%m/%Y')
        to_date = datetime.datetime.strptime(to_date, '%d/%m/%Y')

        data = ItemRequest.objects.order_by('request')
        data = data.filter(request__warehouse_id = warehouse, date__range=[from_date, to_date], type__iexact='Cash')
        serializer = ItemRequestSerializer(data, many=True)

        return Response(serializer.data, status=200)

    @detail_route(methods=['get'])
    def item_requests(self, request, pk=None):
        item_requests = ItemRequest.objects.filter(request=self.get_object())
        return Response(ItemRequestSerializer(item_requests, many=True).data)

    def total(self, request, pk=None):
        total = ItemRequest.objects.filter(request=self.get_object()).aggregate(Sum('total'))
        return Response({'total': total['total__sum'] or 0}, status=200)

    def partial_update(self, request, *args, **kwargs):
        requestInstance = self.get_object()

        serializer = RequestSerializer(requestInstance, data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()

            requestInstance.refresh_from_db()

            items_history = ItemHistory.objects.filter(pseudo_id = requestInstance.id).update(date = requestInstance.date)

            return Response(status=200, data=serializer.data)



        return Response(code=400, data="wrong parameters")

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_request'):
            return Response({'details': '%s is forbidden to view requests' % request.user.id}, status=403)

        from_date = request.query_params.get('from', None)
        to_date = request.query_params.get('to', None)

        if from_date is None or to_date is None:
            return Response({'detail': "Expected 'from' and 'to' parameters with the \
                format 'DD/MM/YYYY'"}, status=400)

        warehouseIds = WarehouseUser.objects.filter(user = request.user).values('warehouse')
        listIds = []

        from_date = datetime.datetime.strptime(from_date, '%d/%m/%Y')
        to_date = datetime.datetime.strptime(to_date, '%d/%m/%Y')

        for warehouseId in warehouseIds:
            listIds.append(warehouseId['warehouse'])

        requests = Request.objects.filter(warehouse__in = listIds)
        requests = requests.filter(date__range=[from_date, to_date])
        serializer = RequestSerializer(requests, many=True)

        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_request'):
            return Response({'details': '%s is forbidden to view requests' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_request'):
            return Response({'details': '%s is forbidden to add requests' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'update_request'):
            return Response({'details': '%s is forbidden to update requests' % request.user.id}, status=403)

        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_request'):
            return Response({'details': '%s is forbidden to delete requests' % request.user.id}, status=403)

        try:
            return super(RequestViewSet, self).destroy(request, *args, **kwargs)
        except ProtectedError as e:
            name = e.protected_objects.model.__name__
            return Response({'detail': 'Request is still referenced in some %s(s).' % name}, status=400)


class ItemRequestViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = ItemRequest.objects.order_by('item__name')
    serializer_class = ItemRequestSerializer

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_itemrequest'):
            return Response({'details': '%s is forbidden to view item requests' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_itemrequest'):
            return Response({'details': '%s is forbidden to view item requests' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def perform_create(self, serializer):
        with transaction.atomic():
            # perform object creation
            item_request = serializer.save()

            warehouse_items = WarehouseItems.objects.get(warehouse = item_request.request.warehouse, item = item_request.item)

            # deduct stock if status is 'Issued'
            if item_request.status.lower() == 'issued':
                warehouse_items.stock -= item_request.quantity
                warehouse_items.save()

            elif item_request.status.lower() == 'draft':
                item_request.request.status = 'Draft'
                item_request.request.save()


            item_req_count = ItemRequest.objects.filter(request=item_request.request).count()
            item_req_issued = ItemRequest.objects.filter(request=item_request.request, status='Issued').count()
            item_req_draft = ItemRequest.objects.filter(request=item_request.request, status='Draft').count()
            item_req_approved = ItemRequest.objects.filter(request=item_request.request, status='Approved').count()

            if item_req_count is item_req_issued:
                item_request.request.status = 'Issued'
                item_request.request.save()
            elif item_req_draft != 0 and item_req_draft <= item_req_count:
                item_request.request.status = 'Draft'
                item_request.request.save()

            elif item_req_approved != 0 and item_req_approved <= item_req_count:
                item_request.request.status = 'Approved'
                item_request.request.save()


    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'create_itemrequest'):
            return Response({'details': '%s is forbidden to create item requests' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_itemrequest'):
            return Response({'details': '%s is forbidden to change item requests' % request.user.id}, status=403)

        with transaction.atomic():
            item_request = self.get_object()

            warehouse_items = WarehouseItems.objects.get(warehouse = item_request.request.warehouse, item = item_request.item)

            # status is updated to 'Issued' checking
            current_status = item_request.status
            current_quantity = item_request.quantity
            # warehouse_items.stock += item_request.quantity

            # perform normal update operation, saving response
            response = super(ItemRequestViewSet, self).update(request, *args, **kwargs)

            # get latest data from db
            item_request.refresh_from_db()

            # if all item requests under this request are issued, request's status will be updated to 'Issued'
            item_req_count = ItemRequest.objects.filter(request=item_request.request).count()
            item_req_issued = ItemRequest.objects.filter(request=item_request.request, status='Issued').count()
            item_req_draft = ItemRequest.objects.filter(request=item_request.request, status='Draft').count()
            item_req_approved = ItemRequest.objects.filter(request=item_request.request, status='Approved').count()

            if item_req_count is item_req_issued:
                item_request.request.status = 'Issued'
                item_request.request.save()
            elif item_req_draft != 0 and item_req_draft <= item_req_count:
                item_request.request.status = 'Draft'
                item_request.request.save()

            elif item_req_approved != 0 and item_req_approved <= item_req_count:
                item_request.request.status = 'Approved'
                item_request.request.save()

            # status updated to 'Approved'
            # if current_status != item_request.status and item_request.status.lower() == 'approved':
                # warehouse_items.stock -= item_request.quantity
                # warehouse_items.save()

            # status updated to 'Issued'
            if current_status != item_request.status and item_request.status.lower() == 'issued':
                warehouse_items.stock -= item_request.quantity
                warehouse_items.save()
                ItemHistory.objects.create(**{
                    'pseudo_id': item_request.request.id,
                    'item': item_request.item,
                    'request': item_request,
                    'date': item_request.request.date,
                    'transaction_type': 'Item Out',
                    'quantity': item_request.quantity,
                    'unit_price': item_request.unit_price,
                    'total': item_request.total,
                })

            return response

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_itemrequest'):
            return Response({'details': '%s is forbidden to delete item requests' % request.user.id}, status=403)

        item_request = self.get_object()
        request = item_request.request

        if item_request.status.lower() == 'issued':
            return Response({'detail': 'Item Request is already issued'}, status=400)

        response = super(ItemRequestViewSet, self).destroy(request, *args, **kwargs)

        item_req_count = ItemRequest.objects.filter(request=request).count()
        item_req_issued = ItemRequest.objects.filter(request=request, status='Issued').count()
        item_req_draft = ItemRequest.objects.filter(request=request, status='Draft').count()
        item_req_approved = ItemRequest.objects.filter(request=request, status='Approved').count()

        if item_req_count is item_req_issued:
            request.status = 'Issued'
            request.save()
        elif item_req_draft != 0 and item_req_draft <= item_req_count:
            request.status = 'Draft'
            request.save()

        elif item_req_approved != 0 and item_req_approved <= item_req_count:
            request.status = 'Approved'
            request.save()

        return response

class AttendanceViewSet(mixins.CreateModelMixin, mixins.RetrieveModelMixin, mixins.UpdateModelMixin,
                        mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = AttendanceSerializer
    parser_classes = (JSONParser, FormParser, MultiPartParser,)

    def get_queryset(self):
        date_format = api_settings.DATE_FORMAT
        d = self.request.query_params.get('d', None)
        queryset = Attendance.objects.order_by('employee__user__last_name', 'employee__user__first_name')

        if d is not None:
            try:
                # parse date to python date
                d = datetime.datetime.strptime(d, date_format)
                return queryset.filter(date=d)
            except ValueError:
                # date given was invalid
                pass

        # list all
        return queryset

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_attendance'):
            return Response({'details': '%s is forbidden to view attendances' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_attendance'):
            return Response({'details': '%s is forbidden to view attendances' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_attendance'):
            return Response({'details': '%s is forbidden to add attendances' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_attendance'):
            return Response({'details': '%s is forbidden to change attendances' % request.user.id}, status=403)

        return super().update(request, *args, **kwargs)

    @list_route(methods=['post'])
    def import_from_csv(self, request):
        with transaction.atomic():
            count = 0
            csv_file = request.data.get('csv')

            for line in csv_file:
                # skip first line, reserved for headers
                if count is 0:
                    count = 1
                    continue

                tokens = line.decode('utf-8').strip().split(',')
                employee_id = tokens[0]
                date = datetime.datetime.strptime(tokens[2], '%m/%d/%Y').date()
                time_in = datetime.datetime.strptime(tokens[4], '%m/%d/%Y %I:%M %p').time()
                time_out = datetime.datetime.strptime(tokens[5], '%m/%d/%Y %I:%M %p').time()

                if Employee.objects.filter(id=employee_id).count() is 0:
                    return Response({'details': 'Employee with ID=%s was not found.' % employee_id}, status=404)

                attendance = {}
                # am attendance
                if time_in.hour < 12:
                    attendance['am_time_in'] = time_in
                    attendance['am_time_out'] = time_out
                # pm attendance
                else:
                    attendance['pm_time_in'] = time_in
                    attendance['pm_time_out'] = time_out

                queryset = Attendance.objects.filter(employee__id=employee_id, date=date)
                if queryset.count() is not 0:
                    queryset.update(**attendance)
                else:
                    attendance['employee_id'] = employee_id
                    attendance['date'] = date
                    Attendance.objects.create(**attendance)

                count += 1

            return Response({'details': 'Successfully imported %d entries' % (count - 1)}, status=200)


class OvertimeViewSet(mixins.CreateModelMixin, mixins.RetrieveModelMixin, mixins.UpdateModelMixin,
                      mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = OvertimeSerializer

    def get_queryset(self):
        date_format = api_settings.DATE_FORMAT
        d = self.request.query_params.get('d', None)
        queryset = Overtime.objects.order_by('date', 'time_in')

        if d is not None:
            try:
                # parse date to python date
                d = datetime.datetime.strptime(d, date_format)
                return queryset.filter(date=d)
            except ValueError:
                # date given was invalid
                pass

        # list all
        return queryset

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_overtime'):
            return Response({'details': '%s is forbidden to view overtimes' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_overtime'):
            return Response({'details': '%s is forbidden to view overtimes' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_overtime'):
            return Response({'details': '%s is forbidden to add overtimes' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_overtime'):
            return Response({'details': '%s is forbidden to change overtimes' % request.user.id}, status=403)

        # updates are not allowed if it's already processed
        temp_addition = self.get_object().temporary_addition
        schedule = PayrollSchedule.objects.filter(addition=temp_addition).first()
        if schedule.status == 'Processed':
            return Response(status=400)

        return super(OvertimeViewSet, self).update(request, *args, **kwargs)


class SalaryInfoViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = SalaryInfoSerializer
    queryset = SalaryInfo.objects.all()

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_salaryinfo'):
            return Response({'details': '%s is forbidden to view salary infos' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_salaryinfo'):
            return Response({'details': '%s is forbidden to view salary infos' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_salaryinfo'):
            return Response({'details': '%s is forbidden to add salary infos' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_salaryinfo'):
            return Response({'details': '%s is forbidden to change salary infos' % request.user.id}, status=403)

        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_salaryinfo'):
            return Response({'details': '%s is forbidden to delete salary infos' % request.user.id}, status=403)

        return super().destroy(request, *args, **kwargs)


class FixedDeductionViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = FixedDeductionSerializer
    queryset = FixedDeduction.objects.order_by('category', 'description')

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_fixeddeduction'):
            return Response({'details': '%s is forbidden to view fixed deductions' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_fixeddeduction'):
            return Response({'details': '%s is forbidden to view fixed deductions' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_fixeddeduction'):
            return Response({'details': '%s is forbidden to add fixed deductions' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_fixeddeduction'):
            return Response({'details': '%s is forbidden to change fixed deductions' % request.user.id}, status=403)

        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_fixeddeduction'):
            return Response({'details': '%s is forbidden to delete fixed deductions' % request.user.id}, status=403)

        return super().destroy(request, *args, **kwargs)


class FixedAdditionViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = FixedAdditionSerializer
    queryset = FixedAddition.objects.all()

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_fixedaddition'):
            return Response({'details': '%s is forbidden to view fixed additions' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_fixedaddition'):
            return Response({'details': '%s is forbidden to view fixed additions' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_fixedaddition'):
            return Response({'details': '%s is forbidden to add fixed additions' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_fixedaddition'):
            return Response({'details': '%s is forbidden to change fixed additions' % request.user.id}, status=403)

        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_fixedaddition'):
            return Response({'details': '%s is forbidden to delete fixed additions' % request.user.id}, status=403)

        return super().destroy(request, *args, **kwargs)


class TemporaryDeductionViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = TemporaryDeductionSerializer
    queryset = TemporaryDeduction.objects.order_by('date')

    @detail_route(methods=['get'])
    def payroll_schedules(self, request, pk=None):
        if not PermissionService.has_permission(request.user, 'view_payrollschedule'):
            return Response({'details': '%s is forbidden to view payroll schedules' % request.user.id}, status=403)

        payroll_schedules = PayrollSchedule.objects.filter(deduction=self.get_object())
        return Response(PayrollScheduleSerializer(payroll_schedules, many=True).data)

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_temporarydeduction'):
            return Response({'details': '%s is forbidden to view temporary deductions' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_temporarydeduction'):
            return Response({'details': '%s is forbidden to view temporary deductions' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_temporarydeduction'):
            return Response({'details': '%s is forbidden to add temporary deductions' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_temporarydeduction'):
            return Response({'details': '%s is forbidden to change temporary deductions' % request.user.id}, status=403)

        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_temporarydeduction'):
            return Response({'details': '%s is forbidden to delete temporary deductions' % request.user.id}, status=403)

        deduction = self.get_object()
        schedules = PayrollSchedule.objects.filter(deduction=deduction)

        if schedules.filter(status__iexact='processed').count() is not 0:
            return Response({'detail': "Temporary deduction's payroll schedule is already processed."}, status=400)

        return super(TemporaryDeductionViewSet, self).destroy(request, *args, **kwargs)


class TemporaryAdditionViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = TemporaryAdditionSerializer
    queryset = TemporaryAddition.objects.order_by('date')

    @detail_route(methods=['get'])
    def payroll_schedules(self, request, pk=None):
        if not PermissionService.has_permission(request.user, 'view_payrollschedule'):
            return Response({'details': '%s is forbidden to view payroll schedules' % request.user.id}, status=403)

        payroll_schedules = PayrollSchedule.objects.filter(addition=self.get_object())
        return Response(PayrollScheduleSerializer(payroll_schedules, many=True).data)

    def list(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_temporaryaddition'):
            return Response({'details': '%s is forbidden to view temporary additions' % request.user.id}, status=403)

        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'view_temporaryaddition'):
            return Response({'details': '%s is forbidden to view temporary additions' % request.user.id}, status=403)

        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'add_temporaryaddition'):
            return Response({'details': '%s is forbidden to add temporary additions' % request.user.id}, status=403)

        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'change_temporaryaddition'):
            return Response({'details': '%s is forbidden to change temporary additions' % request.user.id}, status=403)

        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        if not PermissionService.has_permission(request.user, 'delete_temporaryaddition'):
            return Response({'details': '%s is forbidden to delete temporary additions' % request.user.id}, status=403)

        addition = self.get_object()
        schedules = PayrollSchedule.objects.filter(addition=addition)

        if schedules.filter(status__iexact='processed').count() is not 0:
            return Response({'detail': "Temporary addition's payroll schedule is already processed."}, status=400)

        return super(TemporaryAdditionViewSet, self).destroy(request, *args, **kwargs)


class ExportHRR01(View):
    @staticmethod
    def get_payroll_coverage(date):
        start_date = datetime.datetime.strftime(date - datetime.timedelta(days=6), '%d-%b-%Y')
        end_date = datetime.datetime.strftime(date, '%d-%b-%Y')
        return '%s to %s' % (start_date, end_date)

    @staticmethod
    def write_xlsx_contents(workbook, given_date, employees):
        # get payroll object for the given date
        payroll = Payroll.objects.get(date=given_date)

        # start date for payroll
        date_range = [given_date - datetime.timedelta(days=6), given_date]

        # refer to xlsx writer documentation
        # https://xlsxwriter.readthedocs.io/
        worksheet = workbook.add_worksheet()

        # write headers
        worksheet.write(0, 4, 'Temporary Addition')
        worksheet.write(0, 8, 'Fixed Deduction')
        worksheet.write(0, 15, 'Temporary Deduction')

        worksheet.write(1, 0, 'Payroll Period')
        worksheet.write(1, 1, 'Name of Employee')
        worksheet.write(1, 2, 'Position')
        worksheet.write(1, 3, 'Basic Pay')
        worksheet.write(1, 4, 'Unpaid Excess Pay')
        worksheet.write(1, 5, 'Allowance')
        worksheet.write(1, 6, 'Gross Pay')
        worksheet.write(1, 7, 'SSS Contribution')
        worksheet.write(1, 8, 'SSS Loan')
        worksheet.write(1, 9, 'PHIC Contribution')
        worksheet.write(1, 10, 'HDMF Contribution')
        worksheet.write(1, 11, 'HDMF Loan')
        worksheet.write(1, 12, 'Withholding Tax')
        worksheet.write(1, 13, 'Net Pay')
        worksheet.write(1, 14, 'Paid Allowance')
        worksheet.write(1, 15, 'Canteen')
        worksheet.write(1, 16, 'Load')
        worksheet.write(1, 17, 'Rice')
        worksheet.write(1, 18, 'CA')
        worksheet.write(1, 19, 'Housing materials')
        worksheet.write(1, 20, 'Negative & Others')
        worksheet.write(1, 21, 'Total')

        # get current payroll period
        payroll_period = ExportHRR01.get_payroll_coverage(given_date)

        # write data
        for index, employee in enumerate(employees):
            # get payroll breakdown according to payroll status
            if payroll.status.lower() == 'processed':
                rows = [{
                    'id': breakdown.breakdown_id,
                    'employee': employee.id,
                    'date': breakdown.date,
                    'category': breakdown.category,
                    'description': breakdown.description,
                    'amount': breakdown.amount,
                } for breakdown in PayrollBreakdown.objects.filter(payroll=payroll, employee=employee)]
            else:
                rows = PayrollService.get_total_breakdown(employee, given_date)
                rows = rows['fixed_additions'] + rows['temporary_additions'] + \
                       rows['fixed_deductions'] + rows['temporary_deductions']

            # write payroll period
            worksheet.write(index + 2, 0, payroll_period)

            # write employee name
            worksheet.write(index + 2, 1, '%s, %s' % (employee.user.first_name, employee.user.last_name))

            # write employee position
            worksheet.write(index + 2, 2, employee.title)

            # contain gross pay
            grossPay = 0

            # contain total amount of fixed deductions for net pay
            totalFixedDeductions = 0

            # contain total amount of temporary deductions for total pay
            total_temp_deductions = 0

            # write employee basic pay
            if employee.type.lower() == 'per trip':
                per_trip_pay = 0
                temp_additions = TemporaryAddition.objects.filter(employee=employee, category__id='basicpertrip')
                for t_add in temp_additions:
                    p_schedules = PayrollSchedule.objects.filter(addition=t_add, payroll_date__range=date_range)
                    per_trip_pay += p_schedules.aggregate(Sum('amount'))['amount__sum'] or 0

                worksheet.write(index + 2, 3, str(per_trip_pay))
                grossPay += per_trip_pay
            else:
                basic_pay = \
                    FixedAddition.objects.filter(employee=employee, category__id='basicsalary').aggregate(
                        Sum('amount'))['amount__sum'] or 0

                worksheet.write(index + 2, 3, str(basic_pay))
                grossPay += basic_pay

            # write employee unpaid excess pay
            unpaid = 0
            temp_additions = TemporaryAddition.objects.filter(employee=employee, category__id='unpaid')
            for t_add in temp_additions:
                p_schedules = PayrollSchedule.objects.filter(addition=t_add, payroll_date__range=date_range)
                unpaid += p_schedules.aggregate(Sum('amount'))['amount__sum'] or 0

            worksheet.write(index + 2, 4, str(unpaid))
            grossPay += unpaid

            # write allowance
            allowance = 0
            temp_additions = TemporaryAddition.objects.filter(employee=employee, category__id='allowance')
            for t_add in temp_additions:
                p_schedules = PayrollSchedule.objects.filter(addition=t_add, payroll_date__range=date_range)
                allowance += p_schedules.aggregate(Sum('amount'))['amount__sum'] or 0

            worksheet.write(index + 2, 5, str(allowance))
            grossPay += allowance

            # write gross pay
            worksheet.write(index + 2, 6, str(grossPay))

            # write sss contribution
            sss_contribution = \
                FixedDeduction.objects.filter(employee=employee, category__id='ssscontrib').aggregate(Sum('amount'))[
                    'amount__sum'] or 0
            totalFixedDeductions += sss_contribution
            worksheet.write(index + 2, 7, str(sss_contribution))

            # write sss loan
            sss_loan = \
                FixedDeduction.objects.filter(employee=employee, category__id='sssloan').aggregate(Sum('amount'))[
                    'amount__sum'] or 0
            totalFixedDeductions += sss_loan
            worksheet.write(index + 2, 8, str(sss_loan))

            # write phic contribution
            phic_contribution = \
                FixedDeduction.objects.filter(employee=employee, category__id='phiccontrib').aggregate(Sum('amount'))[
                    'amount__sum'] or 0
            totalFixedDeductions += phic_contribution
            worksheet.write(index + 2, 9, str(phic_contribution))

            # write hdmf contribution
            hdmf_contribution = \
                FixedDeduction.objects.filter(employee=employee, category__id='hdmfcontrib').aggregate(Sum('amount'))[
                    'amount__sum'] or 0
            totalFixedDeductions += hdmf_contribution
            worksheet.write(index + 2, 10, str(hdmf_contribution))

            # write hdmf loan
            hdmf_loan = \
                FixedDeduction.objects.filter(employee=employee, category__id='hdmfloan').aggregate(Sum('amount'))[
                    'amount__sum'] or 0
            totalFixedDeductions += hdmf_loan
            worksheet.write(index + 2, 11, str(hdmf_loan))

            # write withholding tax
            tax = FixedDeduction.objects.filter(employee=employee, category__id='tax').aggregate(Sum('amount'))[
                      'amount__sum'] or 0
            totalFixedDeductions += tax
            worksheet.write(index + 2, 12, str(tax))

            # write net pay
            netPay = grossPay - totalFixedDeductions
            worksheet.write(index + 2, 13, str(netPay))

            # write paid allowance
            paid_allowance = 0
            temp_additions = TemporaryDeduction.objects.filter(employee=employee, category__id='paidallowance')
            for t_add in temp_additions:
                p_schedules = PayrollSchedule.objects.filter(addition=t_add, payroll_date__range=date_range)
                paid_allowance += p_schedules.aggregate(Sum('amount'))['amount__sum'] or 0

            worksheet.write(index + 2, 14, str(paid_allowance))
            total_temp_deductions += paid_allowance

            # write canteen
            canteen = 0
            temp_additions = TemporaryDeduction.objects.filter(employee=employee, category__id='canteen')
            for t_add in temp_additions:
                p_schedules = PayrollSchedule.objects.filter(addition=t_add, payroll_date__range=date_range)
                canteen += p_schedules.aggregate(Sum('amount'))['amount__sum'] or 0

            worksheet.write(index + 2, 15, str(canteen))
            total_temp_deductions += canteen

            # write load
            load = 0
            temp_additions = TemporaryDeduction.objects.filter(employee=employee, category__id='load')
            for t_add in temp_additions:
                p_schedules = PayrollSchedule.objects.filter(addition=t_add, payroll_date__range=date_range)
                load += p_schedules.aggregate(Sum('amount'))['amount__sum'] or 0

            worksheet.write(index + 2, 16, str(load))
            total_temp_deductions += load

            # write rice
            rice = 0
            temp_additions = TemporaryDeduction.objects.filter(employee=employee, category__id='rice')
            for t_add in temp_additions:
                p_schedules = PayrollSchedule.objects.filter(addition=t_add, payroll_date__range=date_range)
                rice += p_schedules.aggregate(Sum('amount'))['amount__sum'] or 0

            worksheet.write(index + 2, 17, str(rice))
            total_temp_deductions += rice

            # write cash advance
            cash_advance = 0
            temp_additions = TemporaryDeduction.objects.filter(employee=employee, category__id='cash')
            for t_add in temp_additions:
                p_schedules = PayrollSchedule.objects.filter(addition=t_add, payroll_date__range=date_range)
                cash_advance += p_schedules.aggregate(Sum('amount'))['amount__sum'] or 0

            worksheet.write(index + 2, 18, str(cash_advance))
            total_temp_deductions += cash_advance

            # write housing materials
            housing = 0
            temp_additions = TemporaryDeduction.objects.filter(employee=employee, category__id='housing')
            for t_add in temp_additions:
                p_schedules = PayrollSchedule.objects.filter(addition=t_add, payroll_date__range=date_range)
                housing += p_schedules.aggregate(Sum('amount'))['amount__sum'] or 0

            worksheet.write(index + 2, 19, str(housing))
            total_temp_deductions += housing

            # write negative & others
            others = 0
            temp_additions = TemporaryDeduction.objects.filter(employee=employee, category__id='others')
            for t_add in temp_additions:
                p_schedules = PayrollSchedule.objects.filter(addition=t_add, payroll_date__range=date_range)
                others += p_schedules.aggregate(Sum('amount'))['amount__sum'] or 0

            total_temp_deductions += others
            worksheet.write(index + 2, 20, str(others))

            # write total
            total = netPay - total_temp_deductions
            worksheet.write(index + 2, 21, str(total))

        print('ExportHRR01.write_xlsx_contents END')

    def get(self, request):
        print('ExportHRR01.get START')

        # get current user
        token = request.GET.get('token')
        print('ExportHRR01.get :: passed token = %s' % token)
        if not token:
            print('ExportHRR01.get :: NO PERMISSION -- END')
            return JsonResponse({'details': 'No permission'}, status=403)

        try:
            current_user = Token.objects.get(key=token).user
        except Token.DoesNotExist:
            print('ExportHRR01.get :: TOKEN DOES NOT EXIST -- END')
            return JsonResponse({'details': 'No permission'}, status=403)

        # if current user is an admin
        if current_user.is_staff:
            print('ExportHRR01.get :: Current user is ADMIN')
            employees = Employee.objects.order_by('user__last_name', 'user__first_name', 'middle_name')
        elif current_user.groups.filter(name__iexact='hr-group').exists():
            print('ExportHRR01.get :: Current user belongs to HR GROUP')
            employees = Employee.objects.order_by('user__last_name', 'user__first_name', 'middle_name').filter(
                type__iexact='Per Trip')
        else:
            print('ExportHRR01.get :: Current user is not an ADMIN and doesnt belong to HR group')
            return JsonResponse({'details': 'No permission'}, status=403)

        # get given payroll date
        date_format = '%d-%b-%Y'
        date_as_str = request.GET.get('date')

        if date_as_str is None:
            return JsonResponse({'details': 'Expecting "date" query parameter with format %s' % date_format},
                                status=400)

        try:
            # parse date to python date
            date = datetime.datetime.strptime(date_as_str, date_format).date()
        except ValueError:
            return JsonResponse({'details': 'Expecting "date" query parameter with format %s' % date_format},
                                status=400)

        # check payroll with the given date exists
        if not Payroll.objects.filter(date=date).exists():
            return JsonResponse({'details': 'Payroll with date=%s does not exist' % date},
                                status=404)

        # filename variables
        current_timestamp = int(time.time())
        filename_xlsx = '/tmp/%d.xlsx' % current_timestamp
        filename_docx = '/extras/HRR01.docx'

        # create xlsx file
        workbook = xlsxwriter.Workbook(filename_xlsx)
        self.write_xlsx_contents(workbook, date, employees)
        workbook.close()

        # grab in-memory ZIP contents
        s = io.BytesIO()

        # pack xlsx file and docx file to zip
        with zipfile.ZipFile(s, 'w') as hrr01zip:
            hrr01zip.write(filename_xlsx, 'HRR01.xlsx')
            hrr01zip.write(filename_docx, 'HRR01.docx')

        # prepare file response
        response = HttpResponse(s.getvalue(), content_type='application/x-zip-compressed')
        response['Content-Disposition'] = 'attachment; filename=HRR01.zip'

        return response


class ExportHRR02(View):
    @staticmethod
    def write_xlsx_contents(given_date, workbook, employees):
        # refer to xlsx writer documentation
        # https://xlsxwriter.readthedocs.io/
        worksheet = workbook.add_worksheet()

        # write headers
        worksheet.write(0, 0, 'No')
        worksheet.write(0, 1, 'Name')
        worksheet.write(0, 2, 'Account Number')
        worksheet.write(0, 3, 'Amount')

        # write data
        for index, employee in enumerate(employees):
            # write autogenerated number
            worksheet.write(index + 1, 0, str(index + 1))

            # write name
            worksheet.write(index + 1, 1, '%s, %s' % (employee.user.last_name, employee.user.first_name))

            # write account number
            if hasattr(employee, 'salary_info'):
                worksheet.write(index + 1, 2, employee.salary_info.bank_account)
            else:
                worksheet.write(index + 1, 2, '-')

            # write amount
            worksheet.write(index + 1, 3, PayrollService.get_total(employee, given_date))

    def get(self, request):
        # get current user
        token = request.GET.get('token')
        if not token:
            return JsonResponse({'details': 'No permission'}, status=403)

        try:
            current_user = Token.objects.get(key=token).user
        except Token.DoesNotExist:
            return JsonResponse({'details': 'No permission'}, status=403)

        # get given payroll date
        date_format = '%d-%b-%Y'
        date_as_str = request.GET.get('date')

        if date_as_str is None:
            return JsonResponse({'details': 'Expecting "date" query parameter with format %s' % date_format},
                                status=400)

        try:
            # parse date to python date
            date = datetime.datetime.strptime(date_as_str, date_format).date()
        except ValueError:
            return JsonResponse({'details': 'Expecting "date" query parameter with format %s' % date_format},
                                status=400)

        # check payroll with the given date exists
        if not Payroll.objects.filter(date=date).exists():
            return JsonResponse({'details': 'Payroll with date=%s does not exist' % date},
                                status=404)

        if current_user.is_staff:
            employees = Employee.objects.order_by('user__last_name', 'user__first_name', 'middle_name')
        elif current_user.groups.filter(name__iexact='hr-group').exists():
            employees = Employee.objects.order_by('user__last_name', 'user__first_name', 'middle_name').filter(
                type__iexact='Per Trip')
        else:
            return JsonResponse({'details': 'No permission. user.is_staff=%s' % current_user.is_staff}, status=403)

        # grab in-memory excel contents
        s = io.BytesIO()

        workbook = xlsxwriter.Workbook(s)
        self.write_xlsx_contents(date, workbook, employees)
        workbook.close()

        response = HttpResponse(s.getvalue(),
                                content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = 'attachment; filename=HRR01_%s.xlsx' % date_as_str

        return response


class ChangePassword(APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (parsers.JSONParser,)
    serializer_class = ChangePasswordSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        with transaction.atomic():
            new_password = serializer.validated_data['new_password']
            user = serializer.validated_data['user']

            # set user's password
            user.set_password(new_password)

            # also in its meta (because they want to expose the user's password)
            try:
                # wrap in a try-except block, because it is not certain if every user
                # already has meta field
                user.meta.password = new_password
                user.meta.save()
            except UserMeta.DoesNotExist:
                user.meta = UserMeta.objects.create(**{
                    'user': user,
                    'password': new_password,
                })

            # save modifications
            user.save()

            token, created = Token.objects.get_or_create(user=user)
            return Response({'token': token.key})
