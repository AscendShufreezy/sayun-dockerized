import calendar
import datetime

from django.db.models import Sum, Q

from app.models import PayrollSchedule, FixedAddition, FixedDeduction, Payroll, PayrollBreakdown


class PermissionService:
    @staticmethod
    def has_permission(user, permission_codename):
        return True

        # superuser means root
        if user.is_superuser:
            return True

        # check if user has direct permission
        if user.user_permissions.filter(permission__codename=permission_codename).exists():
            return True

        # check if user belongs to a group that has the given permission
        if user.groups.filter(permissions__codename=permission_codename).exists():
            return True

        return False


class PayrollService:
    @staticmethod
    def get_total(employee, given_date):
        min_date = given_date - datetime.timedelta(days=6)

        # get payroll of the given_date
        payroll = Payroll.objects.get(date=given_date)

        # processed payrolls should retrieve frozen data
        if payroll.status.lower() == 'processed':
            queryset = PayrollBreakdown.objects.filter(payroll=payroll, employee=employee)
            return queryset.filter(type='fa').aggregate(Sum('amount'))['amount__sum'] or 0 - queryset.filter(
                type='fd').aggregate(Sum('amount'))['amount__sum'] or 0 + queryset.filter(type='ta').aggregate(
                Sum('amount'))['amount__sum'] or 0 - queryset.filter(type='td').aggregate(Sum('amount'))[
                'amount__sum'] or 0

        # prepare querysets
        payroll_schedules = PayrollSchedule.objects.filter(employee=employee,
                                                           date__range=[min_date, given_date])

        # timeline
        weeks = calendar.monthcalendar(given_date.year, given_date.month)
        first_week = weeks[0]
        second_week = weeks[1]
        third_week = weeks[2]
        fourth_week = weeks[3]

        # collect all fixed additions and deductions
        t_additions = payroll_schedules.filter(addition__isnull=False).aggregate(Sum('amount'))['amount__sum'] or 0
        t_deductions = payroll_schedules.filter(deduction__isnull=False).aggregate(Sum('amount'))['amount__sum'] or 0

        f_additions = 0
        f_deductions = 0

        # iterate fixed additions
        for fa in FixedAddition.objects.filter(~Q(status__iexact='inactive'), employee=employee):
            if fa.schedule.lower() == 'monthly (first week)':
                if given_date.day in first_week:
                    f_additions += fa.amount
            elif fa.schedule.lower() == 'monthly (second week)':
                if given_date.day in second_week:
                    f_additions += fa.amount
            elif fa.schedule.lower() == 'monthly (third week)':
                if given_date.day in third_week:
                    f_additions += fa.amount
            elif fa.schedule.lower() == 'monthly (fourth week)':
                if given_date.day in fourth_week:
                    f_additions += fa.amount
            elif fa.schedule.lower().startswith('weekly'):
                f_additions += fa.amount

        # iterate fixed deductions
        for fd in FixedDeduction.objects.filter(~Q(status__iexact='inactive'), employee=employee):
            if fd.schedule.lower() == 'monthly (first week)':
                if given_date.day in first_week:
                    f_deductions += fd.amount
            elif fd.schedule.lower() == 'monthly (second week)':
                if given_date.day in second_week:
                    f_deductions += fd.amount
            elif fd.schedule.lower() == 'monthly (third week)':
                if given_date.day in third_week:
                    f_deductions += fd.amount
            elif fd.schedule.lower() == 'monthly (fourth week)':
                if given_date.day in fourth_week:
                    f_deductions += fd.amount
            elif fd.schedule.lower().startswith('weekly'):
                f_deductions += fd.amount

        return f_additions - f_deductions + t_additions - t_deductions

    @staticmethod
    def get_total_breakdown(employee, given_date):
        min_date = given_date - datetime.timedelta(days=6)

        # prepare querysets
        fixed_additions = FixedAddition.objects.filter(~Q(status__iexact='inactive'), employee=employee)
        fixed_deductions = FixedDeduction.objects.filter(~Q(status__iexact='inactive'), employee=employee)
        payroll_schedules = PayrollSchedule.objects.filter(employee=employee, date__range=[min_date, given_date])

        # prepare response
        rows_fixed_additions = []
        rows_temp_additions = []
        rows_fixed_deductions = []
        rows_temp_deductions = []

        # timeline
        weeks = calendar.monthcalendar(given_date.year, given_date.month)
        first_week = weeks[0]
        second_week = weeks[1]
        third_week = weeks[2]
        fourth_week = weeks[3]

        # stringify date
        payroll_date = datetime.datetime.strftime(given_date, '%d-%b-%Y')

        # iterate fixed additions
        for fa in fixed_additions:
            # get current fixed addition's schedule
            fa_schedule = fa.schedule.lower()

            if fa_schedule == 'monthly (first week)':
                if given_date.day in first_week:
                    rows_fixed_additions.append({
                        'id': fa.id,
                        'employee': employee.id,
                        'date': payroll_date,
                        'category': fa.category.type,
                        'description': fa.description,
                        'amount': fa.amount,
                    })
            elif fa_schedule == 'monthly (second week)':
                if given_date.day in second_week:
                    rows_fixed_additions.append({
                        'id': fa.id,
                        'employee': employee.id,
                        'date': payroll_date,
                        'category': fa.category.type,
                        'description': fa.description,
                        'amount': fa.amount,
                    })
            elif fa_schedule == 'monthly (third week)':
                if given_date.day in third_week:
                    rows_fixed_additions.append({
                        'id': fa.id,
                        'employee': employee.id,
                        'date': payroll_date,
                        'category': fa.category.type,
                        'description': fa.description,
                        'amount': fa.amount,
                    })
            elif fa_schedule == 'monthly (fourth week)':
                if given_date.day in fourth_week:
                    rows_fixed_additions.append({
                        'id': fa.id,
                        'employee': employee.id,
                        'date': payroll_date,
                        'category': fa.category.type,
                        'description': fa.description,
                        'amount': fa.amount,
                    })
            elif fa_schedule.startswith('weekly'):
                rows_fixed_additions.append({
                    'id': fa.id,
                    'employee': employee.id,
                    'date': payroll_date,
                    'category': fa.category.type,
                    'description': fa.description,
                    'amount': fa.amount,
                })

        # iterate fixed deductions
        for fd in fixed_deductions:
            # get current fixed deduction's schedule
            fd_schedule = fd.schedule.lower()

            if fd_schedule == 'monthly (first week)':
                if given_date.day in first_week:
                    rows_fixed_deductions.append({
                        'id': fd.id,
                        'employee': employee.id,
                        'date': payroll_date,
                        'category': fd.category.type,
                        'description': fd.description,
                        'amount': fd.amount,
                    })
            elif fd_schedule == 'monthly (second week)':
                if given_date.day in second_week:
                    rows_fixed_deductions.append({
                        'id': fd.id,
                        'employee': employee.id,
                        'date': payroll_date,
                        'category': fd.category.type,
                        'description': fd.description,
                        'amount': fd.amount,
                    })
            elif fd_schedule == 'monthly (third week)':
                if given_date.day in third_week:
                    rows_fixed_deductions.append({
                        'id': fd.id,
                        'employee': employee.id,
                        'date': payroll_date,
                        'category': fd.category.type,
                        'description': fd.description,
                        'amount': fd.amount,
                    })
            elif fd_schedule == 'monthly (fourth week)':
                if given_date.day in fourth_week:
                    rows_fixed_deductions.append({
                        'id': fd.id,
                        'employee': employee.id,
                        'date': payroll_date,
                        'category': fd.category.type,
                        'description': fd.description,
                        'amount': fd.amount,
                    })
            elif fd_schedule.startswith('weekly'):
                rows_fixed_deductions.append({
                    'id': fd.id,
                    'employee': employee.id,
                    'date': payroll_date,
                    'category': fd.category.type,
                    'description': fd.description,
                    'amount': fd.amount,
                })

        # iterate payroll schedules
        for schedule in payroll_schedules:
            row = {
                'employee': employee.id,
                'date': datetime.datetime.strftime(schedule.date, '%d-%b-%Y'),
                'amount': schedule.amount,
            }

            if schedule.type == 'addition':
                row['id'] = schedule.addition.id
                row['category'] = schedule.addition.category.type
                row['description'] = schedule.addition.description
                rows_temp_additions.append(row)
            elif schedule.type == 'deduction':
                row['id'] = schedule.deduction.id
                row['category'] = schedule.deduction.category.type
                row['description'] = schedule.deduction.description
                rows_temp_deductions.append(row)

        return {
            'fixed_additions': rows_fixed_additions,
            'fixed_deductions': rows_fixed_deductions,
            'temporary_additions': rows_temp_additions,
            'temporary_deductions': rows_temp_deductions
        }
