# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-14 09:27
from __future__ import unicode_literals

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Attendance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(default=datetime.date.today)),
                ('am_time_in', models.TimeField(null=True)),
                ('am_time_out', models.TimeField(null=True)),
                ('pm_time_in', models.TimeField(null=True)),
                ('pm_time_out', models.TimeField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='AvatarUpload',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('image', models.FileField(upload_to='')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='CafeteriaDeduction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField()),
                ('date', models.DateField()),
                ('amount', models.DecimalField(decimal_places=2, max_digits=20)),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.CharField(max_length=100, primary_key=True, serialize=False, verbose_name='Category ID')),
                ('type', models.CharField(max_length=200)),
                ('description', models.CharField(max_length=200)),
            ],
            options={
                'verbose_name_plural': 'categories',
            },
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.CharField(max_length=200, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=200)),
                ('type', models.CharField(max_length=200)),
                ('description', models.TextField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.CharField(max_length=100, primary_key=True, serialize=False, verbose_name='Employee ID')),
                ('middle_name', models.CharField(max_length=100)),
                ('birthdate', models.DateField()),
                ('gender', models.CharField(choices=[('Male', 'Male'), ('Female', 'Female')], max_length=100)),
                ('type', models.CharField(choices=[('Weekly', 'Weekly'), ('Per Trip', 'Per Trip'), ('Office', 'Office')], max_length=100)),
                ('title', models.CharField(max_length=100)),
                ('date_hired', models.DateField(verbose_name='Date Hired')),
                ('status', models.CharField(choices=[('Active', 'Active'), ('AWOL', 'Away on Leave'), ('Resigned', 'Resigned'), ('Terminated', 'Terminated')], default='active', max_length=100)),
                ('exit_date', models.DateField(blank=True, null=True, verbose_name='Exit Date')),
                ('remarks', models.TextField(blank=True, max_length=3000, null=True)),
                ('image', models.URLField(blank=True, null=True)),
                ('customer', models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.Customer')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='FixedAddition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField()),
                ('schedule', models.CharField(max_length=200)),
                ('amount', models.DecimalField(decimal_places=2, max_digits=20)),
                ('status', models.CharField(max_length=200)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Category')),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.Employee')),
            ],
        ),
        migrations.CreateModel(
            name='FixedDeduction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField()),
                ('schedule', models.CharField(max_length=200)),
                ('amount', models.DecimalField(decimal_places=2, max_digits=20)),
                ('status', models.CharField(max_length=200)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Category')),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.Employee')),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.CharField(blank=True, max_length=200, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField()),
                ('unit_price', models.DecimalField(decimal_places=2, max_digits=20)),
                ('stock', models.IntegerField(default=0)),
                ('barcode', models.CharField(max_length=200)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Date Created')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Date Updated')),
            ],
        ),
        migrations.CreateModel(
            name='ItemHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pseudo_id', models.CharField(blank=True, max_length=200)),
                ('date', models.DateField()),
                ('transaction_type', models.CharField(choices=[('Item In', 'Item In'), ('Item Out', 'Item Out')], max_length=200)),
                ('quantity', models.IntegerField(default=0)),
                ('unit_price', models.DecimalField(decimal_places=2, max_digits=20)),
                ('total', models.DecimalField(decimal_places=2, max_digits=20)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Date Created')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Date Updated')),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Item')),
            ],
            options={
                'verbose_name_plural': 'item histories',
            },
        ),
        migrations.CreateModel(
            name='ItemReceipt',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('unit_price', models.DecimalField(decimal_places=2, max_digits=20)),
                ('quantity', models.IntegerField(default=0)),
                ('total', models.DecimalField(decimal_places=2, max_digits=20)),
                ('date', models.DateField()),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.Item')),
            ],
        ),
        migrations.CreateModel(
            name='ItemRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('unit_price', models.DecimalField(decimal_places=2, max_digits=20)),
                ('quantity', models.IntegerField(default=0)),
                ('status', models.CharField(max_length=200)),
                ('total', models.DecimalField(decimal_places=2, max_digits=20)),
                ('date', models.DateField()),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.Item')),
            ],
        ),
        migrations.CreateModel(
            name='Overtime',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(default=datetime.date.today)),
                ('time_in', models.TimeField()),
                ('time_out', models.TimeField()),
                ('total_hours', models.DecimalField(decimal_places=2, max_digits=20)),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.Employee')),
            ],
        ),
        migrations.CreateModel(
            name='OvertimeAddition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.DecimalField(decimal_places=2, max_digits=20)),
                ('date', models.DateField()),
                ('description', models.TextField()),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.Employee')),
                ('overtime', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Overtime')),
            ],
        ),
        migrations.CreateModel(
            name='Payroll',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('coverage', models.CharField(max_length=200)),
                ('status', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='PayrollBreakdown',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(choices=[('fa', 'fixed addition'), ('fd', 'fixed deduction'), ('ta', 'temporary addition'), ('td', 'temporary deduction')], max_length=2)),
                ('breakdown_id', models.IntegerField()),
                ('date', models.CharField(max_length=11)),
                ('category', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('amount', models.DecimalField(decimal_places=2, max_digits=20)),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.Employee')),
                ('payroll', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.Payroll')),
            ],
        ),
        migrations.CreateModel(
            name='PayrollCalendar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('payroll_calendar', models.CharField(blank=True, max_length=200)),
                ('status', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='PayrollComputation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField()),
                ('amount', models.DecimalField(decimal_places=2, max_digits=20)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Category')),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.Employee')),
                ('payroll', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Payroll')),
            ],
        ),
        migrations.CreateModel(
            name='PayrollSchedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(choices=[('addition', 'addition'), ('deduction', 'deduction')], max_length=200)),
                ('date', models.DateField()),
                ('status', models.CharField(max_length=200)),
                ('payroll_date', models.DateField()),
                ('amount', models.DecimalField(decimal_places=2, max_digits=20)),
            ],
        ),
        migrations.CreateModel(
            name='Receipt',
            fields=[
                ('id', models.CharField(max_length=200, primary_key=True, serialize=False)),
                ('date', models.DateField()),
                ('supplier', models.TextField(max_length=200)),
                ('purchaser', models.TextField(max_length=200)),
                ('receipt_number', models.CharField(blank=True, max_length=200)),
                ('invoice_number', models.CharField(blank=True, max_length=200)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Date Created')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Date Updated')),
            ],
        ),
        migrations.CreateModel(
            name='Request',
            fields=[
                ('id', models.CharField(blank=True, max_length=200, primary_key=True, serialize=False)),
                ('date', models.DateField(verbose_name='Issued Date')),
                ('status', models.CharField(max_length=200)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Date Created')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Date Updated')),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.Customer')),
            ],
        ),
        migrations.CreateModel(
            name='SalaryInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bank_account', models.CharField(blank=True, max_length=200)),
                ('hourly_rate', models.DecimalField(decimal_places=2, max_digits=20)),
                ('employee', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='salary_info', to='app.Employee')),
            ],
        ),
        migrations.CreateModel(
            name='TemporaryAddition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField(blank=True)),
                ('date', models.DateField(verbose_name='Date Entered')),
                ('total', models.DecimalField(decimal_places=2, max_digits=20)),
                ('processed', models.DecimalField(decimal_places=2, max_digits=20)),
                ('pending', models.DecimalField(decimal_places=2, max_digits=20)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Category')),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.Employee')),
                ('overtime', models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='temporary_addition', to='app.Overtime')),
            ],
        ),
        migrations.CreateModel(
            name='TemporaryDeduction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField()),
                ('date', models.DateField(verbose_name='Date Entered')),
                ('loan_id', models.CharField(blank=True, max_length=200)),
                ('total', models.DecimalField(decimal_places=2, max_digits=20)),
                ('processed', models.DecimalField(decimal_places=2, max_digits=20)),
                ('pending', models.DecimalField(decimal_places=2, max_digits=20)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Category')),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.Employee')),
            ],
        ),
        migrations.CreateModel(
            name='UserMeta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=200)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='meta', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Warehouse',
            fields=[
                ('id', models.CharField(blank=True, max_length=200, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=200)),
                ('location', models.TextField()),
                ('description', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Date Created')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Date Updated')),
                ('owner', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='warehouses', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='request',
            name='warehouse',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='app.Warehouse'),
        ),
        migrations.AddField(
            model_name='receipt',
            name='warehouse',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Warehouse'),
        ),
        migrations.AddField(
            model_name='payrollschedule',
            name='addition',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='app.TemporaryAddition'),
        ),
        migrations.AddField(
            model_name='payrollschedule',
            name='deduction',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='app.TemporaryDeduction'),
        ),
        migrations.AddField(
            model_name='payrollschedule',
            name='employee',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Employee'),
        ),
        migrations.AddField(
            model_name='payroll',
            name='calendar',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.PayrollCalendar'),
        ),
        migrations.AddField(
            model_name='itemrequest',
            name='request',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='item_requests', to='app.Request'),
        ),
        migrations.AddField(
            model_name='itemreceipt',
            name='receipt',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='item_receipts', to='app.Receipt'),
        ),
        migrations.AddField(
            model_name='itemhistory',
            name='receipt',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='app.ItemReceipt'),
        ),
        migrations.AddField(
            model_name='itemhistory',
            name='request',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='app.ItemRequest'),
        ),
        migrations.AddField(
            model_name='cafeteriadeduction',
            name='employee',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.Employee'),
        ),
        migrations.AddField(
            model_name='attendance',
            name='employee',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='app.Employee'),
        ),
        migrations.AlterUniqueTogether(
            name='payroll',
            unique_together=set([('calendar', 'date')]),
        ),
    ]
