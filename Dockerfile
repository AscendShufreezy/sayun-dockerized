FROM python:3.6.2
ENV PYTHONUNBUFFERED 1

# ----------------------------
# Setup python environment
# ----------------------------
RUN mkdir /config
ADD /config/requirements.txt /config/
RUN pip install -r /config/requirements.txt

# ----------------------------
# Workdir for client codes
# ----------------------------
RUN mkdir /sayun

# ----------------------------
# Workdir for extra docs
# ----------------------------
RUN mkdir /extras
COPY /extras/ /extras

# ----------------------------
# Workdir for server codes
# ----------------------------
RUN mkdir /src
WORKDIR /src
